![Logo](./figs/RATLogo.png)
# Coil Design Library
<em>Hellooo Rat is very pleased with coil modeler. Many possibilities yes?</em>

## Introduction
Setting up coil geometries for performing field calculations, optimization, quench analysis and ultimately coil construction, can prove to be a challenging task. This library aims to simplify the setup of coil geometries and provides basic classes for calculating their magnetic, vector potential at any point in space. In addition specialized calculation classes are available, allowing to calculate, for instance, the magnetic field on the surface (for peak field calculation) and coil harmonics. The generated meshes and field-maps can be exported and used in for instance quench codes. The ultimate aim is that this code holds the reference geometry of any magnet. This to avoid slight variations in the magnet geometry across different calculation tools, as is now often the case. 

The main concept of the code is by generating the cables and coil blocks by extruding their cross section along a line path. This is done through a coordinate transformation that takes into account the orientation (pitch) of the path. The path consists of one or multiple sections defined by a simple geometric objects, such as a straight line or an arc. 

## Installation
As this library is part of a collection of inter-dependent libraries a detailed description of the installation process is provided in a separate repository located here: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation). It is required that you install the [rat-common](https://gitlab.com/Project-Rat/rat-common), [distmesh-ccp](https://gitlab.com/Project-Rat/distmesh-cpp), [rat-mlfmm](https://gitlab.com/Project-Rat/rat-mlfmm) libraries before installing this.In addition it is required to set the ENABLE_CUDA flag in the cmakelists.txt file. It decides whether the code is build using NVidia Cuda or not. A quick build and installation is achieved by using

```bash
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=T ..
make -jX
```

where X is the number of cores you wish to use for the build. When the library is build run unit tests and install using

```bash
make test
sudo make install
```

## Modeling a Solenoid
This section demonstrates how to do the most basic thing with the rat-models library: creating a solenoid and calculating the magnetic field inside the conductor. The idea here is to demonstrate how the library works and what it can accomplish. This is shown from a code point of view. Later on to run the code one will need to be able to compile it and link the required libraries. In Rat any coil is modeled using its cross section in combination with a path, a space-curve with corresponding [moving frame](https://en.wikipedia.org/wiki/Moving_frame). The coil is then converted into a mesh, which is used both as sources and as targets for the MLFMM. Eventually the mesh and corresponding magnetic field is written to a VTK output file, to be analyzed with Paraview. The code reads as follows and is added to the rat-models example files under the name solenoid.cpp for convenience. More examples can be found in the examples directory.

```cpp
// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcmlfmm.hh"

// main function
int main(){
	// model geometric input parameters
	const rat::fltp time = 0; // output time [s]
	const rat::fltp radius = 40e-3; // coil inner radius [m]
	const rat::fltp dcoil = 10e-3; // thickness of the coil [m]
	const rat::fltp wcable = 12e-3; // width of the cable [m]
	const rat::fltp delem = 1e-3; // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const rat::fltp operating_current = 200; // operating current in [A]
	const arma::uword num_turns = 100; // number of turns

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(dcoil);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, 0, wcable, delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);

	// create calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(coil);
	mlfmm->set_target_meshes(); // calculates field on surface

	// calculate everything
	mlfmm->calculate_write({time},"./solenoid/",lg);
}
```

The resulting Paraview Data (pvd) file located at "./solenoid/mesh.pvd" visualized in Paraview, after slicing it in half, looks like this:

![Solenoid](./figs/solenoid.png)

## Modeling Advanced Geometry
Obviously one doesn't need Rat for calculating solenoids, this can be done more effectively in other codes (such as Soleno). It becomes much more interesting when complicated coil geometries are modeled. In the examples folder you find many examples demonstrating the capabilities of this code. Until a more detailed tutorial becomes available the user is referred here.

## Gallery
The output of the [rat-models](https://gitlab.com/Project-Rat/rat-models) library can be in VTK XML format and can be visualized using Kitware [VTK](https://vtk.org/) and [Paraview](https://www.paraview.org/), which are completely free and open-source. Additionally, they are available on all platforms and even offer a web-interface (need further investigation). These are excellent software packages allow for creating stunning pictures.

For instance the clover_rt.cpp example in rat-models can be visualized as:
![clover_rt](./figs/clover_rt2.png)

and the toroid.cpp example in rat-models as:
![mini_toroid](./figs/mini_toroid4.png)

and the cct.cpp example in rat-models as:
![cct_dipole](./figs/cct_dipole.png)

radially magnetized ring:
![ring](./figs/ring_radial.png)

## Graphical User Interface
As not everyone is too keen on coding in C++, work has started to develop a graphical user interface. I plan to make this interface available later this year. Because, much work has gone into it it will be necessary to charge for it. The library will always remain open source. Here is a teaser of what the GUI looks like.
![gui](./figs/gui2.png)

![gui](./figs/gui3.png)

## ToDo List
Not available.

## Contribution
Not yet.

## Versioning
* v1.100.0 - The code now allows compiling with single or double precision using the ENABLE_DOUBLE_PRECISION setting in Rat-Common (change with ccmake). The other libraries automatically adapt to this setting. To ensure the correct floating point type, Rat supplies type definition for it named rat::fltp. Floating point constants should be set using the macro RAT_CONST(x) to automatically append "f" when using single precision. The precision of the GPU CUDA code in the MLFMM can be set separately and automatic conversion between double and float is applied when necessary. However, the conversion causes some overhead causing the code to run slower. In Rat-Models the meshes now derive from the MLFMM sources and targets and can be directly used in a calculation. This avoids copying the mesh and thereby reduces memory overhead. This also allows the VTK file output to store each mesh into a separate file. A Paraview Data file is added, which automatically loads all the meshes into Paraview. Time is no longer set in the calculation (now "ModelMlfmm"). Instead the meshes are re-created each time step. This allows for greater flexibility in the code. Most calculations can be performed using the "rat::mdl::ModelMlfmm" and "rat::mdl::ModelInductance" classes, which act as wrappers. To avoid compilation problems the code is compiled in C++11. Communication with the file-system is now done through Boost, a new dependency.
* v1.000.x - This is the original code that came out of Alpha.

## Authors
* Jeroen van Nugteren

## Acknowledgements
* Dr. Conrad Sanderson and Dr. Ryan Curtin are acknowledged for creating the [Armadillo](http://arma.sourceforge.net) library which is extensively used throughout the project.
* Kitware is acknowledged for providing excellent Open-Source visualisation packages [VTK](https://vtk.org/) and [Paraview](https://www.paraview.org/).
* Per-Olof Persson is acknowledged for implementing the origininal distmesh code and allowing me to re-implement it in C++.
* Dr. Nikkie Deelen is acknowledged for writing the documentation and rat support on MacOS.
* Saman Ghannadzadeh is acknowledged for helping improve the CMake system and making it compatible for debian based systems.
* Dr. Antti Stenvall is acknowledged for his advice on the graphical user interface.
* Dr. Bernhard Auchmann is acknowledged for his advice on the constant perimeter coil ends.
* Thomas Nes is acknowledged for writing a [manual](doc/RAT_installation_guide.pdf) for installing Rat in a virtual box machine. 
* [CERN](https://home.cern/) is acknowledged for supporting me in 2019 durin the development for the first version of Rat (version alpha).
* Daniel Barna for his particle tracking contribution.
* D&#243;ra Veres for her contribution to curved CCT coils.

## License
This project is licensed under the [MIT](LICENSE).

## References
[1] L. Greengard and V. Rokhlin. A Fast Algorithm for Particle Simulations. J. Comput. Phys. 73, 325–348 (1987).

[2] Conrad Sanderson and Ryan Curtin. Armadillo: a template-based C++ library for linear algebra. Journal of Open Source Software, Vol. 1, pp. 26, 2016.

[3] P.-O. Persson, G. Strang, "A Simple Mesh Generator in MATLAB"
SIAM Review, Volume 46 (2), pp. 329-345, June 2004.

[4] P.-O. Persson, "Mesh Generation for Implicit Geometries",
Ph.D. thesis, Department of Mathematics, MIT, Dec 2004.

[5] P.D. Bourke, "A Contouring Subroutine", BYTE Magazine, June, 1987.