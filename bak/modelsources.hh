// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_SOURCES_HH
#define MDL_MODEL_SOURCES_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "common/defines.hh"
#include "modelgroup.hh"
#include "mlfmm/mgntargets.hh"
#include "mlfmm/currentsources.hh"
#include "meshcoil.hh"
#include "mesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelSources> ShModelSourcesPr;
	typedef arma::field<ShModelSourcesPr> ShModelSourcesPrList;

	// template for coil
	class ModelSources: public ModelGroup, public fmm::CurrentSources, public fmm::MgnTargets{		
		// methods
		public:
			// constructor	
			ModelSources();
			ModelSources(ShModelPr model);

			// factory
			static ShModelSourcesPr create();
			static ShModelSourcesPr create(ShModelPr model);

			// merge sources into self
			void merge_sources(const fmm::ShCurrentSourcesPrList &csl);

			// setup function
			void setup(cmn::ShLogPr lg = cmn::NullLog::create());
	};

}}

#endif
