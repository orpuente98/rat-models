// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef CALCULATE_SURFACE_HH
#define CALCULATE_SURFACE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "model.hh"
#include "coilmesh.hh"
#include "mlfmm.hh"
#include "coilsurface.hh"
#include "mlfmm/currentsources.hh"

// shared pointer definition
typedef std::shared_ptr<class CalcSurface> ShCalcSurfacePr;
typedef arma::field<ShCalcSurfacePr> ShCalcSurfacePrList;

// template for coil
class CalcSurface{
	// properties
	protected:
		// settings
		bool use_line_sources_ = true;
		
		// model
		ShModelPr model_;

		// surface mesh
		arma::Mat<double> R_;

		// orientation
		arma::Mat<double> L_;
		arma::Mat<double> N_;
		arma::Mat<double> D_;
		
		// elements
		arma::Mat<arma::uword> n_;
		arma::Row<arma::uword> id_;

		// calculated field
		arma::Mat<double> A_;
		arma::Mat<double> H_;

	// methods
	public:
		// constructor
		CalcSurface();
		CalcSurface(ShModelPr model);

		// factory methods
		static ShCalcSurfacePr create();
		static ShCalcSurfacePr create(ShModelPr model);

		// setting
		void set_model(ShModelPr model);	
		
		// calculation function
		void calculate(ShLogPr lg = NullLog::create());

		// function that calculates field angle
		arma::Row<double> get_field_angle() const;
		arma::Mat<double> get_magnetic_field() const;
		arma::Mat<double> get_coords() const;
		arma::Mat<arma::uword> get_elements() const;

		// set field
		void set_vector_potential(const arma::Mat<double> &A);

		// write results to gmsh file
		void export_gmsh(ShGmshFilePr gmsh) const;
};

#endif
