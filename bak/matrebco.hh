// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MATERIAL_REBCO_HH
#define MDL_MATERIAL_REBCO_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"
#include "common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatReBCO> ShMatReBCOPr;

	// Lubell/Kramer scaling relation
	class MatReBCO: public Material{
		protected:
			// density
			double density_ = 6380;

			// general parameters
			double Tc0_; // [K]
			double n_;
			double n1_;
			double n2_;

			// Parameters for ab-plane
			double pab_;
			double qab_;
			double Bi0ab_; // [T]
			double a_;
			double yab_;
			double alpha_ab_; // [AT/m^2]

			// Parameters for c-plane
			double pc_;
			double qc_;
			double Bi0c_; // [T]
			double yc_;
			double alphac_; // [AT/m^2]

			// Parameters for anisotropy
			double g0_;
			double g1_;
			double g2_;
			double g3_;
			double v_;
			double pkoff_;

			// power law settings
			double N_; // N value 
			double E0_; // electric field criterion [V/m]

			// type-0 pair flipping
			bool type0_flip_;

			// interpolation arrays
			arma::Col<double> Ti_;
			arma::Col<double> ki_;
			arma::Col<double> Cpi_; 
			arma::Col<double> rhoi_; 

		// methods
		public:
			// conductor
			MatReBCO();

			// factory
			static ShMatReBCOPr create();

			// default conductor settings
			void set_fujikura_cern();
			// void set_superpower_te();
			// void set_shanghai_te();
			// void set_fujikura_te();

			// calculate critical current
			arma::Mat<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const bool combined = true) const;

			// calculate electric field
			arma::Row<double> calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;
			arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;

			// thermal properties
			// arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const;
			virtual void deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif