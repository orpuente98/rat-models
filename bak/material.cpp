// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <filesystem>

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "rat/mat/baseconductor.hh"
#include "rat/mat/parallelconductor.hh"

#include "serializer.hh"

// main function
int main(){
	// settings
	double temperature = 20;

	// construct conductor
	rat::mat::ShSerializerPr slzr = rat::mdl::Serializer::create("json/materials/htstape/Fujikura_CERN.json");
	rat::mat::ShConductorPr mat = slzr->construct_tree<rat::mat::Conductor>();

	// get properties
	double shm = mat->calc_specific_heat(temperature);

	std::cout<<shm<<std::endl;
}
