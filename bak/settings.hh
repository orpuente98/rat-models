/* MagRat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef MDL_SETTINGS_HH
#define MDL_SETTINGS_HH

// general headers
#include <armadillo>
#include <cassert>

// specific headers
#include "common/defines.hh"
#include "common/extra.hh"
#include "common/log.hh"
#include "common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{
	// shared pointer definition
	typedef std::shared_ptr<class Settings> ShSettingsPr;

	// settings struct
	class Settings: virtual public cmn::Node{
		private:
			// data directory
			std::string datdir_ = "./";
			std::string model_name_ = "none";

			// output time
			arma::Row<double> output_times_ = {0};

			// output data
			bool enable_orientation_ = false;
			bool enable_magnetic_flux_ = false;
			bool enable_vector_potential_ = false;
			bool enable_current_density_ = false;
			bool enable_temperature_ = false;
			bool enable_field_angle_ = false;
			bool enable_eng_current_density_ = false;
			bool enable_percent_crit_curent_ = false;
			bool enable_critical_temperature_ = false;
			bool enable_temperature_margin_ = false;
			bool enable_power_density_ = false;
			bool enable_force_density_ = false;
			bool enable_electric_field_ = false;

			// units?

		public:
			// constructor
			Settings();

			// factory
			static ShSettingsPr create();

			// set data directory
			void set_datdir(const std::string &datdir);
			void set_model_name(const std::string &model_name);

			// get data directory
			std::string get_datdir() const;
			std::string get_model_name() const;

			// set output times
			void set_output_times(const arma::Row<double> &output_times);
			arma::Row<double> get_output_times() const;

			// set switchboard
			void set_enable_orientation(const bool enable_orientation);
			void set_enable_magnetic_flux(const bool enable_magnetic_flux);
			void set_enable_vector_potential(const bool enable_vector_potential);
			void set_enable_current_density(const bool enable_current_density);
			void set_enable_temperature(const bool enable_temperature);
			void set_enable_field_angle(const bool enable_field_angle);
			void set_enable_eng_current_density(const bool enable_eng_current_density);
			void set_enable_percent_crit_curent(const bool enable_percent_crit_curent);
			void set_enable_critical_temperature(const bool enable_critical_temperature);
			void set_enable_temperature_margin(const bool enable_temperature_margin);
			void set_enable_power_density(const bool enable_power_density);
			void set_enable_force_density(const bool enable_force_density);
			void set_enable_electric_field(const bool enable_electric_field);

			// set switchboard
			bool get_enable_orientation() const;
			bool get_enable_magnetic_flux() const;
			bool get_enable_vector_potential() const;
			bool get_enable_current_density() const;
			bool get_enable_temperature() const;
			bool get_enable_field_angle() const;
			bool get_enable_eng_current_density() const;
			bool get_enable_percent_crit_curent() const;
			bool get_enable_critical_temperature() const;
			bool get_enable_temperature_margin() const;
			bool get_enable_power_density() const;
			bool get_enable_force_density() const;
			bool get_enable_electric_field() const;
		
			// display settings
			void display(cmn::ShLogPr lg) const;

			// enable all output
			void enable_all();
			void disable_all();

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const;
			virtual void deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif