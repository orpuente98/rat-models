// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_COILSURFACE_HH
#define MDL_COILSURFACE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "common/gmshfile.hh"
#include "nameable.hh"
#include "circuit.hh"
#include "coolant.hh"
#include "conductor.hh"
#include "area.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CoilSurface> ShCoilSurfacePr;
	typedef arma::field<ShCoilSurfacePr> ShCoilSurfacePrList;

	// surface mesh of a coil
	class CoilSurface: public Nameable{
		// properties
		protected:
			// material of this coil
			ShConductorPr con_ = NULL;

			// drive
			arma::uword drive_id_;

			// store generators
			ShFramePr gen_;

			// store area
			ShAreaPr area_;

			// winding geometry
			double number_turns_;

			// mesh
			arma::Mat<double> R_;
			
			// node orientation vectors
			arma::Mat<double> L_;
			arma::Mat<double> N_;
			arma::Mat<double> D_;

			// elements
			arma::Mat<arma::uword> s_;
			
			// field storage if calculated
			arma::Mat<double> A_; // vector potential
			arma::Mat<double> H_; // magnetic field

		// methods
		public:
			// default constructor
			CoilSurface();
			CoilSurface(const arma::Mat<double> &Rn, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<arma::uword> &s);
			
			// factory
			static ShCoilSurfacePr create();
			static ShCoilSurfacePr create(const arma::Mat<double> &Rn, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<arma::uword> &s);
			
			// access circuit
			void set_circuit(ShCircuitPr circuit);
			ShCircuitPr get_circuit() const;

			// access coolant
			void set_coolant(ShCoolantPr coolant);
			ShCoolantPr get_coolant() const;

			// access conductor
			void set_conductor(ShConductorPr con);
			ShConductorPr get_conductor() const;

			// access generators
			void set_generators(ShFramePr gen);
			ShFramePr get_generators() const;

			// access area
			void set_area(ShAreaPr area);
			ShAreaPr get_area() const;

			// getting of number of turns
			void set_number_turns(const double number_turns);
			double get_number_turns() const;

			// access to vectors per section
			arma::Mat<double> get_coords() const;
			arma::Mat<double> get_direction() const;
			arma::Mat<double> get_normal() const;
			arma::Mat<double> get_transverse() const;
			arma::Mat<arma::uword> get_elements() const;

			// get counters
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// mesh export
			void export_gmsh(cmn::ShGmshFilePr gmsh, const bool incl_vectors = true);
	};

}}

#endif
