// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALCULATE_GRID_HH
#define MDL_CALCULATE_GRID_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"

#include "rat/mlfmm/mgntargets.hh"

#include "model.hh"
#include "path.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"
#include "vtkimg.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcGrid> ShCalcGridPr;

	// template for coil
	class CalcGrid: public CalcFieldMap{		
		// properties
		protected:
			// grid extends
			double x1_ = 0;
			double x2_ = 0;
			double y1_ = 0;
			double y2_ = 0;
			double z1_ = 0;
			double z2_ = 0;

			// number of points
			arma::uword num_x_ = 0;
			arma::uword num_y_ = 0;
			arma::uword num_z_ = 0;
			
		// methods
		public:
			// constructor
			CalcGrid();
			CalcGrid(const double x1, const double x2, const arma::uword num_x, const double y1, const double y2, const arma::uword num_y, const double z1, const double z2, const arma::uword num_z);

			// factory methods
			static ShCalcGridPr create();
			static ShCalcGridPr create(const double x1, const double x2, const arma::uword num_x, const double y1, const double y2, const arma::uword num_y, const double z1, const double z2, const arma::uword num_z);

			// set properties
			void set_dim_x(const double x1, const double x2, const arma::uword num_x);
			void set_dim_y(const double y1, const double y2, const arma::uword num_y);
			void set_dim_z(const double z1, const double z2, const arma::uword num_z);

			// calculation
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// VTK export
			ShVTKImgPr export_vtk(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
