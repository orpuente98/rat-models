// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MATERIAL_GROUP_HH
#define MDL_MATERIAL_GROUP_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatGroup> ShMatGroupPr;

	// (superconducting) material using current sharing model
	class MatGroup: public Material{
		protected: 
			// list of conductors
			ShMaterialPrList con_; 

			// fraction of cross section taken by each conductor
			arma::Col<double> fraction_;

			// materials are in parallel
			bool is_parallel_ = true;

			// parallel bi-section
			bool use_parallel_ = true;

			// tolerance for bisection
			double tolerance_ = 1e-7;

		public:
			// constructor
			MatGroup();

			// factory
			static ShMatGroupPr create();

			// get fraction list
			arma::Col<double> get_fraction() const;

			// set tolerance
			void set_tolerance(const double tolerance);

			// add new conductors
			void add_material(const double fraction, ShMaterialPr con);
			void add_material(const arma::Col<double> fractions, ShMaterialPrList con);

			// precalculate material properties for fast electric field calculations
			arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// calculate electric field using bi-section solver
			arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const;
			arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const;

			// calculation of critical current
			arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			arma::Row<double> calc_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;

			// thermal properties
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy this group
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};
}}

#endif