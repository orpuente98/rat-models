// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>

// header files for FX-Models
#include "material.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"
#include "matrebco.hh"
#include "matstainless316.hh"

// main
int main(){
	// settings
	const double tol_bisection = 1e-7;

	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	rat::mdl::ShMatGroupPr rebco_cu = rat::mdl::MatGroup::create();
	rebco_cu->set_tolerance(tol_bisection);

	// add ReBCO superconductor
	rat::mdl::ShMatReBCOPr rebco = rat::mdl::MatReBCO::create();
	rebco->set_fujikura_cern(); // one of the default scaling relations
	rebco_cu->add_material(0.01,rebco); // 1% of the cable is superconductor

	// add copper coatings
	rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
	copper->set_RRR(20); // standard copper with RRR of 100
	rebco_cu->add_material(0.20,copper); // 20 % of the cable is copper (10 mu on each side)

	// add copper coatings
	rat::mdl::ShMatStainless316Pr stst = rat::mdl::MatStainless316::create();
	rebco_cu->add_material(0.70,stst); // 70 % hastelloy layer

	// current sharing time
	// arma::Row<double> Bm = arma::Row<double>(20,arma::fill::ones)*8;
	// arma::Row<double> T = arma::Row<double>(20,arma::fill::ones)*1.9;
	// arma::Row<double> alpha = arma::Row<double>(20,arma::fill::zeros);

	// // chose critical current density of NbTi
	// arma::Row<double> Jop = arma::linspace<arma::Row<double> >(0,2e9,20);

	// std::cout<<Jop<<std::endl;

	// // calculate electric field
	// arma::Row<double> E = rebco_cu->calc_electric_field(Jop,Bm,T,alpha);
	
	// std::cout<<Jop<<std::endl;

	// // output
	// std::cout<<E<<std::endl;

	// write to VTK table
	rat::mdl::ShVTKTablePr vtkmat1 = rebco_cu->export_vtk_T(4,500,100e6);
	vtkmat1->write("sharing_temperature");

	// write to VTK table
	rat::mdl::ShVTKTablePr vtkmat2 = rebco_cu->export_vtk_J(-1e9,1e9,20,5);
	vtkmat2->write("sharing_current");
}	