// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MATERIAL_HH
#define MDL_MATERIAL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/extra.hh"
#include "rat/common/node.hh"
#include "area.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Material> ShMaterialPr;
	typedef arma::field<ShMaterialPr> ShMaterialPrList;

	// template for materials
	class Material: virtual public cmn::Node{
		// methods
		public:
			// virtual destructor
			virtual ~Material(){};
			
			// get fraction list
			arma::Col<double> get_fraction() const;

			// precalculate material properties for fast electric field calculations
			virtual arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const = 0;

			// calculate electric field
			virtual arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const = 0;
			virtual arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const = 0;
			virtual arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props, const ShAreaPr area) const;

			// without pre-calculation
			virtual arma::Row<double> calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			virtual arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			virtual arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const ShAreaPr area) const;

			// calculate electric field with scalar input
			virtual double calc_specific_heat(const double T) const;
			virtual double calc_electric_field(const double J, const double Bm, const double T, const double alpha) const;
			virtual double calc_thermal_conductivity(const double Bm, const double T) const;

			// critical current function
			// if critical current returns 0 it is either 
			// a superconductor in normal conducting state
			// or a normal conductor
			virtual arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			virtual arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const ShAreaPr area) const;

			// critical temperature calculation
			virtual arma::Row<double> calc_critical_temperature(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &alpha, const ShAreaPr area = NULL) const;

			// thermal properties
			// virtual arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const = 0;
			virtual arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const = 0;
			virtual arma::Row<double> calc_specific_heat(const arma::Row<double> &T) const = 0;
			virtual arma::Row<double> calc_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;

			// export vtk table
			ShVTKTablePr export_vtk_T(const double Tmin = 1.0, const double Tmax = 300.0, const double Jfix = 100e6, const double Bfix = 5.0) const;
			ShVTKTablePr export_vtk_J(const double Jmin = -1e9, const double Jmax = 1e9, const double Tfix = 20, const double Bfix = 5.0) const;

			// copy
			virtual ShMaterialPr copy() const = 0;
	};

}}

#endif