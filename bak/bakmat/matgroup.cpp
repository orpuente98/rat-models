// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "matgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatGroup::MatGroup(){

	}

	// factory
	ShMatGroupPr MatGroup::create(){
		return std::make_shared<MatGroup>();
	}

	// get fraction
	arma::Col<double> MatGroup::get_fraction()const{
		return fraction_;
	}

	// setting tolerance for bisection
	void MatGroup::set_tolerance(const double tolerance){
		if(tolerance_<=0)rat_throw_line("tolerance must be positive");
		tolerance_ = tolerance;
	}

	// add new conductor
	void MatGroup::add_material(const double fraction, ShMaterialPr con){
		// check input
		assert(con!=NULL); 

		// allocate new conductor list
		ShMaterialPrList new_con(con_.n_elem + 1);

		// set old and new conductors
		for(arma::uword i=0;i<con_.n_elem;i++)new_con(i) = con_(i);
		new_con(con_.n_elem) = con;

		// set new conductor list
		con_ = new_con;

		// extend fractions
		fraction_ = arma::join_vert(fraction_,arma::Col<double>::fixed<1>{fraction});
	}

	// add new conductor
	void MatGroup::add_material(const arma::Col<double> fraction, ShMaterialPrList con){
		// allocate new source list
		ShMaterialPrList new_con(con_.n_elem + con.n_elem);

		// set old and new sources
		for(arma::uword i=0;i<con_.n_elem;i++)new_con(i) = con_(i);
		for(arma::uword i=0;i<con.n_elem;i++)new_con(con_.n_elem+i) = con(i);

		// set new source list
		con_ = new_con;

		// extend fractions
		fraction_ = arma::join_vert(fraction_,fraction);
	}

	// calculate current density from electric field
	arma::Row<double> MatGroup::calc_current_density_fast(
		const arma::Row<double> &E, const arma::Mat<double> &props) const{

		// check filling fractions
		if(arma::accu(fraction_)>1.0)rat_throw_line("Total filling fraction exceeds 1.0");

		// check pre-calculation
		assert(props.n_rows==con_.n_elem);

		// allocate
		arma::Row<double> J(E.n_rows,E.n_cols,arma::fill::zeros);
		
		// walk over conductors and accumulate current density
		for(arma::uword i=0;i<con_.n_elem;i++){
			J += fraction_(i)*con_(i)->calc_current_density_fast(E,props.row(i));
		}

		// return current density
		return J;
	}


	// calculate electric field using bi-section solver
	arma::Row<double> MatGroup::calc_electric_field_fast(
		const arma::Row<double> &J, const arma::Mat<double> &props) const{

		// check input
		assert(props.n_rows==con_.n_rows);

		// estimate lower bound
		arma::Row<double> Elow(J.n_elem,arma::fill::zeros);
		arma::Row<double> Ehigh(J.n_elem); Ehigh.fill(arma::datum::inf);

		// walk over conductors
		for(arma::uword i=0;i<con_.n_elem;i++){
			// we assume all current runs through this material
			const arma::Row<double> Jcon = arma::abs(J)/fraction_(i);
			
			// calculate electric field
			const arma::Row<double> Econ = con_(i)->calc_electric_field_fast(Jcon,props.row(i));
			
			// find lowest electric field value
			Ehigh = arma::min(Ehigh,Econ);
		}

		// calculate current density at these bounds
		// arma::Row<double> vlow = calc_current_density_fast(Elow,Bm,T,alpha,Jc,sigma) - J;
		// arma::Row<double> vhigh = calc_current_density_fast(Ehigh,Bm,T,alpha,Jc,sigma) - J;

		// allocate midpoint output electric field
		arma::Row<double> Emid(J.n_elem);

		// start bisection algorithm
		for(;;){
			// update mid point
			Emid = (Elow+Ehigh)/2;

			// calculate intermeditae value
			const arma::Row<double> vmid = calc_current_density_fast(Emid,props) - arma::abs(J);

			// update lower and upper bounds
			const arma::Row<arma::uword> idx1 = arma::find(vmid<0).t();
			const arma::Row<arma::uword> idx2 = arma::find(vmid>=0).t();

			// update mid-point
			Elow(idx1) = Emid(idx1); Ehigh(idx2) = Emid(idx2);

			// check convergence
			if(arma::max(Ehigh-Elow)<tolerance_)break;
		}

		// find an electric field where all the 
		// supplied current density is soaked by the materials
		return arma::sign(J)%Emid;
	}

	// critical currnet summation
	arma::Row<double> MatGroup::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{

		// check filling fractions
		if(arma::accu(fraction_)>1.0)rat_throw_line("Total filling fraction exceeds 1.0");

		// allocate
		arma::Mat<double> Je(con_.n_elem,Bm.n_elem);

		// walk over materials
		for(arma::uword i=0;i<con_.n_elem;i++){
			Je.row(i) = con_(i)->calc_critical_current_density(Bm,T,alpha); 
		}

		// combine and return
		return arma::sum(Je.each_col()%fraction_,0);
	}

	// critical currnet summation
	arma::Row<double> MatGroup::calc_conductivity(
		const arma::Row<double> &Bm, const arma::Row<double> &T) const{

		// check filling fractions
		if(arma::accu(fraction_)>1.0)rat_throw_line("Total filling fraction exceeds 1.0");

		// allocate
		arma::Mat<double> sigma(con_.n_elem,Bm.n_elem);

		// walk over materials
		for(arma::uword i=0;i<con_.n_elem;i++){
			sigma.row(i) = con_(i)->calc_conductivity(Bm,T);
		}

		// combine and return
		return arma::sum(fraction_%sigma.each_col(),0);
	}

	// precalculate material properties for fast electric field calculations
	arma::Mat<double> MatGroup::calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		// allocate
		arma::Mat<double> props(con_.n_elem,Bm.n_elem);
		
		// walk over materials
		for(arma::uword i=0;i<con_.n_elem;i++){
			props.row(i) = con_(i)->calc_properties(Bm,T,alpha); 
		}

		// return property matrix
		return props;
	}


	// copy this conductor group
	ShMaterialPr MatGroup::copy() const{
		// create new conductor
		ShMatGroupPr mycopy = MatGroup::create();

		// copy the stored conductors in this group
		mycopy->con_.set_size(con_.n_elem);
		for(arma::uword i=0;i<con_.n_elem;i++)
			mycopy->con_(i) = con_(i)->copy();
		
		// copy my properties
		mycopy->fraction_ = fraction_;
		mycopy->use_parallel_ = use_parallel_;
		mycopy->tolerance_ = tolerance_;

		// return copy
		return mycopy;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Row<double> MatGroup::calc_specific_heat(const arma::Row<double> &T)const{
		// allocate
		arma::Row<double> sh(T); sh.zeros();

		// add materials
		for(arma::uword i=0;i<con_.n_elem;i++)
			sh += fraction_(i)*con_(i)->calc_specific_heat(T);

		// return values
		return sh;
	}


	// thermal conductivity 
	arma::Row<double> MatGroup::calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const{
		// check length
		assert(con_.n_elem==fraction_.n_elem);

		// allocate
		arma::Row<double> k(T); k.zeros();

		// materials are stacked in parallel
		// conductivities are added
		if(is_parallel_){
			// add materials
			for(arma::uword i=0;i<con_.n_elem;i++){
				k += fraction_(i)*con_(i)->calc_thermal_conductivity(Bm,T);
			}
		}

		// materials are stacked in series
		// resistivities are added
		else{
			// allocate thermal resistivity
			arma::Row<double> r(T); r.zeros();

			// add materials
			for(arma::uword i=0;i<con_.n_elem;i++)
				k += fraction_(i)*(1.0/con_(i)->calc_thermal_conductivity(Bm,T));

			// set k
			k = 1.0/r;
		}

		// return values
		return k;
	}

	// get type
	std::string MatGroup::get_type(){
		return "rat::mdl::matgroup";
	}

	// method for serialization into json
	void MatGroup::serialize(Json::Value &js, cmn::SList &list) const{
		// settings
		js["type"] = get_type();
		js["tolerance"] = tolerance_;
		js["parallel"] = use_parallel_;

		// conductor objects
		const arma::uword num_con = con_.n_elem;
		for(arma::uword i=0;i<num_con;i++){
			js["con"].append(cmn::Node::serialize_node(con_(i), list));
			js["fraction"].append(fraction_(i));
		}
	}

	// method for deserialisation from json
	void MatGroup::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// settings
		use_parallel_ = js["parallel"].asBool();
		tolerance_ = js["tolerance"].asDouble();

		// conductors
		const arma::uword num_con = js["con"].size();
		const arma::uword num_frac = js["fraction"].size();
		if(num_con!=num_frac)rat_throw_line("conductor group inconsistent");
		con_.set_size(num_con);	fraction_.set_size(num_frac);
		for(arma::uword i=0;i<num_con;i++){
			con_(i) = cmn::Node::deserialize_node<Material>(js["con"].get(i,0), list, factory_list);
			fraction_(i) = js["fraction"].get(i,0).asDouble();
		}
	}

}}

