// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALCULATE_LINE_HH
#define MDL_CALCULATE_LINE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "model.hh"
#include "path.hh"
#include "vtkunstr.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcLine> ShCalcLinePr;

	// template for coil
	class CalcLine: public CalcFieldMap{		
		// properties
		protected:
			// line definition
			ShPathPr base_ = NULL;
			
			// coordinates
			//arma::Mat<double> R_;
			arma::Mat<double> L_;
			arma::Mat<double> D_;
			arma::Mat<double> N_;

			// position along line
			arma::Row<double> ell_;
		
		// methods
		public:
			// constructor
			CalcLine();
			CalcLine(ShModelPr model, ShPathPr base);

			// factory methods
			static ShCalcLinePr create();
			static ShCalcLinePr create(ShModelPr model, ShPathPr base);

			// setting of the path
			void set_base(ShPathPr base);

			// calculation
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;
			
			// access to vectors per section
			virtual arma::Mat<double> get_coords() const;

			// orientation vectors
			arma::Mat<double> get_direction() const;
			arma::Mat<double> get_normal() const;
			arma::Mat<double> get_transverse() const;

			// export to vtk
			ShVTKUnstrPr export_vtk_coord(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};
}}

#endif
