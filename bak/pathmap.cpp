// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathmap.hh"

// default constructor
PathMap::PathMap(){

}

// constructor with dimension input
PathMap::PathMap(ShPathPr base, ShPathPr path){
	set_base(base); set_path(path);
}

// factory
ShPathMapPr PathMap::create(){
	//return ShPathFlaredPr(new PathFlared);
	return std::make_shared<PathFlared>();
}

// factory with dimension input
ShPathMapPr PathMap::create(ShPathPr base, ShPathPr path){
	return std::make_shared<PathMap>(base, path);
}

// set base path
void PathMap::set_base(ShPathPr base){
	if(base==NULL)throw_line("base points to NULL");
	base = base_;
}

// set target path
void PathMap::set_path(ShPathPr path){
	if(path==NULL)throw_line("path points to NULL");
	path_ = path;
}

// get generators
ShDbGenPr PathMap::create_generators() const{
	// get generators
	ShDbGenPr gen_base = base_->create_generators();
	ShDbGenPr gen_path = path_->create_generators();

	// combine base path
	gen_base->combine();

	// get elements
	arma::Mat<double> R = gen_base->get_coord();
	arma::Mat<double> L = gen_base->get_longitudinal();
	arma::Mat<double> N = gen_base->get_normal();
	arma::Mat<double> D = gen_base->get_transverse();

}