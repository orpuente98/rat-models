// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_MERGE_HH
#define MDL_MODEL_MERGE_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"
#include "rat/common/parfor.hh"

#include "transformations.hh"
#include "modelgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelMerge> ShModelMergePr;

	// a collection of coils
	class ModelMerge: public ModelMerge, public Transformations, public Nameable{
		// methods
		public:
			// constructor
			ModelMerge();
			ModelMerge(ShModelPr model);
			ModelMerge(ShModelPrList models);

			// factory methods
			static ShModelMergePr create();
			static ShModelMergePr create(ShModelPr model);
			static ShModelMergePr create(ShModelPrList models);

			// get number of calculation objects in all submodels
			virtual arma::uword get_num_objects() const override;

			// create calculation data objects
			virtual ShCalcMeshPrList create_meshes(const fltp time = 0, const bool low_poly = false) const override; // create a mesh of the stored coils

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
