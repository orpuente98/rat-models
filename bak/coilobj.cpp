// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "coilobj.hh"

// set conductor
void CoilObj::set_circuit(ShCircuitPr circ){
	assert(circ!=NULL);
	circ_ = circ;
}

// get conductor
ShCircuitPr CoilObj::get_circuit() const{
	assert(circ_!=NULL);
	return circ_;
}

// set conductor
void CoilObj::set_coolant(ShCoolantPr clnt){
	assert(clnt!=NULL);
	clnt_ = clnt;
}

// get conductor
ShCoolantPr CoilObj::get_coolant() const{
	assert(clnt_!=NULL);
	return clnt_;
}

// set conductor
void CoilObj::set_conductor(ShConductorPr con){
	assert(con!=NULL);
	con_ = con;
}

// get conductor
ShConductorPr CoilObj::get_conductor() const{
	assert(con_!=NULL);
	return con_;
}

// set number of turns
void CoilObj::set_number_turns(const double number_turns){
	number_turns_ = number_turns;
}

// getting of coil number of turns
double CoilObj::get_number_turns() const{
	return number_turns_;
}