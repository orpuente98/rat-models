// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for common
#include "rat/common/log.hh"

// materials headers
#include "rat/mat/conductor.hh"
#include "rat/mat/parallelconductor.hh"

// header files for Models
#include "pathaxis.hh"
#include "crosscircle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelmlfmm.hh"

// DESCRIPTION
// In this example the custom CCT class is used to 
// create a combined function magnet with both
// dipole and quadrupole components.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true;
	const bool run_grid = true;
	const bool run_plane = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./earnshaw/";

	// geometry settings
	const rat::fltp length = 0.4; // bend the magnet
	const rat::fltp current = 1000;
	const rat::fltp dwire = 2e-3;
	const rat::fltp radius = 40e-3;
	const arma::uword num_wires = 20;
	const rat::fltp delem = 1e-3;
	

	// GEOMETRY SETUP
	rat::mdl::ShCrossCirclePr circle = rat::mdl::CrossCircle::create(dwire, delem);

	rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('z', 'x', length, {0,0,0}, delem);

	// model for wires
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();

	// axis
	for(arma::uword i=0;i<num_wires;i++){
		rat::mdl::ShModelCoilPr wire = rat::mdl::ModelCoil::create(axis,circle);
		if(i!=0)wire->add_reverse();
		rat::fltp theta = i*2*arma::Datum<rat::fltp>::pi/num_wires;
		theta += 0.1*std::sin(theta);
		wire->add_translation(radius*std::cos(theta),radius*std::sin(theta),0);
		wire->set_operating_current(current);
		model->add_model(wire);
	}

	// central wire
	rat::mdl::ShModelCoilPr center_wire = rat::mdl::ModelCoil::create(axis,circle);
	center_wire->set_operating_current(current);
	center_wire->add_reverse();
	model->add_model(center_wire);


	
	// CALCULATION AND OUTPUT
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create calculation
	rat::mdl::ShModelMlfmmPr mlfmm = 
		rat::mdl::ModelMlfmm::create(model);
	mlfmm->set_output_dir(output_dir);
	mlfmm->set_target_meshes(run_coil_field); 
	mlfmm->set_target_grid(run_grid);
	
	// plane calculation
	if(run_plane){
			// Create a grid
		rat::mdl::ShCalcGridPr grid = 
			rat::mdl::CalcGrid::create(-0.1,0.1,800, -0.1,0.1,800, 0,0,1);
		grid->set_output_type("plane");
		grid->set_name("xy-plane");

		// add calculation
		mlfmm->add_target(grid);
	}

	// calculate everything
	mlfmm->calculate(lg);


	// done
	return 0;
}