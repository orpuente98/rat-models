// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef COIL_OBJ_HH
#define COIL_OBJ_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "common/defines.hh"
#include "circuit.hh"
#include "coolant.hh"
#include "conductor.hh"
#include "conductornist.hh"

// object containing coil properties
class CoilObj{
	// properties
	protected:
		// powering circuit
		ShCircuitPr circ_ = Circuit::create(0);

		// coolant 
		ShCoolantPr clnt_ = Coolant::create(4.5);

		// material of this coil
		ShConductorPr con_ = ConductorNIST::create();

		// winding geometry
		double number_turns_ = 1;

	// methods
	public:
		// access circuit
		void set_circuit(ShCircuitPr circuit);
		ShCircuitPr get_circuit() const;

		// access coolant
		void set_coolant(ShCoolantPr coolant);
		ShCoolantPr get_coolant() const;

		// set conductor
		void set_conductor(ShConductorPr con);
		ShConductorPr get_conductor() const;

		// getting of number of turns
		void set_number_turns(const double number_turns);
		double get_number_turns() const;
};

#endif
