// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "griddata.hh"
#include "meshdata.hh"

// main function
int main(){

	// settings
	const rat::fltp x1 = -1, x2 = 1; const arma::uword num_x = 50;
	const rat::fltp y1 = -1, y2 = 1; const arma::uword num_y = 50;
	const rat::fltp z1 = -1, z2 = 1; const arma::uword num_z = 50;
	
	// create grid
	const rat::mdl::ShGridDataPr grd = rat::mdl::GridData::create(x1,x2,num_x,y1,y2,num_y,z1,z2,num_z);
	
	// setup grid for fake calculation
	grd->set_field_type('R',3); grd->setup_targets(); grd->allocate();

	// get target coordinates from grid
	const arma::Mat<rat::fltp> R = grd->get_target_coords();

	// sphere equation
	grd->add_field('R',R);

	// extract level
	const rat::fltp level = 0.5;

	// export to VTK
	const rat::mdl::ShMeshDataPr md = grd->extract_isosurface('R','m',level);
	md->allocate(); md->setup_targets();
	md->export_vtk()->write("test.vtu");

	// 
	return 0;
}
