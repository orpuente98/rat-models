// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef PATH_MAP_HH
#define PATH_MAP_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "common/defines.hh"
#include "path.hh"
#include "dbgen.hh"
#include "transformations.hh"

// shared pointer definition
typedef std::shared_ptr<class PathMap> ShPathMapPr;

// map one path onto another
class PathMap: public Path, public Transformations{
	// properties
	protected:
		// base path
		ShPathPr base_; // defines overall shape
		ShPathPr path_; // the mapped path

		// select longitudinal axis
		char longitudinal_axis_ = 'z';
		char transverse_axis_ = 'x';

	// methods
	public:
		// constructor
		PathMap();
		PathMap(ShPathPr base, ShPathPr path);
		
		// factory
		static ShPathMapPr create();
		static ShPathMapPr create(ShPathPr base, ShPathPr path);

		// set properties
		void set_base(ShPathPr base);
		void set_path(ShPathPr path)

		// get generators
		virtual ShDbGenPr create_generators() const;
};

#endif
