// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_STRAIGHT_HH
#define MDL_PATH_STRAIGHT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathStraight> ShPathStraightPr;

	// cross section of coil
	class PathStraight: public Path{
		// properties
		protected:		
			// geometry 
			fltp offset_ = RAT_CONST(0.0); // offset in [m]
			fltp element_size_ = RAT_CONST(2e-3); // target element length in [m]	
			fltp length_ = RAT_CONST(0.1); // length of the path in [m]

			// number of elements must be divisible by this number
			// arma::uword element_divisor_ = 2;

		// methods
		public:
			// constructor
			PathStraight();
			PathStraight(const fltp length, const fltp element_size);

			// factory
			static ShPathStraightPr create();
			static ShPathStraightPr create(const fltp length, const fltp element_size);

			// elements
			// void set_element_divisor(const arma::uword element_divisor);

			// set properties
			void set_length(const fltp length);
			virtual void set_element_size(const fltp element_size);
			virtual void set_offset(const fltp offset);

			// get geometry
			fltp get_length() const;
			fltp get_element_size() const;
			// arma::uword get_element_divisor() const;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
