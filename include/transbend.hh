// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRANS_BEND_HH
#define MDL_TRANS_BEND_HH

#include <armadillo>
#include <memory>

#include "trans.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransBend> ShTransBendPr;

	// cross section of coil
	class TransBend: public Trans{
		// properties
		private:
			// bending over three axes
			arma::Col<fltp>::fixed<3> axis_ = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)}; // axis over which the bending takes place
			arma::Col<fltp>::fixed<3> direction_ = {RAT_CONST(0.0),RAT_CONST(1.0),RAT_CONST(0.0)}; // bending direction
			fltp radius_ = RAT_CONST(5.0); // bending radius
			fltp offset_ = RAT_CONST(0.0); // offset applied in the bending direction

		// methods
		public:
			// constructors
			TransBend();
			TransBend(const arma::Col<fltp>::fixed<3> &axis, const arma::Col<fltp>::fixed<3> &direction, const fltp radius, const fltp offset);
			
			// factory methods
			static ShTransBendPr create();
			static ShTransBendPr create(const arma::Col<fltp>::fixed<3> &axis, const arma::Col<fltp>::fixed<3> &direction, const fltp radius, const fltp offset = RAT_CONST(0.0));
			
			// setting
			void set_bending(const arma::Col<fltp>::fixed<3> &axis, const arma::Col<fltp>::fixed<3> &direction, const fltp radius, const fltp offset = RAT_CONST(0.0));

			// set axis and direction
			void set_axis(const arma::Col<fltp>::fixed<3> &axis);
			void set_direction(const arma::Col<fltp>::fixed<3> &direction);
			void set_radius(const fltp radius);
			void set_offset(const fltp offset);

			// get axis and direction
			arma::Col<fltp>::fixed<3> get_axis() const;
			arma::Col<fltp>::fixed<3> get_direction() const;
			fltp get_radius() const;
			fltp get_offset() const;

			// applying
			void apply_coords(arma::Mat<fltp> &R) const override;
			void apply_vectors(const arma::Mat<fltp> &R, arma::Mat<fltp> &V, const char str) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
