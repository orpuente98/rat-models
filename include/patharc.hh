// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_ARC_HH
#define MDL_PATH_ARC_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "path.hh"
#include "frame.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathArc> ShPathArcPr;

	// circular softway bend
	class PathArc: public Path{
		// properties
		protected:
			// bend in normal or transverse direction
			// where transverse implies hard-way bending
			bool transverse_ = false;

			// offset in [m]
			fltp offset_ = 0;

			// target element length in [m]
			fltp element_size_ = 0;

			// radius of circle section in [m]
			fltp radius_ = 0;

			// angle of circle section in [rad]
			fltp arc_length_ = 0;


		// methods
		public:
			// constructor
			PathArc();
			PathArc(
				const fltp radius, 
				const fltp arc_length, 
				const fltp element_size, 
				const fltp offset = 0);

			// factory
			static ShPathArcPr create();
			static ShPathArcPr create(
				const fltp radius, 
				const fltp arc_length, 
				const fltp element_size, 
				const fltp offset = 0);

			// set geometry
			void set_transverse(const bool transverse);
			void set_arc_length(const fltp arc_length);
			void set_radius(const fltp radius);
			void set_offset(const fltp offset);
			void set_element_size(const fltp element_size);
			// void set_element_divisor(const arma::uword element_divisor);

			// get geometry
			bool get_transverse()const;
			fltp get_offset()const;
			fltp get_element_size()const;
			// arma::uword get_element_divisor()const;
			fltp get_arc_length()const;
			fltp get_radius()const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
