// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_SURFACE_COIL_HH
#define MDL_CALC_SURFACE_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/mat/conductor.hh"

#include "rat/common/elements.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/currentmesh.hh"

//#include "coilsurface.hh"
#include "meshdata.hh"
#include "data.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CoilSurfaceData> ShCoilSurfaceDataPr;
	typedef arma::field<ShCoilSurfaceDataPr> ShCoilSurfaceDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class CoilSurfaceData: public SurfaceData{
		// properties
		protected:
			// operating current
			fltp operating_current_;
			
			// number of turns
			fltp number_turns_;
			
			// material of this coil
			rat::mat::ShConductorPr material_;

		// methods
		public:
			// default constructor
			CoilSurfaceData();

			// virtual destructor
			virtual ~CoilSurfaceData(){};

			// factory
			static ShCoilSurfaceDataPr create();

			// operating current
			void set_operating_current(const fltp operating_current);
			fltp get_operating_current() const;

			// number of turns
			void set_number_turns(const fltp number_turns);
			fltp get_number_turns() const;

			// material
			void set_material(rat::mat::ShConductorPr material);
			rat::mat::ShConductorPr get_material() const;

			// get the current density at mesh nodes
			fltp calc_current_density() const;
			
			// export vtk
			ShVTKObjPr export_vtk() const override;
	};

}}

#endif
