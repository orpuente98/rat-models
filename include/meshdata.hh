// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MESH_DATA_HH
#define MDL_MESH_DATA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/mat/conductor.hh"

#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/sources.hh"
#include "rat/mlfmm/interp.hh"

#include "rat/common/elements.hh"
#include "rat/common/extra.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"
#include "rat/common/gmshfile.hh"

#include "trans.hh"
#include "area.hh"
#include "frame.hh"
#include "nameable.hh"
#include "surfacedata.hh"
#include "vtkunstr.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MeshData> ShMeshDataPr;
	typedef arma::field<ShMeshDataPr> ShMeshDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class MeshData: public TargetData, virtual public rat::fmm::Sources{
		// properties
		protected:
			// circuit index
			arma::uword circuit_index_;

			// start and end connection
			bool connect_start_;
			bool connect_end_;

			// mesh is made by calculation
			bool calc_mesh_ = false;

			// material of this coil
			rat::mat::ShConductorPr material_;

			// homogenized properties throughout cross section
			bool enable_current_sharing_ = false;

			// frame
			ShFramePr frame_;

			// cross sectional area
			ShAreaPr area_;

			// node orientation vectors
			arma::Mat<fltp> L_;
			arma::Mat<fltp> N_;
			arma::Mat<fltp> D_;

			// operating temperature
			fltp operating_temperature_ = RAT_CONST(0.0);

			// node temperatures
			arma::Row<fltp> temperature_;

			// mesh element indexes
			arma::Mat<arma::uword> n_; // internal (hex)
			arma::Mat<arma::uword> s_; // surface (quad)

			// dimensionality of meshes
			arma::uword n_dim_ = 0;
			arma::uword s_dim_ = 0;

			// trace of indexes storing the 
			// branches of the tree
			std::list<arma::uword> trace_;

		// methods
		public:
			// constructors
			MeshData(); // default
			MeshData(
				const ShFramePr &frame, 
				const ShAreaPr &area);
			MeshData(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);
			
			// factory
			static ShMeshDataPr create();
			static ShMeshDataPr create(
				const ShFramePr &frame, 
				const ShAreaPr &area);
			static ShMeshDataPr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);

			// virtual destructor
			virtual ~MeshData(){};

			// setup mesh using frame and area
			void setup(
				const ShFramePr &frame, 
				const ShAreaPr &area);
			void determine_dimensions();

			// set orientation vectors
			void set_longitudinal(const arma::Mat<fltp> &L);
			void set_transverse(const arma::Mat<fltp> &D);
			void set_normal(const arma::Mat<fltp> &N);

			// set circuit index
			void set_circuit_index(const arma::uword circuit_index);
			void set_connect_start(const bool connect_start);
			void set_connect_end(const bool connect_start);

			// current sharing
			void set_enable_current_sharing(const bool enable_current_sharing);
			bool get_enable_current_sharing()const;
			
			// get circuit index
			arma::uword get_circuit_index()const;
			bool get_connect_start()const;
			bool get_connect_end()const;

			// set mesh
			void set_elements(const arma::Mat<arma::uword> &n);
			void set_surface_elements(const arma::Mat<arma::uword> &s);

			// append id
			void append_trace_id(const arma::uword trace_id);
			void clear_trace_id();

			// get list of id's
			std::list<arma::uword> get_trace();

			// get pointers to area and frame
			ShFramePr get_frame();
			ShAreaPr get_area();

			// set and get calculation mesh
			void set_calc_mesh(const bool calc_mesh = true);
			bool get_calc_mesh() const;

			// material
			void set_material(const rat::mat::ShConductorPr &material);
			rat::mat::ShConductorPr get_material() const;
			
			// setup surface mesh
			void setup_surface(const ShSurfaceDataPr &surf) const;
			virtual ShSurfaceDataPr create_surface() const;
			
			// create an interpolation mesh
			fmm::ShInterpPr create_interp() const;

			// extract edges
			arma::field<arma::Mat<fltp> > get_edges() const;
			void create_xyz(
				arma::field<arma::Mat<fltp> > &x, 
				arma::field<arma::Mat<fltp> > &y, 
				arma::field<arma::Mat<fltp> > &z, 
				arma::uword &num_edges) const;

			// get size and position of mesh
			arma::Col<fltp>::fixed<3> get_lower_bound() const;
			arma::Col<fltp>::fixed<3> get_upper_bound() const;

			// access coordinates and elements 
			arma::Mat<fltp> get_element_coords()const;
			const arma::Mat<fltp>& get_nodes()const;
			const arma::Mat<arma::uword>& get_elements() const;
			const arma::Mat<arma::uword>& get_surface_elements() const;

			// get orientation vectors
			const arma::Mat<fltp>& get_longitudinal() const;
			const arma::Mat<fltp>& get_normal() const;
			const arma::Mat<fltp>& get_transverse() const;

			// get element dimensions
			arma::uword get_n_dim() const;
			arma::uword get_s_dim() const;

			// apply a transformation to the mesh and orientation vectors
			void apply_transformations(const std::list<ShTransPr> &trans_list);
			void apply_transformation(const ShTransPr &trans);

			// getting counters
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;
			arma::uword get_num_surface() const;

			// calculation of volume, length and surface area
			fltp calc_total_volume() const;
			fltp calc_total_surface_area() const;
			arma::Row<fltp> calc_volume() const;
			fltp calc_ell() const;
			
			// setting operating conditions
			void set_operating_temperature(const fltp operating_temperature);
			void set_temperature(const fltp temperature);
			void set_temperature(const arma::Row<fltp> &temperature);
			
			// get operating temperature
			fltp get_operating_temperature() const;

			// get node temperatures
			arma::Row<fltp> get_temperature() const;

			// get number of turns
			virtual fltp get_number_turns() const;
			virtual fltp get_operating_current() const;

			// calculate enclosed flux
			virtual fltp calculate_flux() const;
			virtual fltp calculate_element_self_inductance() const;

			// VTK export
			virtual ShVTKObjPr export_vtk() const override;

			// FreeCAD export
			void export_freecad(const cmn::ShFreeCADPr &freecad) const;
			virtual void export_opera(const cmn::ShOperaPr &opera) const;
			void export_gmsh(const cmn::ShGmshFilePr &gmsh)const;

			// write to file
			fltp calculate_mfom_2d(fltp bore_length = 0) const;
			fltp calculate_mfom_3d() const;

			// calculate force density
			virtual arma::Mat<fltp> calc_force_density() const;

			// calculate integrated Lorentz forces
			virtual arma::Col<fltp>::fixed<3> calc_lorentz_force() const;

			// calculate bending radius on mesh
			arma::Mat<fltp> calc_curvature() const;

			// material properties
			fltp calc_mass()const; // output in [kg]
			fltp calc_thermal_energy(
				const fltp temperature, 
				const fltp delta_temperature) const; // output in [J]
			fltp calc_temperature_increase(
				const fltp initial_temperature, 
				const fltp added_heat,
				const fltp temperature_stepsize = RAT_CONST(1.0)) const;

			// display function for multiple meshes
			static void display(
				const cmn::ShLogPr &lg, 
				const std::list<ShMeshDataPr> &meshes);	
	};

}}

#endif
