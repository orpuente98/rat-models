// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_EQUATION_HH
#define MDL_PATH_EQUATION_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>

#include "rat/common/error.hh"
#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathEquation> ShPathEquationPr;

	// define the coordinate function
	typedef std::function<void(arma::Mat<fltp> &R,arma::Mat<fltp> &L, 
		arma::Mat<fltp> &D, const arma::Row<fltp>&)> CoordFun;

	// cross section of coil
	class PathEquation: public Path, public Transformations{
		// properties
		private:
			// number of steps used for evaluating length
			arma::uword num_steps_ini_ = 100; // number of points initially
			arma::uword max_iter_ = 1000;  // maximum number of iterations
			fltp ell_tol_ = 1e-4; // tolerance for determining the length [m]

			// geometry
			fltp element_size_ = 0; // size of the elements [m]

			// coordinate function definition R(t) 
			// where t runs from 0 to 1
			CoordFun coord_fun_ = NULL;

			// number of sections
			arma::uword num_sections_ = 1;

			// number of elements must be divisible by this number
			arma::uword element_divisor_ = 1;


		// methods
		public:
			// constructor
			PathEquation();
			PathEquation(const fltp element_size, CoordFun fun);

			// factory methods
			static ShPathEquationPr create();
			static ShPathEquationPr create(const fltp element_size, CoordFun fun);

			// set properties
			void set_num_steps_ini(const arma::uword num_steps_ini);
			void set_element_size(const fltp element_size);
			void set_coord_fun(CoordFun fun);
			void set_num_sections(const arma::uword num_sections);
			void set_ell_tol(const fltp ell_tol);
			
			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// function for calculating path length
			fltp calc_length() const;

			// user coordinate function checking
			void check_user_fun_output(const arma::Mat<fltp> &R, const arma::Mat<fltp> &L, const arma::Mat<fltp> &D, const arma::Row<fltp> &t) const;
	};

}}

#endif