// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CCT_CUSTOM_HH
#define MDL_PATH_CCT_CUSTOM_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>
#include <functional>

#include "path.hh"
#include "transformations.hh"
#include "frame.hh"
#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonic> ShCCTHarmonicPr;
	typedef arma::field<ShCCTHarmonicPr> ShCCTHarmonicPrList;

	// cross section of coil
	class CCTHarmonic: public cmn::Node{
		// properties
		protected:
			// normalize length from 0 to 1 for harmonic calculatoin
			bool normalize_length_ = true;

			// which harmonic
			arma::uword num_poles_ = 0;
			bool is_skew_ = false;
			
		public:
			// virtual destructor (obligatory)
			virtual ~CCTHarmonic(){};

			// set functions
			void set_num_poles(const arma::uword num_poles);
			void set_is_skew(const bool is_skew = true);

			// get functions
			arma::uword get_num_poles() const;
			bool get_is_skew() const;

			// set normalization of the length coordinates
			void set_normalize_length(const bool normalize_length = true);
			bool get_normalize_length() const;

			// calculate position from theta
			arma::Row<fltp> calc_position(
				const arma::Row<fltp> &theta, 
				const fltp thetamin, 
				const fltp thetamax) const;

			// geometry functions
			virtual arma::field<arma::Row<fltp> > calc_z(const arma::Row<fltp> &theta, const arma::Row<fltp> &rho, const fltp thetamin, const fltp thetamax) const = 0;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};
		
	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicInterp> ShCCTHarmonicInterpPr;

	// harmonics by interpolation
	class CCTHarmonicInterp: public CCTHarmonic{
		protected:
			// interpolation arrays
			arma::Row<fltp> turn_{RAT_CONST(-1e5),RAT_CONST(1e5)};
			arma::Row<fltp> a_{RAT_CONST(1.0),RAT_CONST(1.0)};

		public:
			// constructor
			CCTHarmonicInterp();
			CCTHarmonicInterp(const arma::uword num_poles, const bool is_skew, const arma::Row<fltp> &turn, const arma::Row<fltp> &a, const bool normalize_length = true);
			
			// factory
			static ShCCTHarmonicInterpPr create();
			static ShCCTHarmonicInterpPr create(const arma::uword num_poles, const bool is_skew, const arma::Row<fltp> &turn, const arma::Row<fltp> &a, const bool normalize_length = true);

			// geometry functions
			arma::field<arma::Row<fltp> > calc_z(const arma::Row<fltp> &theta, const arma::Row<fltp> &rho, const fltp thetamin, const fltp thetamax) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicEquation> ShCCTHarmonicEquationPr;

	// define the coordinate function
	typedef std::function<arma::Row<fltp>(const arma::Row<fltp> &turn)> CCTCustomFun;


	// harmonics by interpolation
	class CCTHarmonicEquation: public CCTHarmonic{
		protected:
			// interpolation arrays
			CCTCustomFun fun_;

		public:
			// constructor
			CCTHarmonicEquation(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun);
			
			// factory
			static ShCCTHarmonicEquationPr create(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun);

			// geometry functions
			arma::field<arma::Row<fltp> > calc_z(const arma::Row<fltp> &theta, const arma::Row<fltp> &rho, const fltp thetamin, const fltp thetamax) const override;
	};

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicDrive> ShCCTHarmonicDrivePr;

	// harmonics by interpolation
	class CCTHarmonicDrive: public CCTHarmonic{
		protected:
			// drive
			ShDrivePr drive_ = DriveDC::create(RAT_CONST(1.0));

		public:
			// constructor
			CCTHarmonicDrive();
			CCTHarmonicDrive(const arma::uword num_poles, const bool is_skew, const ShDrivePr &drive = DriveDC::create(RAT_CONST(0.0)));
			CCTHarmonicDrive(const arma::uword num_poles, const bool is_skew, const fltp amplitude);
			
			// factory
			static ShCCTHarmonicDrivePr create();
			static ShCCTHarmonicDrivePr create(const arma::uword num_poles, const bool is_skew, const ShDrivePr &drive = DriveDC::create(RAT_CONST(0.0)));
			static ShCCTHarmonicDrivePr create(const arma::uword num_poles, const bool is_skew, const fltp amplitude);

			// set drive
			void set_drive(const ShDrivePr &drive);
			ShDrivePr get_drive() const;

			// geometry functions
			arma::field<arma::Row<fltp> > calc_z(const arma::Row<fltp> &theta, const arma::Row<fltp> &rho, const fltp thetamin, const fltp thetamax) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};


	// shared pointer definition
	typedef std::shared_ptr<class PathCCTCustom> ShPathCCTCustomPr;

	// cross section of coil
	class PathCCTCustom: public Path, public Transformations{
		// properties
		protected:
			

			// spacing 
			bool use_omega_normal_ = false;

			// reverse skew direction
			bool is_reverse_ = false;

			// number of turns
			fltp nt1_ = -RAT_CONST(10.0);
			fltp nt2_ = RAT_CONST(10.0);
				
			// normalize length from 0 to 1 
			// for pitch and radius
			bool normalize_length_ = true;
			fltp nt1nrm_ = -RAT_CONST(10.0);
			fltp nt2nrm_ = RAT_CONST(10.0);

			// discretization
			arma::uword num_nodes_per_turn_ = 120;

			// apply bending
			fltp bending_arc_length_ = RAT_CONST(0.0);

			// stored harmonics
			std::map<arma::uword,ShCCTHarmonicPr> harmonics_;
		
			// drives for pitch and radius
			ShDrivePr omega_ = DriveDC::create(RAT_CONST(6e-3));
			ShDrivePr rho_ = DriveDC::create(RAT_CONST(40e-3));

			// num layers
			arma::uword num_layers_ = 1;
			fltp rho_increment_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			PathCCTCustom();

			// factory methods
			static ShPathCCTCustomPr create();

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// set normalization
			void set_normalize_length(const bool normalize_length = true);
			bool get_normalize_length() const;

			// set functions
			void set_is_reverse(const bool is_reverse);
			void set_bending_arc_length(const fltp bending_arc_length);
			void set_omega(const ShDrivePr &omega);
			void set_omega(const fltp omega);
			void set_rho(const ShDrivePr &rho);
			void set_rho(const fltp rho);
			arma::uword add_harmonic(const ShCCTHarmonicPr &harm);
			bool delete_harmonic(const arma::uword index);
			arma::uword num_harmonics()const;
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_range(const fltp nt1, const fltp nt2);
			void set_range(const fltp nt1, const fltp nt2, const fltp nt1nrm, const fltp nt2nrm);
			void set_nt1(const fltp nt1);
			void set_nt2(const fltp nt2);
			void set_num_layers(const arma::uword num_layers);
			void set_rho_increment(const fltp rho_increment);
			void set_use_omega_normal(const bool use_omega_normal = true);
			void set_nt1nrm(const fltp nt1nrm);
			void set_nt2nrm(const fltp nt2nrm);

			// get functions
			bool get_is_reverse() const;
			ShCCTHarmonicPr get_harmonic(const arma::uword index) const;
			fltp get_bending_arc_length() const;
			ShDrivePr get_omega()const;
			ShDrivePr get_rho()const;
			arma::uword get_num_nodes_per_turn() const;
			fltp get_nt1() const;
			fltp get_nt2() const;
			arma::uword get_num_layers() const;
			fltp get_rho_increment()const;
			bool get_use_omega_normal() const;
			fltp get_nt1nrm() const;
			fltp get_nt2nrm() const;
			
			// interpolation function
			static arma::Mat<rat::fltp> interp_matrix(
				const arma::Col<fltp> &X, 
				const arma::Mat<fltp> &M, 
				const arma::Col<fltp> &Xi);

			// calculate position from theta
			arma::Row<fltp> calc_position(
				const arma::Row<fltp> &theta, 
				const fltp thetamin, 
				const fltp thetamax) const;
			
			// tree structure
			void reindex() override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
