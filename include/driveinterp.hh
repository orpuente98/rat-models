// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_INTERP_HH
#define MDL_DRIVE_INTERP_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveInterp> ShDriveInterpPr;
	typedef arma::field<ShDriveInterpPr> ShDriveInterpPrList;

	// circuit class
	class DriveInterp: public Drive{
		// properties
		protected:
			// interpolation table
			arma::Col<fltp> ti_;
			arma::Col<fltp> vi_;

		// methods
		public:
			// constructor
			DriveInterp();
			DriveInterp(
				const arma::Col<fltp> &ti, 
				const arma::Col<fltp> &vi);

			// factory
			static ShDriveInterpPr create();
			static ShDriveInterpPr create(
				const arma::Col<fltp> &ti, 
				const arma::Col<fltp> &vi);

			// set and get current
			void set_interpolation_table(
				const arma::Col<fltp> &ti, 
				const arma::Col<fltp> &vi);
			void set_interpolation_times(
				const arma::Col<fltp> &ti);
			void set_interpolation_values(
				const arma::Col<fltp> &vi);
			arma::uword get_num_times()const;

			// get interpolation
			arma::Col<fltp> get_interpolation_times()const;
			arma::Col<fltp> get_interpolation_values()const;

			// get data
			fltp get_scaling(const fltp time) const override;
			// arma::Col<fltp> get_scaling(const arma::Col<fltp> &times) const;
			fltp get_dscaling(const fltp time) const override;
				
			// get inflection points
			arma::Col<fltp> get_inflection_times() const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
