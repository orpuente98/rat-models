// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_FLARED_HH
#define MDL_MODEL_FLARED_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"

#include "modelcoilwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelFlared> ShModelFlaredPr;

	// cross section of coil
	class ModelFlared: public ModelCoilWrapper{
		// properties
		protected:
			// coil geometry settings
			fltp ell1_ = RAT_CONST(0.1); // straight section length [m]
			fltp ell2_ = RAT_CONST(0.03); // flared section length [m]

			// radii
			fltp radius1_ = RAT_CONST(0.2); // hardway bending radius [m]
			fltp radius2_ = RAT_CONST(0.03); // softway bending radius [m]

			// length of the arc
			fltp arcl_ = -arma::Datum<fltp>::pi/6; // hardway bending angle [rad]

			// coil pack dimensions
			fltp coil_thickness_ = RAT_CONST(0.02);
			fltp coil_width_ = RAT_CONST(0.012);

			// discretization
			fltp element_size_ = RAT_CONST(2e-3);

		// methods
		public:
			// constructor
			ModelFlared();
			ModelFlared(
				const fltp ell1, 
				const fltp ell2, 
				const fltp arcl, 
				const fltp radius1, 
				const fltp radius2, 
				const fltp coil_width, 
				const fltp coil_thickness, 
				const fltp element_size);

			// factory
			static ShModelFlaredPr create();
			static ShModelFlaredPr create(
				const fltp ell1, 
				const fltp ell2, 
				const fltp arcl, 
				const fltp radius1, 
				const fltp radius2, 
				const fltp coil_width, 
				const fltp coil_thickness, 
				const fltp element_size);

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// set properties
			void set_arcl(const fltp arcl);
			void set_ell1(const fltp ell1);
			void set_ell2(const fltp ell2);
			void set_radius1(const fltp radius1);
			void set_radius2(const fltp radius2);
			void set_coil_thickness(const fltp coil_thickness);
			void set_coil_width(const fltp coil_width);
			void set_element_size(const fltp element_size);

			// get properties
			fltp get_ell1() const;
			fltp get_ell2() const;
			fltp get_arcl() const;
			fltp get_radius1() const;
			fltp get_radius2() const;
			fltp get_coil_thickness()const;
			fltp get_coil_width()const;
			fltp get_element_size()const;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
