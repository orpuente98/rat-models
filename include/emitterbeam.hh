// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EMITTER_BEAM_HH
#define MDL_EMITTER_BEAM_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"
#include "particle.hh"
#include "emitter.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EmitterBeam> ShEmitterBeamPr;

	// emitter for accelerator beam
	class EmitterBeam: public Emitter{
		protected:
			// seed for random distribution
			arma::uword seed_ = 1001;

			// following unit system from CERN MAD
			// particle settings
			fltp beam_energy_ = 0; // [GeV] including rest mass
			fltp rest_mass_ = 0; // [GeV/C^2]
			fltp charge_ = 0; // elementary charge

			// number of particles spawned
			arma::uword num_particles_ = 1000;

			// emitter location
			arma::Col<fltp>::fixed<3> R_ = {0,0,0};
			
			// emitter orientation
			arma::Col<fltp>::fixed<3> L_ = {0,1,0};
			arma::Col<fltp>::fixed<3> N_ = {1,0,0};
			arma::Col<fltp>::fixed<3> D_ = {0,0,1};

			// emitter properties
			fltp sigma_ = 1e-2;

			// normal xx' relationship
			fltp corr_xx_ = 0.9;
			fltp sigma_x_ = 0.004;
			fltp sigma_xa_ = 0.5*2*arma::datum::pi/360;
			
			// transverse yy' relationship
			fltp corr_yy_ = 0.9;
			fltp sigma_y_ = 0.004;
			fltp sigma_ya_ = 0.5*2*arma::datum::pi/360;

			// track settings
			arma::uword lifetime_ = 501;
			arma::uword start_idx_ = 250;


		public:
			// constructor
			EmitterBeam();
				
			// factory
			static ShEmitterBeamPr create();

			// default particles
			void set_proton();
			void set_electron();

			// setting of properties
			void set_beam_energy(const fltp beam_energy);
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			void set_lifetime(const arma::uword lifetime);
			void set_start_idx(const arma::uword start_idx);
			void set_num_particles(const arma::uword num_particles);
			void set_spawn_coord(
				const arma::Col<fltp>::fixed<3> &R, 
				const arma::Col<fltp>::fixed<3> &L, 
				const arma::Col<fltp>::fixed<3> &N, 
				const arma::Col<fltp>::fixed<3> &D);
			void set_coord(
				const arma::Col<fltp>::fixed<3> &R);
			void set_longitudinal(
				const arma::Col<fltp>::fixed<3> &L);
			void set_normal(
				const arma::Col<fltp>::fixed<3> &N);
			void set_transverse(
				const arma::Col<fltp>::fixed<3> &D);

			// getters
			arma::uword get_num_particles() const;
			fltp get_rest_mass() const;
			fltp get_charge()const;
			fltp get_beam_energy()const;
			arma::uword get_lifetime()const;

			// set relation ship for normal direction
			void set_xx(const fltp corr_xx, const fltp sigma_x, const fltp sigma_xa);
			void set_yy(const fltp corr_yy, const fltp sigma_y, const fltp sigma_ya);

			// normal distribution but correlated
			static arma::Mat<fltp> correlated_normdist(
				const fltp correlation, 
				const arma::uword num_cols);

			// particle
			arma::field<Particle> spawn_particles() const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif