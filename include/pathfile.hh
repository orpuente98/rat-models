// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_FILE_HH
#define MDL_PATH_FILE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>

#include "rat/common/error.hh"

#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathFile> ShPathFilePr;

	// cross section of coil
	class PathFile: public Path, public Transformations{
		// properties
		private:
			// geometry
			boost::filesystem::path file_name_ = "";

		// methods
		public:
			// constructor
			PathFile();
			explicit PathFile(const boost::filesystem::path &file_name);

			// factory methods
			static ShPathFilePr create();
			static ShPathFilePr create(const boost::filesystem::path &file_name);

			// set properties
			void set_file_name(const boost::filesystem::path &file_name);
			
			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;
	};

}}

#endif