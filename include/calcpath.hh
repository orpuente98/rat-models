// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_PATH_HH
#define MDL_CALC_PATH_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calcleaf.hh"
#include "linedata.hh"
#include "inputpath.hh"
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcPath> ShCalcPathPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcPath: public CalcLeaf, public InputPath{
		// properties
		protected:
			// enable visibility
			bool visibility_ = true;

		// methods
		public:
			// constructor
			CalcPath();
			explicit CalcPath(
				const ShModelPr &model, 
				const ShPathPr &input_path = PathAxis::create());
			
			// factory methods
			static ShCalcPathPr create();
			static ShCalcPathPr create(
				const ShModelPr &model, 
				const ShPathPr &input_path = PathAxis::create());

			// enable mesh
			bool get_visibility() const;
			void set_visibility(const bool visibility);

			// calculate with inductance data output
			ShLineDataPr calculate_path(
				const fltp time, const cmn::ShLogPr &lg);

			// generalized calculation
			virtual std::list<ShDataPr> calculate(
				const fltp time, const cmn::ShLogPr &lg) override;

			// creation of calculation data objects
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
