// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CLOVER_HH
#define MDL_PATH_CLOVER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "pathbezier.hh"
#include "patharc.hh"
#include "pathstraight.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathClover> ShPathCloverPr;

	// cross section of coil
	class PathClover: public Path, public Transformations{
		// properties
		protected:
			// transition length
			fltp ell_trans_ = 0;

			// thickness of the coil pack
			fltp dpack_ = 0;

			// height of the bridge
			fltp height_ = 0;

			// strengths for bezier curves
			fltp str12_ = 0;
			fltp str34_ = 0;

			// element sizes
			fltp element_size_ = 0;

			// straight section lengths
			fltp ellstr1_ = 0;
			fltp ellstr2_ = 0;

			// bend for small synchrotrons
			// this is different from bending 
			// transformation as it only bends 
			// the straight section. When
			// set to zero the feature is 
			// disabled.
			fltp bending_radius_ = 0;

			// angle of the bridge section [rad]
			fltp bridge_angle_ = 0;

			// coil planar winding feature
			bool planar_winding_ = true;

		// methods
		public:
			// constructor
			PathClover();
			PathClover(
				const fltp ellstr1, 
				const fltp ellstr2, 
				const fltp height, 
				const fltp dpack, 
				const fltp ell_trans, 
				const fltp str12, 
				const fltp str34, 
				const fltp element_size);

			// factory
			static ShPathCloverPr create();
			static ShPathCloverPr create(
				const fltp ellstr1, 
				const fltp ellstr2, 
				const fltp height, 
				const fltp dpack, 
				const fltp ell_trans, 
				const fltp str12, 
				const fltp str34, 
				const fltp element_size);

			// set properties
			void set_ellstr1(const fltp ellstr1);
			void set_ellstr2(const fltp ellstr2);
			void set_ell_trans(const fltp ell_trans);
			void set_dpack(const fltp dpack);
			void set_height(const fltp height);
			void set_str12(const fltp str12);
			void set_str34(const fltp str34);
			void set_element_size(const fltp element_size);
			void set_bending_radius(const fltp bending_radius);
			void set_bridge_angle(const fltp bridge_angle);
			void set_planar_winding(const bool planar_winding);

			// get properties
			bool get_planar_winding() const;
			fltp get_ellstr1() const;
			fltp get_ellstr2() const;
			fltp get_height() const;
			fltp get_dpack() const;
			fltp get_ell_trans() const;
			fltp get_str12() const;
			fltp get_str34() const;
			fltp get_element_size() const;
			fltp get_bending_radius() const;
			fltp get_bridge_angle() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
