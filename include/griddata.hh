// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_GRID_DATA_HH
#define MDL_GRID_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"
#include "rat/common/error.hh"

#include "vtkimg.hh"
#include "meshdata.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class GridData> ShGridDataPr;

	// template for coil
	class GridData: public TargetData{		
		// properties
		protected:
			// grid extends
			fltp x1_ = 0; fltp x2_ = 0; 
			fltp y1_ = 0; fltp y2_ = 0; 
			fltp z1_ = 0; fltp z2_ = 0;

			// number of points
			arma::uword num_x_ = 0;
			arma::uword num_y_ = 0;
			arma::uword num_z_ = 0;
			
		// methods
		public:
			// constructor
			GridData();
			GridData(
				const fltp x1, const fltp x2, const arma::uword num_x, 
				const fltp y1, const fltp y2, const arma::uword num_y, 
				const fltp z1, const fltp z2, const arma::uword num_z);
			GridData(
				const std::list<ShMeshDataPr> &meshes, 
				const fltp scale_factor, 
				const fltp delem, 
				const arma::uword lim_num_elements = 5e6);

			// factory methods
			static ShGridDataPr create();
			static ShGridDataPr create(
				const fltp x1, const fltp x2, const arma::uword num_x, 
				const fltp y1, const fltp y2, const arma::uword num_y, 
				const fltp z1, const fltp z2, const arma::uword num_z);
			static ShGridDataPr create(
				const std::list<ShMeshDataPr> &meshes, 
				const fltp scale_factor, 
				const fltp delem, 
				const arma::uword lim_num_elements = 5e6);

			// set properties
			void set_dim_x(const fltp x1, const fltp x2, const arma::uword num_x);
			void set_dim_y(const fltp y1, const fltp y2, const arma::uword num_y);
			void set_dim_z(const fltp z1, const fltp z2, const arma::uword num_z);

			// get grid data
			arma::uword get_num_x() const;
			arma::uword get_num_y() const;
			arma::uword get_num_z() const;

			// get grid data
			fltp get_x1() const;
			fltp get_x2() const;
			fltp get_y1() const;
			fltp get_y2() const;
			fltp get_z1() const;
			fltp get_z2() const;
			
			// calculation
			void setup_targets() override;

			// interpolate to grid
			arma::Mat<fltp> interpolate(
				const char field_type, 
				const arma::Mat<fltp> &R, 
				const bool use_parallel = true) const override;

			// extract iso surface
			ShMeshDataPr extract_isosurface(
				const char field_type, 
				const char component, 
				const fltp iso_value, 
				const bool use_parallel = true) const;
				
			// export vtk function
			ShVTKObjPr export_vtk() const override;

			// write surface to VTK file
			// ShVTKObjPr export_flux_surface_vtk() const;

			// try calculate flux surfaces
			// arma::Row<fltp> calc_flux_surfaces() const;

			// display function
			void display(const cmn::ShLogPr &lg);

			// VTK export
			// ShVTKImgPr export_vtk(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			// void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// // serialization
			// static std::string get_type();
			// virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			// virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
