// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_BAR_HH
#define MDL_CALC_BAR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// common header files
#include "rat/common/elements.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm header files
#include "rat/mlfmm/boundmesh.hh"

//#include "coilsurface.hh"
#include "meshdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class BarData> ShBarDataPr;
	typedef arma::field<ShBarDataPr> ShBarDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class BarData: public MeshData, public fmm::BoundMesh{
		// properties
		private:
			// use line elements or volume elements
			//bool use_volume_elements_ = false;

			// number of gauss points to use for current sources
			fltp softening_ = 0.7;

			// magnetisation at the nodes
			arma::Col<fltp>::fixed<3> Mf_;
			
			// number of gauss points
			//arma::sword num_gauss_volume_ = 1;
			//arma::sword num_gauss_surface_ = 5;

		// methods
		public:
			// default constructor
			BarData();
			BarData(const ShFramePr &frame, const ShAreaPr &area);

			// factory
			static ShBarDataPr create();
			static ShBarDataPr create(const ShFramePr &frame, const ShAreaPr &area);

			// set magnetisation
			void set_magnetisation(const arma::Col<fltp>::fixed<3> &Mf);

			// set softening
			void set_softening(const fltp softening);

			// method for creating MLFMM sources
			void setup_sources() override;

			// calculate integrated Lorentz forces
			virtual arma::Col<fltp>::fixed<3> calc_lorentz_force() const override;
	};

}}

#endif
