// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_TRACKS_HH
#define MDL_CALC_TRACKS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calcleaf.hh"
#include "emitter.hh"
#include "model.hh"
#include "trackdata.hh"
#include "modelgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcTracks> ShCalcTracksPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcTracks: public CalcLeaf{
		// properties
		protected:
			// visibility (of tracking mesh)
			bool visibility_ = true;

			// tracking step size
			fltp stepsize_ = 5e-3; // [m]

			// particle emitters
			std::map<arma::uword, ShEmitterPr> emitters_;

			// tracking model
			ShModelPr tracking_model_;

		// methods
		public:
			// track calculations
			CalcTracks();
			CalcTracks(
				const ShModelPr &model);
			CalcTracks(
				const ShModelPr &model, 
				const ShModelPr &tracking_model, 
				const std::list<ShEmitterPr> &emitters);
			static ShCalcTracksPr create();
			static ShCalcTracksPr create(
				const ShModelPr &model);
			static ShCalcTracksPr create(
				const ShModelPr &model, 
				const ShModelPr &tracking_model, 
				const std::list<ShEmitterPr> &emitters);

			// enable mesh
			bool get_visibility() const;
			void set_visibility(const bool visibility = true);

			// add emitter
			arma::uword add_emitter(const ShEmitterPr &emitter);
			arma::Row<arma::uword> add_emitters(const std::list<ShEmitterPr> &emitters);
			ShEmitterPr get_emitter(const arma::uword index) const;
			std::list<ShEmitterPr> get_emitters() const;
			bool delete_emitter(const arma::uword index);
			void reindex() override;
			arma::uword num_emitters() const;

			// set model to define the shape of the tracking region
			void set_tracking_model(const ShModelPr &tracking_model);
			ShModelPr get_tracking_model()const;

			// setters
			void set_stepsize(const fltp step_size);
			fltp get_stepsize()const;

			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// calculate tracks
			ShTrackDataPr calculate_tracks(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create());

			// generalized calculation
			std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg) override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
