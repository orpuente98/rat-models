// Demo4 Tor0id Model
// Copyright (C) 2020  Jer0en van Nugteren*/

#ifndef MDL_PATH_PLASMA_HH
#define MDL_PATH_PLASMA_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathPlasma> ShPathPlasmaPr;

	// path based on the shape of a plasma in a spherical tokamak
	// thanks to Tokamak Energy for supplying the equation
	class PathPlasma: public mdl::Path, public mdl::Transformations{
		// pr0perties
		private:
			// velocity
			fltp velocity_ = 0;

			// D-shape parameters
			fltp r0_ = RAT_CONST(0.0); // [m]
			fltp a_ = RAT_CONST(0.0); // [m]
			fltp delta_ = RAT_CONST(0.0); // [m]
			fltp kappa_ = RAT_CONST(0.0); // [m]

			// localization
			fltp theta1_ = RAT_CONST(0.0); 
			fltp theta2_ = 2*arma::datum::pi;

			// element size
			fltp element_size_;
			// arma::uword element_divisor_ = 1;
			arma::uword num_sections_ = 1;

		// methods
		public:
			// constructor
			PathPlasma();
			PathPlasma(const arma::uword num_sections, const fltp a, const fltp r0, const fltp delta, const fltp kappa, const fltp element_size);

			// factory methods
			static ShPathPlasmaPr create();
			static ShPathPlasmaPr create(const arma::uword num_sections, const fltp a, const fltp r0, const fltp delta, const fltp kappa, const fltp element_size);

			// set plasma parameters
			void set_r0(const fltp r0);
			void set_a(const fltp a);
			void set_delta(const fltp delta);
			void set_kappa(const fltp kappa);
			void set_theta1(const fltp theta1);
			void set_theta2(const fltp theta2);
			void set_velocity(const fltp velocity);

			// set discretization parameters
			// void set_element_divisor(const arma::uword element_divisor);
			void set_element_size(const fltp element_size);
			void set_theta(const fltp theta1, const fltp theta2);
			void set_num_sections(const arma::uword num_sections);

			// get plasma parameters
			fltp get_r0() const;
			fltp get_a() const;
			fltp get_delta() const;
			fltp get_kappa() const;
			fltp get_theta1() const;
			fltp get_theta2() const;
			fltp get_velocity()const;

			// get discretization parameters
			fltp get_element_size() const;
			arma::uword get_num_sections() const;

			// calculate distance to origin 
			fltp calc_origin_distance() const;

			// set shape parameters
			void set_r0tf(const fltp r0tf);

			// get frame
			mdl::ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif