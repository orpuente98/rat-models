// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_VTK_IMG_HH
#define MDL_VTK_IMG_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// magrat headers
#include "rat/common/error.hh"
#include "rat/common/log.hh"

#include "vtkobj.hh"

// vtk headers
#include <vtkSmartPointer.h>
#include <vtkRectilinearGrid.h>
#include <vtkDoubleArray.h>
#include <vtkXMLRectilinearGridWriter.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkPointData.h>
#include <vtkImageData.h>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKImg> ShVTKImgPr;

	// VTK Unstructured grid
	class VTKImg: public VTKObj{
		protected:
			// pointer to dataset
			vtkSmartPointer<vtkImageData> vtk_igrid_;

		public:
			// constructor
			VTKImg();

			// factory
			static ShVTKImgPr create();

			// write mesh
			void set_dims(const fltp x1, const fltp x2, const arma::uword num_x, const fltp y1, const fltp y2, const arma::uword num_y, const fltp z1, const fltp z2, const arma::uword num_z);

			// write data at nodes
			void set_nodedata(const arma::Mat<fltp> &v, const std::string &name);

			// get filename extension
			std::string get_filename_ext() const override;

			// write output file
			void write(const boost::filesystem::path fname, cmn::ShLogPr lg = cmn::NullLog::create()) override;
	};

}}

#endif
