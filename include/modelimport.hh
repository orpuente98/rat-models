// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_IMPORT_HH
#define MDL_MODEL_IMPORT_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"

#include "model.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelImport> ShModelImportPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelImport: public Model{
		// properties
		protected:
			// file
			boost::filesystem::path fpath_;

			// position
			arma::Col<fltp>::fixed<3> R0_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// scaling factor
			fltp scaling_factor_ = RAT_CONST(1.0);

			// temperature of the coil
			fltp operating_temperature_ = RAT_CONST(4.5);

			// current drive
			ShDrivePr temperature_drive_;

		// methods
		public:
			// constructor
			ModelImport();

			// factory methods
			static ShModelImportPr create();

			// set operating conditions
			void set_operating_temperature(const fltp operating_temperature);
			fltp get_operating_temperature() const;

			// set drive for temperature
			void set_temperature_drive(const ShDrivePr &temperature_drive);

			// set file path
			void set_fpath(const boost::filesystem::path &fpath);
			boost::filesystem::path get_fpath() const;

			// set scaling
			void set_scaling_factor(const rat::fltp scaling_factor);
			fltp get_scaling_factor()const;

			// set position
			void set_position(const arma::Col<fltp>::fixed<3> &R0);
			arma::Col<fltp>::fixed<3> get_position() const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
