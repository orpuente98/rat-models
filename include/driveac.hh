// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_AC_HH
#define MDL_DRIVE_AC_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveAC> ShDriveACPr;
	typedef arma::field<ShDriveACPr> ShDriveACPrList;

	// circuit class
	class DriveAC: public Drive{
		// properties
		protected:
			// amplitude
			fltp offset_ = RAT_CONST(0.0);

			// scaling amplitude
			fltp amplitude_ = RAT_CONST(1.0);

			// frequency
			fltp frequency_ = RAT_CONST(0.1);

			// phase
			fltp phase_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			DriveAC();
			DriveAC(
				const fltp amplitude, 
				const fltp frequency, 
				const fltp phase, 
				const fltp offset);

			// factory
			static ShDriveACPr create();
			static ShDriveACPr create(
				const fltp amplitude, 
				const fltp frequency, 
				const fltp phase = RAT_CONST(0.0), 
				const fltp offset = RAT_CONST(0.0));

			// set properties
			void set_amplitude(const fltp amplitude);
			void set_frequency(const fltp frequency);
			void set_phase(const fltp phase);
			void set_offset(const fltp offset);

			// get properties
			fltp get_frequency() const;
			fltp get_phase() const;
			fltp get_amplitude() const;
			fltp get_offset() const;

			// get scaling as function of time
			fltp get_scaling(const fltp time) const override;
			fltp get_dscaling(const fltp time) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
