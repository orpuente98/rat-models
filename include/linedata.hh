// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_LINE_DATA_HH
#define MDL_LINE_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/error.hh"
#include "rat/common/log.hh"

#include "rat/mlfmm/mgntargets.hh"

#include "model.hh"
#include "frame.hh"
#include "vtkobj.hh"
#include "vtktable.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class LineData> ShLineDataPr;

	// template for coil
	class LineData: public TargetData{		
		// properties
		protected:
			// line definition
			ShFramePr frame_ = NULL;
			
			// coordinates
			//arma::Mat<fltp> R_;
			arma::Mat<fltp> L_;
			arma::Mat<fltp> D_;
			arma::Mat<fltp> N_;

			// position along line
			arma::Row<fltp> ell_;
		
		// methods
		public:
			// constructor
			LineData();
			explicit LineData(const ShFramePr &frame);

			// factory methods
			static ShLineDataPr create();
			static ShLineDataPr create(const ShFramePr &frame);

			// setting of the path
			void set_frame(const ShFramePr &frame);

			// calculation
			void setup_targets() override;
			
			// orientation vectors
			arma::Row<fltp> get_ell()const;
			arma::Mat<fltp> get_direction() const;
			arma::Mat<fltp> get_normal() const;
			arma::Mat<fltp> get_transverse() const;

			// export as unstructured
			ShVTKObjPr export_vtk() const override;

			// export as table
			ShVTKTablePr export_vtk_table() const;

			// void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// // serialization
			// static std::string get_type();
			// virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			// virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};
}}

#endif
