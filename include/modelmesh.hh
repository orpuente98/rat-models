// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_MESH_HH
#define MDL_MODEL_MESH_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"

#include "model.hh"
#include "path.hh"
#include "cross.hh"
#include "meshdata.hh"
#include "area.hh"
#include "nameable.hh"
#include "drive.hh"
#include "inputpath.hh"
#include "inputcross.hh"
#include "rat/mat/inputconductor.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelMesh> ShModelMeshPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelMesh: public Model, public InputPath, public InputCross, public mat::InputConductor{
		// properties
		protected:
			// circuit index
			arma::uword circuit_index_ = 0;

			// start and end connection
			bool connect_start_ = true;
			bool connect_end_ = true;

			// temperature of the coil
			fltp operating_temperature_ = RAT_CONST(4.5);

			// current drive
			ShDrivePr temperature_drive_;

			// current sharing feature
			// when enabled the current can flow freely over
			// the cross section of the conductor (use for cables)
			bool enable_current_sharing_ = false;

		// methods
		public:
			// constructor
			ModelMesh();
			ModelMesh(
				const ShPathPr &input_path, 
				const ShCrossPr &input_cross);

			// factory methods
			static ShModelMeshPr create();
			static ShModelMeshPr create(
				const ShPathPr &input_path, 
				const ShCrossPr &input_cross);

			// set circuit index
			void set_circuit_index(const arma::uword circuit_index);
			void set_connect_start(const bool connect_start = true);
			void set_connect_end(const bool connect_start = true);
			void set_enable_current_sharing(const bool enable_current_sharing);

			// get circuit index
			arma::uword get_circuit_index()const;
			bool get_connect_start()const;
			bool get_connect_end()const;
			bool get_enable_current_sharing()const;
			
			// set operating conditions
			void set_operating_temperature(
				const fltp operating_temperature);
			
			// get operating temperature
			fltp get_operating_temperature() const;

			// set the drive for the temperature
			void set_temperature_drive(
				const ShDrivePr &temperature_drive);

			// create data
			virtual ShMeshDataPr create_data()const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
