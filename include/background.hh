// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_BACKGROUND_HH
#define MDL_BACKGROUND_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/mlfmm/background.hh"

#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Background> ShBackgroundPr;
	typedef arma::field<ShBackgroundPr> ShBackgroundPrList;

	// area carries the mesh of the 
	// cross-sectional area of the coil
	class Background: virtual public cmn::Node{
		// properties
		protected:
			// drive for each direction
			arma::field<ShDrivePr> bg_drive_;

		// methods
		public:
			// default constructor
			Background();
			Background(
				const ShDrivePr &drivex, 
				const ShDrivePr &drivey, 
				const ShDrivePr &drivez);
			Background(
				const arma::Col<fltp>::fixed<3> &bg_field);

			// factory
			static ShBackgroundPr create();
			static ShBackgroundPr create(
				const ShDrivePr &drivex, 
				const ShDrivePr &drivey, 
				const ShDrivePr &drivez);
			static ShBackgroundPr create(
				const arma::Col<fltp>::fixed<3> &bg_field);

			// set drives
			void set_bg_drive(
				const ShDrivePr &drivex, 
				const ShDrivePr &drivey, 
				const ShDrivePr &drivez);
			void set_bg_drive(
				const arma::field<ShDrivePr> &drive);
			void set_bg_drive(
				const char dir, 
				const rat::mdl::ShDrivePr &drive);

			// set dc drive with constant field
			void set_bg_field(
				const char dir, 
				const rat::fltp strength);

			// get background field in various forms
			arma::Col<fltp>::fixed<3> get_background_field(
				const fltp time = 0)const;
			arma::Col<fltp>::fixed<3> get_dbackground_field(
				const fltp time = 0)const;
			
			fltp get_background_field(
				const char dir, 
				const fltp time = 0)const;
			fltp get_dbackground_field(
				const char dir, 
				const fltp time = 0)const;

			fmm::ShBackgroundPr create_fmm_background(
				const fltp time = 0)const;
			fmm::ShBackgroundPr create_fmm_dbackground(
				const fltp time = 0)const;

			// get drives
			ShDrivePr get_bg_drive(const char dir)const;
			arma::field<ShDrivePr> get_bg_drive()const;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
