// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_ROOT_HH
#define MDL_MODEL_ROOT_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/node.hh"

#include "modelgroup.hh"
#include "calcgroup.hh"
#include "model.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelRoot> ShModelRootPr;

	// template for coil
	class ModelRoot: public Model{
		// properties
		protected:
			// model tree
			ShModelGroupPr model_tree_;

			// calculation tree
			ShCalcGroupPr calc_tree_;

		// methods
		public:
			// constructor
			ModelRoot();
			ModelRoot(const ShModelGroupPr &model_tree, const ShCalcGroupPr &calc_tree);

			// factory
			static ShModelRootPr create();
			static ShModelRootPr create(const ShModelGroupPr &model_tree, const ShCalcGroupPr &calc_tree);

			// set/get calculation tree
			void set_calc_tree(const ShCalcGroupPr &calc_tree);
			ShCalcGroupPr get_calc_tree() const;

			// set/get model tree
			void set_model_tree(const ShModelGroupPr &model_tree);
			ShModelGroupPr get_model_tree() const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(const std::list<arma::uword> &trace = {}, const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
