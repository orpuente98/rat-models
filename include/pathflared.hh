// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_FLARED_HH
#define MDL_PATH_FLARED_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "patharc.hh"
#include "pathstraight.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathFlared> ShPathFlaredPr;

	// cross section of coil
	class PathFlared: public Path, public Transformations{
		// properties
		protected:
			// coil geometry settings
			fltp ell1_ = 0; // straight section length [m]
			fltp ell2_ = 0; // flared section length [m]

			// length of the arc
			fltp arcl_ = 0; // hardway bending angle [rad]
			
			// radii
			fltp radius1_ = 0; // hardway bending radius [m]
			fltp radius2_ = 0; // softway bending radius [m]
			
			// element size
			fltp element_size_ = 0; // size of the elements [m]

			// radial offset
			fltp offset_ = 0; // softway bend offset [m]
			fltp offset_arc_ = 0; // hardway bend offset [m]

		// methods
		public:
			// constructor
			PathFlared();
			PathFlared(const fltp ell1, const fltp ell2, const fltp arcl, const fltp radius1, const fltp radius2, const fltp element_size, const fltp offset = 0, const fltp offset_arc = 0);

			// factory
			static ShPathFlaredPr create();
			static ShPathFlaredPr create(const fltp ell1, const fltp ell2, const fltp arcl, const fltp radius1, const fltp radius2, const fltp element_size, const fltp offset = 0, const fltp offset_arc = 0);

			// set properties
			void set_element_size(const fltp element_size);
			void set_offset(const fltp offset);
			void set_offset_arc(const fltp offset_arc);
			void set_arcl(const fltp arcl);
			void set_ell1(const fltp ell1);
			void set_ell2(const fltp ell2);
			void set_radius1(const fltp radius1);
			void set_radius2(const fltp radius2);

			// get properties
			fltp get_element_size() const;
			fltp get_offset() const;
			fltp get_offset_arc() const;
			fltp get_ell1() const;
			fltp get_ell2() const;
			fltp get_arcl() const;
			fltp get_radius1() const;
			fltp get_radius2() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
