// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_PTRAIN_HH
#define MDL_DRIVE_PTRAIN_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DrivePTrain> ShDrivePTrainPr;
	typedef arma::field<ShDrivePTrainPr> ShDrivePTrainPrList;

	// circuit class
	class DrivePTrain: public Drive{
		// properties
		protected:
			// amplitude
			fltp amplitude_ = 1.0;

			// scaling amplitude
			fltp tstart_ = 1.0; // [s]

			// duration of the pulses
			fltp tpulse_ = 0.1; // [s]

			// time between the pulses
			fltp tdelay_ = 100.0; // [s]

			// increment
			fltp increment_ = 0.5;

			// number of pulses
			arma::uword num_pulses_ = 10;


		// methods
		public:
			// constructor
			DrivePTrain();
			DrivePTrain(
				const arma::uword num_pulses, 
				const fltp amplitude, 
				const fltp increment, 
				const fltp tpulse, 
				const fltp tstart, 
				const fltp tdelay);

			// factory
			static ShDrivePTrainPr create();
			static ShDrivePTrainPr create(
				const arma::uword num_pulses, 
				const fltp amplitude, 
				const fltp increment, 
				const fltp tpulse, 
				const fltp tstart, 
				const fltp tdelay);

			// set and get definition
			void set_num_pulses(const arma::uword num_pulses);
			void set_amplitude(const fltp amplitude);
			void set_tstart(const fltp tstart);
			void set_tpulse(const fltp tpulse);
			void set_tdelay(const fltp tdelay);
			void set_increment(const fltp increment);
						
			// get scaling at given time
			fltp get_scaling(const fltp time) const override;
			fltp get_dscaling(const fltp time) const override;
			
			// get inflection points
			arma::Col<fltp> get_inflection_times() const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
