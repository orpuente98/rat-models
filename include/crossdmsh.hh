// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_DMSH_HH
#define MDL_CROSS_DMSH_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/dmsh/distmesh2d.hh"
#include "rat/dmsh/distfun.hh"
#include "rat/dmsh/dfones.hh"

#include "area.hh"
#include "perimeter.hh"
#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossDMsh> ShCrossDMshPr;

	// cross section of coil
	class CrossDMsh: public Cross{
		// properties
		private:
			// quad mesh
			bool quad_ = true;

			// element size
			fltp h0_;

			// mesh input
			dm::ShDistFunPr fd_;
			dm::ShDistFunPr fh_;

			// additional fixed points
			arma::Mat<fltp> pfix_ext_ = arma::Mat<fltp>(0,2);

		// methods
		public:
			// constructors
			CrossDMsh(){};
			CrossDMsh(
				const fltp h0, 
				const dm::ShDistFunPr &df, 
				const dm::ShDistFunPr &hf = dm::DFOnes::create());
			
			// factory methods
			static ShCrossDMshPr create(
				const fltp h0, 
				const dm::ShDistFunPr &df, 
				const dm::ShDistFunPr &hf = dm::DFOnes::create());
		
			// set properties
			void set_h0(const fltp h0);

			// get properties
			fltp get_h0()const;

			// set fixed points
			void set_fixed(const arma::Mat<fltp> &pfix_ext);

			// get bounding box
			arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
