// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_VTK_UNSTR_HH
#define MDL_VTK_UNSTR_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// VTK headers
#include <vtkUnstructuredGrid.h>
#include <vtkProbeFilter.h>
#include <vtkSmartPointer.h>

// magrat headers
#include "rat/common/error.hh"
#include "rat/common/log.hh"

// rat headers
#include "vtkobj.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKUnstr> ShVTKUnstrPr;

	// VTK Unstructured grid
	class VTKUnstr: public VTKObj{
		protected:
			// mesh data
			vtkSmartPointer<vtkPoints> nodes_;
			vtkSmartPointer<vtkCellArray> elements_;

			// node data
			std::list<vtkSmartPointer<vtkDoubleArray> > node_data_;
			std::list<vtkSmartPointer<vtkDoubleArray> > element_data_;

			// pointer to dataset
			vtkSmartPointer<vtkUnstructuredGrid> vtk_ugrid_;

			// probe for data interpolation
			vtkSmartPointer<vtkProbeFilter> vtk_probe_;

			// number of nodes and elements
			arma::uword num_nodes_;
			arma::uword num_elements_;

		public:
			// constructor
			VTKUnstr();
			// ~VTKUnstr();

			explicit VTKUnstr(std::list<ShVTKUnstrPr> &unstr_list, const bool merge_points = false, const fltp merge_tolerance = 1e-6);

			// factory
			static ShVTKUnstrPr create();
			static ShVTKUnstrPr create(std::list<ShVTKUnstrPr> &unstr_list, const bool merge_points = false, const fltp merge_tolerance = 1e-6);

			// point merge
			void merge_points(const fltp merge_tolerance = 1e-6);

			// get raw data grid 
			vtkSmartPointer<vtkUnstructuredGrid> get_vtk_ugrid() const;

			// get number of nodes and number of elements
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// write mesh
			void set_mesh(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n, const arma::uword element_type);
			void set_nodes(const arma::Mat<fltp> &Rn);
			void set_elements(const arma::Mat<arma::uword> &n, const arma::uword element_type);
			void set_elements(const arma::field<arma::Mat<arma::uword> > &n, const arma::Row<arma::uword> &element_types);

			// add time stamp to data
			void set_global(const fltp val, const std::string &data_name);

			// write data at nodes
			void set_nodedata(const arma::Mat<fltp> &v, const std::string &data_name);
			void set_elementdata(const arma::Mat<fltp> &v, const std::string &data_name);

			// get filename extension
			std::string get_filename_ext() const override;

			// write output file
			void write(const boost::filesystem::path fname, cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// interpolation methods
			void setup_interpolation();
			arma::Mat<fltp> interpolate_field(const arma::Mat<fltp> &Rp, const std::string &field_name);

			// element type list
			static int get_element_type(const arma::uword num_rows);
	};

}}

#endif
