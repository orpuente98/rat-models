// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRACK_DATA_HH
#define MDL_TRACK_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/mlfmm/trackinggrid.hh"

#include "rat/common/error.hh"
#include "rat/common/log.hh"

#include "emitter.hh"
#include "particle.hh"
#include "vtkunstr.hh"
#include "meshdata.hh"
#include "data.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TrackData> ShTrackDataPr;

	// template for coil
	class TrackData: public Data{
		// properties
		protected:
			// track data
			arma::field<Particle> particles_;

		// methods
		public:
			// constructor
			TrackData();
			TrackData(
				const ShMeshDataPr &tracking_mesh, 
				const ShEmitterPr &emitter);

			// factory methods
			static ShTrackDataPr create();
			static ShTrackDataPr create(
				const ShMeshDataPr &tracking_mesh, 
				const ShEmitterPr &emitter);
			
			// setting of emitter
			void set_emitter(const ShEmitterPr &emitter);

			// setters
			void set_stepsize(const fltp step_size);


			// field interpolation
			static arma::Mat<fltp> calc_field(
				const arma::Mat<fltp> &RV,
				const fmm::ShTrackingGridPr &tracking_grid);

			// tracking functions
			// static arma::Mat<fltp> interpolate_field(
			// 	const ShVTKUnstrPr &vtk_mesh, 
			// 	const arma::Mat<fltp> &Rp);
			static arma::Mat<fltp> system_function(
				const fltp t, 
				const arma::Mat<fltp> &Xp, 
				const arma::Row<fltp> &q, 
				const arma::Row<fltp> &m, 
				const fmm::ShTrackingGridPr &tracking_grid);
			static arma::Mat<fltp> runge_kutta(
				const fltp t, 
				const fltp dt, 
				const arma::Mat<fltp> &Xp, 
				const arma::Row<fltp> &q, 
				const arma::Row<fltp> &m, 
				const fmm::ShTrackingGridPr &tracking_grid);

			// calculate tracks
			void create_tracks(
				const fmm::ShTrackingGridPr &tracking_grid,
				const std::list<ShEmitterPr> &emitters,
				const fltp step_size = 5e-3, // [m]
				const rat::cmn::ShLogPr &lg = rat::cmn::NullLog::create());

			// export to vtk
			ShVTKObjPr export_vtk() const override;
			//virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// get particle
			Particle get_particle(const arma::uword idx) const;
	};
}}

#endif
