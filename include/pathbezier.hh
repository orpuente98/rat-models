// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_BEZIER2_HH
#define MDL_PATH_BEZIER2_HH

#include <armadillo>
#include <memory>

#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/mlfmm/extra.hh"
#include "path.hh"
#include "frame.hh"
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathBezier> ShPathBezierPr;

	// bezier spline implementation
	class PathBezier: public Path{
		// properties
		protected:
			// calculate the frame analytically or numerically
			bool analytic_frame_ = true;
			//bool spline_order_ = 5;
			
			// number of points used for calculating length
			const arma::uword num_reg_ = 20;

			// start point
			arma::Col<fltp>::fixed<3> pstart_;
			arma::Col<fltp>::fixed<3> nstart_;
			arma::Col<fltp>::fixed<3> lstart_;
			
			// end point 
			arma::Col<fltp>::fixed<3> pend_;
			arma::Col<fltp>::fixed<3> nend_;
			arma::Col<fltp>::fixed<3> lend_;

			// strengths
			fltp str1_;
			fltp str2_;
			fltp str3_;
			fltp str4_;

			// transition length
			fltp ell_trans_;

			// element size
			fltp element_size_;

			// use planar windings (winding plane determined by start point)
			bool planar_winding_ = false;

			// path offset
			fltp path_offset_;

			// number of sections
			arma::uword num_sections_ = 1;

		// methods
		public:
			// constructor
			PathBezier();
			PathBezier(const arma::Col<fltp>::fixed<3> pstart, const arma::Col<fltp>::fixed<3> lstart, const arma::Col<fltp>::fixed<3> nstart,const arma::Col<fltp>::fixed<3> pend, const arma::Col<fltp>::fixed<3> lend, const arma::Col<fltp>::fixed<3> nend, const fltp str1, const fltp str2, const fltp str3, const fltp str4, const fltp ell_trans, const fltp element_size, const fltp path_offset = 0);
			
			// factory
			static ShPathBezierPr create();
			static ShPathBezierPr create(const arma::Col<fltp>::fixed<3> pstart, const arma::Col<fltp>::fixed<3> lstart, const arma::Col<fltp>::fixed<3> nstart,const arma::Col<fltp>::fixed<3> pend, const arma::Col<fltp>::fixed<3> lend, const arma::Col<fltp>::fixed<3> nend, const fltp str1, const fltp str2, const fltp str3, const fltp str4, const fltp ell_trans, const fltp element_size, const fltp path_offset = 0);

			// access to properties
			void set_planar_winding(const bool planar_winding);
			void set_analytic_frame(const bool analytic_frame);
			void set_num_sections(const arma::uword num_sections);

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// create regular time array
			arma::Row<fltp> regular_times(const arma::Mat<fltp> &P) const;
			
			// bezier function
			static void create_bezier(arma::Mat<fltp> &R,arma::Mat<fltp> &V,arma::Mat<fltp> &A,const arma::Row<fltp> &t, const arma::Mat<fltp> &P);

			// recursive bezier function
			static arma::field<arma::Mat<fltp> > create_bezier_tn(const arma::Row<fltp> &t, const arma::Mat<fltp> &P, const arma::uword depth);
	};

}}

#endif
