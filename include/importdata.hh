// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_IMPORT_DATA_HH
#define MDL_IMPORT_DATA_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"

#include "targetdata.hh"
#include "modelmesh.hh"
#include "surfacedata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ImportData> ShImportDataPr;

	// imported mesh from VTK file
	// boost::filesystem::path pth = "Bulbasaur.stl";
	// rat::mdl::ShImportDataPr mi = rat::mdl::ImportData::create(pth); 
	// mi->set_scaling_factor(0.3e-3);
	// mlfmm->add_target(mi);
	class ImportData: public TargetData{
		// properties
		private:
			// scaling o fthe mesh
			fltp scaling_factor_ = RAT_CONST(1.0);

			// file location
			boost::filesystem::path fname_;

			// mesh
			arma::Mat<arma::uword> n_;

			// translation
			arma::Col<fltp>::fixed<3> R0_;

		// methods
		public:
			// constructor
			ImportData();
			explicit ImportData(const boost::filesystem::path &fname);

			// factory methods
			static ShImportDataPr create();
			static ShImportDataPr create(const boost::filesystem::path &fname);

			// translate
			void set_translation(const arma::Col<fltp> &R0);

			// target setup function
			void setup_targets() override;

			// set scaling factor
			void set_scaling_factor(const rat::fltp scaling_factor);

			// VTK export
			ShVTKObjPr export_vtk() const override;

	};

}}

#endif
