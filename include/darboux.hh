// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DARBOUX_HH
#define MDL_DARBOUX_HH

#include <armadillo> 
#include <memory>
#include <cmath>

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Darboux> ShDarbouxPr;

	// template class for cross section
	class Darboux{
		// properties
		protected:
			// derivatives
			arma::Mat<fltp> V_; 
			arma::Mat<fltp> A_;
			arma::Mat<fltp> J_;

			// twist and bending
			arma::Row<fltp> kappa_;
			arma::Row<fltp> tau_;

			// orientation
			arma::Mat<fltp> B_;
			arma::Mat<fltp> T_;

			// darboux vectors
			arma::Mat<fltp> D_;

		// methods
		public:
			// constructors
			Darboux();
			Darboux(
				const arma::Mat<fltp> &V, 
				const arma::Mat<fltp> &A, 
				const arma::Mat<fltp> &J);

			// factory
			static ShDarbouxPr create();
			static ShDarbouxPr create(
				const arma::Mat<fltp> &V, 
				const arma::Mat<fltp> &A, 
				const arma::Mat<fltp> &J);

			// calculate
			void setup(const bool analytic_frame);
			void correct_sign(const arma::Col<fltp>::fixed<3> &dstart);

			// get orientation vectors
			arma::Mat<fltp> get_longitudinal() const;
			arma::Mat<fltp> get_transverse() const;
			arma::Mat<fltp> get_normal() const;
	};

}}

#endif
