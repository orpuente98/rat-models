// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_INDUCTANCE_HH
#define MDL_MODEL_INDUCTANCE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "calcleaf.hh"
#include "extra.hh"
#include "data.hh"
#include "inductancedata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcInductance> ShCalcInductancePr;

	// template for coil
	class CalcInductance: public CalcLeaf{
		// properties
		protected:
			// parallel calculation of all combinations
			// uses more memory but can be a little faster
			bool use_parallel_ = false;

			// calculate per circuit
			bool group_circuits_ = false;

		// methods
		public:
			// constructor
			CalcInductance();
			explicit CalcInductance(const ShModelPr &model);
			
			// factory methods
			static ShCalcInductancePr create();
			static ShCalcInductancePr create(const ShModelPr &model);

			// setters
			void set_use_parallel(const bool use_parallel);
			void set_group_circuits(const bool group_circuits = true);

			// getters
			bool get_group_circuits() const;

			// calculate with inductance data output
			ShInductanceDataPr calculate_inductance(const fltp time, const cmn::ShLogPr &lg);

			// generalized calculation
			std::list<ShDataPr> calculate(const fltp time, const cmn::ShLogPr &lg) override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
