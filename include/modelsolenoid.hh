// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_SOLENOID_HH
#define MDL_MODEL_SOLENOID_HH

#include <armadillo> 
#include <memory>

#include "modelcoilwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelSolenoid> ShModelSolenoidPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelSolenoid: public ModelCoilWrapper{
		// properties
		protected:
			// coil dimensions
			fltp inner_radius_ = 0.05;
			fltp dcoil_ = 0.01;
			fltp height_ = 0.01;

			// discretization
			arma::uword num_sections_ = 4;
			fltp element_size_ = 2e-3;

		// methods
		public:
			// constructor
			ModelSolenoid();
			ModelSolenoid(const fltp inner_radius, const fltp dcoil, const fltp height, const fltp element_size, const arma::uword num_sections = 4);

			// factory methods
			static ShModelSolenoidPr create();
			static ShModelSolenoidPr create(const fltp inner_radius, const fltp dcoil, const fltp height, const fltp element_size, const arma::uword num_sections = 4);

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// setting
			void set_inner_radius(const fltp inner_radius);
			void set_dcoil(const fltp dcoil);
			void set_height(const fltp height);
			void set_num_sections(const arma::uword num_sections);
			void set_element_size(const fltp azimuthal_element_size);

			// getting
			fltp get_inner_radius()const;
			fltp get_dcoil()const;
			fltp get_height()const;
			arma::uword get_num_sections()const;
			fltp get_element_size()const;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
