// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// frame are defined as
// D L
// |/
// o--N
// and are in principle orthogonal 
// although this is not enforced.

#ifndef MDL_FRAME_HH
#define MDL_FRAME_HH

#include <memory>
#include <cassert>
#include <armadillo>

#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/node.hh"

#include "cross.hh"
#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Frame> ShFramePr;
	typedef arma::field<ShFramePr> ShFramePrList;

	// frame for describing three dimensional
	// ribbon like shapes.
	class Frame{
		// enums
		public:
			// type of the frame
			enum DbType {block,cable};

		// properties
		private:
			// type for the frame
			// determines how the coordinate 
			// transformations are performed
			DbType conductor_type_ = block;

			// properties in (num_sections)[3XN] matrices
			arma::field<arma::Mat<fltp> > R_; // coordinates
			arma::field<arma::Mat<fltp> > L_; // longitudinal vectors
			arma::field<arma::Mat<fltp> > N_; // normal vectors
			arma::field<arma::Mat<fltp> > D_; // transverse vectors
			arma::field<arma::Mat<fltp> > B_; // block winding direction

			// section and turn
			arma::uword num_section_base_;
			arma::Row<arma::uword> section_;
			arma::Row<arma::uword> turn_;

			// tolerance for checking if closed
			const fltp tol_closed_ = 1e-6;

		public:
			// constructors
			Frame();
			Frame(
				const arma::Mat<fltp> &R, 
				const arma::Mat<fltp> &L, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<fltp> &B); 
			Frame(
				const arma::Mat<fltp> &R, 
				const arma::Mat<fltp> &L, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<fltp> &B, 
				const arma::uword num_sections);
			Frame(
				const arma::field<arma::Mat<fltp> > &R, 
				const arma::field<arma::Mat<fltp> > &L, 
				const arma::field<arma::Mat<fltp> > &N, 
				const arma::field<arma::Mat<fltp> > &D, 
				const arma::field<arma::Mat<fltp> > &B);
			explicit Frame(
				const std::list<ShFramePr> &frames);

			// factory
			static ShFramePr create();
			static ShFramePr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<fltp> &L, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D);
			static ShFramePr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<fltp> &L,
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::uword num_sections);
			static ShFramePr create(
				const arma::field<arma::Mat<fltp> > &R, 
				const arma::field<arma::Mat<fltp> > &L, 
				const arma::field<arma::Mat<fltp> > &N, 
				const arma::field<arma::Mat<fltp> > &D);
			static ShFramePr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<fltp> &L, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<fltp> &B);
			static ShFramePr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<fltp> &L, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<fltp> &B, 
				const arma::uword num_sections);
			static ShFramePr create(
				const arma::field<arma::Mat<fltp> > &R, 
				const arma::field<arma::Mat<fltp> > &L, 
				const arma::field<arma::Mat<fltp> > &N, 
				const arma::field<arma::Mat<fltp> > &D, 
				const arma::field<arma::Mat<fltp> > &B);
			static ShFramePr create(
				const std::list<ShFramePr> &frames);

			// access to vectors
			arma::uword get_num_sections() const;
			arma::Row<arma::uword> get_num_nodes() const;
			arma::uword get_num_nodes(const arma::uword index) const;
			const arma::field<arma::Mat<fltp> >& get_coords() const;
			const arma::field<arma::Mat<fltp> >& get_direction() const;
			const arma::field<arma::Mat<fltp> >& get_normal() const;
			const arma::field<arma::Mat<fltp> >& get_transverse() const;
			const arma::field<arma::Mat<fltp> >& get_block() const;

			// access to vectors per section
			const arma::Mat<fltp>& get_coords(const arma::uword section) const;
			const arma::Mat<fltp>& get_direction(const arma::uword section) const;
			const arma::Mat<fltp>& get_normal(const arma::uword section) const;
			const arma::Mat<fltp>& get_transverse(const arma::uword section) const;
			const arma::Mat<fltp>& get_block(const arma::uword section) const;

			// simplification function
			void simplify(const fltp tolerance, const arma::Mat<fltp>::fixed<2,2> &bounds);

			// function for calculating shearing angle
			arma::Row<fltp> calc_ashear(const arma::uword section) const;

			// perform coordinate transformations
			arma::Mat<fltp> map_coords(const fltp d, const fltp n) const;

			// check if the sections are closed in on themselves
			arma::Row<arma::uword> is_closed() const;	
			bool is_loop() const;

			// access flag for keeping track of type
			void set_conductor_type(const DbType type);
			DbType get_conductor_type() const;

			// set section and turn indices to understand where we are located
			void set_location(
				const arma::Row<arma::uword> &section, 
				const arma::Row<arma::uword> &turn,
				const arma::uword num_section_base);
				
			// number of base sections
			void set_num_section_base(const arma::uword num_section_base);
			arma::uword get_num_section_base() const;

			// calculate length
			arma::Row<fltp> calc_ell() const;
			fltp calc_total_ell() const;

			// find enclosing box size of frame
			arma::Col<fltp>::fixed<3> calc_size() const;

			// get indexes to section or turn
			const arma::Row<arma::uword>& get_section() const;
			const arma::Row<arma::uword>& get_turn() const;

			// method for aligning the sections
			void align_sections();
			void align_sections(
				const arma::Col<fltp>::fixed<3> &R0, 
				const arma::Col<fltp>::fixed<3> &L0, 
				const arma::Col<fltp>::fixed<3> &N0, 
				const arma::Col<fltp>::fixed<3> &D0);
			
			// combine sections into single set of frame
			void combine();
			void split(const arma::uword num_sub);
			std::list<ShFramePr> separate()const;

			// reverse sections and their contents
			void reverse();

			// apply a transformation to these frame
			void apply_transformations(
				const std::list<ShTransPr> &trans);
			void apply_transformation(
				const ShTransPr &trans);

			// extrusions
			//ShCoilMeshPr create_loft(const ShAreaPr &area) const;
			void create_loft(
				arma::Mat<fltp> &R, 
				arma::Mat<fltp> &L, 
				arma::Mat<fltp> &N, 
				arma::Mat<fltp> &D, 
				arma::Mat<arma::uword> &n, 
				arma::Mat<arma::uword> &s, 
				const arma::Mat<fltp> &R2d, 
				const arma::Mat<fltp> &N2d, 
				const arma::Mat<fltp> &D2d, 
				const arma::Mat<arma::uword> &n2d, 
				const arma::Mat<arma::uword> &s2d,
				const bool cap_start = true,
				const bool cap_end = true) const;

			// shifting of the basebath
			arma::Mat<fltp> perform_normal_shift(
				const arma::uword section, 
				const arma::Row<fltp> &n) const; 
			arma::Mat<fltp> perform_shift(
				const arma::uword section, 
				const arma::Row<fltp> &n, 
				const arma::Row<fltp> &d) const;
			arma::Mat<fltp> perform_shift(
				const arma::uword section, 
				const fltp n, const fltp d) const;

			// calculate curvature of the frame
			arma::field<arma::Mat<fltp> > calc_curvature() const;

			// get sub frame
			ShFramePr get_sub(
				const arma::uword section, 
				const arma::uword idx1, 
				const arma::uword idx2) const;
			ShFramePr get_sub(
				const arma::uword section) const;
	};

}}

#endif

