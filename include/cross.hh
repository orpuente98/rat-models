// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_HH
#define MDL_CROSS_HH

#include <armadillo> 
#include <memory>
#include <json/json.h>

#include "rat/common/node.hh"

#include "perimeter.hh"
#include "meshsettings.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Cross> ShCrossPr;

	// template class for cross section
	class Cross: virtual public cmn::Node{
		// methods
		public:
			// destructor
			virtual ~Cross(){};

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const = 0;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const = 0;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const = 0;
	};

}}

#endif
