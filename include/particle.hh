// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PARTICLE_HH
#define MDL_PARTICLE_HH

#include <armadillo> 
#include <memory>

#include "targetdata.hh"

#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// particle and its track
	class Particle{
		protected:
			// properties
			fltp rest_mass_ = 0; // [eV/C^2] 
			fltp charge_ = 0; // elementary charge

			// start location
			arma::Col<fltp>::fixed<3> R0_ = {0,0,0};
			arma::Col<fltp>::fixed<3> V0_ = {0,0,0};

			// track information
			arma::Row<fltp> t_;
			arma::Mat<fltp> Rt_;
			arma::Mat<fltp> Vt_;

			// magnetic field on track
			arma::Mat<fltp> Bt_;

			// track status
			bool alive_start_ = true;
			bool alive_end_ = true;
			arma::uword num_steps_ = 0;
			arma::uword idx_max_ = 0;
			arma::uword idx_min_ = 0;
			
		public:
			// constructor
			Particle();

			// setting basic properties
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			
			// get basic particle properties
			fltp get_rest_mass() const;
			fltp get_charge() const;

			// set start coordinate
			void set_num_steps(const arma::uword num_steps);
			void set_start_index(const arma::uword start_idx);
			void set_startcoord(
				const arma::Col<fltp>::fixed<3> &R0, 
				const arma::Col<fltp>::fixed<3> &V0);

			// allocate
			void setup();

			// get whether the track is alive
			bool is_alive_start() const;
			bool is_alive_end() const;

			// get the current time, position and velocity
			fltp get_first_time() const;
			arma::Col<fltp>::fixed<3> get_first_coord() const;
			arma::Col<fltp>::fixed<3> get_first_velocity() const;
			fltp get_last_time() const;
			arma::Col<fltp>::fixed<3> get_last_coord() const;
			arma::Col<fltp>::fixed<3> get_last_velocity() const;
			
			// set the next position
			void set_prev_coord(
				const fltp t, 
				const arma::Col<fltp>::fixed<3> &Rp, 
				const arma::Col<fltp>::fixed<3> &Vp);
			void set_next_coord(
				const fltp t, 
				const arma::Col<fltp>::fixed<3> &Rp, 
				const arma::Col<fltp>::fixed<3> &Vp);

			// termination
			void terminate_end();
			void terminate_start();

			// get full track
			arma::uword get_num_coords();
			arma::Mat<fltp> get_track_coords() const;
			arma::Mat<fltp> get_track_velocities() const;

			// set magnetic field on track
			void set_magnetic_field(const arma::Mat<fltp> &Bt);
			arma::Mat<fltp> get_magnetic_field()const;
	};

}}

#endif