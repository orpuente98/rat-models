// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_GROUP_HH
#define MDL_CALC_GROUP_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcGroup> ShCalcGroupPr;

	// template for coil
	class CalcGroup: public Calc{
		// properties
		protected:
			// input model
			std::map<arma::uword, ShCalcPr> calc_list_;

		// methods
		public:
			// constructor
			CalcGroup();
			CalcGroup(const std::list<ShCalcPr> &calculations);

			// factory
			static ShCalcGroupPr create();
			static ShCalcGroupPr create(const std::list<ShCalcPr> &calculations);

			
			// calculation function
			std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create()) override;

			// creation of calculation data objects
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// add/delete calculations
			arma::uword add_calculation(const ShCalcPr &calc);
			arma::Row<arma::uword> add_calculations(const std::list<ShCalcPr> &calcs);
			ShCalcPr get_calculation(const arma::uword index) const;
			std::list<ShCalcPr> get_calculations() const;
			bool delete_calculation(const arma::uword index);
			void reindex() override;
			arma::uword num_calculations() const;

			// calculations
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
