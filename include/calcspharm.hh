// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_SPHERICAL_HARMONICS_HH
#define MDL_CALC_SPHERICAL_HARMONICS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "calcpath.hh"
#include "spharmdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcSpHarm> ShCalcSpHarmPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcSpHarm: public CalcLeaf{
		// properties
		protected:
			// enable visibility
			bool visibility_ = true;

			// reference radius
			fltp reference_radius_ = RAT_CONST(20e-3);

			// max number of harmonics
			arma::uword nmax_ = 10;
			arma::uword mmax_ = 10;

			// position
			arma::Col<fltp>::fixed<3> R_{0,0,0};

		// methods
		public:
			// constructor
			CalcSpHarm();
			explicit CalcSpHarm(const ShModelPr &model);
			CalcSpHarm(
				const ShModelPr &model, 
				const fltp reference_radius, 
				const arma::uword nmax, 
				const arma::uword mmax,
				const arma::Col<fltp>::fixed<3> &R);
			
			// factory methods
			static ShCalcSpHarmPr create();
			static ShCalcSpHarmPr create(
				const ShModelPr &model, 
				const fltp reference_radius = 20e-3, 
				const arma::uword nmax = 10, 
				const arma::uword mmax = 10,
				const arma::Col<fltp>::fixed<3> &R = {0,0,0});

			// setters
			void set_visibility(const bool visibility);
			void set_reference_radius(const fltp reference_radius);
			void set_nmax(const arma::uword nmax);
			void set_mmax(const arma::uword mmax);
			void set_coordinate(const arma::Col<fltp>::fixed<3> &R);

			// getters 
			bool get_visibility() const;
			fltp get_reference_radius() const;
			arma::uword get_nmax()const;
			arma::uword get_mmax()const;
			arma::Col<fltp>::fixed<3> get_coordinate()const;

			// calculate with inductance data output
			ShSpHarmDataPr calculate_spherical_harmonics(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create());

			// generalized calculation
			std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg) override;

			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
