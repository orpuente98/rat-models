// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_CCT_HH
#define MDL_MODEL_CCT_HH

#include <armadillo> 
#include <memory>

#include "modelcoilwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelCCT> ShModelCCTPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelCCT: public ModelCoilWrapper{
		// properties
		protected:
			// reverse skew
			bool is_reverse_ = false;
			
			// bending along length
			fltp bending_arc_length_ = RAT_CONST(0.0);
			
			// harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
			arma::uword num_poles_ = 1; 
			
			// radius
			fltp radius_ = RAT_CONST(30e-3); 
			
			// skew angle (for inner layer)
			fltp skew_angle_ = 30.0*2*arma::Datum<fltp>::pi/360.0;

			// spacing between turns (winding pitch)
			fltp drib_ = RAT_CONST(0.5e-3); 

			// number of turns (can be non-integer)
			fltp num_turns_ = 20; 

			// additional twisting along length of cable
			fltp twist_ = RAT_CONST(0.0); 

			// num layers
			arma::uword num_layers_ = 2;
			fltp rho_increment_ = RAT_CONST(6e-3);

			// coil pack dimensions
			fltp cable_thickness_ = RAT_CONST(0.002); // this becomes width of slot
			fltp cable_width_ = RAT_CONST(0.005); // this becomes depth of slot

			// discretization
			arma::uword num_nodes_per_turn_ = 180;

			// element size
			fltp element_size_ = RAT_CONST(2e-3);

		// methods
		public:
			// constructor
			ModelCCT();

			// factory methods
			static ShModelCCTPr create();

			// set properties
			void set_is_reverse(const bool is_reverse);
			void set_bending_arc_length(const fltp bending_arc_length);
			void set_twist(const fltp twist);
			void set_skew_angle(const fltp skew_angle);
			void set_drib(const fltp drib);
			void set_num_turns(const fltp num_turns);
			void set_radius(const fltp radius);
			void set_num_poles(const arma::uword num_poles);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_num_layers(const arma::uword num_layers);
			void set_rho_increment(const fltp rho_increment);
			void set_cable_thickness(const fltp cable_thickness);
			void set_cable_width(const fltp cable_width);
			void set_element_size(const fltp element_size);

			// get properties
			bool get_is_reverse() const;
			fltp get_bending_arc_length() const;
			fltp get_twist()const;
			fltp get_skew_angle()const;
			fltp get_drib()const;
			fltp get_num_turns()const;
			fltp get_radius()const;
			arma::uword get_num_poles()const;
			arma::uword get_num_nodes_per_turn() const;
			arma::uword get_num_layers() const;
			fltp get_rho_increment()const;
			fltp get_cable_thickness()const;
			fltp get_cable_width()const;
			fltp get_element_size()const;

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
