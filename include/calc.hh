// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_HH
#define MDL_CALC_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/node.hh"
#include "rat/common/log.hh"

#include "meshgen.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Calc> ShCalcPr;

	// template for coil
	class Calc: virtual public cmn::Node, public MeshGen{
		// settings
		protected:
			// output directory
			boost::filesystem::path output_dir_;

			// output time for single calculation
			fltp time_ = RAT_CONST(0.0);

			// output times for time range calculation
			fltp t1_ = RAT_CONST(0.0);
			fltp t2_ = RAT_CONST(0.0);

			// number of times
			arma::uword num_times_ = 0;

		// methods
		public:
			// constructor
			Calc(){};

			// virtual destructor
			virtual ~Calc(){};

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create()) = 0;
			
			// creation of calculation data objects
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// setters
			void set_time(const fltp time);
			void set_t1(const fltp t1);
			void set_t2(const fltp t2);
			void set_num_times(const arma::uword num_times);
			void set_output_dir(const boost::filesystem::path &output_dir);

			// getters
			fltp get_time()const;
			fltp get_t1()const;
			fltp get_t2()const;
			arma::uword get_num_times()const;
			boost::filesystem::path get_output_dir()const;

			// calculate and write using set time and directory
			void calculate_write(const cmn::ShLogPr &lg);

			// calculation function
			void calculate_write(
				const arma::Row<fltp> &output_times, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr &lg = cmn::NullLog::create());

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
