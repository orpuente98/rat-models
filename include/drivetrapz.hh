// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_TRAPZ_HH
#define MDL_DRIVE_TRAPZ_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveTrapz> ShDriveTrapzPr;
	typedef arma::field<ShDriveTrapzPr> ShDriveTrapzPrList;

	// circuit class
	class DriveTrapz: public Drive{
		// properties
		protected:
			// tstart is center of pulse
			bool centered_ = false;

			// amplitude
			fltp offset_ = RAT_CONST(0.0);

			// amplitude
			fltp amplitude_ = RAT_CONST(1.0);

			// scaling amplitude
			fltp tstart_ = RAT_CONST(1.0);

			// frequency
			fltp ramprate_ = RAT_CONST(0.1);

			// phase
			fltp tpulse_ = RAT_CONST(2.0);

		// methods
		public:
			// constructor
			DriveTrapz();
			DriveTrapz(
				const fltp amplitude, 
				const fltp tstart, 
				const fltp ramprate, 
				const fltp tpulse, 
				const fltp offset, 
				const bool centered);

			// factory
			static ShDriveTrapzPr create();
			static ShDriveTrapzPr create(
				const fltp amplitude, 
				const fltp tstart, 
				const fltp ramprate, 
				const fltp tpulse, 
				const fltp offset = RAT_CONST(0.0), 
				const bool centered = false);

			// set properties
			void set_centered(const bool centered = true);
			void set_amplitude(const fltp amplitude);
			void set_tstart(const fltp tstart);
			void set_ramprate(const fltp ramprate);
			void set_tpulse(const fltp tpulse);
			void set_offset(const fltp offset);
				
			// get properties
			bool get_centered()const;
			fltp get_amplitude()const;
			fltp get_tstart()const;
			fltp get_ramprate()const;
			fltp get_tpulse()const;
			fltp get_offset()const;

			// get scaling at given time
			fltp get_scaling(const fltp time) const override;
			fltp get_dscaling(const fltp time) const override;
			
			// get inflection points
			arma::Col<fltp> get_inflection_times() const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
