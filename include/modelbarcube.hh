// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_BAR_CUBE_HH
#define MDL_MODEL_BAR_CUBE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"

#include "modelbarwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelBarCube> ShModelBarCubePr;

	// wrapper for a magnetised cube
	class ModelBarCube: public ModelBarWrapper{
		// properties
		protected:
			// cube dimensions
			fltp width_ = RAT_CONST(1e-2);
			fltp thickness_ = RAT_CONST(1e-2);
			fltp height_ = RAT_CONST(5e-2); 

			// discretization
			fltp element_size_ = RAT_CONST(2e-3);

		// methods
		public:
			// constructor
			ModelBarCube();
			ModelBarCube(const fltp width, const fltp thickness, const fltp height, const fltp element_size);
			static ShModelBarCubePr create();
			static ShModelBarCubePr create(const fltp width, const fltp thickness, const fltp height, const fltp element_size);

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// setters
			void set_width(const fltp width);
			void set_thickness(const fltp thickness);
			void set_height(const fltp height);
			void set_element_size(const fltp element_size);
			
			// getters
			fltp get_width() const;
			fltp get_thickness() const;
			fltp get_height() const;
			fltp get_element_size() const;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
