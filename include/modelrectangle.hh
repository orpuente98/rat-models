// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_RECTANGLE_HH
#define MDL_MODEL_RECTANGLE_HH

#include <armadillo> 
#include <memory>

#include "modelcoilwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelRectangle> ShModelRectanglePr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelRectangle: public ModelCoilWrapper{
		// properties
		protected:
			// coil dimensions
			fltp arc_radius_ = 0.02;
			fltp length_ = 0.25;
			fltp width_ = 0.1;
			
			// coil pack dimensions
			fltp coil_thickness_ = 0.01;
			fltp coil_width_ = 0.01;

			// discretization
			fltp element_size_ = 2e-3;

		// methods
		public:
			// constructor
			ModelRectangle();

			// factory methods
			static ShModelRectanglePr create();

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// setting
			void set_arc_radius(const fltp arc_radius);
			void set_length(const fltp length);
			void set_width(const fltp width);
			void set_coil_thickness(const fltp coil_thickness);
			void set_coil_width(const fltp coil_width);
			void set_element_size(const fltp element_size);

			// getting
			fltp get_arc_radius()const;
			fltp get_length()const;
			fltp get_width()const;
			fltp get_coil_thickness()const;
			fltp get_coil_width()const;
			fltp get_element_size()const;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
