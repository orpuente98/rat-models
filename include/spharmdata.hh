// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_SPHERICAL_HARMONICS_DATA_HH
#define MDL_SPHERICAL_HARMONICS_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "targetdata.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class SpHarmData> ShSpHarmDataPr;

	// calculates 2D pseudo harmonics along a path
	// this path uses the same object as used for setting
	// up the coils and can thus be curved.
	class SpHarmData: public TargetData{
		// properties
		protected:
			// use factor two for nmax
			// this apparently gives better 
			// convergence when converting
			// to field and back several times
			bool use_nmax_factor_ = true;

			// position
			arma::Col<fltp>::fixed<3> Rcen_{0,0,0};

			// highest harmonic to output
			int nmax_;
			int mmax_;

			// reference radius
			fltp reference_radius_;

			// theta and phi
			arma::Row<fltp> theta_;
			arma::Row<fltp> wtheta_;
			arma::Row<fltp> phi_;
			arma::Row<fltp> wphi_;
			fltp wr_;

			// zonal harmonics
			arma::Col<std::complex<fltp> > Znm_;

		// methods
		public:
			// constructor
			SpHarmData();
			explicit SpHarmData(
				const fltp reference_radius, 
				const int nmax = 10, 
				const int mmax = 10,
				const arma::Col<fltp>::fixed<3> &Rcen = {0,0,0});

			// factory methods
			static ShSpHarmDataPr create();
			static ShSpHarmDataPr create(
				const fltp reference_radius, 
				const int nmax = 10, 
				const int mmax = 10,
				const arma::Col<fltp>::fixed<3> &Rcen = {0,0,0});

			// setters
			void set_reference_radius(
				const fltp reference_radius);
			void set_nmmax(
				const int nmax, 
				const int mmax);
			void set_coordinate(
				const arma::Col<fltp>::fixed<3> &Rcen);

			// getters
			arma::uword get_num_planes()const;
			arma::uword get_num_points()const;

			// calculation function
			void setup();

			// helper maths
			static void quadrature(
				arma::Row<fltp> &abscissae, 
				arma::Row<fltp> &weights,
				const arma::uword num_points, 
				const arma::uword k);

			// post processing
			void post_process() override;

			// convert harmonics back to Bz 
			arma::Row<fltp> spharm2field()const;
			arma::Row<fltp> spharm2field(const fltp Rp)const;

			// getting calculated harmonic tables
			arma::Col<std::complex<fltp> > get_harmonics() const;
			arma::Col<std::complex<fltp> > get_harmonics_ppm() const;

			// VTK exporting
			ShVTKObjPr export_vtk() const override;
			// ShVTKTablePr export_vtk_table() const;

			// display function
			void display(const cmn::ShLogPr &lg) const; // in T
			void display_ppm(const cmn::ShLogPr &lg) const; // parts per million
	};

}}

#endif
