// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_RECTANGLE_HH
#define MDL_CROSS_RECTANGLE_HH

#include <armadillo> 
#include <memory>
#include <cmath>
#include <json/json.h>

#include "rat/common/gauss.hh"
#include "rat/common/error.hh"
#include "area.hh"
#include "perimeter.hh"
#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossRectangle> ShCrossRectanglePr;

	// template class for cross section
	class CrossRectangle: public Cross{
		// properties
		protected:
			// gaussian distribution
			bool enable_gauss_ = false;

			// dimensions in [m]
			fltp n1_;
			fltp n2_;
			fltp t1_;
			fltp t2_;

			// element size in [m]
			fltp dn_;
			fltp dt_;

		// methods
		public:
			// constructor
			CrossRectangle();

			// constructor with input
			CrossRectangle(
				const fltp n1, const fltp n2, 
				const fltp t1, const fltp t2, 
				const fltp dl);
			CrossRectangle(
				const fltp n1, const fltp n2, 
				const fltp t1, const fltp t2, 
				const fltp dn, const fltp dt);
			CrossRectangle(
				const fltp n1, const fltp n2, 
				const fltp t1, const fltp t2, 
				const arma::uword num_n, 
				const arma::uword num_t);

			// factory
			static ShCrossRectanglePr create();

			// factory with input
			static ShCrossRectanglePr create(
				const fltp n1, const fltp n2, 
				const fltp t1, const fltp t2, 
				const fltp dl);
			static ShCrossRectanglePr create(
				const fltp n1, const fltp n2, 
				const fltp t1, const fltp t2, 
				const fltp dn, const fltp dt);
			static ShCrossRectanglePr create(
				const fltp n1, const fltp n2, 
				const fltp t1, const fltp t2, 
				const arma::uword num_n, 
				const arma::uword num_t);

			// set properties
			void set_enable_gauss(const bool enable_gauss = true);

			// set multiple
			void set_dim_transverse(
				const fltp t1, const fltp t2, 
				const arma::uword num_t);
			void set_dim_transverse(
				const fltp t1, const fltp t2, 
				const fltp dt);
			void set_dim_normal(
				const fltp n1, const fltp n2, 
				const arma::uword num_n);
			void set_dim_normal(
				const fltp n1, const fltp n2, 
				const fltp dn);

			// getters and setters
			void set_n1(const fltp n1); 
			void set_n2(const fltp n1); 
			void set_dn(const fltp dn);
			void set_t1(const fltp t1); 
			void set_t2(const fltp t1); 
			void set_dt(const fltp dt);
			fltp get_n1() const; 
			fltp get_n2() const; 
			fltp get_dn() const;
			fltp get_t1() const; 
			fltp get_t2() const; 
			fltp get_dt() const;

			// get bounding box
			arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
