// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CCT_HH
#define MDL_PATH_CCT_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>

// models headers
#include "transformations.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCCT> ShPathCCTPr;

	// cross section of coil
	class PathCCT: public Path, public Transformations{
		// properties
		private:
			// reverse skew
			bool is_reverse_ = false;
			
			// bending along length
			fltp bending_arc_length_ = RAT_CONST(0.0);
			
			// harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
			arma::uword num_poles_ = 1; 
			
			// radius
			fltp radius_ = RAT_CONST(30e-3); 
			
			// skew amplitude
			fltp a_ = RAT_CONST(40e-3);

			// spacing between turns (winding pitch)
			fltp omega_ = RAT_CONST(6e-3); 

			// number of turns (can be non-integer)
			fltp num_turns_ = 20; 

			// discretization
			arma::uword num_nodes_per_turn_ = 180;

			// additional twisting along length of cable
			fltp twist_ = RAT_CONST(0.0); 

			// squeeze transformation
			fltp scale_x_ = RAT_CONST(1.0); // 1.0 is not elliptic at all
			fltp scale_y_ = RAT_CONST(1.0); // 1.0 is not elliptic at all

			// num layers
			arma::uword num_layers_ = 1;
			fltp rho_increment_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			PathCCT();
			PathCCT(const arma::uword num_poles, const fltp radius, const fltp a, const fltp omega, const fltp num_turns, const arma::uword num_nodes_per_turn);

			// factory methods
			static ShPathCCTPr create();
			static ShPathCCTPr create(const arma::uword num_poles, const fltp radius, const fltp a, const fltp omega, const fltp num_turns, const arma::uword num_nodes_per_turn);

			// set properties
			void set_is_reverse(const bool is_reverse);
			void set_bending_arc_length(const fltp bending_arc_length);
			void set_twist(const fltp twist);
			void set_amplitude(const fltp a);
			void set_pitch(const fltp omega);
			void set_num_turns(const fltp num_turns);
			void set_scale_x(const fltp scale_x);
			void set_scale_y(const fltp scale_y);
			void set_radius(const fltp radius);
			void set_num_poles(const arma::uword num_poles);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_num_layers(const arma::uword num_layers);
			void set_rho_increment(const fltp rho_increment);

			// get properties
			bool get_is_reverse() const;
			fltp get_bending_arc_length() const;
			fltp get_twist()const;
			fltp get_amplitude()const;
			fltp get_pitch()const;
			fltp get_num_turns()const;
			fltp get_radius()const;
			fltp get_scale_x()const;
			fltp get_scale_y()const;
			arma::uword get_num_poles()const;
			arma::uword get_num_nodes_per_turn() const;
			arma::uword get_num_layers() const;
			fltp get_rho_increment()const;

			// omega calculation
			fltp calc_alpha() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
