// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_HH
#define MDL_DRIVE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Drive> ShDrivePr;
	typedef arma::field<ShDrivePr> ShDrivePrList;

	// circuit class
	class Drive: public cmn::Node{
		// properties
		protected:
			// plot range
			fltp t1_ = RAT_CONST(-0.5);
			fltp t2_ = RAT_CONST(0.5);

		// methods
		public:
			// virtual destructor
			virtual ~Drive(){};

			// set and get current
			virtual fltp get_scaling(const fltp time) const = 0;
			virtual fltp get_dscaling(const fltp time) const = 0;

			// set range start
			void set_t1(const fltp t1);
			void set_t2(const fltp t2);

			// get range
			fltp get_t1() const;
			fltp get_t2() const;

			// get scaling for arrays
			arma::Row<fltp> get_scaling(const arma::Row<fltp> &time) const;
			arma::Row<fltp> get_dscaling(const arma::Row<fltp> &time) const;
			
			// get time array
			virtual arma::Col<fltp> get_inflection_times() const;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
