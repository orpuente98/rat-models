// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EMITTER_OMNI_HH
#define MDL_EMITTER_OMNI_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"
#include "rat/common/extra.hh"

#include "particle.hh"
#include "emitter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EmitterOmni> ShEmitterOmniPr;

	// omni directional particle emitter
	class EmitterOmni: public Emitter{
		protected:
			// seed for random distribution
			arma::uword seed_ = 1001;

			// following unit system from CERN MAD
			// particle settings
			fltp beam_energy_ = 0; // [GeV] including rest mass
			fltp rest_mass_ = 0; // [GeV/C^2]
			fltp charge_ = 0; // elementary charge

			// number of particles spawned
			arma::uword num_particles_ = 1000;

			// emitter location
			arma::Col<fltp>::fixed<3> R_ = {0,0,0};
			
			// track settings
			arma::uword lifetime_ = 501;


		public:
			// constructor
			EmitterOmni();
				
			// factory
			static ShEmitterOmniPr create();

			// default particles
			void set_proton();
			void set_electron();

			// setting of properties
			void set_beam_energy(const fltp beam_energy);
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			void set_lifetime(const arma::uword lifetime);
			void set_num_particles(const arma::uword num_particles);
			void set_spawn_coord(const arma::Col<fltp>::fixed<3> &R);
				
			// getters
			fltp get_rest_mass() const;
			arma::uword get_num_particles() const;
			fltp get_charge()const;
			fltp get_beam_energy()const;
			arma::uword get_lifetime()const;
			arma::Col<fltp>::fixed<3> get_spawn_coord()const;

			// particle
			arma::field<Particle> spawn_particles() const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif