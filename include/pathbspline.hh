// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_BEZIER_HH
#define MDL_PATH_BEZIER_HH

#include <armadillo>
#include <memory>

#include "rat/common/extra.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{
	// shared pointer definition
	typedef std::shared_ptr<class PathBSpline> ShPathBSplinePr;

	// bezier spline implementation
	class PathBSpline: public Path{
		// properties
		protected:
			// settings
			arma::uword Nreg_ = 20;
			fltp offset_ = 0;
			bool in_plane_ = false; // keeps n-vector in LN-plane

			// start point
			arma::Col<fltp>::fixed<3> pstart_;
			arma::Col<fltp>::fixed<3> lstart_;
			arma::Col<fltp>::fixed<3> nstart_;
			
			// end point 
			arma::Col<fltp>::fixed<3> pend_;
			arma::Col<fltp>::fixed<3> lend_;
			arma::Col<fltp>::fixed<3> nend_;
			
			// strengths
			fltp str1_;
			fltp str2_;

			// additional twists
			arma::sword num_twist_ = 0;

			// bezier constant
			const fltp bc_ = RAT_CONST(0.551784);

			// element size
			fltp element_size_;

		// methods
		public:
			// constructor
			PathBSpline();
			PathBSpline(const arma::Col<fltp>::fixed<3> pstart, const arma::Col<fltp>::fixed<3> lstart, const arma::Col<fltp>::fixed<3> nstart, const arma::Col<fltp>::fixed<3> pend,  const arma::Col<fltp>::fixed<3> lend, const arma::Col<fltp>::fixed<3> nend, const fltp str1, const fltp str2, const fltp element_size, const fltp offset = 0);
			PathBSpline(const fltp pstartx, const fltp pstarty, const fltp pstartz, const fltp lstartx, const fltp lstarty,	const fltp lstartz, const fltp nstartx, const fltp nstarty,	const fltp nstartz, const fltp pendx, const fltp pendy,	const fltp pendz, const fltp lendx, const fltp lendy,	const fltp lendz, const fltp nendx, const fltp nendy,	const fltp nendz, const fltp str1, const fltp str2, const fltp element_size, const fltp offset = 0);
			
			// factory
			static ShPathBSplinePr create();
			static ShPathBSplinePr create(const arma::Col<fltp>::fixed<3> pstart, const arma::Col<fltp>::fixed<3> lstart, const arma::Col<fltp>::fixed<3> nstart, const arma::Col<fltp>::fixed<3> pend,  const arma::Col<fltp>::fixed<3> lend, const arma::Col<fltp>::fixed<3> nend, const fltp str1, const fltp str2, const fltp element_size, const fltp offset = 0);
			static ShPathBSplinePr create(const fltp pstartx, const fltp pstarty, const fltp pstartz, const fltp lstartx, const fltp lstarty,	const fltp lstartz, const fltp nstartx, const fltp nstarty,	const fltp nstartz, const fltp pendx, const fltp pendy,	const fltp pendz, const fltp lendx, const fltp lendy,	const fltp lendz, const fltp nendx, const fltp nendy,	const fltp nendz, const fltp str1, const fltp str2, const fltp element_size, const fltp offset = 0);

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// set element size 
			void set_element_size(const fltp element_size);

			// path offset
			void set_offset(const fltp offset);

			// calculate
			void get_frame_from_time(arma::Mat<fltp> &R, arma::Mat<fltp> &L, arma::Mat<fltp> &N, arma::Mat<fltp> &D, const arma::Row<fltp> &t, const bool make_regular) const;
			
			// helper functions
			// calculate direction
			fltp get_length() const;
			arma::Mat<fltp> calculate_direction(const arma::Col<fltp>::fixed<3> &a, const arma::Col<fltp>::fixed<3> &b, const arma::Col<fltp>::fixed<3> &c, const arma::Row<fltp> &t) const;
			arma::Mat<fltp> calculate_acceleration(const arma::Col<fltp>::fixed<3> &a, const arma::Col<fltp>::fixed<3> &b, const arma::Col<fltp>::fixed<3> &c, const arma::Row<fltp> &t) const;
			arma::Row<fltp> calculate_lengths(const arma::Col<fltp>::fixed<3> &a, const arma::Col<fltp>::fixed<3> &b, const arma::Col<fltp>::fixed<3> &c, const arma::Row<fltp> &t) const;
			arma::Mat<fltp> calculate_coords(const arma::Col<fltp>::fixed<3> &a, const arma::Col<fltp>::fixed<3> &b, const arma::Col<fltp>::fixed<3> &c, const arma::Row<fltp> &t) const;
			arma::Row<fltp> regular_times(const arma::Col<fltp>::fixed<3> &a, const arma::Col<fltp>::fixed<3> &b, const arma::Col<fltp>::fixed<3> &c, const arma::Row<fltp> &t) const;
			
			// setting
			void set_num_twist(const arma::sword num_twist);
			void set_in_plane(const bool in_plane);
	};

}}

#endif
