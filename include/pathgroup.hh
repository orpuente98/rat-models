// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_UNIFIED_HH
#define MDL_PATH_UNIFIED_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathGroup> ShPathGroupPr;

	// cross section of coil
	class PathGroup: public Path, public Transformations{
		// properties
		protected:
			// symmetry
			arma::uword sym_ = 1;

			// coordinate
			arma::Col<fltp>::fixed<3> R0_; // start point
			
			// orientation
			arma::Col<fltp>::fixed<3> L0_; // start direction vector
			arma::Col<fltp>::fixed<3> N0_; // start normal vector

			// align frame
			// when enabled (default) the sections making up the
			// pathgroup are automatically lined up such that 
			// a smooth transition is made between them
			bool align_frame_ = true;

			// list of pointers to the path sections	
			std::map<arma::uword, ShPathPr> input_paths_;

		// methods
		public:
			// constructor
			PathGroup();
			PathGroup(const arma::Col<fltp>::fixed<3> &R0, const arma::Col<fltp>::fixed<3> &L0, const arma::Col<fltp>::fixed<3> &N0);

			// factory
			static ShPathGroupPr create();
			static ShPathGroupPr create(const arma::Col<fltp>::fixed<3> &R0, const arma::Col<fltp>::fixed<3> &L0, const arma::Col<fltp>::fixed<3> &N0);

			// set properties
			void set_sym(const arma::uword sym);
			void set_start_coord(const arma::Col<fltp>::fixed<3> &R0);
			void set_start_longitudinal(const arma::Col<fltp>::fixed<3> &L0);
			void set_start_normal(const arma::Col<fltp>::fixed<3> &N0);
			void set_align_frame(const bool align_frame = true);

			// get vectors
			arma::Col<fltp>::fixed<3> get_start_coord()const;
			arma::Col<fltp>::fixed<3> get_start_longitudinal()const;
			arma::Col<fltp>::fixed<3> get_start_normal()const;


			// get properties
			arma::uword get_sym() const;

			// function for adding a path to the path list
			arma::uword add_path(const ShPathPr &path);
				
			// get input path
			ShPathPr get_path(const arma::uword index) const;
			bool delete_path(const arma::uword index);
			void reindex() override;

			// get number of paths stored inside
			arma::uword get_num_paths() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
