// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRANS_REFLECT_HH
#define MDL_TRANS_REFLECT_HH

#include <armadillo> 
#include <string.h>
#include <memory>

#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransReflect> ShTransReflectPr;

	// cross section of coil
	class TransReflect: public Trans{
		// properties
		protected:
			// rotation vector
			arma::Col<fltp>::fixed<3> origin_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};
			arma::Col<fltp>::fixed<3> plane_vector_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)};

		// methods
		public:
			// constructors
			TransReflect();
			explicit TransReflect(const arma::Col<fltp>::fixed<3> plane_vector);
			TransReflect(const arma::Col<fltp>::fixed<3> origin, const arma::Col<fltp>::fixed<3> plane_vector);

			// factory methods
			static ShTransReflectPr create();
			static ShTransReflectPr create(const arma::Col<fltp>::fixed<3> plane_vector);
			static ShTransReflectPr create(const arma::Col<fltp>::fixed<3> origin, const arma::Col<fltp>::fixed<3> plane_vector);

			// get reflection matrix
			arma::Mat<fltp>::fixed<3,3> create_reflection_matrix() const;

			// set origin and vector
			void set_origin(const arma::Col<fltp>::fixed<3> &origin);
			void set_plane_vector(const arma::Col<fltp>::fixed<3> &plane_vector);

			// get origin and vector
			arma::Col<fltp>::fixed<3> get_origin() const;
			arma::Col<fltp>::fixed<3> get_plane_vector() const;

			// commonly used reflection
			void set_reflection_xy();
			void set_reflection_yz();
			void set_reflection_xz();

			// apply reflection
			// void apply(arma::Mat<fltp> &R, arma::Mat<fltp> &L, arma::Mat<fltp> &N, arma::Mat<fltp> &D) const;
			virtual void apply_coords(arma::Mat<fltp> &R) const override;
			virtual void apply_vectors(const arma::Mat<fltp> &R, arma::Mat<fltp> &V, const char str) const override;
			virtual void apply_mesh(arma::Mat<arma::uword> &n, const arma::uword num_nodes) const override;

			// transformation to area
			void apply_area_coords(arma::Mat<fltp> &R) const override;
			void apply_area_mesh(arma::Mat<arma::uword> &n, const arma::uword) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
