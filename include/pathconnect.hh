// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CONNECT_HH
#define MDL_PATH_CONNECT_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "frame.hh"
#include "pathbspline.hh" // cubic spline
#include "pathbezier.hh" // quintic spline with constant perimeter

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathConnect> ShPathConnectPr;

	// connects two paths with a connector
	// this then becomes a new combined path
	class PathConnect: public Path{
		// properties
		private:
			// geometric parameters
			ShPathPr prev_path_ = NULL;
			ShPathPr next_path_ = NULL;

			// strength of start and end points
			fltp str1_;
			fltp str2_; 
			fltp str3_; 
			fltp str4_; 
			fltp ell_trans_; 
			fltp element_size_;

		// methods
		public:
			// constructor
			PathConnect();
			PathConnect(const ShPathPr &prev_path, const ShPathPr &next_path, const fltp str1, const fltp str2, const fltp str3, const fltp str4, const fltp ell_trans, const fltp element_size);

			// factory methods
			static ShPathConnectPr create();
			static ShPathConnectPr create(const ShPathPr &prev_path, const ShPathPr &next_path, const fltp str1, const fltp str2, const fltp str3, const fltp str4, const fltp ell_trans, const fltp element_size);
			
			// set paths
			void set_prev_path(ShPathPr prev_path);
			void set_next_path(ShPathPr next_path);
			void set_strengths(const fltp str1, const fltp str2, const fltp str3, const fltp str4);
			void set_ell_trans(const fltp ell_trans);
			void set_element_size(const fltp element_size);

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;
	};

}}

#endif
