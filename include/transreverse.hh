// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRANS_REVERSE_HH
#define MDL_TRANS_REVERSE_HH

#include <armadillo> 
#include <string.h>
#include <memory>

#include "trans.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransReverse> ShTransReversePr;

	// cross section of coil
	class TransReverse: public Trans{
		// methods
		public:
			// constructors
			TransReverse();
			
			// factory methods
			static ShTransReversePr create();

			// transformation functions (have empty implementations)
			virtual void apply_coords(arma::Mat<fltp> &R) const override;
			virtual void apply_vectors(const arma::Mat<fltp> &R, arma::Mat<fltp> &V, const char str) const override;
			virtual void apply_mesh(arma::Mat<arma::uword> &n, const arma::uword num_nodes) const override;

			// for field arrays
			virtual void apply_vectors(const arma::field<arma::Mat<fltp> > &R, arma::field<arma::Mat<fltp> > &V, const char str) const override;
			virtual void apply_coords(arma::field<arma::Mat<fltp> > &R) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
