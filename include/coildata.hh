// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_COIL_HH
#define MDL_CALC_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// common headers
#include "rat/common/elements.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// multipole method headers
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/currentmesh.hh"

//#include "coilsurface.hh"
#include "meshdata.hh"
#include "coilsurfacedata.hh"
#include "vtkunstr.hh"
#include "vtktable.hh"
#include "data.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CoilData> ShCoilDataPr;
	typedef arma::field<ShCoilDataPr> ShCoilDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class CoilData: public MeshData, public rat::fmm::CurrentSources{
		// properties
		protected:
			// operating current
			fltp operating_current_;
			
			// number of turns
			fltp number_turns_;

			// number of gauss points to use for current sources
			bool use_volume_elements_ = false;

			// number of gauss points
			arma::sword num_gauss_ = 1;

			// softening factor
			fltp softening_ = 1.0;

			// current elements cross sections
			arma::Row<fltp> Acrss_;

		// methods
		public:
			// default constructor
			CoilData();
			CoilData(const ShFramePr &frame, const ShAreaPr &area);

			// destructor
			virtual ~CoilData(){};

			// factory
			static ShCoilDataPr create();
			static ShCoilDataPr create(const ShFramePr &frame, const ShAreaPr &area);

			// create surface
			ShSurfaceDataPr create_surface() const override;

			// setup sources and targets
			void setup_sources() override;

			// set/get operating current
			fltp calc_current_density() const;

			// set softening
			void set_softening(const fltp softening);
			void set_use_volume_elements(const bool use_volume_elements);
			void set_num_gauss(const arma::sword num_gauss);

			// operating current
			void set_operating_current(const fltp operating_current);
			fltp get_operating_current() const override;

			// number of turns
			void set_number_turns(const fltp number_turns);
			fltp get_number_turns() const override;

			// calculate enclosed flux
			fltp calculate_flux() const override;
			fltp calculate_element_self_inductance() const override;

			// VTK export
			ShVTKObjPr export_vtk() const override;

			// calculate separate properties
			ShVTKTablePr export_line_elements() const;

			// calculation of properties at nodes
			arma::Row<fltp> calc_magnetic_field_angle()const;
			arma::Row<fltp> calc_critical_current_density()const;
			arma::Mat<fltp> calc_nodal_current_density()const;
			arma::Row<fltp> calc_critical_temperature() const;
			arma::Row<fltp> calc_temperature_margin() const;
			arma::Mat<fltp> calc_electric_field() const;
			arma::Row<fltp> calc_power_density() const;
			arma::Row<fltp> calc_loadline_fraction() const;
			arma::Row<fltp> calc_critical_current_fraction() const;
			arma::Mat<fltp> calc_pressure()const;
			arma::Row<fltp> calc_normal_pressure()const;
			arma::Row<fltp> calc_transverse_pressure()const;
			arma::Row<fltp> calc_pressure(const arma::uword int_dir)const;
			arma::Row<fltp> calc_von_mises()const;

			// calculate integrated Lorentz forces
			virtual arma::Col<fltp>::fixed<3> calc_lorentz_force() const override;
			virtual arma::Mat<fltp> calc_force_density() const override;

	};

}}

#endif
