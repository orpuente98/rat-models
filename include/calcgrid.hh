// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_GRID_HH
#define MDL_CALC_GRID_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calcleaf.hh"
#include "griddata.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcGrid> ShCalcGridPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcGrid: public CalcLeaf{
		// properties
		protected:
			// position 
			arma::Col<fltp>::fixed<3> offset_ = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// dimensions
			fltp size_x_ = RAT_CONST(0.3);
			fltp size_y_ = RAT_CONST(0.2);
			fltp size_z_ = RAT_CONST(0.2);

			// number of points
			arma::uword num_x_ = 50;
			arma::uword num_y_ = 50;
			arma::uword num_z_ = 50;

			// enable visibility
			bool visibility_ = true;

		// methods
		public:
			// constructor
			CalcGrid();
			explicit CalcGrid(const ShModelPr &model);
			CalcGrid(
				const ShModelPr &model, 
				const fltp size_x, 
				const fltp size_y, 
				const fltp size_z, 
				const arma::uword num_x, 
				const arma::uword num_y, 
				const arma::uword num_z);

			// factory methods
			static ShCalcGridPr create();
			static ShCalcGridPr create(const ShModelPr &model);
			static ShCalcGridPr create(
				const ShModelPr &model, 
				const fltp size_x, 
				const fltp size_y, 
				const fltp size_z, 
				const arma::uword num_x, 
				const arma::uword num_y, 
				const arma::uword num_z);

			// set properties
			void set_offset(const arma::Col<fltp>::fixed<3> &offset);
			void set_size_x(const fltp ell1);
			void set_size_y(const fltp ell2);
			void set_size_z(const fltp ell2);
			void set_num_x(const arma::uword num_x);
			void set_num_y(const arma::uword num_y);
			void set_num_z(const arma::uword num_z);
			void set_visibility(const bool visibility);

			// getters
			arma::Col<fltp>::fixed<3> get_offset() const;
			fltp get_size_x()const;
			fltp get_size_y()const;
			fltp get_size_z()const;
			arma::uword get_num_x()const;
			arma::uword get_num_y()const;
			arma::uword get_num_z()const;
			bool get_visibility()const;

			// calculate with inductance data output
			ShGridDataPr calculate_grid(
				const fltp time, 
				const cmn::ShLogPr &lg);

			// generalized calculation
			std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg) override;

			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
