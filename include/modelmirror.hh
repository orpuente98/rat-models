// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_MIRROR_HH
#define MDL_MODEL_MIRROR_HH

#include <armadillo> 
#include <memory>

#include "modelgroup.hh"
#include "transreflect.hh"
#include "serializer.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelMirror> ShModelMirrorPr;

	// a collection of coils
	class ModelMirror: public ModelGroup, public TransReflect{
		// set coils
		protected:
			// keep original
			bool keep_original_ = true;

			// anti-mirror
			bool anti_mirror_ = false;

		// methods
		public:
			// constructor
			ModelMirror();
			explicit ModelMirror(
				const ShModelPr &model, 
				const bool keep_original = true);

			// factory methods
			static ShModelMirrorPr create();
			static ShModelMirrorPr create(
				const ShModelPr &model, 
				const bool keep_original = true);

			// keep original
			void set_keep_original(const bool keep_original = true);
			bool get_keep_original() const;
			void set_anti_mirror(const bool anti_mirror = true);
			bool get_anti_mirror() const;

			// splitting the modelgroup
			std::list<ShModelPr> split(
				const ShSerializerPr &slzr = Serializer::create()) const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes_core(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
