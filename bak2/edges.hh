// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EDGES_HH
#define MDL_EDGES_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"

#include "trans.hh"
#include "nameable.hh"
#include "frame.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Edges> ShEdgesPr;
	typedef arma::field<ShEdgesPr> ShEdgesPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class Edges: public Nameable{
		// properties
		protected:
			// store frame
			ShFramePr gen_;

			// store area
			ShAreaPr area_;

			// edge matrix
			arma::field<arma::Mat<fltp> > Re_;

			// section and turn
			arma::Row<arma::uword> section_;
			arma::Row<arma::uword> turn_;

			// temperature
			fltp operating_temperature_;

		// methods
		public:
			// constructor
			Edges();
			Edges(ShFramePr &frame, ShAreaPr &area);

			// destructor
			virtual ~Edges(){};

			// factory
			static ShEdgesPr create();
			static ShEdgesPr create(ShFramePr &frame, ShAreaPr &area);

			// setup function
			void setup(ShFramePr &frame, ShAreaPr &area);

			// get stored edge matrix
			arma::field<arma::Mat<fltp> > get_edges() const;

			// operating temperature
			void set_operating_temperature(const fltp operating_temperature);
			fltp get_operating_temperature() const;

			// setting coil properties
			void set_area_crss(const fltp area_crss);

			// calculate edge length
			arma::Mat<fltp> calc_ell() const;

			// transformations
			void apply_transformations(const ShTransPrList &trans);
			void apply_transformation(const ShTransPr &trans);

			// construct edge matrices
			void create_xyz(arma::field<arma::Mat<fltp> > &x, arma::field<arma::Mat<fltp> > &y, arma::field<arma::Mat<fltp> > &z) const;

			// export edges to freecad file
			virtual void export_freecad(cmn::ShFreeCADPr freecad) const;
			virtual void export_opera(cmn::ShOperaPr opera) const;
	};

}}

#endif
