// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// get type
	std::string Calc::get_type(){
		return "rat::mdl::calc";
	}


	// set data directory
	void Calc::set_output_dir(const boost::filesystem::path &output_dir){
		output_dir_ = output_dir;
	}

	// set data filename
	void Calc::set_output_fname(const std::string &output_fname){
		output_fname_ = output_fname;
	}


	// set output times
	void Calc::set_output_times(const arma::Row<double> &output_times){
		if(output_times.is_empty())rat_throw_line("output times array is empty");
		output_times_ = output_times;
	}

	// get output times
	arma::Row<double> Calc::get_output_times() const{
		return output_times_;
	}

	// set output type
	void Calc::set_output_type(const CalcOutTypes type){
		output_types_.clear(); 
		add_output_type(type);
	}

	// add output type
	void Calc::add_output_type(const CalcOutTypes type){
		// check for all types
		if(type==ALL){
			output_types_.clear();
			for(int inttype=0;inttype<CalcOutTypes::ALL;inttype++)
				add_output_type((CalcOutTypes)inttype);
			return;
		}

		// check if the output type is already on the list
		std::list<CalcOutTypes>::const_iterator it = 
			std::find(output_types_.begin(), output_types_.end(), type);

		// add it to the list
		if(it == output_types_.end())
			output_types_.push_back(type);
	}

	// display calculation settings
	void Calc::display_settings(cmn::ShLogPr lg) const{
		lg->msg(2, "%soutput settings%s\n",KBLU,KNRM);
		lg->msg("directory: %s%s%s\n",KYEL,output_dir_.c_str(),KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,output_fname_.c_str(),KNRM);
		lg->msg("time span: %s%8.2e - %8.2e [s] (%llu steps)%s\n",KYEL,
			output_times_(0),output_times_(output_times_.n_elem-1),output_times_.n_elem,KNRM);
		lg->msg(-2,"\n");
	}

	// calculate and write
	void Calc::calculate_and_write(cmn::ShLogPr lg){
		calculate(lg); write(lg);
	}

	// method for serialization into json
	void Calc::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize fieldmap
		Nameable::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["output_dir"] = output_dir_.string();
		js["output_fname"] = output_fname_;
		js["output_times"] = Node::serialize_arma(output_times_);
		
		// walk over output types
		for(std::list<CalcOutTypes>::const_iterator it = output_types_.begin(); it!=output_types_.end(); it++)
			js["output_types"].append((int)*it);

		// properties
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void Calc::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// serialize fieldmap
		Nameable::deserialize(js,list,factory_list,pth);

		// data directory and model name
		output_dir_ = js["output_dir"].asString();
		output_fname_ = js["output_fname"].asString();
		output_times_ = Node::deserialize_arma(js["output_times"]);
		arma::uword num_types = js["output_types"].size();
		for(arma::uword i=0;i<num_types;i++)
			add_output_type((CalcOutTypes)js["output_types"].get(i,0).asInt());
	}


}}