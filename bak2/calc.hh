// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_HH
#define MDL_CALC_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"
#include "rat/common/node.hh"

#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Calc> ShCalcPr;
	typedef arma::field<ShCalcPr> ShCalcPrList;

	// output types
	enum CalcOutTypes{
		ORIENTATION, MAGNETIC_FLUX, MAGNETISATION, VECTOR_POTENTIAL, 
		MAGNETIC_FIELD, CURRENT_DENSITY, TEMPERATURE, FIELD_ANGLE,
		ENG_CURRENT_DENSITY, CRIT_TEMPERATURE, FORCE_DENSITY, 
		PCT_LOAD, ELECTRIC_FIELD, ALL
	};

	// template for coil
	class Calc: public Nameable{
		// output settings
		protected:
			// data directory
			boost::filesystem::path output_dir_;
			std::string output_fname_ = "meshdata";

			// list of output times (by default only zero)
			arma::Row<double> output_times_ = {0};

			// list of output data types (better implemented with std::set)
			std::list<CalcOutTypes> output_types_ = {VECTOR_POTENTIAL, MAGNETIC_FLUX};

		// methods
		public:
			// virtual destructor
			virtual ~Calc(){};

			// set data directory
			void set_output_dir(const boost::filesystem::path &output_dir);
			void set_output_fname(const std::string &output_fname);

			// set output times
			void set_output_times(const arma::Row<double> &output_times);
			arma::Row<double> get_output_times() const;

			// set type for writing to output
			void set_output_type(const CalcOutTypes type);
			void add_output_type(const CalcOutTypes type);

			// settings display function
			void display_settings(cmn::ShLogPr lg) const;

			// virtual functions
			virtual void calculate_and_write(cmn::ShLogPr lg = cmn::NullLog::create());
			virtual void calculate(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;
			virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) const override;
	};

}}

#endif
