// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_SURFACE_HH
#define MDL_MODEL_SURFACE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/defines.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/parfor.hh"

#include "rat/mlfmm/mgntargets.hh"

#include "mesh.hh"
#include "surface.hh"
#include "vtkunstr.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcSurface> ShCalcSurfacePr;
	typedef arma::field<ShCalcSurfacePr> ShCalcSurfacePrList;

	// template for coil
	class CalcSurface: public CalcFieldMap{		
		protected:
			// target model
			ShModelPr target_model_;

			// elements
			arma::field<arma::Mat<arma::uword> > s_;

			// list of mesh geometries
			ShSurfacePrList surfaces_;

			// number of coils
			arma::uword num_objects_;

			// number of nodes and elements in each object
			arma::Row<arma::uword> num_nodes_object_;
			arma::Row<arma::uword> num_elements_object_;
			
			// total number of nodes and elements
			arma::uword num_nodes_;
			arma::uword num_elements_;

			// indexing into the separate objects
			arma::Row<arma::uword> idx_nodes_;
			arma::Row<arma::uword> idx_elem_;

		// methods
		public:
			// constructor	
			CalcSurface();
			CalcSurface(ShModelPr source_model, ShModelPr target_model);

			// factory
			static ShCalcSurfacePr create();
			static ShCalcSurfacePr create(ShModelPr source_model, ShModelPr target_model);

			// get properties from coils
			arma::Mat<double> get_nodal_current_density() const;
			arma::Row<double> get_nodal_temperature() const;

			// calculation of properties
			arma::Row<double> calc_Tc(const arma::Mat<double> &Bn, const arma::Mat<double> &Jn, const bool use_parallel = true) const;
			arma::Row<double> calc_alpha(const arma::Mat<double> &Bn) const;
			arma::Row<double> calc_Je(const arma::Mat<double> &Bn, const arma::Mat<double> &Tn, const bool use_parallel = true) const;
			arma::Mat<double> calc_E(const arma::Mat<double> &Bn, const arma::Mat<double> &Jn, const arma::Mat<double> &Tn, const bool use_parallel = true) const;

			// get properties from coils
			arma::Mat<double> get_longitudinal() const;
			arma::Mat<double> get_transverse() const;
			arma::Mat<double> get_normal() const;

			// setting the model
			void set_target_model(ShModelPr target_model);

			// setup function
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;
		
			// vtk export
			ShVTKUnstrPr export_vtk(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// gmsh file export
			// void export_gmsh(cmn::ShGmshFilePr gmsh) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
