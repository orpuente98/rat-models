// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelSolenoid::ModelSolenoid(){

	}

	// constructor
	ModelSolenoid::ModelSolenoid(ShPathPr base, ShCrossPr crss, rat::mat::ShConductorPr material){
		set_base(base); set_cross(crss); set_material(material);
	}


	// factory
	ShModelSolenoidPr ModelSolenoid::create(){
		return std::make_shared<ModelSolenoid>();
	}

	// factory immediately setting base and cross section
	ShModelSolenoidPr ModelSolenoid::create(ShPathPr base, ShCrossPr crss, rat::mat::ShConductorPr material){
		return std::make_shared<ModelSolenoid>(base,crss,material);
	}

	// factory for mesh objects
	ShCalcMeshPrList ModelSolenoid::create_meshes(const fltp time) const{
		// allocate mesh data
		ShCalcCoilPr mesh = CalcCoil::create();

		// fill mesh data
		setup_mesh(mesh, time);

		// check input
		assert(current_drive_!=NULL);

		// calculate current with drive scaling
		const fltp coil_current = current_drive_->get_scaling(time)*operating_current_;

		// copy properties
		mesh->set_material(get_material());
		mesh->set_operating_current(coil_current);
		mesh->set_number_turns(number_turns_);
		mesh->set_enable_current_sharing(enable_current_sharing_);
		mesh->set_softening(softening_);
		mesh->set_num_gauss(num_gauss_);

		// in case of volume elements
		if(use_volume_elements_)rat_throw_line("not yet implemented");

		// combine into list
		ShCalcMeshPrList meshes(1);
		meshes(0) = mesh;

		// return mesh data
		return meshes;
	}

	// factory for edge objects
	ShEdgesPrList ModelSolenoid::create_edge(const fltp time) const{
		// allocate edge data
		ShEdgesCoilPr edge = EdgesCoil::create();

		// calculate current with drive scaling
		const fltp coil_current = current_drive_->get_scaling(time)*operating_current_;

		// fill edge data
		setup_edge(edge, time);

		// copy properties
		edge->set_material(get_material());
		edge->set_operating_current(coil_current);
		edge->set_number_turns(number_turns_);

		// combine into list
		ShEdgesPrList edges(1);
		edges(0) = edge;

		// return edge data
		return edges;
	}

	// get type
	std::string ModelSolenoid::get_type(){
		return "rat::mdl::modelsolenoid";
	}

	// method for serialization into json
	void ModelSolenoid::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelMesh::serialize(js,list);

		// current drive
		js["current_drive"] = cmn::Node::serialize_node(current_drive_, list);

		// properties
		js["type"] = get_type();
		// js["num_gauss"] = (int)num_gauss_;
		js["enable_current_sharing"] = enable_current_sharing_;
		js["softening"] = softening_;
		js["number_turns"] = number_turns_;
		js["operating_current"] = operating_current_;
		js["num_gauss"] = (int)num_gauss_;

		// subnodes
		//js["circ"] = cmn::Node::serialize_node(circ_, list);
		js["material"] = cmn::Node::serialize_node(material_, list);
	}

	// method for deserialisation from json
	void ModelSolenoid::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		ModelMesh::deserialize(js,list,factory_list,pth);

		// current drive
		set_current_drive(cmn::Node::deserialize_node<Drive>(js["current_drive"], list, factory_list, pth));

		// material
		set_material(cmn::Node::deserialize_node<rat::mat::Conductor>(js["material"], list, factory_list, pth));

		// properties
		// set_num_gauss(js["num_gauss"].asInt64());
		set_enable_current_sharing(js["enable_current_sharing"].asBool());
		set_softening(js["softening"].asDouble());
		set_number_turns(js["number_turns"].asDouble());
		set_operating_current(js["operating_current"].asDouble());
		set_num_gauss(js["num_gauss"].asInt64());	
	}


}}