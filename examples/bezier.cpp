// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for models
#include "pathbezier.hh"
#include "crossrectangle.hh"
#include "modelmesh.hh"
#include "calcmesh.hh"

// DESCRIPTION
// Example that shows how the bezier 
// spline path can be used to draw three
// dimensional coil shapes. Note that the 
// code provides not check for edge regression. 
// This means thet it is possible to generate 
// a horrible mess without errors or warnings.

// main
int main(){
	// INPUT SETTINGS
	// output directory
	boost::filesystem::path output_dir = "./bezier/";

	// cable settings
	const rat::fltp wcable = 12e-3; // width of the cable [m]
	const rat::fltp dcable = 1.2e-3; // thickness of the cable [m]

	// spline
	const rat::fltp ell_trans = 0e-3; // transition length [m]
	const rat::fltp element_size = 2e-3; // size of the elements [m]

	// The following design cases are provided
	// uncomment the one you wish to run
	// s-shape layer jump
	// const arma::Col<rat::fltp> R0 = {0,0,0}; // start point xyz [m]
	// const arma::Col<rat::fltp> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> N0 = {1,0,0}; // start normal vector xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> R1 = {40e-3,40e-3,14e-3}; // end point xyz [m]
	// const arma::Col<rat::fltp> L1 = {0,1,0}; // end direction xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> N1 = {1,0,0}; // end normal vector xyz [m] (must be normalized)
	// const rat::fltp str1 = 30e-3;
	// const rat::fltp str2 = 10e-3;
	// const rat::fltp str3 = -10e-3;
	// const rat::fltp str4 = 10e-3;
	// const rat::fltp offset = 10e-3;

	// cosine theta coil end
	// const arma::Col<rat::fltp> R0 = {-25e-3,0,0}; // start point xyz [m]
	// const arma::Col<rat::fltp> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> N0 = {0,0,-1}; // start normal vector xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> R1 = {25e-3,0,0}; // end point xyz [m]
	// const arma::Col<rat::fltp> L1 = {0,-1,0}; // end direction xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> N1 = {0,0,-1}; // end normal vector xyz [m] (must be normalized)
	// const rat::fltp str1 = 40e-3; // [m]
	// const rat::fltp str2 = 10e-3; // [m]
	// const rat::fltp str3 = 15e-3; // [m]
	// const rat::fltp str4 = 15e-3; // [m]
	// const rat::fltp offset = 0;  // [m]

	// racetrack coil layer jump
	// const arma::Col<rat::fltp> R0 = {0,0,0}; // start point xyz [m]
	// const arma::Col<rat::fltp> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> N0 = {1,0,0}; // start normal vector xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> R1 = {40e-3,120e-3,20e-3}; // end point xyz [m]
	// const arma::Col<rat::fltp> L1 = {0,1,0}; // end direction xyz [m] (must be normalized)
	// const arma::Col<rat::fltp> N1 = {-1,0,0}; // end normal vector xyz [m] (must be normalized)
	// const rat::fltp str1 = 10e-3; // [m]
	// const rat::fltp str2 = 10e-3; // [m]
	// const rat::fltp str3 = 2e-3; // [m]
	// const rat::fltp str4 = 2e-3; // [m]
	// const rat::fltp offset = 0; // [m]

	// cloverleaf coil end
	const rat::fltp height = 20e-3; // bridge height [m]
	const arma::Col<rat::fltp>::fixed<3> R0 = {0,0,0}; // start point xyz [m]
	const arma::Col<rat::fltp>::fixed<3> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	const arma::Col<rat::fltp>::fixed<3> N0 = {1,0,0}; // start normal vector xyz [m] (must be normalized)
	const arma::Col<rat::fltp>::fixed<3> R1 = {0,0,height}; // end point xyz [m]
	const arma::Col<rat::fltp>::fixed<3> L1 = {-1,0,0}; // end direction xyz [m] (must be normalized)
	const arma::Col<rat::fltp>::fixed<3> N1 = {0,1,0}; // end normal vector xyz [m] (must be normalized)
	const rat::fltp str1 = 0.37*35e-3; // [m]
	const rat::fltp str2 = 0.37*35e-3; // [m]
	const rat::fltp str3 = 0.37*14e-3; // [m]
	const rat::fltp str4 = 0.37*14e-3; // [m]
	const rat::fltp offset = 0; // [m]

	// GEOMETRY SETUP
	// create unified path
	rat::mdl::ShPathBezierPr path_bezier = rat::mdl::PathBezier::create(
		R0,L0,N0, R1,L1,N1, str1,str2,str3,str4, ell_trans,element_size,offset);
	path_bezier->set_num_sections(4);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelMeshPr coil = rat::mdl::ModelMesh::create(path_bezier, cross_rect);

	// create mesh calculatoin
	const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil);

	// calculate and write vtk output files to specified directory
	mesh->calculate_write({0},"./bezier/",rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT));
}