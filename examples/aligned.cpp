// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for materials
#include "rat/mat/parallelconductor.hh"
#include "rat/mat/database.hh"

// header files for Common
#include "rat/common/log.hh"
#include "rat/common/gmshfile.hh"

// header files for models
#include "pathprofile.hh"
#include "pathstraight.hh"
#include "patharc.hh"
#include "pathgroup.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "pathcable.hh"
#include "pathconnect.hh"
#include "griddata.hh"
#include "meshdata.hh"
#include "calcmlfmm.hh"

// DESCRIPTION
// This is an example on how to create an alligned block coil 
// similar to the to Feather-M2 (CERN) coil. It uses the PathProfile
// class to merge a top view and a side view of the magnet into
// one single path.

// main
int main(){
	// SETTINGS
	// calculation settings
	const bool run_grid = true;
	const bool run_coil_field = true; // calculate field on coil

	// data storage directory and filename
	const std::string output_dir = "./aligned/";

	// general coil geometry
	const rat::fltp element_size = 2e-3; // size of the elements [m]
	const arma::uword num_turns1 = 9; // number of turns on central deck
	const arma::uword num_turns2 = 5; // number of turns on wing deck
	const rat::fltp ashear1 = 0.5*2*arma::Datum<rat::fltp>::pi/360; // central deck shear angle [rad]
	const rat::fltp ashear2 = 7.0*2*arma::Datum<rat::fltp>::pi/360; // wing deck shear angle [rad]
	const rat::fltp dspace = 1e-3; // spacing between coils [m]
	const rat::fltp dmid = 2e-3; // spacing between coils on midplane [m]

	// side profile settings
	const rat::fltp ellstr1 = 0.1; // straight section length [m]
	const rat::fltp ellstr2 = 0.5; // length of coil-end profile [m] (not magnet length)
	const rat::fltp aend = 4.0*2*arma::Datum<rat::fltp>::pi/360; // max slope angle at coil end [rad]
	const rat::fltp Rhard = 2.0; // hardway bending radius [m]

	// top profile settings
	const rat::fltp bend = 2.0*2*arma::Datum<rat::fltp>::pi/360; // max diamond shape cangle at coil end [rad]
	const rat::fltp Rsoft = 0.016; // softway bending radius [m]
	const rat::fltp Rmid = 0.2; // midway bending radius [m]
	const rat::fltp ell1 = 0.2; // length of straight bit [m]
	const rat::fltp ell2 = 0.25; // length of end [m]

	// cable settings
	const rat::fltp wcable = 12e-3; // width of cable [m]
	const rat::fltp dcable = 1.2e-3; // thickness of cable [m]
	const rat::fltp dinsu = 0.1e-3; // thickness of insulation [m]

	// conductor settings
	const rat::fltp wtape = 5.9e-3; // width of Roebel punched tapes in cable [m]
	const rat::fltp dtape = 0.15e-3; // thickness of each tape [m]
	// const rat::fltp dsc = 1e-6; // superconductor thickness [m]
	// const rat::fltp dcu = 40e-6; // copper thickness [m]
	const arma::uword num_tapes = 13; // number of tapes in cable
	
	// operating conditions
	const rat::fltp Iop = 12000; // operating current [A]
	const rat::fltp Top = 4.5;	// operating temperature [K]

	// background field in z
	const arma::Col<rat::fltp>::fixed<3> Bbg = {0,0,RAT_CONST(13.0)};



	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);


	// GEOMETRY SETUP
	// calculate remaining properties
	// conductor filling fraction
	const rat::fltp ffill = wtape*dtape*num_tapes/(dcable*wcable);

	// calculate width of coil
	const rat::fltp w1 = Rsoft*std::sin(arma::Datum<rat::fltp>::pi/2 - bend) + 
		std::sin(bend)*ell2 + (1.0-std::cos(bend))*Rmid;

	// calculate end radius of second coil
	const rat::fltp Rsoft2 = w1 - (1.0-std::cos(bend))*Rmid + std::sin(ashear1)*wcable;

	// create side profile
	rat::mdl::ShPathGroupPr side = rat::mdl::PathGroup::create();
	side->set_align_frame(false);

	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	// construct conductor
	rat::mat::ShParallelConductorPr roebel_cable = rat::mat::ParallelConductor::create();;
	roebel_cable->add_conductor(ffill, rat::mat::Database::rebco_fujikura_tape_cern());


	// add sections
	{
		// start point
		const arma::Col<rat::fltp>::fixed<3> R0 = {0,0,0};
		const arma::Col<rat::fltp>::fixed<3> L0 = {0,1,0};
		const arma::Col<rat::fltp>::fixed<3> N0 = {1,0,0};
		// const arma::Col<rat::fltp>::fixed<3> D0 = {0,0,1};

		// side yz-profile	
		rat::mdl::ShPathGroupPr sideA = rat::mdl::PathGroup::create(R0,L0,N0);

		// straight bit
		sideA->add_path(rat::mdl::PathStraight::create(ellstr1,element_size));
		
		// upward arc
		rat::mdl::ShPathArcPr arc = rat::mdl::PathArc::create(Rhard,-aend,element_size);
		arc->set_transverse(true); // enable hardway bending
		sideA->add_path(arc);

		// straight 
		sideA->add_path(rat::mdl::PathStraight::create(ellstr2,element_size));
		
		// create mirror for other side y<0
		rat::mdl::ShPathGroupPr sideB = rat::mdl::PathGroup::create();
		sideB->add_path(sideA);
		sideB->add_rotation(0,0,arma::Datum<rat::fltp>::pi);
		
		// reverse this side
		sideB->add_reverse();
		sideB->add_flip();

		// combine sides
		side->add_path(sideB);
		side->add_path(sideA);
	}

	// create top profile of first coil
	rat::mdl::ShPathGroupPr top1 = rat::mdl::PathGroup::create();
	top1->add_translation(w1,0,0);

	// add sections to top profile
	// this is the diamond shape of the coil
	{
		// create unique sections
		rat::mdl::ShPathStraightPr str1 = rat::mdl::PathStraight::create(ell1/2,element_size);
		rat::mdl::ShPathArcPr arc1 = rat::mdl::PathArc::create(Rmid, bend, element_size);
		rat::mdl::ShPathStraightPr str2 = rat::mdl::PathStraight::create(ell2,element_size);
		rat::mdl::ShPathArcPr arc2 = rat::mdl::PathArc::create(Rsoft, arma::Datum<rat::fltp>::pi/2 - 
			bend, element_size,num_turns1*(dcable+2*dinsu));

		// add sections to path
		top1->add_path(str1); top1->add_path(arc1); top1->add_path(str2); top1->add_path(arc2);
		top1->add_path(arc2); top1->add_path(str2); top1->add_path(arc1); top1->add_path(str1);
		top1->add_path(str1); top1->add_path(arc1); top1->add_path(str2); top1->add_path(arc2);
		top1->add_path(arc2); top1->add_path(str2); top1->add_path(arc1); top1->add_path(str1);
	}
	
	// create top profile of second coil
	rat::mdl::ShPathGroupPr top2 = rat::mdl::PathGroup::create();
	top2->add_translation(Rsoft2,0,0);

	// add sections to top profile
	{
		// create unique sections
		rat::mdl::ShPathStraightPr str1 = rat::mdl::PathStraight::create(ell1/2,element_size);
		rat::mdl::ShPathArcPr arc1 = rat::mdl::PathArc::create(Rmid, bend, element_size);
		rat::mdl::ShPathArcPr arc2 = rat::mdl::PathArc::create(Rsoft2, arma::Datum<rat::fltp>::pi/2 - bend, 
			element_size,num_turns2*(dcable+2*dinsu));

		// add sections to path
		top2->add_path(str1); top2->add_path(arc1); top2->add_path(arc2);
		top2->add_path(arc2); top2->add_path(arc1); top2->add_path(str1);
		top2->add_path(str1); top2->add_path(arc1); top2->add_path(arc2);
		top2->add_path(arc2); top2->add_path(arc1); top2->add_path(str1);
	}

	// create coil path by combining top and side profiles
	rat::mdl::ShPathProfilePr profile1 = rat::mdl::PathProfile::create(top1,side);
	profile1->set_shearing(ashear1,0.7,ell1/2 + Rmid*std::cos(bend) + ell2);
	profile1->add_translation(0,0,dmid/2);

	// create coil path by combining top and side profiles
	rat::mdl::ShPathProfilePr profile2 = rat::mdl::PathProfile::create(top2,side);
	profile2->set_shearing(ashear2,0.3,ell1/2 + Rsoft2 + 0.012);
	profile2->add_translation(0,0,dmid/2 + wcable + dspace);

	// create cable for central deck
	rat::mdl::ShPathCablePr path_cable1 = rat::mdl::PathCable::create(profile1);
	path_cable1->set_turn_step(dcable + 2*dinsu);
	path_cable1->set_num_turns(num_turns1);
	path_cable1->set_idx_incr(2);
	path_cable1->set_idx_start(1);
	path_cable1->set_num_add(2);
	path_cable1->set_reverse_base(true);

	// create cable for wing deck
	rat::mdl::ShPathCablePr path_cable2 = rat::mdl::PathCable::create(profile2);
	path_cable2->set_turn_step(dcable + 2*dinsu);
	path_cable2->set_num_turns(num_turns2);
	path_cable2->set_idx_incr(3);
	path_cable2->set_idx_start(1);
	path_cable2->set_num_add(7);

	// connect paths with a layer jump
	rat::mdl::ShPathConnectPr pole_path = rat::mdl::PathConnect::create(
		path_cable1,path_cable2,70e-3,70e-3,0,0,10e-3,element_size);

	// create cable cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,0,wcable,dcable+2*dinsu);

	// create coil
	rat::mdl::ShModelCoilPr pole1 = rat::mdl::ModelCoil::create(pole_path, cross_rect, roebel_cable);
	pole1->set_operating_temperature(Top);
	pole1->set_operating_current(Iop);

	// create second magnet pole
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create();
	pole2->add_model(pole1);
	pole2->add_rotation(0,1,0,arma::Datum<rat::fltp>::pi);
	pole2->add_reverse();
	pole2->add_flip();

	// create coil model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(pole1); model->add_model(pole2);

	// create background field
	rat::mdl::ShBackgroundPr bg = rat::mdl::Background::create(Bbg);
	

	// CALCULATION AND OUTPUT
	// create calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model);
	mlfmm->set_target_meshes(run_coil_field); 
	mlfmm->set_target_grid(run_grid);
	mlfmm->set_background(bg);
	
	// calculate everything
	mlfmm->calculate_write({0},output_dir,lg);

}