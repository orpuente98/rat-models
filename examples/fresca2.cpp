// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Rat-Common
#include "rat/common/gmshfile.hh"

// materials headers
#include "rat/mat/database.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "pathrectangle.hh"
#include "transbend.hh"
#include "modelgroup.hh"
#include "pathflared.hh"
#include "meshdata.hh"
#include "griddata.hh"
#include "background.hh"
#include "calcinductance.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "calcplane.hh"


// DESCRIPTION
// FRESCA2 is a large aperture Nb3Sn coil developed 
// at TE-MSC-MDT CERN. This example shows how to produce
// a model of its coils with fx-models. Note that this 
// demonstration model was derived from papers, 
// presentations and an Opera model and is by 
// no means the official version. 

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate magnetic field on the coil
	const bool run_inductance = true; // calculate coil inductance and stored energy
	const bool run_grid = true;
	const bool run_json_export = true; // export geometry to json file
	const bool run_plane = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./fresca2/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// coil geometry settings
	const rat::fltp wcable = RAT_CONST(21.8e-3); // width of the cable [m] (incl insulation)
	// const rat::fltp dcable = RAT_CONST(0.002334); // thickness of cable [m] (incl insulation)
	const rat::fltp radius1 = RAT_CONST(0.7679); // hardway radius of flared end [m]
	const rat::fltp radius21 = RAT_CONST(0.058); // radius at end of the central coils [m]
	const rat::fltp radius22 = RAT_CONST(0.04468); // radius at end of the outer coils [m]
	const rat::fltp element_size = RAT_CONST(6e-3); // size of the elements [m]
	const rat::fltp dcoil1 = RAT_CONST(0.084024);	// thickness of coil pack for first set of layers [m]
	const rat::fltp dcoil2 = RAT_CONST(0.098028); // thickness of coil pack for second set of layers [m]
	const rat::fltp ell1 = 2*RAT_CONST(0.3640615); // straight section length [m]
	const rat::fltp ell21 = RAT_CONST(0.024); // straight section length between bend and coil-end for central coils [m]
	const rat::fltp ell22 = RAT_CONST(0.032); // straight section length between bend and coil-end for outer coils [m]
	const rat::fltp arcl = RAT_CONST(17.0)*2*arma::Datum<rat::fltp>::pi/360; // flared end angle [rad]
	const rat::fltp dspace1 = RAT_CONST(0.5e-3); // spacing between the single pancakes [m]
	const rat::fltp dspace2 = RAT_CONST(1.5e-3); // spacing between the rat::fltp pancakes [m]
	const rat::fltp operating_current = RAT_CONST(10900.0); // operating current [A] (12T)
	// const rat::fltp operating_current = 12400; // operating current [A] (15T)
	const rat::fltp operating_temperature = RAT_CONST(1.9); // operating temperature [K]
	const rat::fltp num_turns1 = RAT_CONST(36.0); // number of turns in the central coils
	const rat::fltp num_turns2 = RAT_CONST(42.0); // number of turns in the outer coils
	// const rat::fltp Bzbg = RAT_CONST(1.0); // extra background field to simulate iron [T]


	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShConductorPr nb3sn_cable = 
		rat::mat::Database::nb3sn_fresca2_cable(true);

	// create flared end path
	rat::mdl::ShPathFlaredPr pathflared1 = rat::mdl::PathFlared::create(
		ell1,ell21,-arcl,radius1,
		radius21,element_size,dcoil1,-wcable);
	rat::mdl::ShPathFlaredPr pathflared2 = rat::mdl::PathFlared::create(
		ell1,ell21,-arcl,radius1-wcable-dspace1,
		radius21,element_size,dcoil1,-2*wcable-dspace1);
	rat::mdl::ShPathFlaredPr pathflared3 = rat::mdl::PathFlared::create(
		ell1,ell22,-arcl,radius1-2*wcable-dspace1-dspace2,
		radius22,element_size,dcoil2,-3*wcable-dspace1-dspace2);
	rat::mdl::ShPathFlaredPr pathflared4 = rat::mdl::PathFlared::create(
		ell1,ell22,-arcl,radius1-3*wcable-2*dspace1-dspace2,
		radius22,element_size,dcoil2,-4*wcable-2*dspace1-dspace2);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect1 = rat::mdl::CrossRectangle::create(0,dcoil1,-wcable,0,element_size);
	rat::mdl::ShCrossRectanglePr cross_rect2 = rat::mdl::CrossRectangle::create(0,dcoil2,-wcable,0,element_size);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(pathflared1, cross_rect1);
	coil1->set_name("l1");
	coil1->add_translation(0,0,0.0144 + wcable/2);
	coil1->set_number_turns(num_turns1);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);
	coil1->set_input_conductor(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(pathflared2, cross_rect1);
	coil2->set_name("l2");
	coil2->add_translation(0,0,0.0367 + wcable/2);
	coil2->set_number_turns(num_turns1);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);
	coil2->set_input_conductor(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil3 = rat::mdl::ModelCoil::create(pathflared3, cross_rect2);
	coil3->set_name("l3");
	coil3->add_translation(0,0,0.06 + wcable/2);
	coil3->set_number_turns(num_turns2);
	coil3->set_operating_current(operating_current);
	coil3->set_operating_temperature(operating_temperature);
	coil3->set_input_conductor(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil4 = rat::mdl::ModelCoil::create(pathflared4, cross_rect2);
	coil4->set_name("l4");
	coil4->add_translation(0,0,0.0823 + wcable/2);
	coil4->set_number_turns(num_turns2);
	coil4->set_operating_current(operating_current);
	coil4->set_operating_temperature(operating_temperature);
	coil4->set_input_conductor(nb3sn_cable);
	
	// magnet upper pole
	rat::mdl::ShModelGroupPr pole1 = rat::mdl::ModelGroup::create();
	pole1->set_name("p1");
	pole1->add_model(coil1); pole1->add_model(coil2);
	pole1->add_model(coil3); pole1->add_model(coil4);

	// magnet lower pole
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create();
	pole2->set_name("p2");
	pole2->add_model(coil1); pole2->add_model(coil2);
	pole2->add_model(coil3); pole2->add_model(coil4);
	pole2->add_rotation(1,0,0,arma::Datum<rat::fltp>::pi);
	pole2->add_reverse();

	// model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->set_name("f2");
	model->add_model(pole1); 
	model->add_model(pole2);

	// // background field
	// rat::mdl::ShBackgroundPr bg = rat::mdl::Background::create(
	// 	arma::Col<rat::fltp>::fixed<3>{0,0,Bzbg}/arma::Datum<rat::fltp>::mu_0);


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// background field
	const rat::mdl::ShBackgroundPr bg = rat::mdl::Background::create({0.0,0.0,1.0});

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);
		mesh->set_background(bg);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_grid){
		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(model,0.4,1.6,0.4,100,400,100);
		grid->set_background(bg);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_plane){
		// create grid calculation
		const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(model,'x',1.6,0.4,1200,300);
		plane->set_background(bg);

		// calculate and write vtk output files to specified directory
		plane->calculate_write({output_time},output_dir,lg);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(model);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write({output_time},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// write to output file
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

}



