// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcspharm.hh"

// main
int main(){
	// settings
	const rat::fltp tol = RAT_CONST(1e-2);

	// spherical harmonic input
	const rat::fltp reference_radius = RAT_CONST(20e-3);
	const arma::uword nmax = 23;
	const arma::uword mmax = 10;
	const arma::uword num_it = 200;

	// model geometric input parameters
	const rat::fltp radius = RAT_CONST(40e-3); // coil inner radius [m]
	const rat::fltp dcoil = RAT_CONST(10e-3); // thickness of the coil [m]
	const rat::fltp wcable = RAT_CONST(12e-3); // width of the cable [m]
	const rat::fltp delem = RAT_CONST(1e-3); // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const rat::fltp operating_current = RAT_CONST(200.0); // operating current in [A]
	const arma::uword num_turns = 100; // number of turns

	// calculation output settings
	const rat::fltp output_time = 0; // output time [s]

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a circular path object
	const rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(dcoil);

	// create a rectangular cross section object
	const rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, -wcable/2, wcable/2, delem);

	// create a coil object
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);

	// create calculation
	const rat::mdl::ShCalcSpHarmPr calc_spharm = rat::mdl::CalcSpHarm::create(coil,reference_radius,nmax,mmax,{0.01,0,0.01});

	// calculate everything
	const rat::mdl::ShSpHarmDataPr data = calc_spharm->calculate_spherical_harmonics({output_time},lg);
	const arma::Row<rat::fltp> Bzfmm = data->get_field('B').row(2).t();

	// figure for checking
	data->export_vtk()->write("spharm.vtu");

	// difference
	arma::Col<rat::fltp> dff(num_it);

	// now copy back and forth between field
	lg->msg(2,"%s%sCYCLES%s\n",KBLD,KGRN,KNRM);
	lg->msg(2,"%sSpharm2field - Field2Spharm%s\n",KBLU,KNRM);
	lg->msg("%2s %6s\n","it","diff");
	for(arma::uword i=0;i<num_it;i++){
		// compare fields
		const arma::Row<rat::fltp> Bzspharm = data->spharm2field();

		// get difference by comparing to original calculated field
		dff(i) = arma::max(arma::abs(Bzspharm - Bzfmm)/arma::abs(Bzfmm));

		// perform cycle field2harm
		// we ignore the Bx and By fields here
		// but they are not used for spharm analysis anyways
		data->allocate(); data->add_field('H',arma::join_vert(
			arma::Mat<rat::fltp>(2,Bzspharm.n_elem,arma::fill::zeros),
			Bzspharm/arma::Datum<rat::fltp>::mu_0)); data->post_process();

		// display difference
		lg->msg("%02llu %s%6.2e%s\n",i,dff(i)>tol ? KRED : KNRM, dff(i), KNRM);
	}

	// end
	lg->msg(-2,"\n");
	
}