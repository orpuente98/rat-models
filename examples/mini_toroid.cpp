// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// mlfmm headers
#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets.hh"
#include "rat/mlfmm/targets.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "meshdata.hh"
#include "modelgroup.hh"
#include "modeltoroid.hh"
#include "modelarray.hh"
#include "surfacedata.hh"
#include "griddata.hh"
#include "pathaxis.hh"
#include "linedata.hh"
#include "lengthdata.hh"
#include "vtkpvdata.hh"
#include "calcinductance.hh"
#include "calcpath.hh"
#include "calcmesh.hh"

// main function
int main(){
	// SETTINGS
	// switch board
	const bool run_coil_field = true;
	const bool run_inductance = true;
	const bool run_axis = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./mini_toroid/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// model geometric input parameters
	const rat::fltp radius = RAT_CONST(40e-3); // coil inner radius [m]
	const rat::fltp dcoil = RAT_CONST(10e-3); // thickness of the coil [m]
	const rat::fltp wcable = RAT_CONST(12e-3); // width of the cable [m]
	const rat::fltp delem = RAT_CONST(2e-3); // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const rat::fltp operating_current = RAT_CONST(200.0); // operating current in [A]
	const rat::fltp num_turns = RAT_CONST(100.0); // number of turns


	// MODEL
	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(dcoil);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, -wcable/2, wcable/2, delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(circle, rectangle);
	coil1->set_operating_current(operating_current);
	coil1->set_number_turns(num_turns);
	coil1->set_name("lm");

	// create reflection
	rat::mdl::ShModelToroidPr toroid = rat::mdl::ModelToroid::create(coil1);
	toroid->set_num_coils(6);
	toroid->set_radius(0.075);
	toroid->set_name("td");


	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(toroid);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// calculate field on axis
	if(run_axis){
		// axis of magnet for field quality calculation
		const rat::fltp ell = RAT_CONST(0.4); const rat::fltp dell = RAT_CONST(2e-3);
		const rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('x','z',ell,{0,0,0},dell);

		// create mesh calculatoin
		const rat::mdl::ShCalcPathPr pth = rat::mdl::CalcPath::create(toroid, axis);

		// calculate and write vtk output files to specified directory
		pth->calculate_write({output_time},output_dir,lg);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(toroid);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write({output_time},output_dir,lg);
	}

}
