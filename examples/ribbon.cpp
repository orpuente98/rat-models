// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for materials
#include "rat/mat/conductor.hh"

// header files for model
#include "pathcircle.hh"
#include "modelcoil.hh"
#include "crossline.hh"
#include "vtkunstr.hh"
#include "meshdata.hh"
#include "extra.hh"
#include "calcmesh.hh"
#include "calcplane.hh"
#include "calcinductance.hh"
#include "serializer.hh"

// main function
int main(){
	// SETTINGS
	// switchboard
	const bool run_coil_field = true;
	const bool run_inductance = true;
	const bool run_plane = true;

	// output file
	const boost::filesystem::path output_dir = "./ribbon/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// model geometric input parameters
	const rat::fltp radius = RAT_CONST(40e-3); // coil inner radius [m]
	const rat::fltp delem = RAT_CONST(2e-3); // element size [m]
	const rat::fltp w = RAT_CONST(12e-3);
	const arma::uword num_sections = 4; // number of coil sections
	const rat::fltp Iop = RAT_CONST(100.0);
	const rat::fltp thickness = RAT_CONST(1e-4);
	const rat::fltp num_turns = RAT_CONST(100.0);


	// MODEL
	// construct conductor
	rat::mat::ShConductorPr rebco_cu = 
		rat::mdl::Serializer::create<rat::mat::Conductor>(
		"json/materials/htstape/Fujikura_CERN.json");

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
		
	// create a rectangular cross section object
	rat::mdl::ShCrossLinePr line = rat::mdl::CrossLine::create(0,-w/2,0,w/2,thickness,delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, line);
	coil->set_number_turns(num_turns); coil->set_operating_current(Iop);
	coil->set_input_conductor(rebco_cu);
	coil->set_num_gauss(10);



	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_plane){
		// create grid calculation
		const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(coil,'z',0.1,0.1,800,800);

		// calculate and write vtk output files to specified directory
		plane->calculate_write({output_time},output_dir,lg);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(coil);

		// calculate and show matrix in terminal
		ind->calculate_write({output_time},output_dir,lg);
	}

}
