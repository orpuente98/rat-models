// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/serializer.hh"
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathequation.hh"
#include "calcmesh.hh"
#include "modelgroup.hh"

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_freecad = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./spiral/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// parameters
	const rat::fltp inner_radius = RAT_CONST(0.0025);
	const rat::fltp width = RAT_CONST(4e-3);
	const rat::fltp pitch = RAT_CONST(13e-3);
	const rat::fltp element_size = RAT_CONST(0.5e-3);
	const rat::fltp num_turns = RAT_CONST(10.0);
	const arma::uword num_tapes = 40;
	const rat::fltp dtape = RAT_CONST(0.1e-3);

	// temperature and current
	const rat::fltp Iop = RAT_CONST(1000.0);
	const rat::fltp Top = RAT_CONST(20.0);

	// both coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dtape,-width/2,width/2,element_size);

	// walk over layers
	for(arma::uword i=0;i<num_tapes;i++){
		// radius of this layer
		const rat::fltp radius = inner_radius + i*dtape;

		// calculate winding angle
		const rat::fltp alpha = std::atan(2*arma::Datum<rat::fltp>::pi*radius/pitch);

		// shape function
		// provided by TE for the inner shape of their coils
		rat::mdl::CoordFun spiral_fun = [=](
			arma::Mat<rat::fltp> &R, arma::Mat<rat::fltp> &L, arma::Mat<rat::fltp> &D, const arma::Row<rat::fltp> &t){

			// calculate in cylindrical
			const arma::Row<rat::fltp> theta = t*num_turns*2*arma::Datum<rat::fltp>::pi;

			// position vector
			R = arma::join_vert(radius*arma::cos(theta),radius*arma::sin(theta),pitch*theta/(2*arma::Datum<rat::fltp>::pi));

			// direction vector
			L = arma::join_vert(-radius*arma::sin(theta),radius*arma::cos(theta),
				arma::Row<rat::fltp>(theta.n_elem,arma::fill::ones)*pitch/(2*arma::Datum<rat::fltp>::pi));
			L.each_row() /= rat::cmn::Extra::vec_norm(L);

			// darboux (width) vector
			D = arma::join_vert(
				arma::Mat<rat::fltp>(2,theta.n_elem,arma::fill::zeros),
				arma::Mat<rat::fltp>(1,theta.n_elem,arma::fill::ones)/std::sin(alpha));
		};

		// create a spiral path
		rat::mdl::ShPathEquationPr path_spiral = rat::mdl::PathEquation::create(element_size, spiral_fun);
		path_spiral->set_num_sections(num_turns*4);

		// create coil
		rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_spiral, cross_rect);
		coil1->set_operating_current(Iop); coil1->set_operating_temperature(Top);
		coil1->set_number_turns(1);
		model->add_model(coil1);

		// create coil
		rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_spiral, cross_rect);
		coil2->set_operating_current(Iop); coil2->set_operating_temperature(Top);
		coil2->add_translation(0,0,pitch/2);
		coil2->set_number_turns(1);
		coil2->add_reverse();
		model->add_model(coil2);
	}

	// CALCULATION AND OUTPUT
	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// write a freecad macro
	if(run_freecad){
		rat::cmn::ShFreeCADPr fc = rat::cmn::FreeCAD::create(output_dir/"freecad.FCMacro","spiral");
		fc->set_num_sub(1);	model->export_freecad(fc);
	}

}