// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"
#include "rat/common/serializer.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "pathrectangle.hh"
#include "transbend.hh"
#include "calcmesh.hh"

// DESCRIPTION
// This example shows how to create a simple saddle coil

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_freecad = false; // export model to freecad
	const bool run_json_export = true; // export geometry to json file

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./saddle/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// coil geometry
	const arma::uword num_turns = 24;
	const rat::fltp width = RAT_CONST(0.25); // width of the coil [m] (before bending)
	const rat::fltp height = RAT_CONST(0.4); // height of the coil [m] (before bending)
	const rat::fltp radius = RAT_CONST(0.1); // cylinder radius on which the coil is bend [m]
	const rat::fltp element_size = RAT_CONST(4e-3); // size of the elements [m] 
	
	// cable settings
	const rat::fltp wcable = RAT_CONST(12e-3); // width of the cable [m]
	const rat::fltp dcable = RAT_CONST(1.2e-3); // thickness of the cable [m]
	const rat::fltp dinsu = RAT_CONST(0.1e-3); // thickness of the insulation around each cable [m]
	
	// operating conditions
	const rat::fltp operating_current = RAT_CONST(12e3); // current in coil [A]
	const rat::fltp operating_temperature = RAT_CONST(20.0); // temperature at coil [K]


	// GEOMETRY SETUP
	// create unified path
	rat::mdl::ShPathRectanglePr path_rect = rat::mdl::PathRectangle::create(width,height,radius,element_size);

	// add bending
	path_rect->add_transformation(rat::mdl::TransBend::create({0,1,0}, {0,0,1}, 0.15));

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_rect);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(1);
	path_cable->set_idx_start(0);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// write a freecad macro
	if(run_freecad){
		coil->export_freecad(rat::cmn::FreeCAD::create("saddle.FCMacro","saddle"));
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
		slzr->flatten_tree(coil); slzr->export_json(output_dir/"model.json");
	}

}