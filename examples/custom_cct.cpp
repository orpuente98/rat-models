// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for common
#include "rat/common/log.hh"

// materials headers
#include "rat/mat/database.hh"
#include "rat/mat/rutherfordcable.hh"

// header files for Models
#include "pathcctcustom.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "transbend.hh"
#include "griddata.hh"
#include "serializer.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "calcgrid.hh"

// DESCRIPTION
// In this example the custom CCT class is used to 
// create a combined function magnet with both
// dipole and quadrupole components.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	// const bool run_grid = true;
	const bool run_json_export = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./custom_cct/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// geometry settings
	const rat::fltp Rbend = RAT_CONST(0.15); // bend the magnet
	const rat::fltp nt = RAT_CONST(40.0); // number of turns
	const rat::fltp radius1 = RAT_CONST(0.025); // inner coil radius [m]
	const rat::fltp radius2 = RAT_CONST(0.025) + RAT_CONST(7e-3); // outer coil radius [m]
	const rat::fltp omega = RAT_CONST(6e-3); // winding pitch [m]
	const arma::uword nnpt = 120; // number of nodes per turn

	// strength of the different components
	const rat::fltp dipole_amplitude = RAT_CONST(30e-3); // strength of dipole [m]
	const rat::fltp quad_amplitude = RAT_CONST(30e-3); // strength of quadrupole [m]

	// operating conditions
	const rat::fltp operating_current = RAT_CONST(480.0); // operating current [A]
	const rat::fltp operating_temperature = RAT_CONST(4.5); // operating temperature [K]

	// conductor settings
	const arma::uword nd = 2; // number of wires across cable thickness
	const arma::uword nw = 5; // number of wires across cable width
	const arma::uword num_strands = nd*nw;	
	const rat::fltp dstr = RAT_CONST(0.825e-3); // strand diameter [m]
	const rat::fltp ddstr = RAT_CONST(1.0e-3); // strand diameter incl insulation [m]
	const rat::fltp fcu2sc = 1.9; // copper to superconductor ratio

	// cable dimensions
	const rat::fltp dcable = 2*ddstr; // thickness of cable [m] (slot width)
	const rat::fltp wcable = 5*ddstr; // width of cable [m] (slot depth)
	const rat::fltp element_size = RAT_CONST(2e-3); // size of the elements [m]


	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
	nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(dstr,fcu2sc));
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);
	nbti_cable->setup();


	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct1 = rat::mdl::PathCCTCustom::create();
	path_cct1->set_range(-nt/2,nt/2);
	path_cct1->set_omega(omega);
	path_cct1->set_rho(radius1);
	path_cct1->set_num_nodes_per_turn(nnpt);
	
	// add dipole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,{-nt/2,nt/2}, {-dipole_amplitude,-dipole_amplitude}, false));

	// add quadrupole changing from minus 
	// to plus along the length of the magnet
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		2,false,{-nt/2,nt/2}, {quad_amplitude,-quad_amplitude}, false));

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct2 = rat::mdl::PathCCTCustom::create();
	path_cct2->set_range(-nt/2,nt/2);
	path_cct2->set_omega(omega);
	path_cct2->set_rho(radius2);
	path_cct2->set_num_nodes_per_turn(nnpt);
	
	// add dipole
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,{-nt/2,nt/2},{dipole_amplitude,dipole_amplitude}, false));

	// add quadrupole changing from minus 
	// to plus along the length of the magnet
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		2,false,{-nt/2,nt/2},{quad_amplitude,-quad_amplitude}, false));

	// set reverse
	path_cct2->add_reverse();
	path_cct2->add_flip();

	// axis of magnet for field quality calculation
	rat::mdl::ShPathGroupPr pth_axis = rat::mdl::PathGroup::create();
	pth_axis->add_path(rat::mdl::PathStraight::create(0.5,1e-3));
	pth_axis->add_translation(0,-0.5/2,0);
	pth_axis->add_rotation(1,0,0,arma::Datum<rat::fltp>::pi/2);
	pth_axis->add_rotation(0,0,1,arma::Datum<rat::fltp>::pi);

	// add bending
	if(Rbend!=0){
		rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create({0,1,0}, {1,0,0}, Rbend);
		path_cct1->add_transformation(bending);
		path_cct2->add_transformation(bending);
		pth_axis->add_transformation(bending);
	}

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,element_size/2);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect, nbti_cable);	
	coil1->set_name("inner");
	coil1->set_number_turns(num_strands);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect, nbti_cable);	
	coil2->set_name("outer");
	coil2->set_number_turns(num_strands);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);

	// create model 
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1); model->add_model(coil2);


	// CALCULATION AND OUTPUT
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// mesh calculatoin
	if(run_coil_field){
		// create calculation
		rat::mdl::ShCalcMeshPr mesh_calculator = rat::mdl::CalcMesh::create(model);

		// calculate harmonics and get data
		mesh_calculator->calculate_write({output_time},output_dir,lg);
	}

	// calculate harmonics
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = 2*radius1/3;
		const bool compensate_curvature = true;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(model, pth_axis, reference_radius, compensate_curvature);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({output_time},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// create serializer
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);

		// write to output file
		slzr->export_json(output_dir/"model.json");
	}

	// export json
	if(run_json_export){
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

	// done
	return 0;
}