// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "meshdata.hh"
#include "calcmesh.hh"

// main function
int main(){
	// SETTINGS
	// model geometric input parameters
	const boost::filesystem::path output_dir = "./array/";
	const rat::fltp radius = 25e-3/2; // coil inner radius [m]
	const rat::fltp dcoil = 50e-3; // thickness of the coil [m]
	const rat::fltp wcable = 12e-3; // width of the cable [m]
	const rat::fltp delem = 2e-3; // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const rat::fltp operating_current = 2000; // operating current in [A]
	const arma::uword num_turns = 226; // number of turns

	// array grid size and spacing
	const arma::uword num_x = 1, num_y = 1, num_z = 6;
	const rat::fltp dx = 2.1*(radius+dcoil), dy = 2.1*(radius+dcoil), dz = 13e-3;


	// SETUP GEOMETRY
	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(dcoil);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, 0, wcable, delem);

	// create coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);
	coil->add_translation(0,0,-(dz*num_z)/2);

	// create array
	rat::mdl::ShModelArrayPr model = rat::mdl::ModelArray::create(coil,num_x,num_y,num_z,dx,dy,dz);
	

	// CALCULATION AND OUTPUT
	// create mesh calculatoin
	const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

	// calculate and write vtk output files to specified directory
	mesh->calculate_write({0},output_dir,rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT));

}