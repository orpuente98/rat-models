// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for Models
#include "crosscircle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "emitterbeam.hh"
#include "crosscircle.hh"
#include "pathaxis.hh"
#include "transbend.hh"
#include "surfacedata.hh"
#include "background.hh"
#include "lengthdata.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"


// DESCRIPTION
// This example shows how to create a small CCT magnet with two layers.
// We use the default CCT magnet input which only supports one harmonic.
// For more customizable CCT geometries have a look at the custom_cct example.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_grid = false; 
	const bool run_length = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./corccct/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// geometry settings
	const arma::uword num_poles = 1; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const arma::uword num_layers = 6; // number of layers in the coil 

	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const rat::fltp bending_radius = RAT_CONST(0.0); // coil bending in [m] (0 for no bending)
	const rat::fltp ell_coil = RAT_CONST(0.6); // length of the coil in [m]
	//const rat::fltp ell_coil = 2*arma::Datum<rat::fltp>::pi*bending_radius/4; // coil length calculated from angle [m]
	const rat::fltp aperture_radius = RAT_CONST(30e-3); // aperture radius in [m]
	const rat::fltp dradial = RAT_CONST(0.3e-3); // spacing between cylinders [m]
	const rat::fltp dformer = RAT_CONST(2e-3); // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 180;	// number of nodes each turn
	const rat::fltp delem = RAT_CONST(1e-3);

	// geometry specific per layer
	// const rat::fltp alpha = 30.0*arma::Datum<rat::fltp>::pi*2.0/360;

	// conductor geometryde
	const rat::fltp dcable = RAT_CONST(4e-3); // cable diameter [m]
	const rat::fltp delta = RAT_CONST(0.5e-3); // spacing between turns

	// scaling
	const rat::fltp scale_x = RAT_CONST(1.0);
	const rat::fltp scale_y = RAT_CONST(1.0)/scale_x;

	// current
	const rat::fltp operating_current = RAT_CONST(400e6)*arma::Datum<rat::fltp>::pi*(dcable/2)*(dcable/2);
	const rat::fltp operating_temperature = RAT_CONST(4.2);
	
	// background magnetic field
	//arma::Col<rat::fltp>::fixed<3> Hbg = {0,-1.0/arma::Datum<rat::fltp>::mu_0,0};
	// arma::Col<rat::fltp>::fixed<3> Hbg = {0,0,0};

	// geometry specific per layer
	const arma::Row<rat::fltp> alpha = arma::Row<rat::fltp>{
		RAT_CONST(20.0),RAT_CONST(26.0),RAT_CONST(32.0)}*2*arma::Datum<rat::fltp>::pi/360; // skew angle per two layers [rad]
	const arma::Row<arma::uword> is_horizontal = {0,0,0}; // specifies whether the coil generates horizontal field



	// GEOMETRY SETUP
	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossCirclePr cross_circle = rat::mdl::CrossCircle::create(dcable/2,delem);

	// create bending transformation
	rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create({0,1,0}, {1,0,0}, bending_radius);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	
	// walk over layers
	for(arma::uword i=0;i<num_layers;i++){
		// calculate radius
		const rat::fltp coil_radius = aperture_radius + (i+1)*dformer + i*(dcable + dradial);
		const rat::fltp inner_radius = aperture_radius + dformer + (i/2)*2*(dformer + dcable + dradial);

		// inner radius parameters
		const rat::fltp omega = (delta + dcable)/std::sin(alpha(i/2));
		const rat::fltp aest = inner_radius/(num_poles*std::tan(alpha(i/2))); // skew amplitude
		const rat::fltp num_turns = 2*std::floor((ell_coil-2*aest)/(2*omega));

		// account for integer number of turns
		rat::fltp a = num_poles*(ell_coil - num_turns*omega)/2;
		if(i%2==1)a+=omega/(num_poles*4); else a+=omega*(num_poles*4-1)/(num_poles*4);


		// create path
		rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create(
			num_poles,coil_radius,a,omega,num_turns,num_nodes_per_turn);
		
		// slanting direction
		if(i%2==0)path_cct->set_is_reverse(is_reverse); // reverse slanting
		else path_cct->set_is_reverse(!is_reverse); // reverse slanting
			
		// set scaling
		path_cct->set_scale_x(scale_x);
		path_cct->set_scale_y(scale_y);

		// add bending
		path_cct->add_transformation(bending);

		// horizontal
		if(is_horizontal(i/2))path_cct->add_rotation(0,0,1,arma::Datum<rat::fltp>::pi/2);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cct, cross_circle);
		coil->set_name("layer" + std::to_string(i)); 
		coil->set_number_turns(1);
		coil->set_operating_current(operating_current);
		coil->set_operating_temperature(operating_temperature);
		
		// add coil to model
		model->add_model(coil);
	}

	// CALCULATION AND OUTPUT
	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_grid){
		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(model,0.4,1.6,0.4,100,400,100);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg);
	}

	// simple line calculation
	if(run_length){
		rat::mdl::LengthData::create(model->create_meshes())->display(lg);
	}

}