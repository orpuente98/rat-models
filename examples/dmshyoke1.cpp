// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/gmshfile.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfcircle.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "modelmesh.hh"
#include "calcmesh.hh"


// DESCRIPTION
// Example showing how the 2D mesher can be used to 
// draw a mesh for an iron yoke. This can later be 
// used as input for a non-linear solver. Which,
// is high on the list of things to be implemented.
// Refer to distmesh-cpp for more information on 
// how to setup the mesh.

// main
int main(){
	// INPUT SETTINGS
	// settings
	const rat::fltp element_size = 0.02; // requested size of the elements [m]
	const rat::fltp Rin = 0.08; // yoke inner radius [m]
	const rat::fltp Rout = 0.2; // yoke outer radius [m]
	const rat::fltp dchannel = 0.04; // width of the channel [m]
	const rat::fltp ell = 0.2; // length of the yoke [m]

	// GEOMETRY SETUP
	// create cross section with distance function
	// we create a circle defining the outer radius
	// and we use diff to subtract a circle with inner
	// radius as well as the two rectangular regions 
	// at the top and bottom of the yoke
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(element_size,rat::dm::DFDiff::create(rat::dm::DFDiff::create(
		rat::dm::DFDiff::create(rat::dm::DFCircle::create(Rout,0,0),rat::dm::DFCircle::create(Rin,0,0)),
		rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,0.17,0.21)),
		rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,-0.21,-0.17)),rat::dm::DFOnes::create());

	// due to clipping need to add some fixed points in the corners
	// this process needs to be automated still
	arma::Mat<rat::fltp> pfix(4,2);
	pfix.row(0) = arma::Row<rat::fltp>{dchannel/2,Rout*std::sin(std::acos((dchannel/2)/Rout))};
	pfix.row(1) = arma::Row<rat::fltp>{-dchannel/2,Rout*std::sin(std::acos((dchannel/2)/Rout))};
	pfix.row(2) = arma::Row<rat::fltp>{dchannel/2,-Rout*std::sin(std::acos((dchannel/2)/Rout))};
	pfix.row(3) = arma::Row<rat::fltp>{-dchannel/2,-Rout*std::sin(std::acos((dchannel/2)/Rout))};
	cdmsh->set_fixed(pfix);

	// create path
	//rat::mdl::ShPathStraightPr pth = rat::mdl::PathStraight::create(ell,element_size);
	const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',ell,0,0,0,element_size);

	// create modelcoil
	const rat::mdl::ShModelMeshPr model = rat::mdl::ModelMesh::create(pth,cdmsh);

	// create mesh calculatoin
	const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

	// calculate and write vtk output files to specified directory
	mesh->calculate_write({0},"./dmshyoke1/",rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT));
}