// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crosspath.hh"

// code specific to Rat
namespace rat{namespace mdl{


	// constructors
	CrossPath::CrossPath(){
		set_name("cross_path");
	}
	
	// constructors
	CrossPath::CrossPath(const ShPathPr &input_path, const fltp offset, const fltp thickness, const fltp element_size){
		set_name("cross_path");
		set_input_path(input_path); set_offset(offset); 
		set_thickness(thickness); set_element_size(element_size);
	}
	
	// factory methods
	ShCrossPathPr CrossPath::create(){
		return std::make_shared<CrossPath>();
	}
	
	// factory methods
	ShCrossPathPr CrossPath::create(const ShPathPr &input_path, const fltp offset, const fltp thickness, const fltp element_size){
		return std::make_shared<CrossPath>(input_path,offset,thickness,element_size);
	}

	// set properties
	void CrossPath::set_thickness(const fltp thickness){
		thickness_ = thickness;
	}

	void CrossPath::set_offset(const fltp offset){
		offset_ = offset;
	}

	void CrossPath::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get properties
	fltp CrossPath::get_offset()const{
		return offset_;
	}

	fltp CrossPath::get_thickness()const{
		return thickness_;
	}

	fltp CrossPath::get_element_size()const{
		return element_size_;
	}

	void CrossPath::set_is_line(const bool is_line){
		is_line_ = is_line;
	}
	
	bool CrossPath::get_is_line()const{
		return is_line_;
	}

	// volume mesh
	ShAreaPr CrossPath::create_area(const MeshSettings &stngs) const{
		// check settings
		is_valid(true);

		// create frame
		MeshSettings frame_stngs = stngs; 
		frame_stngs.num_sub = 1; frame_stngs.element_divisor = 1;
		const ShFramePr frame = input_path_->create_frame(frame_stngs);

		// combine sections
		frame->combine();

		// check if frame is closed
		const bool is_loop = frame->is_loop();

		// get coordinates
		arma::Mat<fltp> R = cmn::Extra::field2mat(frame->get_coords());
		arma::Mat<fltp> L = cmn::Extra::field2mat(frame->get_direction());
		// const arma::Mat<fltp> D = cmn::Extra::field2mat(frame->get_transverse());
		arma::Mat<fltp> N = cmn::Extra::field2mat(frame->get_normal());

		// remove last in case of closed
		if(is_loop){R.shed_col(R.n_cols-1); L.shed_col(L.n_cols-1); N.shed_col(N.n_cols-1);}

		// number of lines
		const arma::uword num_lines = is_line_ ? 1 : (stngs.low_poly ? 2 : std::max(int(thickness_/element_size_),2));
		const arma::uword num_transverse = R.n_cols;
		const arma::uword num_nodes = num_lines*num_transverse;

		// allocate
		arma::Mat<fltp> u(R.n_cols,num_lines); arma::Mat<fltp> v(R.n_cols,num_lines);
		arma::Mat<fltp> un(R.n_cols,num_lines); arma::Mat<fltp> vn(R.n_cols,num_lines);
		arma::Mat<fltp> ut(R.n_cols,num_lines); arma::Mat<fltp> vt(R.n_cols,num_lines);

		// walk over lines
		for(arma::uword i=0;i<num_lines;i++){
			// position in the normal direction
			const fltp nn = offset_ + (is_line_ ? 0 : i*thickness_/(num_lines-1));

			// coordinates
			u.col(i) = (R.row(0) + nn*N.row(0)).t();
			v.col(i) = (R.row(1) + nn*N.row(1)).t();

			// build
			un.col(i) = (N.row(0)).t();
			vn.col(i) = (N.row(1)).t();

			// build
			ut.col(i) = (L.row(0)).t();
			vt.col(i) = (L.row(1)).t();
		}

		// create node coordinates
		const arma::Mat<fltp> Rn = arma::join_vert(
			arma::vectorise(u).t(), arma::vectorise(v).t());
		const arma::Mat<fltp> Nn = arma::join_vert(
			arma::vectorise(un).t(), arma::vectorise(vn).t());
		const arma::Mat<fltp> Dn = arma::join_vert(
			arma::vectorise(ut).t(), arma::vectorise(vt).t());

		// create matrix of node indices
		arma::Mat<arma::uword> node_idx = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes-1);
		node_idx.reshape(num_transverse, num_lines);

		// close the loop
		if(is_loop){
			node_idx = arma::join_vert(node_idx, node_idx.row(0));
		}

		// 2D cross section
		if(!is_line_){
			// get corner matrices
			const arma::Mat<arma::uword> M0 = node_idx.submat(0, 0, num_transverse-2 + (int)is_loop, num_lines-2);
			const arma::Mat<arma::uword> M1 = node_idx.submat(1, 0, num_transverse-1 + (int)is_loop, num_lines-2);
			const arma::Mat<arma::uword> M2 = node_idx.submat(1, 1, num_transverse-1 + (int)is_loop, num_lines-1);
			const arma::Mat<arma::uword> M3 = node_idx.submat(0, 1, num_transverse-2 + (int)is_loop, num_lines-1);

			// reshape into element matrix
			const arma::Mat<arma::uword> n = arma::join_horiz(
				arma::vectorise(M0), arma::vectorise(M1),
				arma::vectorise(M2), arma::vectorise(M3)).t();

			// return area
			return Area::create(Rn,Nn,Dn,n);
		}

		// 1D cross section
		else{
			// get corner matrices
			const arma::Col<arma::uword> M0 = node_idx.rows(0, num_transverse-2 + (int)is_loop);
			const arma::Col<arma::uword> M1 = node_idx.rows(1, num_transverse-1 + (int)is_loop);

			// reshape into element matrix
			const arma::Mat<arma::uword> n = arma::join_horiz(M0, M1).t();

			// return area
			return Area::create(Rn,Nn,Dn,n,thickness_);
		}

	}

	// surface mesh
	ShPerimeterPr CrossPath::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area(MeshSettings());

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossPath::get_bounding() const{
		const arma::Mat<fltp> Rn = CrossPath::create_perimeter()->get_nodes();
		return arma::join_horiz(arma::min(Rn,1),arma::max(Rn,1));
	}

	// vallidity check
	bool CrossPath::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// serialization
	std::string CrossPath::get_type(){
		return "rat::mdl::crosspath";
	}

	void CrossPath::serialize(Json::Value &js, cmn::SList &list) const{
		Node::serialize(js,list);
		InputPath::serialize(js,list);
		js["type"] = get_type();
		js["thickness"] = thickness_;
		js["offset"] = offset_;
		js["element_size"] = element_size_;
		js["is_line"] = is_line_;
	}

	void CrossPath::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Node::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		thickness_ = js["thickness"].asDouble();
		offset_ = js["offset"].asDouble();
		element_size_ = js["element_size"].asDouble();
		is_line_ = js["is_line"].asBool();
	}

}}