// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "emitteromni.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	EmitterOmni::EmitterOmni(){
		set_name("omni emitter");

		// protons are default
		set_proton();
	}

	// factory
	ShEmitterOmniPr EmitterOmni::create(){
		return std::make_shared<EmitterOmni>();
	}
				
	// set protons
	void EmitterOmni::set_proton(){
		rest_mass_ = 0.938; // [GeV/C^2] 
		charge_ = 1; // elementary charge
		beam_energy_ = rest_mass_ + 0.1;
	}

	// set electrons
	void EmitterOmni::set_electron(){
		rest_mass_ = 0.51099895000e-3; // [GeV/C^2] 
		charge_ = -1; // elementary charge
		beam_energy_ = rest_mass_ + 0.1;
	}

	// set rest mass
	void EmitterOmni::EmitterOmni::set_rest_mass(const fltp rest_mass){
		rest_mass_ = rest_mass;
	}

	// set charge
	void EmitterOmni::set_charge(const fltp charge){
		charge_ = charge;
	}

	// set beam energy
	void EmitterOmni::set_beam_energy(const fltp beam_energy){
		beam_energy_ = beam_energy;
	}

	// set particle lifetime
	void EmitterOmni::set_lifetime(const arma::uword lifetime){
		lifetime_ = lifetime; 
	}

	// set number of particles
	void EmitterOmni::set_num_particles(const arma::uword num_particles){
		num_particles_ = num_particles;
	}

	// set orientation
	void EmitterOmni::set_spawn_coord(const arma::Col<fltp>::fixed<3> &R){
		R_ = R;
	} 


	// getters
	arma::uword EmitterOmni::get_num_particles() const{
		return num_particles_;
	}

	fltp EmitterOmni::get_rest_mass() const{
		return rest_mass_;
	}

	fltp EmitterOmni::get_charge()const{
		return charge_;
	}

	fltp EmitterOmni::get_beam_energy()const{
		return beam_energy_;
	}

	arma::uword EmitterOmni::get_lifetime()const{
		return lifetime_;
	}

	arma::Col<fltp>::fixed<3> EmitterOmni::get_spawn_coord()const{
		return R_;
	}


	// particle creation
	arma::field<Particle> EmitterOmni::spawn_particles() const{		
		// error check
		is_valid(true);

		// set seed for random number generator
		arma::arma_rng::set_seed(seed_);

		// velocity
		const fltp gamma = beam_energy_/rest_mass_;
		const fltp velocity = std::sqrt(1.0 - 1.0/(gamma*gamma));

		// spherical coordinates uniform random distribution
		const arma::Row<fltp> theta = 2*arma::Datum<fltp>::pi*arma::Row<fltp>(num_particles_,arma::fill::randu);
		const arma::Row<fltp> phi = arma::acos(2*arma::Row<fltp>(num_particles_,arma::fill::randu)-1.0);
		
		// convert to cartesian coordinates
		const arma::Mat<fltp> Rs = arma::join_vert(
			arma::cos(theta)%arma::sin(phi),
			arma::sin(theta)%arma::sin(phi),
			arma::cos(phi));

		// allocate list of particles
		arma::field<Particle> particles(num_particles_);

		// create all particles
		for(arma::uword i=0;i<num_particles_;i++){
			// set maximum number of steps
			particles(i).set_num_steps(lifetime_);

			// set start coordinate and velocity
			particles(i).set_startcoord(R_,Rs.col(i)*velocity);

			// set start index
			particles(i).set_start_index(0);

			// set mass
			particles(i).set_rest_mass(rest_mass_);
			particles(i).set_charge(charge_);

			// setup internal storage
			particles(i).setup();
			particles(i).terminate_start();
		}

		// return list of particles
		return particles;
	}

	// validity check
	bool EmitterOmni::is_valid(const bool enable_throws) const{
		if(rest_mass_<=0){if(enable_throws){rat_throw_line("rest mass must be larger than zero");} return false;};
		if(beam_energy_<=rest_mass_){if(enable_throws){rat_throw_line("beam energy must exceed rest mass");} return false;};
		if(lifetime_<=0){if(enable_throws){rat_throw_line("lifetime must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string EmitterOmni::get_type(){
		return "rat::mdl::emitteromni";
	}

	// method for serialization into json
	void EmitterOmni::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);
		
		// properties
		js["type"] = get_type();

		// rng
		js["seed"] = (unsigned int)seed_;

		// beam parameters
		js["num_particles"] = (unsigned int)num_particles_;
		js["beam_energy"] = beam_energy_;
		js["rest_mass"] = rest_mass_;
		js["charge"] = charge_;

		// position orientation
		js["Rx"] = R_(0);
		js["Ry"] = R_(1);
		js["Rz"] = R_(2);

		// tracking
		js["lifetime"] = (unsigned int)lifetime_;
	}

	// method for deserialisation from json
	void EmitterOmni::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Node::deserialize(js,list,factory_list,pth);

		// rng
		seed_ = js["seed"].asUInt64();

		// beam parameters
		num_particles_ = js["num_particles"].asUInt64();
		beam_energy_ = js["beam_energy"].asDouble();
		rest_mass_ = js["rest_mass"].asDouble();
		charge_ = js["charge"].asDouble();

		// position orientation
		R_(0) = js["Rx"].asDouble();
		R_(1) = js["Ry"].asDouble();
		R_(2) = js["Rz"].asDouble();

		// tracking
		lifetime_ = js["lifetime"].asUInt64();
	}

}}