// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "area.hh"

#include "rat/common/error.hh"
#include "rat/common/elements.hh"
#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Area::Area(){

	}


	// constructor with input
	Area::Area(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D, 
		const arma::Mat<arma::uword> &n,
		const fltp hidden_thickness,
		const fltp hidden_width){
		
		// make sure it is a quad mesh
		//assert(n.n_rows==4);
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set mesh
		set_mesh(Rn,N,D,n);

		// set hidden thickness and width
		set_hidden_thickness(hidden_thickness);
		set_hidden_width(hidden_width);

		// calculate areas
		calculate_areas();
	}

	// default constructor
	Area::Area(const ShAreaPr &area){
		assert(area!=NULL);
		Rn_ = area->Rn_; n_ = area->n_;
		element_areas_ = area->element_areas_;
		hidden_thickness_ = area->hidden_thickness_;
		hidden_width_ = area->hidden_width_;
	}

	// factory with dimension input
	ShAreaPr Area::create(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D, 
		const arma::Mat<arma::uword> &n,
		const fltp hidden_thickness,
		const fltp hidden_width){
		return std::make_shared<Area>(
			Rn,N,D,n,hidden_thickness,hidden_width);
	}

	// copy factory
	ShAreaPr Area::create(const ShAreaPr &area){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Area>(area);
	}

	// method for setting the mesh
	void Area::set_mesh(
		const arma::Mat<fltp> &Rn,
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D, 
		const arma::Mat<arma::uword> &n){

		// check mesh
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set to self
		Rn_ = Rn; N_ = N; D_ = D; n_ = n;
	}

	// set hidden dimension
	void Area::set_hidden_thickness(const fltp hidden_thickness){
		// assert(hidden_thickness>0);
		hidden_thickness_ = hidden_thickness;
	}

	// set hidden dimension
	void Area::set_hidden_width(const fltp hidden_width){
		// assert(hidden_width>0);
		hidden_width_ = hidden_width;
	}

	// get hidden dimension
	fltp Area::get_hidden_thickness() const{
		return hidden_thickness_;
	}

	// get hidden dimension
	fltp Area::get_hidden_width() const{
		return hidden_width_;
	}

	// get hidden dimension
	fltp Area::get_hidden_dim() const{
		return hidden_thickness_*hidden_width_;
	}

	// get number of elements
	arma::uword Area::get_num_elements() const{
		return n_.n_cols;
	}

	// get number of nodes
	arma::uword Area::get_num_nodes() const{
		return Rn_.n_cols;
	}

	// get node coordinates
	const arma::Mat<fltp>& Area::get_nodes() const{
		return Rn_;
	}

	// get transverse direction
	const arma::Mat<fltp>& Area::get_transverse() const{
		return D_;
	}

	// get normal direction
	const arma::Mat<fltp>& Area::get_normal() const{
		return N_;
	}

	// get elements
	const arma::Mat<arma::uword>& Area::get_elements() const{
		return n_;
	}

	// smoothen mesh using simple node averaging algorithm
	void Area::smoothen(const arma::uword N, const fltp dmp){
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// create edges
		arma::Mat<arma::uword> s,i; 
		create_edge_matrix(s,i);

		// find indexes of unique nodes
		arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(s,1,s.n_elem));

		// allocate internal node indexes
		arma::Row<arma::uword> idx_node_internal = 
			arma::regspace<arma::Row<arma::uword> >(0,num_nodes-1);

		// check which nodes are internal
		arma::Row<arma::uword> keep(num_nodes,arma::fill::ones);
		keep.cols(idx_node_surface).fill(0);

		// remove non-internal indexes from the list
		idx_node_internal = idx_node_internal.cols(arma::find(keep==1));

		// x and y coordinates of nodes
		arma::Row<fltp> x = Rn_.row(0); 
		arma::Row<fltp> y = Rn_.row(1); 

		// get indexes
		arma::Row<arma::uword> a = i.row(0);
		arma::Row<arma::uword> b = i.row(1);

		// iterations
		for(arma::uword j=0;j<N;j++){
			// get from store
			arma::Row<fltp> xnew = x; 
			arma::Row<fltp> ynew = y;

			// walk over internal nodes
			for(arma::uword k=0;k<idx_node_internal.n_cols;k++){
				// get this node's index
				arma::uword idx = idx_node_internal(k);

				// get neighbour indexes
				arma::Row<arma::uword> idxnb = arma::join_horiz(a.cols(arma::find(b==idx)),b.cols(arma::find(a==idx)));

				// calculate new position through
				// averaging with neighbours
				arma::Row<fltp> xn = x.cols(idxnb); arma::Row<fltp> yn = y.cols(idxnb);
				xnew(idx) = arma::sum(xn)/xn.n_elem; ynew(idx) = arma::sum(yn)/yn.n_elem;
			}

			// update
			x += dmp*(xnew-x); 
			y += dmp*(ynew-y);
		}

		// give p
		Rn_ = arma::join_vert(x,y);
	}

	// analyze function
	void Area::create_edge_matrix(
		arma::Mat<arma::uword> &s, 
		arma::Mat<arma::uword> &in) const{

		// point mesh (2D) will become linemesh in 3D
		if(n_.n_rows==1){
			s = n_; in.set_size(1,0); return;
		}

		// line mesh (2D) will become quadmesh in 3D
		else if(n_.n_rows==2){
			s = n_; in.set_size(1,0); return;
		}

		// must be quad mesh (2D) 
		else if(n_.n_rows!=4){
			rat_throw_line("area mesh not recognised");
		}

		// get counters
		const arma::uword num_elements = get_num_elements();

		// get list of faces for each element
		const arma::Mat<arma::uword>::fixed<4,2> M = cmn::Quadrilateral::get_edges();

		// create list of all faces present in mesh
		arma::Mat<arma::uword> S(2,num_elements*4);
		for(arma::uword i=0;i<4;i++){
			S.cols(i*num_elements,(i+1)*num_elements-1) = n_.rows(M.row(i));
		}

		// sort each column in S
		arma::Mat<arma::uword> Ss(2,num_elements*4);
		for(arma::uword i=0;i<num_elements*4;i++){
			Ss.col(i) = arma::sort(S.col(i));
		}

		// sort S by rows
		for(arma::uword i=0;i<2;i++){
			arma::Col<arma::uword> idx = arma::stable_sort_index(Ss.row(1-i));
			Ss = Ss.cols(idx); S = S.cols(idx);
		}	
		
		// find duplicates and mark
		arma::Row<arma::uword> duplicates = 
			arma::all(Ss.cols(0,num_elements*4-2)==Ss.cols(1,num_elements*4-1),0);

		// extend duplicate list to contain first and second
		arma::Row<arma::uword> extended(num_elements*4,arma::fill::zeros);
		extended.head(num_elements*4-1) += duplicates;
		extended.tail(num_elements*4-1) += duplicates;

		// remove marked indices
		s = S.cols(arma::find(extended==0));
		in = S.cols(arma::find(duplicates!=0));

		// sort edge matrix
		arma::Row<arma::uword> 
		sidx = arma::sort_index(s.row(1)).t();
		s = s.cols(sidx); 
		sidx = arma::stable_sort_index(s.row(0)).t(); 
		s = s.cols(sidx); 

		// start of closed loop
		arma::uword idx0 = 0;

		// walk over s and put edges in the correct order
		for(arma::uword i=0;i<s.n_cols-1;i++){
			// find next section
			const arma::Row<arma::uword> idx = arma::find(s.submat(0,i+1,0,s.n_cols-1)==s(1,i),1,"first").t();

			// check if exists
			if(!idx.is_empty()){
				// move
				s.swap_cols(i+1+arma::as_scalar(idx),i+1);
			}else{
				// check for full circle
				if(s(0,idx0)!=s(1,i))rat_throw_line("surface inconsistent");

				// move idx0
				idx0 = i;
			}
		}

	}

	// get indexes of corner nodes
	// these wil become the edges of the mesh
	arma::Col<arma::uword> Area::get_corner_nodes() const{
		// create edges
		arma::Mat<arma::uword> s,i; 
		create_edge_matrix(s,i);
		if(s.n_elem==1)return s;

		// calculate angle between successive elements
		arma::Row<fltp> alpha(s.n_cols);
		for(arma::uword k=0;k<s.n_cols;k++){
			// prevent overlow using modulus
			const arma::uword idx2 = (k+1)%s.n_cols;

			// calculate direction vectors
			const arma::Col<fltp>::fixed<2> dR1 = Rn_.col(s(1,k)) - Rn_.col(s(0,k));
			const arma::Col<fltp>::fixed<2> dR2 = Rn_.col(s(1,idx2)) - Rn_.col(s(0,idx2));

			// calculate angle
			alpha(k) = std::acos(arma::as_scalar(cmn::Extra::dot(dR1,dR2)/
				(cmn::Extra::vec_norm(dR1)%cmn::Extra::vec_norm(dR2))));
		}

		// find corner node indexes
		const fltp corner_tolerance = arma::Datum<fltp>::pi/4;
		arma::Col<arma::uword> c = s.submat(arma::Col<arma::uword>{1}, arma::find(
			arma::abs(alpha)>corner_tolerance)).t();

		// extend
		if(n_.n_rows==2){
			c = arma::join_vert(arma::Col<arma::uword>{s.front()},
				c,arma::Col<arma::uword>{s.back()});
		}

		// return corner node indexes
		return c;
	}

	// calculate areas by splitting elements into triangles
	void Area::calculate_areas(){
		// check if surface
		if(n_.is_empty())return;

		// for quadrilaterals area 
		if(n_.n_rows==4){
			// get vectors spanning face
			const arma::Mat<fltp> V1 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(0));
			const arma::Mat<fltp> V2 = Rn_.cols(n_.row(3)) - Rn_.cols(n_.row(0));
			const arma::Mat<fltp> V3 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(2));
			const arma::Mat<fltp> V4 = Rn_.cols(n_.row(3)) - Rn_.cols(n_.row(2));

			// calculate cross products (only z component)
			const arma::Row<fltp> A1 = arma::abs(V1.row(0)%V2.row(1) - V1.row(1)%V2.row(0))/2;
			const arma::Row<fltp> A2 = arma::abs(V3.row(0)%V4.row(1) - V3.row(1)%V4.row(0))/2;
			
			// sum values and return
			element_areas_ = get_hidden_dim()*(A1 + A2);
		}

		// for line elements area
		else if(n_.n_rows==2){
			// get edge vector
			const arma::Mat<fltp> V1 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(0));

			// sum values and return
			element_areas_ = get_hidden_dim()*arma::sqrt(arma::sum(V1%V1,0));
		}

		// for point sources area is just 1
		// this ensures that the current is 
		// set fully to this element
		else if(n_.n_rows==1){
			element_areas_ = arma::Row<fltp>{get_hidden_dim()};
		}

		// do not recognise this element type
		else rat_throw_line("unknown element type");

		// calculate total area
		total_area_ = arma::accu(element_areas_);
	}

	// apply multiple transformations
	void Area::apply_transformations(const std::list<ShTransPr> &trans){
		for(auto it=trans.begin();it!=trans.end();it++)
			apply_transformation(*it);
	}

	// apply transformation
	void Area::apply_transformation(const ShTransPr &trans){
		// // apply to vectors	
		// trans->apply_vectors(R_,L_,'L');
		// trans->apply_vectors(Rn_,N_,'N');
		// trans->apply_vectors(Rn_,D_,'D');
		// trans->apply_vectors(R_,B_,'B');

		// apply to coordinates
		trans->apply_area_coords(Rn_);

		// apply to mesh
		trans->apply_area_mesh(n_, get_num_nodes());
	}

	// get conductor area
	// calculated by splitting elements into triangles
	fltp Area::get_area() const{
		// sum values and return
		return total_area_;
	}

	// get conductor area
	// calculated by splitting elements into triangles
	arma::Row<fltp> Area::get_areas() const{
		return element_areas_;
	}

	// extract surface from internal mesh
	ShPerimeterPr Area::extract_perimeter() const{
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// create edges
		arma::Mat<arma::uword> s,i; 
		create_edge_matrix(s,i);

		// find indexes of unique nodes
		arma::Row<arma::uword> idx_node_surface = arma::unique<arma::Row<arma::uword> >(arma::reshape(s,1,s.n_elem));

		// figure out which nodes to drop as 
		// they are no longer contained in the mesh
		arma::Row<arma::uword> node_indexing(1,num_nodes,arma::fill::zeros);
		node_indexing.cols(idx_node_surface) = 
			arma::regspace<arma::Row<arma::uword> >(0,idx_node_surface.n_elem-1);

		// re-index connectivity
		s = arma::reshape(node_indexing(arma::reshape(s,1,s.n_elem)),2,s.n_cols);

		// get node coordinates
		arma::Mat<fltp> Rs = Rn_.cols(idx_node_surface);

		// return peripheral mesh
		return Perimeter::create(Rs,s);
	}


}}