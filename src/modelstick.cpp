// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelstick.hh"
#include "crosspoint.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelStick::ModelStick(){
		set_name("Mr. Stick");
	}

	// factory methods
	ShModelStickPr ModelStick::create(){
		return std::make_shared<ModelStick>();
	}




	// create specific mesh
	std::list<ShMeshDataPr> ModelStick::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check validity
		if(!is_valid(stngs.enable_throws))return{};

		// settings
		const fltp head_radius = 0.1;
		const fltp height = 1.8;
		const arma::uword num_head = 20;
		const fltp leg_height = 1.0;

		// allocate coords
		arma::field<arma::Mat<fltp> > R(6);

		// create head
		{
			const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(0,2*arma::Datum<fltp>::pi,num_head);
			const arma::Row<fltp> x = head_radius*arma::cos(theta);
			const arma::Row<fltp> y = head_radius*arma::sin(theta) + height - head_radius;
			const arma::Row<fltp> z(num_head,arma::fill::zeros);
			R(0) = arma::join_vert(x,z,y);
		}

		// create spine
		{
			const arma::Row<fltp> x = {0,0};
			const arma::Row<fltp> y = {height-2*head_radius,leg_height};
			const arma::Row<fltp> z = {0,0};
			R(1) = arma::join_vert(x,z,y);
		}

 		// create legs
		{
			const arma::Row<fltp> x = {-0.4,-0.2,0,0.2,0.4};
			const arma::Row<fltp> y = {0,0,leg_height,0,0};
			const arma::Row<fltp> z = {0,0,0,0,0};
			R(2) = arma::join_vert(x,z,y);
		}

		// create arms
		{
			const arma::Row<fltp> x = {-height/2,0,height/2};
			const arma::Row<fltp> y = {height-2*head_radius,height-2*head_radius-0.15,height-2*head_radius};
			const arma::Row<fltp> z = {0,0,0};
			R(3) = arma::join_vert(x,z,y);
		}

		// create eyes
		{
			const arma::Row<fltp> x = {-head_radius+head_radius/3,-head_radius+head_radius/2,-head_radius/3};
			const arma::Row<fltp> y = {height-head_radius,height-head_radius+head_radius/5,height-head_radius};
			const arma::Row<fltp> z = {0,0,0};
			R(4) = arma::join_vert(x,z,y);
			R(5) = arma::join_vert(-x,z,y);
		}


		// factory with single section input
		ShFramePr frame = Frame::create(R,R,R,R);

		// create point cross section
		ShCrossPr crss = CrossPoint::create(RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0));

		// create 2d area mesh
		ShAreaPr area = crss->create_area(stngs);

		// mesh data
		ShMeshDataPr mesh_data = MeshData::create(frame, area);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();

		// transform
		mesh_data->apply_transformations(get_transformations());

		// return mesh
		return {mesh_data};
	}

	// get type
	std::string ModelStick::get_type(){
		return "rat::mdl::modelstick";
	}

	// method for serialization into json
	void ModelStick::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		Model::serialize(js,list);

		// set type
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void ModelStick::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Model::deserialize(js,list,factory_list,pth);
	}
}}
