// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcpath.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// model headers
#include "crosspoint.hh"
#include "crosscircle.hh"
#include "meshdata.hh"
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcPath::CalcPath(){
		set_name("path"); set_input_path(PathAxis::create());

		// settins
		stngs_->set_num_exp(5);
		stngs_->set_large_ilist(true);
	}

	CalcPath::CalcPath(const ShModelPr &model, const ShPathPr &input_path) : CalcPath(){
		set_model(model);
		set_input_path(input_path);
	}

	// factory methods
	ShCalcPathPr CalcPath::create(){
		return std::make_shared<CalcPath>();
	}

	ShCalcPathPr CalcPath::create(const ShModelPr &model, const ShPathPr &input_path){
		return std::make_shared<CalcPath>(model, input_path);
	}

	// get mesh enabled
	bool CalcPath::get_visibility() const{
		return visibility_;
	}

	// set mesh enabled
	void CalcPath::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	
	// calculate with inductance data output
	ShLineDataPr CalcPath::calculate_path(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();
		
		// general info
		lg->msg("%s=== Starting Path Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create frame
		ShLineDataPr line = LineData::create(get_input_path()->create_frame(MeshSettings()));
		line->set_field_type("AHM",{3,3,3});
		line->set_time(time);
		
		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 

		// create multilevel fast multipole method object
		fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);

		// add sources and targets to the calculation
		mlfmm->set_sources(src);
		mlfmm->set_targets(line);

		// setup multipole method
		mlfmm->setup(lg);

		// run multipole method
		mlfmm->calculate(lg);

		// return the line data
		return line;
	}

	// generalized calculation
	std::list<ShDataPr> CalcPath::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_path(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcPath::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{
		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// allocate cross section
		ShCrossPr crss;
		// point cross section
		if(stngs.visual_radius==RAT_CONST(0.0))crss = CrossPoint::create(
			RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1e-6),RAT_CONST(1e-6));

		// circular cross section
		else crss = CrossCircle::create(stngs.visual_radius,stngs.visual_radius/2);

		// create frame
		ShFramePr frame = get_input_path()->create_frame(stngs);

		// create 2d area mesh
		ShAreaPr area = crss->create_area(stngs);

		// drop nodes
		if(stngs.low_poly)frame->simplify(stngs.visual_tolerance, crss->get_bounding());

		// combine
		if(stngs.combine_sections)frame->combine();

		// mesh data
		ShMeshDataPr mesh_data = MeshData::create(frame, area);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();
		
		// mesh data object
		return {mesh_data};
	}

	// validity check
	bool CalcPath::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!CalcLeaf::is_valid(enable_throws))return false;
		return true;
	}

	// serialization
	std::string CalcPath::get_type(){
		return "rat::mdl::calcpath";
	}

	void CalcPath::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		InputPath::serialize(js,list);
		js["type"] = get_type();
		js["visibility"] = visibility_;
	}
	
	void CalcPath::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		visibility_ = js["visibility"].asBool();
	}

}}