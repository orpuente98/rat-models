// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathspiral.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathSpiral::PathSpiral(){
		set_name("spiral");
	}

	PathSpiral::PathSpiral(
		const fltp radius, 
		const fltp nt1, 
		const fltp nt2, 
		const fltp pitch,
		const fltp element_size,
		const bool transverse){

		set_radius(radius);
		set_nt1(nt1);
		set_nt2(nt2);
		set_pitch(pitch);
		set_element_size(element_size);
		set_transverse(transverse);
	}

	// factory
	ShPathSpiralPr PathSpiral::create(){
		//return ShPathSpiralPr(new PathSpiral);
		return std::make_shared<PathSpiral>();
	}

	// factory with dimension input
	ShPathSpiralPr PathSpiral::create(
		const fltp radius, 
		const fltp nt1, 
		const fltp nt2, 
		const fltp pitch,
		const fltp element_size,
		const bool transverse){
		return std::make_shared<PathSpiral>(
			radius,nt1,nt2,pitch,element_size,transverse);
	}

	// set properties
	void PathSpiral::set_radius(const fltp radius){
		radius_ = radius;
	}

	void PathSpiral::set_nt1(const fltp nt1){
		nt1_ = nt1;
	}

	void PathSpiral::set_nt2(const fltp nt2){
		nt2_ = nt2;
	}
	
	void PathSpiral::set_pitch(const fltp pitch){
		pitch_ = pitch;
	}
	
	void PathSpiral::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathSpiral::set_transverse(const bool transverse){
		transverse_ = transverse;
	}


	// get properties
	fltp PathSpiral::get_radius() const{
		return radius_;
	}

	fltp PathSpiral::get_nt1() const{
		return nt1_;
	}

	fltp PathSpiral::get_nt2() const{
		return nt2_;
	}

	fltp PathSpiral::get_pitch() const{
		return pitch_;
	}

	fltp PathSpiral::get_element_size() const{
		return element_size_;
	}

	bool PathSpiral::get_transverse()const{
		return transverse_;
	}



	// get frame
	ShFramePr PathSpiral::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// calculate number of nodes
		arma::uword num_nodes = std::max((int)std::ceil((radius_*2*arma::Datum<fltp>::pi*(nt2_-nt1_))/element_size_),2);
		while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

		// create theta
		arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(
			nt1_*2*arma::Datum<fltp>::pi, nt2_*2*arma::Datum<fltp>::pi, num_nodes);

		// allocate
		const arma::Mat<fltp> R = arma::join_vert(
			radius_*arma::sin(theta), radius_*arma::cos(theta),
			pitch_*theta/(2*arma::Datum<fltp>::pi));

		arma::Mat<fltp> L = arma::join_vert(
			radius_*arma::cos(theta), -radius_*arma::sin(theta),
			arma::Row<fltp>(num_nodes,arma::fill::ones)*
			pitch_/(2*arma::Datum<fltp>::pi));

		arma::Mat<fltp> N = arma::join_vert(
			radius_*arma::sin(theta), radius_*arma::cos(theta),
			arma::Row<fltp>(num_nodes,arma::fill::zeros));

		// normalize
		L.each_row()/=cmn::Extra::vec_norm(L);
		N.each_row()/=cmn::Extra::vec_norm(N);

		// calculate last vector
		arma::Mat<fltp> D = cmn::Extra::cross(L,N);

		// create frame
		ShFramePr frame = Frame::create(R,L,
			transverse_ ? D : N, transverse_ ? N : D);

		// apply transformations
		frame->apply_transformations(get_transformations());
		
		// create frame and return
		return frame;
	}

	// validity check
	bool PathSpiral::is_valid(const bool enable_throws) const{
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(nt2_<=nt1_){if(enable_throws){rat_throw_line("nt1 must be smaller than nt2");} return false;};
		return true;
	}


	// get type
	std::string PathSpiral::get_type(){
		return "rat::mdl::pathspiral";
	}

	// method for serialization into json
	void PathSpiral::serialize(Json::Value &js, cmn::SList &list) const{
		// parent nodes
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["transverse"] = transverse_;
		js["radius"] = radius_;
		js["nt1"] = nt1_; 
		js["nt2"] = nt2_; 
		js["pitch"] = pitch_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void PathSpiral::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent nodes
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_transverse(js["transverse"].asBool());
		set_radius(js["radius"].asDouble());
		set_nt1(js["nt1"].asDouble()); 
		set_nt2(js["nt2"].asDouble()); 
		set_pitch(js["pitch"].asDouble());
		set_element_size(js["element_size"].asDouble());
	}

}}



