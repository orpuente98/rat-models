// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crossdmsh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	CrossDMsh::CrossDMsh(const fltp h0, const dm::ShDistFunPr &fd, const dm::ShDistFunPr &fh){
		if(fd==NULL)rat_throw_line("distance function points to null");
		if(fh==NULL)rat_throw_line("scaling function points to null");
		set_name("distmesh"); fd_ = fd; fh_ = fh; h0_ = h0;
	}

	// factory methods
	ShCrossDMshPr CrossDMsh::create(const fltp h0, const dm::ShDistFunPr &fd, const dm::ShDistFunPr &fh){
		return std::make_shared<CrossDMsh>(h0,fd,fh);
	}

	// setting fixed points
	void CrossDMsh::set_fixed(const arma::Mat<fltp> &pfix_ext){
		assert(pfix_ext.n_cols==2);
		pfix_ext_ = pfix_ext;
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossDMsh::get_bounding() const{
		return fd_->get_bounding();
	}

	// set element size
	void CrossDMsh::set_h0(const fltp h0){
		h0_ = h0;
	}

	// get element size
	fltp CrossDMsh::get_h0()const{
		return h0_;
	}


	// setup
	ShAreaPr CrossDMsh::create_area(const MeshSettings &/*stngs*/) const{
		// check validity
		is_valid(true);

		// call distmesh2d
		dm::ShDistMesh2DPr dmsh = dm::DistMesh2D::create();

		// setup distmesher
		dmsh->set_quad(quad_);
		if(quad_)dmsh->set_h0(std::sqrt(2.0)*h0_); else dmsh->set_h0(h0_);
		dmsh->set_distfun(fd_);
		dmsh->set_scalefun(fh_);
		dmsh->set_fixed(pfix_ext_);

		// run mesher
		dmsh->setup();

		// get nodes and elements
		const arma::Mat<fltp> Rn = dmsh->get_nodes().t();
		arma::Mat<arma::uword> n = dmsh->get_elements().t();

		// get orientation
		arma::Mat<fltp> N(2,Rn.n_cols,arma::fill::zeros); N.row(0).fill(RAT_CONST(1.0));
		arma::Mat<fltp> D(2,Rn.n_cols,arma::fill::zeros); D.row(1).fill(RAT_CONST(1.0));

		// circle area
		ShAreaPr area = Area::create(Rn,N,D,n);

		// return positive
		return area;
	}

	// surface mesh
	ShPerimeterPr CrossDMsh::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area(MeshSettings());

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	bool CrossDMsh::is_valid(const bool enable_throws) const{
		if(h0_<=0){if(enable_throws){rat_throw_line("initial element size must be larger than zero");} return false;};
		if(fd_==NULL){if(enable_throws){rat_throw_line("distance function points to NULL");} return false;};
		if(fh_==NULL){if(enable_throws){rat_throw_line("scaling function points to NULL");} return false;};
		if(!fd_->is_valid(enable_throws))return false;
		if(!fh_->is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string CrossDMsh::get_type(){
		return "rat::mdl::crossdmsh";
	}

	// method for serialization into json
	void CrossDMsh::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		js["type"] = get_type();

		// quad mesher enabled
		js["quad"] = quad_;

		// element size
		js["h0"] = h0_;

		// externally supplied fixed point list
		for(arma::uword i=0;i<pfix_ext_.n_rows;i++){
			js["pfix_x"].append(pfix_ext_(i,0));
			js["pfix_y"].append(pfix_ext_(i,1));
		}

		// serialize geometry and scaling distance functions
		js["geometryfun"] = cmn::Node::serialize_node(fd_,list);
		js["scalingfun"] = cmn::Node::serialize_node(fh_,list);
	}

	// method for deserialisation from json
	void CrossDMsh::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// quad mesher enabled 
		quad_ = js["quad"].asBool();

		// element size
		h0_ = js["h0"].asDouble();

		// externally supplied fixed point list
		pfix_ext_.set_size(js["pfix_x"].size(),2);
		auto it1 = js["pfix_x"].begin(); auto it2 = js["pfix_y"].begin(); arma::uword idx = 0;
		for(;it1!=js["pfix_x"].end() && it2!=js["pfix_y"].end();it1++,it2++,idx++){
			pfix_ext_(idx,0) = (*it1).asDouble(); 
			pfix_ext_(idx,1) = (*it2).asDouble();
		}

		// deserialize distance and scaling functions
		fd_ = cmn::Node::deserialize_node<dm::DistFun>(js["geometryfun"], list, factory_list, pth);
		fh_ = cmn::Node::deserialize_node<dm::DistFun>(js["scalingfun"], list, factory_list, pth);
	}

}}