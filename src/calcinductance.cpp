// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcinductance.hh"
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"
#include "coildata.hh"



// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcInductance::CalcInductance(){
		set_name("inductance");
	}

	// constructor
	CalcInductance::CalcInductance(const ShModelPr &model){
		set_name("inductance"); set_model(model);
	}

	// factory
	ShCalcInductancePr CalcInductance::create(){
		return std::make_shared<CalcInductance>();
	}

	// factory
	ShCalcInductancePr CalcInductance::create(const ShModelPr &model){
		return std::make_shared<CalcInductance>(model);
	}

	// set parallel calculation
	void CalcInductance::set_use_parallel(const bool use_parallel){
		use_parallel_ = use_parallel;
	}

	// set group circuits
	void CalcInductance::set_group_circuits(const bool group_circuits){
		group_circuits_ = group_circuits;
	}
	
	// get group circuits
	bool CalcInductance::get_group_circuits() const{
		return group_circuits_;
	}

	// inductance data
	std::list<ShDataPr> CalcInductance::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_inductance(time,lg)};
	}

	// calculate mutual inductance between ms1 and ms2
	ShInductanceDataPr CalcInductance::calculate_inductance(const fltp time, const cmn::ShLogPr &lg){
		// general settings
		const fltp inductance_current = RAT_CONST(1000.0);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Inductance Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create timer
		arma::wall_clock timer;

		// set timer
		timer.tic();

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);
		
		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 
		
		// walk over meshes
		std::list<ShCoilDataPr> coils;
		for(auto it=meshes.begin();it!=meshes.end();it++){
			const ShCoilDataPr coil = std::dynamic_pointer_cast<CoilData>(*it);
			if(coil!=NULL)coils.push_back(coil);
		}

		// number of coils
		const arma::uword num_coils = coils.size();

		// walk over coils
		for(auto it=coils.begin();it!=coils.end();it++){
			// set to vector potential calculation only
			(*it)->set_field_type('A',3);

			// check if current available
			if((*it)->get_operating_current()==0)
				(*it)->set_operating_current(inductance_current);
		}

		// walk over coil combinations and assemble list
		// arma::Mat<arma::uword> ijlist(num_coils*num_coils,2);
		// arma::uword cnt = 0;
		// for(arma::uword i=0;i<num_coils;i++){
		// 	for(arma::uword j=0;j<num_coils;j++){
		// 		ijlist(cnt,0) = i; ijlist(cnt,1) = j; cnt++;
		// 	}
		// }

		// sanity check
		// assert(cnt==num_coils*num_coils);

		// drop lower triangle
		// ijlist = ijlist.rows(arma::find(ijlist.col(0)>=ijlist.col(1)));

		// // create settings for mlfmm
		// fmm::ShSettingsPr stngs = fmm::Settings::create();
		// stngs->set_direct(fmm::DirectMode::TRESHOLD);
		// if(use_parallel_){
		// 	stngs->set_split_s2t(false);
		// 	stngs->set_split_m2l(false);
		// 	stngs->set_parallel_m2m(false);
		// 	stngs->set_parallel_m2l(false);
		// 	stngs->set_parallel_l2l(false);
		// 	stngs->set_parallel_tree_setup(false);
		// }

		// report
		lg->msg("%s%sCALCULATING INDUCTANCE%s\n",KBLD,KGRN,KNRM);

		// groups 
		std::map<arma::uword, std::list<ShCoilDataPr> > coil_groups;

		// list of coil names
		arma::field<std::string> group_names;

		// in case of drives
		if(group_circuits_){
			// report
			lg->msg("grouping by circuit\n");

			// walk over meshes and extract drive
			for(auto it=coils.begin();it!=coils.end();it++)
				coil_groups[(*it)->get_circuit_index()].push_back(*it);
				
			// set group names
			arma::uword idx = 0;
			group_names.set_size(coil_groups.size());
			for(auto it=coil_groups.begin();it!=coil_groups.end();it++,idx++)
				group_names(idx) = "Circuit" + std::to_string((*it).first);
		}

		// in case of single coils
		else{
			// report
			lg->msg("using single coils\n");

			// set source and target coils
			arma::uword idx = 0;
			group_names.set_size(num_coils);
			for(auto it=coils.begin();it!=coils.end();it++,idx++){
				coil_groups[idx].push_back(*it);
				group_names(idx) = (*it)->get_name();
			}
		}

		// number of groups
		const arma::uword num_groups = coil_groups.size();

		// allocate matrix
		arma::Mat<fltp> M(num_groups,num_groups,arma::fill::zeros);
		arma::Mat<fltp> E(num_groups,num_groups,arma::fill::zeros);

		// first index
		arma::uword idx1=0;

		// walk over coils
		for(auto it1=coil_groups.begin();it1!=coil_groups.end();it1++,idx1++){
			// get mesh
			const std::list<ShCoilDataPr> &mesh1 = (*it1).second;

			// second index
			arma::uword idx2 = idx1;

			// walk over meshes
			for(auto it2=it1;it2!=coil_groups.end();it2++,idx2++){
				// get mesh
				const std::list<ShCoilDataPr> &mesh2 = (*it2).second;

				// create multilevel fast multipole method object
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

				// multisources
				const fmm::ShMultiSourcesPr src = fmm::MultiSources::create();
				const fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();

				// add sources and targets to the calculation
				for(auto it=mesh1.begin();it!=mesh1.end();it++)src->add_sources(*it);
				for(auto it=mesh2.begin();it!=mesh2.end();it++)tar->add_targets(*it);
				
				// add to MLFMM
				mlfmm->set_sources(src);
				mlfmm->set_targets(tar);

				// setup multipole method
				mlfmm->setup(lg);

				// run multipole method
				mlfmm->calculate(lg);

				// get circuit current
				const fltp I1 = (*mesh1.begin())->get_operating_current();

				// stored energy in this connection
				for(auto it3=mesh2.begin();it3!=mesh2.end();it3++){
					// calculate captured flux in second coil
					const fltp phi2 = (*it3)->calculate_flux();

					// calculate inductance based on current in first coil
					M(idx1,idx2) += phi2/I1;

					// self inductance
					if(idx1==idx2)M(idx1,idx2) += (*it3)->calculate_element_self_inductance();

					//calculate stored energy
					E(idx1,idx2) += RAT_CONST(0.5)*phi2*(*it3)->get_operating_current();
				}

				// mirror
				if(idx1!=idx2){M(idx2,idx1) = M(idx1,idx2); E(idx2,idx1) = E(idx1,idx2);}
			}
		}

		// get calculation time
		const fltp calculation_time = timer.toc();

		// create data object
		const ShInductanceDataPr ind_data = InductanceData::create(time,M,E,group_names,calculation_time);

		// create inductance object and return
		return ind_data;
	}

	// get type
	std::string CalcInductance::get_type(){
		return "rat::mdl::calcinductance";
	}

	// method for serialization into json
	void CalcInductance::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		CalcLeaf::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["use_parallel"] = use_parallel_;
		js["group_circuits"] = group_circuits_;

	}

	// method for deserialisation from json
	void CalcInductance::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);
		set_use_parallel(js["use_parallel"].asBool());
		set_group_circuits(js["group_circuits"].asBool());
	}

}}