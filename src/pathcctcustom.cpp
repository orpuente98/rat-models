// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcctcustom.hh"

// common headers
#include "rat/common/extra.hh"

// model headers
#include "transbend.hh"

// code specific to Rat
namespace rat{namespace mdl{

	

	// function for acquiring number of poles
	arma::uword CCTHarmonic::get_num_poles() const{
		return num_poles_; 
	}

	void CCTHarmonic::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	// function for acquiring number of poles
	bool CCTHarmonic::get_is_skew() const{
		return is_skew_; 
	}

	void CCTHarmonic::set_is_skew(const bool is_skew){
		is_skew_ = is_skew;
	}

	// vallidity check
	bool CCTHarmonic::is_valid(const bool enable_throws) const{
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string CCTHarmonic::get_type(){
		return "rat::mdl::cctharmonic";
	}

	// get normalization setting
	bool CCTHarmonic::get_normalize_length() const{
		return normalize_length_;
	}

	// set normalization setting
	void CCTHarmonic::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}

	// theta fun
	arma::Row<fltp> CCTHarmonic::calc_position(
		const arma::Row<fltp> &theta, 
		const fltp thetamin, 
		const fltp thetamax)const{
		if(normalize_length_){
			return (theta-thetamin)/(thetamax-thetamin) - RAT_CONST(0.5);
		}else{
			return theta/(2*arma::Datum<fltp>::pi);
		}
	}

	// method for serialization into json
	void CCTHarmonic::serialize(
		Json::Value &js, cmn::SList &list) const{
		
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();

		// harmonic
		js["num_poles"] = (int)num_poles_;
		js["is_skew"] = is_skew_;

		// normalization
		js["normalize_length"] = normalize_length_;
	}

	// method for deserialisation from json
	void CCTHarmonic::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// harmonic
		set_num_poles(js["num_poles"].asUInt64());
		set_is_skew(js["is_skew"].asBool());

		// normalization
		set_normalize_length(js["normalize_length"].asBool());
	}



	// default constructor
	CCTHarmonicInterp::CCTHarmonicInterp(){
		set_name("harmonic");
	}

	// constructor
	CCTHarmonicInterp::CCTHarmonicInterp(
		const arma::uword num_poles, const bool is_skew, 
		const arma::Row<fltp> &turn, const arma::Row<fltp> &a,
		const bool normalize_length){
		set_name("harmonic");
		num_poles_ = num_poles; is_skew_ = is_skew;
		turn_ = turn; a_ = a;
		normalize_length_ = normalize_length;
	}

	// factory
	ShCCTHarmonicInterpPr CCTHarmonicInterp::create(){
		return std::make_shared<CCTHarmonicInterp>();
	}

	// factory
	ShCCTHarmonicInterpPr CCTHarmonicInterp::create(
		const arma::uword num_poles, const bool is_skew, 
		const arma::Row<fltp> &turn, const arma::Row<fltp> &a,
		const bool normalize_length){
		return std::make_shared<CCTHarmonicInterp>(num_poles,is_skew,turn,a,normalize_length);
	}

	// calculate offset in z
	arma::field<arma::Row<fltp> > CCTHarmonicInterp::calc_z(
		const arma::Row<fltp> &theta, const arma::Row<fltp> &rho,
		const fltp thetamin, const fltp thetamax) const{
		
		// calculate position
		const arma::Row<fltp> position = calc_position(theta, thetamin, thetamax);

		// calculate angle
		arma::Col<fltp> a;
		cmn::Extra::interp1(turn_.t(),a_.t(),position.t(),a);

		// convert amplitude to angle
		const arma::Row<fltp> alpha = arma::atan(rho/a.t());

		// allocate z and derivatives thereoff
		arma::field<arma::Row<fltp> > z(4);

		// convert number of poles to float
		const fltp np = fltp(num_poles_);

		// skew harmonics
		if(is_skew_){
			z(0) = rho%arma::cos(np*theta)/(np*arma::tan(alpha));
			z(1) = -rho%arma::sin(np*theta)/(arma::tan(alpha));
			z(2) = -np*rho%arma::cos(np*theta)/(arma::tan(alpha));
			z(3) = np*np*rho%arma::sin(np*theta)/(arma::tan(alpha));
		}

		// normal harmonics
		else{
			z(0) = rho%arma::sin(np*theta)/(np*arma::tan(alpha));
			z(1) = rho%arma::cos(np*theta)/(arma::tan(alpha));
			z(2) = -np*rho%arma::sin(np*theta)/(arma::tan(alpha));
			z(3) = -np*np*rho%arma::cos(np*theta)/(arma::tan(alpha));
		}

		// reverse direction for even num poles
		if(num_poles_%2==0)for(arma::uword i=0;i<z.n_elem;i++)z(i)*=-1;

		// return coordinates
		return z;
	}

	// get type
	std::string CCTHarmonicInterp::get_type(){
		return "rat::mdl::cctharmonicinterp";
	}

	// method for serialization into json
	void CCTHarmonicInterp::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		CCTHarmonic::serialize(js,list);
		js["type"] = get_type();
		js["turn"] = Node::serialize_matrix(turn_);
		js["amplitude"] = Node::serialize_matrix(a_);
	}

	// method for deserialisation from json
	void CCTHarmonicInterp::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		CCTHarmonic::deserialize(js,list,factory_list,pth);
		turn_ = Node::deserialize_matrix(js["turn"]);
		a_ = Node::deserialize_matrix(js["amplitude"]);
	}





	// constructor
	CCTHarmonicEquation::CCTHarmonicEquation(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun){
		fun_ = fun; num_poles_ = num_poles; is_skew_ = is_skew;
	}

	// factory
	ShCCTHarmonicEquationPr CCTHarmonicEquation::create(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun){
		return std::make_shared<CCTHarmonicEquation>(num_poles,is_skew,fun);
	}

	// calculate offset in z
	arma::field<arma::Row<fltp> > CCTHarmonicEquation::calc_z(
		const arma::Row<fltp> &theta, const arma::Row<fltp> &rho,
		const fltp thetamin, const fltp thetamax) const{

		// calculate angle
		const arma::Row<fltp> a = fun_(calc_position(theta, thetamin, thetamax));

		// convert amplitude to angle
		const arma::Row<fltp> alpha = arma::atan(rho/a);
		
		// allocate z and derivatives thereoff
		arma::field<arma::Row<fltp> > z(4);

		// convert number of poles to float
		const fltp np = fltp(num_poles_);

		// skew harmonics
		if(is_skew_){
			z(0) = rho%arma::cos(np*theta)/(np*arma::tan(alpha));
			z(1) = -rho%arma::sin(np*theta)/(arma::tan(alpha));
			z(2) = -np*rho%arma::cos(np*theta)/(arma::tan(alpha));
			z(3) = np*np*rho%arma::sin(np*theta)/(arma::tan(alpha));
		}

		// normal harmonics
		else{
			z(0) = rho%arma::sin(np*theta)/(np*arma::tan(alpha));
			z(1) = rho%arma::cos(np*theta)/(arma::tan(alpha));
			z(2) = -np*rho%arma::sin(np*theta)/(arma::tan(alpha));
			z(3) = -np*np*rho%arma::cos(np*theta)/(arma::tan(alpha));
		}

		// reverse direction for even num poles
		if(num_poles_%2==0)for(arma::uword i=0;i<z.n_elem;i++)z(i)*=-1;

		// return coordinates
		return z;
	}


	// default constructor
	CCTHarmonicDrive::CCTHarmonicDrive(){
		set_name("harmonic");
	}

	// constructor
	CCTHarmonicDrive::CCTHarmonicDrive(const arma::uword num_poles, const bool is_skew, const ShDrivePr &drive){
		// set  to self
		set_num_poles(num_poles); set_is_skew(is_skew); set_drive(drive); 

		// decide on name
		if(num_poles_==1)set_name("dipole");
		else if(num_poles_==2)set_name("quadrupole");
		else if(num_poles_==3)set_name("sextupole");
		else if(num_poles_==4)set_name("octupole");
		else if(num_poles_==5)set_name("decapole");
		else set_name("manypole");
	}

	// constructor
	CCTHarmonicDrive::CCTHarmonicDrive(
		const arma::uword num_poles, const bool is_skew, const fltp amplitude) : 
		CCTHarmonicDrive(num_poles,is_skew,DriveDC::create(amplitude)){}

	// factory
	ShCCTHarmonicDrivePr CCTHarmonicDrive::create(){
		return std::make_shared<CCTHarmonicDrive>();
	}

	// factory
	ShCCTHarmonicDrivePr CCTHarmonicDrive::create(const arma::uword num_poles, const bool is_skew, const ShDrivePr &drive){
		return std::make_shared<CCTHarmonicDrive>(num_poles,is_skew,drive);
	}

	// factory
	ShCCTHarmonicDrivePr CCTHarmonicDrive::create(const arma::uword num_poles, const bool is_skew, const fltp amplitude){
		return std::make_shared<CCTHarmonicDrive>(num_poles,is_skew,amplitude);
	}
	
	// set drive
	void CCTHarmonicDrive::set_drive(const ShDrivePr &drive){
		assert(drive!=NULL);
		drive_ = drive;
	}

	// get the drive
	ShDrivePr CCTHarmonicDrive::get_drive() const{
		assert(drive_!=NULL);
		return drive_;
	}

	// calculate offset in z and its derivatives
	arma::field<arma::Row<fltp> > CCTHarmonicDrive::calc_z(
		const arma::Row<fltp> &theta, const arma::Row<fltp> &rho,
		const fltp thetamin, const fltp thetamax) const{
		// calculate amplitude
		const arma::Row<fltp> a = drive_->get_scaling(calc_position(theta, thetamin, thetamax));

		// convert amplitude to angle
		const arma::Row<fltp> alpha = arma::atan(rho/a);

		// allocate z and derivatives thereoff
		arma::field<arma::Row<fltp> > z(4);

		// convert number of poles to float
		const fltp np = fltp(num_poles_);

		// calculate expensive sin and cos functions
		const arma::Row<fltp> tan_alpha = arma::tan(alpha);
		const arma::Row<fltp> sin_np_theta = arma::sin(np*theta);
		const arma::Row<fltp> cos_np_theta = arma::cos(np*theta);
		
		// skew harmonics
		if(is_skew_){
			z(0) = rho%cos_np_theta/(np*tan_alpha);
			z(1) = -rho%sin_np_theta/(tan_alpha);
			z(2) = -np*rho%cos_np_theta/tan_alpha;
			z(3) = np*np*rho%sin_np_theta/tan_alpha;
		}

		// normal harmonics
		else{
			z(0) = rho%sin_np_theta/(np*tan_alpha);
			z(1) = rho%cos_np_theta/(tan_alpha);
			z(2) = -np*rho%sin_np_theta/tan_alpha;
			z(3) = -np*np*rho%cos_np_theta/tan_alpha;
		}

		// reverse direction for even num poles
		if(num_poles_%2==0)for(arma::uword i=0;i<z.n_elem;i++)z(i)*=-1;

		// return coordinates
		return z;
	}

	// vallidity check
	bool CCTHarmonicDrive::is_valid(const bool enable_throws) const{
		if(!CCTHarmonic::is_valid(enable_throws))return false;
		if(drive_==NULL){if(enable_throws){rat_throw_line("drive points to NULL");} return false;};
		return true;
	}

	// get type
	std::string CCTHarmonicDrive::get_type(){
		return "rat::mdl::cctharmonicdrive";
	}

	// method for serialization into json
	void CCTHarmonicDrive::serialize(
		Json::Value &js, cmn::SList &list) const{
		
		// parent objects
		CCTHarmonic::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize drive
		js["drive"] = cmn::Node::serialize_node(get_drive(), list);
	}

	// method for deserialisation from json
	void CCTHarmonicDrive::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		CCTHarmonic::deserialize(js,list,factory_list,pth);
		set_drive(cmn::Node::deserialize_node<Drive>(js["drive"], list, factory_list, pth));
	}




	// default constructor
	PathCCTCustom::PathCCTCustom(){
		set_name("custom cct");
	}

	// factory
	ShPathCCTCustomPr PathCCTCustom::create(){
		return std::make_shared<PathCCTCustom>();
	}

	// set range
	void PathCCTCustom::set_range(const fltp nt1, const fltp nt2){
		set_nt1(nt1); set_nt2(nt2); set_nt1nrm(nt1); set_nt2nrm(nt2);
	}

	// set range
	void PathCCTCustom::set_range(const fltp nt1, const fltp nt2, const fltp nt1nrm, const fltp nt2nrm){
		set_nt1(nt1); set_nt2(nt2); set_nt1nrm(nt1nrm); set_nt2nrm(nt2nrm);
	}

	// get normalization setting
	bool PathCCTCustom::get_normalize_length() const{
		return normalize_length_;
	}

	// set normalization setting
	void PathCCTCustom::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}

	

	// set range
	void PathCCTCustom::set_nt1(const fltp nt1){
		nt1_ = nt1;
	}

	// set range
	void PathCCTCustom::set_nt2(const fltp nt2){
		nt2_ = nt2;
	}

	// set range for drive normalization
	void PathCCTCustom::set_nt1nrm(const fltp nt1nrm){
		nt1nrm_ = nt1nrm;
	}

	// set range for drive normalization
	void PathCCTCustom::set_nt2nrm(const fltp nt2nrm){
		nt2nrm_ = nt2nrm;
	}

	// set range
	fltp PathCCTCustom::get_nt1() const{
		return nt1_;
	}

	// set range
	fltp PathCCTCustom::get_nt2() const{
		return nt2_;
	}

	// set range
	fltp PathCCTCustom::get_nt1nrm() const{
		return nt1nrm_;
	}

	// set range
	fltp PathCCTCustom::get_nt2nrm() const{
		return nt2nrm_;
	}

	// set the direction of the layer
	void PathCCTCustom::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}
	
	// set the direction of the layer
	bool PathCCTCustom::get_is_reverse() const{
		return is_reverse_;
	}

	// set the direction of the layer
	void PathCCTCustom::set_bending_arc_length(const fltp bending_arc_length){
		bending_arc_length_ = bending_arc_length;
	}

	// set the direction of the layer
	fltp PathCCTCustom::get_bending_arc_length() const{
		return bending_arc_length_;
	}


	// add harmonic
	arma::uword PathCCTCustom::add_harmonic(const ShCCTHarmonicPr &harm){
		// set new source list
		const arma::uword index = harmonics_.size()+1;
		harmonics_.insert({index,harm}); return index;
	}

	// delete harmonic
	bool PathCCTCustom::delete_harmonic(const arma::uword index){
		auto it = harmonics_.find(index);
		if(it==harmonics_.end())return false;
		(*it).second = NULL; return true;
	}

	arma::uword PathCCTCustom::num_harmonics()const{
		return harmonics_.size();
	}

	// get harmonic
	ShCCTHarmonicPr PathCCTCustom::get_harmonic(const arma::uword index) const{
		auto it = harmonics_.find(index);
		if(it==harmonics_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// set omega drive
	void PathCCTCustom::set_omega(const ShDrivePr &omega){
		omega_ = omega;
	}

	// set constant pitch
	void PathCCTCustom::set_omega(const fltp omega){
		omega_ = DriveDC::create(omega);
	}

	// set radius drive
	void PathCCTCustom::set_rho(const ShDrivePr &rho){
		rho_ = rho;
	}

	// set constant radius
	void PathCCTCustom::set_rho(const fltp rho){
		rho_ = DriveDC::create(rho);
	}

	// get omega drive
	ShDrivePr PathCCTCustom::get_omega()const{
		return omega_;
	}

	// get radius drive
	ShDrivePr PathCCTCustom::get_rho()const{
		return rho_;
	}

	// set number of nodes for each turn
	void PathCCTCustom::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		assert(num_nodes_per_turn>0);
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// set number of nodes for each turn
	arma::uword PathCCTCustom::get_num_nodes_per_turn()const{
		return num_nodes_per_turn_;
	}

	// set number of layers
	void PathCCTCustom::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	// get number of layers
	arma::uword PathCCTCustom::get_num_layers() const{
		return num_layers_;
	}

	// set radius increment between layers
	void PathCCTCustom::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	// get radius increment between layers
	fltp PathCCTCustom::get_rho_increment()const{
		return rho_increment_;
	}

	// set omega setting
	void PathCCTCustom::set_use_omega_normal(const bool use_omega_normal){
		use_omega_normal_ = use_omega_normal;
	}

	// get omega setting
	bool PathCCTCustom::get_use_omega_normal() const{
		return use_omega_normal_;
	}

	// theta fun
	arma::Row<fltp> PathCCTCustom::calc_position(
		const arma::Row<fltp> &theta, 
		const fltp thetamin, 
		const fltp thetamax)const{
		if(normalize_length_){
			return (theta-thetamin)/(thetamax-thetamin) - RAT_CONST(0.5);
		}else{
			return theta/(2*arma::Datum<fltp>::pi);
		}
	}

	// function for interpolating matrix
	arma::Mat<rat::fltp> PathCCTCustom::interp_matrix(
		const arma::Col<fltp> &X, 
		const arma::Mat<fltp> &M, 
		const arma::Col<fltp> &Xi){
		// allocate output
		arma::Mat<rat::fltp> Mi(Xi.n_elem, M.n_cols);
		
		// walk over dimensions
		for(arma::uword i=0;i<M.n_cols;i++){
			arma::Col<rat::fltp> Mic;
			arma::interp1(X, M.col(i), Xi, Mic);
			Mi.col(i) = Mic;
		}

		// output matrix
		return Mi;
	}
	

	// setup coordinates and orientation vectors
	ShFramePr PathCCTCustom::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// number of poles
		arma::uword num_poles_max = 1;
		for(auto it=harmonics_.begin();it!=harmonics_.end();it++)
			num_poles_max = std::max(num_poles_max,(*it).second->get_num_poles());

		// calculate number of sections per half
		const arma::uword num_sect_per_turn = 4*num_poles_max;
		const fltp sectsize = 2*arma::Datum<fltp>::pi/num_sect_per_turn;

		// calculate maximum theta
		const fltp thetamin = std::min(RAT_CONST(0.0),nt1_*2*arma::Datum<fltp>::pi);
		const fltp thetamax = std::max(RAT_CONST(0.0),nt2_*2*arma::Datum<fltp>::pi);

		// normalized theta min and theta max
		// the drives will be running from -0.5 to 0.5 on this interval
		const fltp thetaminnrm = nt1nrm_*2*arma::Datum<fltp>::pi;
		const fltp thetamaxnrm = nt2nrm_*2*arma::Datum<fltp>::pi;

		// get number of sections
		const arma::sword num_int1 = arma::sword(std::floor(thetamin/sectsize));
		const arma::sword num_int2 = arma::sword(std::ceil(thetamax/sectsize));

		// calculate number of sections
		arma::uword num_sections = num_int2 - num_int1;

		// allocate theta
		// arma::Row<fltp> theta_start(num_sections);
		// arma::Row<fltp> theta_end(num_sections);
		arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(num_int1*sectsize,num_int2*sectsize,num_sections + 1);

		// calculate number of nodes
		// for this section
		arma::uword num_nodes_section = std::max(2llu,num_nodes_per_turn_/num_sect_per_turn+1);
		while((num_nodes_section-1)%stngs.element_divisor!=0)num_nodes_section++;

		// allocate list of frames
		std::list<ShFramePr> frame_list;

		arma::Row<fltp> magnet_length(num_layers_,arma::fill::zeros);
		arma::Row<fltp> magnet_radius(num_layers_,arma::fill::zeros);

		// walk over layers
		for(arma::uword k=0;k<num_layers_;k++){
			// reverse
			const bool rev = (int(is_reverse_) + k)%2==1;

			// calculate start position
			// const arma::uword nn= arma::uword(std::abs(nt1_))*num_nodes_per_turn_;
			// const arma::Row<fltp> turns = arma::linspace<arma::Row<fltp> >(0,nt1_,nn);
			// const arma::Row<fltp> omegas = omega_->get_scaling(normalize_length_ ? turns/(nt2_ - nt1_) : turns);
			//arma::accu(arma::diff(turns,1,1)%(omegas.head_cols(nn-1) + omegas.tail_cols(nn-1))/2);

			// integrated offset
			fltp zoffset = 0;

			// allocate
			arma::field<arma::Mat<fltp> > R(1,num_sections);
			arma::field<arma::Mat<fltp> > L(1,num_sections);
			arma::field<arma::Mat<fltp> > N(1,num_sections); 
			arma::field<arma::Mat<fltp> > D(1,num_sections);
			arma::field<arma::Row<fltp> > angle(1,num_sections);
			arma::field<arma::Row<fltp> > omega(1,num_sections);
			arma::field<arma::Row<fltp> > rho(1,num_sections);
			arma::field<arma::Row<fltp> > theta_section(1,num_sections);
			arma::field<arma::Row<fltp> > z(1,num_sections);
			arma::field<arma::Row<fltp> > vz(1,num_sections);
			arma::field<arma::Row<fltp> > az(1,num_sections);
			arma::field<arma::Row<fltp> > jz(1,num_sections);

			// allocate section and turn indexes
			arma::Row<arma::uword> section_idx(num_sections);
			arma::Row<arma::uword> turn_idx(num_sections); 
			arma::Row<arma::uword> num_nodes(num_sections); 

			// walk over sections
			for(arma::uword i=0;i<num_sections;i++){
				// calculate section
				const fltp theta1 = theta(i);
				const fltp theta2 = theta(i+1);
				assert(theta2>theta1);

				// create theta (taking into account last section
				theta_section(i) = arma::linspace<arma::Row<fltp> >(theta1, theta2, num_nodes_section);

				// calculate position
				const arma::Row<fltp> position = 
					calc_position(theta_section(i), thetaminnrm, thetamaxnrm);

				// get radius
				rho(i) = k*rho_increment_ + rho_->get_scaling(position);

				// get pitch
				omega(i) = omega_->get_scaling(position);

				// check radius
				magnet_radius(k) = std::max(magnet_radius(k),arma::max(rho(i)));

				// create offset in z
				z(i).zeros(num_nodes_section);
				vz(i).zeros(num_nodes_section);
				az(i).zeros(num_nodes_section);
				jz(i).zeros(num_nodes_section);

				// walk over harmonics
				for(auto it=harmonics_.begin();it!=harmonics_.end();it++){
					const arma::field<arma::Row<fltp> > zfld = (*it).second->calc_z(theta_section(i), rho(i), thetaminnrm, thetamaxnrm);
					z(i) += zfld(0); vz(i) += zfld(1); az(i) += zfld(2); jz(i) += zfld(3);
				}

				// reverse axial direction if needed
				if(rev){
					z(i) *= -1; vz(i) *= -1; az(i) *= -1; jz(i) *= -1;
				}

				// calculate angle
				angle(i) = arma::atan2(vz(i),rho(i)-k*rho_increment_);
			}

			// allocate
			arma::field<arma::Row<fltp> > pitch_req(1,num_sections+num_sect_per_turn);

			// automatic pitch calculation
			if(use_omega_normal_){
				// walk over sections
				for(arma::uword i=0;i<num_sections-num_sect_per_turn;i++){
					const arma::Row<fltp> pitch1 = omega(i)/arma::cos(angle(i));
					const arma::Row<fltp> pitch2 = omega(i+num_sect_per_turn)/arma::cos(angle(i+num_sect_per_turn));
					const arma::Row<fltp> delta = z(i+num_sect_per_turn) - z(i);
					pitch_req(i+num_sect_per_turn) = arma::abs((pitch1+pitch2)/2 - delta);
				}
			}

			// otherwise use omega
			else{
				for(arma::uword i=0;i<num_sections-num_sect_per_turn;i++){
					pitch_req(i+num_sect_per_turn) = (omega(i) + omega(i+num_sect_per_turn))/2;
				}
			}

			// repeat first and last turn
			for(arma::uword i=0;i<num_sect_per_turn;i++){
				pitch_req(i) = pitch_req(i+num_sect_per_turn);
				pitch_req(num_sections+num_sect_per_turn-i-1) = pitch_req(num_sections-i-1);
			}

			// walk over sections
			for(arma::uword i=0;i<num_sections;i++){
				// calculate turn number
				const arma::Row<fltp> turn = theta_section(i)/(2*arma::Datum<fltp>::pi);

				// calculate mid sections
				fltp max_pitch_mid = 0;
				const arma::uword idx1 = i;
				const arma::uword idx2 = i+num_sect_per_turn;
				for(arma::uword j=idx1+1;j<=idx2-1;j++){
					max_pitch_mid = std::max(max_pitch_mid,arma::max(pitch_req(j)));
				}

				// calculate locally maximum
				arma::Row<fltp> pitch(num_nodes_section);
				for(arma::uword j=0;j<num_nodes_section;j++)
					pitch(j) = std::max(max_pitch_mid, 
						arma::as_scalar(arma::max(arma::join_horiz(
						pitch_req(idx1).tail_cols(num_nodes_section-j),
						pitch_req(idx2).head_cols(j)))));

				// calculate local offset
				const arma::Row<fltp> dz = 
					arma::join_horiz(arma::Row<fltp>{0},
					arma::cumsum(arma::diff(turn,1,1)%(
					pitch.head_cols(num_nodes_section-1) + 
					pitch.tail_cols(num_nodes_section-1))/2));

				// offset z coordinates with the pitch
				z(i) += dz + zoffset; vz(i) += pitch/(2*arma::Datum<fltp>::pi); 

				// increment offset
				zoffset += dz.back();

				// create coordinates for circle
				const arma::Row<fltp> x = rho(i)%arma::cos(theta_section(i));
				const arma::Row<fltp> y = rho(i)%arma::sin(theta_section(i));
				const arma::Row<fltp> vx = -rho(i)%arma::sin(theta_section(i));
				const arma::Row<fltp> vy = rho(i)%arma::cos(theta_section(i));
				const arma::Row<fltp> ax = -rho(i)%arma::cos(theta_section(i));
				const arma::Row<fltp> ay = -rho(i)%arma::sin(theta_section(i));
				const arma::Row<fltp> jx = rho(i)%arma::sin(theta_section(i));
				const arma::Row<fltp> jy = -rho(i)%arma::cos(theta_section(i));

				// store position vectors
				R(i) = arma::join_vert(x,y,z(i));

				// convert to 3XN matrices
				arma::Mat<fltp> V = arma::join_vert(vx,vy,vz(i));
				arma::Mat<fltp> A = arma::join_vert(ax,ay,az(i));
				arma::Mat<fltp> J = arma::join_vert(jx,jy,jz(i));

				// store longitudinal vectors
				L(i) = V.each_row()/cmn::Extra::vec_norm(V);

				// calculate radial vector
				arma::Mat<fltp> Vax(3,num_nodes_section,arma::fill::zeros);
				Vax.row(2).fill(RAT_CONST(1.0));
				D(i) = cmn::Extra::cross(L(i),Vax);		

				// create normal vectors
				N(i) = cmn::Extra::cross(L(i),D(i));

				// normalize
				D(i).each_row()/=cmn::Extra::vec_norm(D(i));
				N(i).each_row()/=cmn::Extra::vec_norm(N(i));

				// check size
				assert(R(i).n_rows==3);	assert(L(i).n_rows==3);
				assert(N(i).n_rows==3);	assert(D(i).n_rows==3);

				// set section and turn
				section_idx(i) = i%num_sect_per_turn;
				turn_idx(i) = i/num_sect_per_turn;
			}

			// get offset
			fltp zmid = 0;
			for(arma::uword i=0;i<num_sections;i++){
				if(theta_section(i).front()<=0 && theta_section(i).back()>=0){
					zmid = rat::cmn::Extra::interp1(theta_section(i), R(i).row(2), RAT_CONST(0.0)); break;
				}
			}
			
			// apply offset
			for(arma::uword i=0;i<num_sections;i++){
				R(i).row(2) -= zmid;
			}

			// cutoff
			fltp thetamin_clip = nt1_*2*arma::Datum<fltp>::pi;
			fltp thetamax_clip = nt2_*2*arma::Datum<fltp>::pi;
			arma::uword idx_first = 0, idx_last = 0;
			for(arma::uword i=0;i<num_sections;i++){
				if(theta_section(i).back()>thetamin_clip){idx_first = i; break;}
			}
			for(arma::uword i=num_sections;i>0;i--){
				if(theta_section(i-1).front()<thetamax_clip){idx_last = i-1; break;}
			}
			if(idx_first==0 && idx_last==0)
				rat_throw_line("no sections available to clip");

			// perform clip
			z = z.cols(idx_first, idx_last);
			R = R.cols(idx_first, idx_last);
			L = L.cols(idx_first, idx_last);
			N = N.cols(idx_first, idx_last);
			D = D.cols(idx_first, idx_last);
			section_idx.cols(idx_first, idx_last);
			turn_idx.cols(idx_first, idx_last);
			theta_section = theta_section.cols(idx_first, idx_last);

			// clip first section
			arma::Col<arma::uword> idx_first_vec = arma::find(theta_section(0)>thetamin_clip+1e-14,1,"first");
			if(!idx_first_vec.empty()){
				idx_first = arma::as_scalar(idx_first_vec);
				arma::Row<rat::fltp> theta_interp1 = arma::join_horiz(
					arma::Row<fltp>{thetamin_clip}, 
					theta_section(0).cols(idx_first, theta_section(0).n_elem-1));
				theta_interp1 = arma::unique(theta_interp1);
				R(0) = interp_matrix(theta_section(0).t(), R(0).t(), theta_interp1).t();
				L(0) = interp_matrix(theta_section(0).t(), L(0).t(), theta_interp1).t();
				N(0) = interp_matrix(theta_section(0).t(), N(0).t(), theta_interp1).t();
				D(0) = interp_matrix(theta_section(0).t(), D(0).t(), theta_interp1).t();
				z(0) = interp_matrix(theta_section(0).t(), z(0).t(), theta_interp1).t();
				theta_section(0) = theta_interp1;
			}else{
				R(0).clear(); L(0).clear(); N(0).clear(); 
				D(0).clear(); z(0).clear(); theta_section(0).clear();
			}

			// clip last section
			const arma::uword lsid = theta_section.n_elem-1;
			arma::Col<arma::uword> idx_last_vec = arma::find(theta_section(lsid)<thetamax_clip-1e-14,1,"last");
			if(!idx_last_vec.empty()){
				idx_last = arma::as_scalar(idx_last_vec);
				arma::Row<rat::fltp> theta_interp2 = arma::join_horiz(
					theta_section(lsid).cols(0, idx_last),
					arma::Row<fltp>{thetamax_clip});
				theta_interp2 = arma::unique(theta_interp2);
				R(lsid) = interp_matrix(theta_section(lsid).t(), R(lsid).t(), theta_interp2).t();
				L(lsid) = interp_matrix(theta_section(lsid).t(), L(lsid).t(), theta_interp2).t();
				N(lsid) = interp_matrix(theta_section(lsid).t(), N(lsid).t(), theta_interp2).t();
				D(lsid) = interp_matrix(theta_section(lsid).t(), D(lsid).t(), theta_interp2).t();
				z(lsid) = interp_matrix(theta_section(lsid).t(), z(lsid).t(), theta_interp2).t();
				theta_section(lsid) = theta_interp2;
			}else{
				R(lsid).clear(); L(lsid).clear(); N(lsid).clear(); 
				D(lsid).clear(); z(lsid).clear(); theta_section(lsid).clear();
			}

			// check if we completely removed section
			const arma::uword idx_start = arma::uword(R(0).n_cols<=1);
			const arma::uword idx_end = R.n_cols-1-arma::uword(R(lsid).n_cols<=1);
			R = R.cols(idx_start, idx_end); L = L.cols(idx_start, idx_end); N = N.cols(idx_start, idx_end);
			D = D.cols(idx_start, idx_end); z = z.cols(idx_start, idx_end); theta_section = theta_section.cols(idx_start, idx_end);

			// magnet length from first and last z
			magnet_length(k) = z(z.n_elem-1).back() - z(0).front();

			// reverse frame
			if(rev){
				cmn::Extra::reverse_field(R); cmn::Extra::reverse_field(L);
				cmn::Extra::reverse_field(D); cmn::Extra::reverse_field(N);
				for(arma::uword i=0;i<L.n_elem;i++){
					L(i) *= -1; N(i) *= -1;
				}
			}

			// create frame
			ShFramePr frame = Frame::create(R,L,N,D);

			// set section and turn indices
			frame->set_location(section_idx,turn_idx,num_sections);

			// add frame to list
			frame_list.push_back(frame);
		}

		// combine frames
		ShFramePr gen = Frame::create(frame_list);

		// circle CCT
		if(bending_arc_length_!=RAT_CONST(0.0)){
			const fltp bend_radius = arma::max(magnet_length)/bending_arc_length_;
			gen->apply_transformation(TransBend::create({0,1,0},{1,0,0},bend_radius,-arma::max(magnet_radius)));
			gen->apply_transformation(TransTranslate::create({bend_radius+arma::max(magnet_radius),0,0}));
		}

		// transformations
		gen->apply_transformations(get_transformations());

		// create frame
		return gen;
	}


	// re-index nodes after deleting
	void PathCCTCustom::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShCCTHarmonicPr> new_harmonics; arma::uword idx = 1;
		for(auto it=harmonics_.begin();it!=harmonics_.end();it++)
			if((*it).second!=NULL)new_harmonics.insert({idx++, (*it).second});
		harmonics_ = new_harmonics;
	}

	// validity check
	bool PathCCTCustom::is_valid(const bool enable_throws) const{
		if(!Transformations::is_valid(enable_throws))return false;
		if(num_layers_<=0){if(enable_throws){rat_throw_line("number of layers must be larger than zero");} return false;};
		if(num_nodes_per_turn_<=0){if(enable_throws){rat_throw_line("number of nodes per turn must be larger than zero");} return false;};
		if(nt2_<=nt1_){if(enable_throws){rat_throw_line("nt1 must be smaller than nt2");} return false;};
		if(omega_==NULL){if(enable_throws){rat_throw_line("omega drive points to NULL");} return false;};
		if(rho_==NULL){if(enable_throws){rat_throw_line("rho drive points to NULL");} return false;};
		if(normalize_length_){
			if(nt1nrm_>=nt2nrm_){if(enable_throws){rat_throw_line("nt1nrm must be larger than nt2nrm when normalization enabled");} return false;};
		}
		for(auto it=harmonics_.begin();it!=harmonics_.end();it++){
			const ShCCTHarmonicPr &harmonic = (*it).second;
			if(harmonic==NULL)rat_throw_line("harmonic list contains NULL");
			if(!harmonic->is_valid(enable_throws))return false;
		}
		return true;
	}

	// get type
	std::string PathCCTCustom::get_type(){
		return "rat::mdl::pathcctcustom";
	}

	// method for serialization into json
	void PathCCTCustom::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent objects
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// geometry
		js["nt1"] = nt1_;
		js["nt2"] = nt2_;
		js["nt1nrm"] = nt1nrm_;
		js["nt2nrm"] = nt2nrm_;
		js["num_nodes_per_turn"] = (unsigned int)num_nodes_per_turn_;
		js["normalize_length"] = normalize_length_;
		js["use_omega_normal"] = use_omega_normal_;

		// arrays
		js["omega"] = cmn::Node::serialize_node(omega_, list);
		js["rho"] = cmn::Node::serialize_node(rho_, list);
		js["bending_arc_length"] = bending_arc_length_;
		js["is_reverse"] = is_reverse_;

		// layers
		js["num_layers"] = (int)num_layers_;
		js["rho_increment"] = rho_increment_;

		// harmonics
		for(auto it = harmonics_.begin();it!=harmonics_.end();it++)
			js["harmonics"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void PathCCTCustom::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Transformations::deserialize(js,list,factory_list,pth);

		// geometry
		nt1_ = js["nt1"].asDouble();
		nt2_ = js["nt2"].asDouble();
		nt1nrm_ = js["nt1nrm"].asDouble();
		nt2nrm_ = js["nt2nrm"].asDouble();
		num_nodes_per_turn_ = js["num_nodes_per_turn"].asUInt64();
		set_is_reverse(js["is_reverse"].asBool());

		// arrays
		set_omega(cmn::Node::deserialize_node<Drive>(js["omega"], list, factory_list, pth));
		set_rho(cmn::Node::deserialize_node<Drive>(js["rho"], list, factory_list, pth));
		set_bending_arc_length(js["bending_arc_length"].asDouble());
		set_normalize_length(js["normalize_length"].asBool());
		set_use_omega_normal(js["use_omega_normal"].asBool());

		// layers
		set_num_layers(js["num_layers"].asUInt64());
		set_rho_increment(js["rho_increment"].asDouble());

		// subnodes
		for(auto it = js["harmonics"].begin();it!= js["harmonics"].end();it++)
			add_harmonic(cmn::Node::deserialize_node<CCTHarmonic>(*it, list, factory_list, pth));
	}

}}
