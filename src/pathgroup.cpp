// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathGroup::PathGroup(){
		// set default name
		set_name("pathgroup");

		// default orientation
		set_start_coord(arma::Col<fltp>::fixed<3>{0,0,0}); // origin
		set_start_longitudinal(arma::Col<fltp>::fixed<3>{0,1,0}); // along y
		set_start_normal(arma::Col<fltp>::fixed<3>{1,0,0}); // along x
	}

	// constructor with start point input
	PathGroup::PathGroup(
		const arma::Col<fltp>::fixed<3> &R0, 
		const arma::Col<fltp>::fixed<3> &L0, 
		const arma::Col<fltp>::fixed<3> &N0){
		// set default name
		set_name("pathgroup");

		// set to self
		set_start_coord(R0);
		set_start_longitudinal(L0);
		set_start_normal(N0);
	}

	// factory
	ShPathGroupPr PathGroup::create(){
		return std::make_shared<PathGroup>();
	}

	// factory with start point
	ShPathGroupPr PathGroup::create(
		const arma::Col<fltp>::fixed<3> &R0, 
		const arma::Col<fltp>::fixed<3> &L0, 
		const arma::Col<fltp>::fixed<3> &N0){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathGroup>(R0,L0,N0);
	}

	// set startpoint
	void PathGroup::set_start_coord(const arma::Col<fltp>::fixed<3> &R0){
		R0_ = R0;
	}

	// set start direction vector
	void PathGroup::set_start_longitudinal(const arma::Col<fltp>::fixed<3> &L0){
		L0_ = L0;
	}

	// set start normal vector
	void PathGroup::set_start_normal(const arma::Col<fltp>::fixed<3> &N0){
		N0_ = N0;
	}

	// set start transverse vector
	arma::Col<fltp>::fixed<3> PathGroup::get_start_coord() const{
		return R0_;
	}

	// set start direction vector
	arma::Col<fltp>::fixed<3> PathGroup::get_start_longitudinal() const{
		return L0_;
	}

	// set start normal vector
	arma::Col<fltp>::fixed<3> PathGroup::get_start_normal() const{
		return N0_;
	}

	

	// set frame
	void PathGroup::set_align_frame(const bool align_frame){
		align_frame_ = align_frame;
	}

	// retreive model at index
	ShPathPr PathGroup::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}
	

	// delete model at index
	bool PathGroup::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void PathGroup::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword PathGroup::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword PathGroup::get_num_paths() const{
		return input_paths_.size();
	}

	// get frame
	ShFramePr PathGroup::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// check start point orientation
		if(arma::as_scalar(cmn::Extra::vec_norm(L0_))<1e-12)
			rat_throw_line("longitudinal vector should have finite length");
		if(arma::as_scalar(cmn::Extra::vec_norm(N0_))<1e-12)
			rat_throw_line("normal vector should have finite length");
		if(arma::as_scalar(cmn::Extra::dot(L0_,N0_))>1e-12)
			rat_throw_line("longitudinal and normal start vectors should be orthogonal");

		// calculate transverse vector
		const arma::Col<fltp>::fixed<3> D0 = cmn::Extra::cross(N0_,L0_);

		// check if paths were set
		if(input_paths_.empty())return NULL;

		// allocate generator list
		std::list<ShFramePr> frame;

		// symmetry
		for(arma::uword i=0;i<sym_;i++){
			// is reverse
			const bool is_reverse = i%2==1;

			// frame unit
			std::list<ShFramePr> frame_unit;

			// gather frame from all paths
			for(auto it = input_paths_.begin();it!=input_paths_.end();it++){
				frame_unit.push_back((*it).second->create_frame(stngs));
				if(is_reverse)frame_unit.back()->reverse();
			}

			// reverse
			if(is_reverse)frame_unit.reverse();

			// add to frame
			frame.splice(frame.end(), frame_unit);
		}

		// combine frame
		ShFramePr output_frame = Frame::create(frame);

		// align sections
		if(align_frame_){
			output_frame->align_sections(R0_,
				L0_.each_row()/cmn::Extra::vec_norm(L0_),
				N0_.each_row()/cmn::Extra::vec_norm(N0_),
				D0.each_row()/cmn::Extra::vec_norm(D0));
		}

		// apply transformations
		output_frame->apply_transformations(get_transformations());

		// return the combined frame
		return output_frame;
	}

	// set properties
	void PathGroup::set_sym(const arma::uword sym){
		sym_ = sym;
	}
	
	// get properties
	arma::uword PathGroup::get_sym() const{
		return sym_;
	}

	// validity check
	bool PathGroup::is_valid(const bool enable_throws) const{
		if(!Transformations::is_valid(enable_throws))return false;
		if(arma::as_scalar(cmn::Extra::vec_norm(L0_))<1e-12){if(enable_throws){rat_throw_line("L vector has no length");} return false;};
		if(arma::as_scalar(cmn::Extra::vec_norm(N0_))<1e-12){if(enable_throws){rat_throw_line("N vector has no length");} return false;};
		if(arma::as_scalar(cmn::Extra::dot(L0_,N0_))>1e-12){if(enable_throws){rat_throw_line("L and N must be orthogonal");} return false;};
		if(sym_<1){if(enable_throws){rat_throw_line("symmetry must be larger than zero");} return false;};
		if(input_paths_.empty())return false;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++){
			const ShPathPr &input_path = (*it).second;
			if(input_path==NULL)rat_throw_line("path list contains NULL");
			if(!input_path->is_valid())return false;
		}
		return true;
	}

	// get type
	std::string PathGroup::get_type(){
		return "rat::mdl::pathgroup";
	}

	// method for serialization into json
	void PathGroup::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize the models
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));

		// properties
		js["sym"] = (int)sym_;
		js["align_frame"] = align_frame_;

		// start point orientation
		js["R0_x"] = R0_(0); js["R0_y"] = R0_(1); js["R0_z"] = R0_(2);
		js["L0_x"] = L0_(0); js["L0_y"] = L0_(1); js["L0_z"] = L0_(2);
		js["N0_x"] = N0_(0); js["N0_y"] = N0_(1); js["N0_z"] = N0_(2);
	}

	// method for deserialisation from json
	void PathGroup::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// deserialize the models
		for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
			add_path(cmn::Node::deserialize_node<Path>(
				(*it), list, factory_list, pth));

		// properties
		sym_ = js["sym"].asUInt64();
		align_frame_ = js["align_frame"].asBool();

		// start point orientation
		R0_(0) = js["R0_x"].asDouble(); R0_(1) = js["R0_y"].asDouble(); R0_(2) = js["R0_z"].asDouble();
		L0_(0) = js["L0_x"].asDouble(); L0_(1) = js["L0_y"].asDouble(); L0_(2) = js["L0_z"].asDouble();
		N0_(0) = js["N0_x"].asDouble(); N0_(1) = js["N0_y"].asDouble(); N0_(2) = js["N0_z"].asDouble();
	}

}}