// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "surfacedata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	SurfaceData::SurfaceData(){
		set_output_type("surface");
	}

	// default constructor
	SurfaceData::SurfaceData(
		const arma::Mat<fltp> &R, const arma::Mat<fltp> &L,
		const arma::Mat<fltp> &N, const arma::Mat<fltp> &D,
		const arma::Mat<arma::uword> &s){
		set_output_type("surface");
		set_mesh(R,s); set_longitudinal(L); set_normal(N); 
		set_transverse(D); 
	}

	// factory
	ShSurfaceDataPr SurfaceData::create(){
		return std::make_shared<SurfaceData>();
	}

	ShSurfaceDataPr SurfaceData::create(
		const arma::Mat<fltp> &R, const arma::Mat<fltp> &L,
		const arma::Mat<fltp> &N, const arma::Mat<fltp> &D,
		const arma::Mat<arma::uword> &s){
		return std::make_shared<SurfaceData>(R,L,N,D,s);
	}

	// VTK export
	ShVTKObjPr SurfaceData::export_vtk() const{
		// create unstructured mesh
		ShVTKUnstrPr vtk = VTKUnstr::create();

		// get element type
		const arma::uword element_type = VTKUnstr::get_element_type(s_.n_rows);

		// set the mesh
		vtk->set_mesh(Rt_, s_, element_type);
		
		// set name
		vtk->set_name(get_name());

		// add temperature to VTK
		vtk->set_nodedata(temperature_,"Temperature [K]");

		// add calculated vector potential
		if(has('A') && has_field())vtk->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");
		
		// add calculated magnetic flux density
		if(has('H') && has_field())vtk->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");
		
		// add calculated magnetisation
		if(has('M') && has_field())vtk->set_nodedata(get_field('M'),"Magnetisation [A/m]");
		
		// add calculated magnetic field
		if(has('H') && has_field())vtk->set_nodedata(get_field('H'),"Magnetic Field [A/m]");
	
		// return the unstructured mesh
		return vtk;
	}

	// set area 
	void SurfaceData::set_area(const ShAreaPr &area){
		area_ = area;
	}

	// set frame 
	void SurfaceData::set_frame(const ShFramePr &gen){
		gen_ = gen;
	}

	// set operating temperature
	void SurfaceData::set_operating_temperature(
		const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}


	void SurfaceData::set_temperature(const arma::Row<fltp> &temperature){
		temperature_ = temperature;
	}

	// set mesh
	void SurfaceData::set_mesh(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &s){
		assert(R.n_cols>s.max()); assert(R.n_rows == 3);
		assert(s.n_rows == 4 || s.n_rows==2 || s.n_rows==1);
		s_ = s; Rt_ = R; num_targets_ = Rt_.n_cols;
	}

	// set longitudinal vector
	void SurfaceData::set_longitudinal(const arma::Mat<fltp> &L){
		assert(L.n_rows==3);
		L_ = L;
	}

	// set transverse vector
	void SurfaceData::set_transverse(const arma::Mat<fltp> &D){
		assert(D.n_rows==3);
		D_ = D;
	}

	// set normal vectors
	void SurfaceData::set_normal(const arma::Mat<fltp> &N){
		assert(N.n_rows==3);
		N_ = N;
	}

	// calculate face normals at elements
	arma::Mat<fltp> SurfaceData::get_face_normals() const{
		if(s_.n_rows==4)return rat::cmn::Quadrilateral::calc_face_normals(Rt_,s_);
		if(s_.n_rows==3)return rat::cmn::Triangle::calc_face_normals(Rt_,s_);
		if(s_.n_rows<=2)return arma::Mat<fltp>(3,Rt_.n_cols);
		rat_throw_line("unkown mesh type");
		return{};
	}

	// get coordinates for section
	const arma::Mat<fltp>& SurfaceData::get_direction() const{
		return L_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& SurfaceData::get_normal() const{
		return N_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& SurfaceData::get_transverse() const{
		return D_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& SurfaceData::get_coords() const{
		return Rt_;
	}

	// create element coordinates
	arma::Mat<fltp> SurfaceData::get_element_coords()const{
		arma::Mat<fltp> Re(3,s_.n_cols);
		for(arma::uword i=0;i<s_.n_cols;i++)
			Re.col(i) = arma::mean(Rt_.cols(s_.col(i)),1);
		return Re;
	}
	
	// get coordinates for section
	const arma::Mat<arma::uword>& SurfaceData::get_elements() const{
		return s_;
	}

	// get number of elements
	arma::uword SurfaceData::get_num_elements() const{
		return s_.n_cols;
	}

}}