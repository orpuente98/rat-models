// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathfile.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathFile::PathFile(){

	}

	// constructor
	PathFile::PathFile(const boost::filesystem::path &file_name){
		set_file_name(file_name);
	}

	// factory
	ShPathFilePr PathFile::create(){
		return std::make_shared<PathFile>();
	}

	// factory
	ShPathFilePr PathFile::create(const boost::filesystem::path &file_name){
		return std::make_shared<PathFile>(file_name);
	}

	// set file name
	void PathFile::set_file_name(const boost::filesystem::path &file_name){
		file_name_ = file_name;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathFile::create_frame(const MeshSettings &/*stngs*/) const{
		if(file_name_=="")rat_throw_line("file name is not set");

		arma::Mat<fltp> filedata; //position

		filedata.load(file_name_.string(), arma::raw_ascii);

		//Lines have 9 numbers:
		//posx posy posz dirx diryx dirz outx outy outz
		//pos: position
		//dir: directon in which the cable is travelling
		//out: one of the normal vectors. The other is calculated using cross product.

		assert(filedata.n_cols == 9);
		int n = int(filedata.n_rows);
		//std::cout << "Found " << n << " rows" << std::endl;

		arma::Mat<fltp> R = filedata.cols(0, 2).t(); //position
		arma::Mat<fltp> D = filedata.cols(3, 5).t(); //direction
		arma::Mat<fltp> O = filedata.cols(6, 8).t(); //out

		filedata.clear();

		// normalize
		D = arma::normalise(D, 2, 0);
		O = arma::normalise(O, 2, 0);

		// transverse direction from cross product
		arma::Mat<fltp> N(3, n); //norm (3rd direction)
		N = rat::cmn::Extra::cross(D,O);
		N = arma::normalise(N, 2, 0);

		// create frame
		ShFramePr gen = Frame::create(R, D, O, N, 1);

		R.clear();
		D.clear();
		O.clear();
		N.clear();

		// transformations
		gen->apply_transformations(get_transformations());

		// create frame
		return gen;
	}

}}