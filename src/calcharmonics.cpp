// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcharmonics.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// model headers
#include "pathaxis.hh"
#include "crosspoint.hh"
#include "crosscircle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcHarmonics::CalcHarmonics(){
		// name
		set_name("harmonics"); 

		// settins
		stngs_->set_num_exp(7);
		stngs_->set_large_ilist(true);
	}

	// constructor
	CalcHarmonics::CalcHarmonics(const ShModelPr &model) : CalcHarmonics(){
		set_model(model);
	}

	// constructor
	CalcHarmonics::CalcHarmonics(
		const ShModelPr &model, const ShPathPr &input_path, 
		const fltp reference_radius, const bool compensate_curvature) : CalcHarmonics(model){
		set_input_path(input_path); 
		set_reference_radius(reference_radius);
		set_compensate_curvature(compensate_curvature);
	}
	
	// factory methods
	ShCalcHarmonicsPr CalcHarmonics::create(){
		return std::make_shared<CalcHarmonics>();
	}

	ShCalcHarmonicsPr CalcHarmonics::create(
		const ShModelPr &model, const ShPathPr &input_path, 
		const fltp reference_radius, const bool compensate_curvature){
		return std::make_shared<CalcHarmonics>(model,input_path,reference_radius,compensate_curvature);
	}

	// set the reference radius
	void CalcHarmonics::set_reference_radius(const fltp reference_radius){
		reference_radius_ = reference_radius;
	}

	// set maximum order of harmonics calculated
	void CalcHarmonics::set_num_max(const arma::uword num_max){
		num_max_ = num_max;
	}

	// set number of points in azymuthal direction
	void CalcHarmonics::set_num_theta(const arma::uword num_theta){
		num_theta_ = num_theta;
	}

	// set compensation for curvature
	void CalcHarmonics::set_compensate_curvature(const bool compensate_curvature){
		compensate_curvature_ = compensate_curvature;
	}

	// get the reference radius
	fltp CalcHarmonics::get_reference_radius() const{
		return reference_radius_;
	}

	// get number of points in azymuthal direction 
	arma::uword CalcHarmonics::get_num_theta() const{
		return num_theta_;
	}

	// get maximum order of harmonics calculated
	arma::uword CalcHarmonics::get_num_max() const{
		return num_max_;
	}

	// get compensation for curvature 
	bool CalcHarmonics::get_compensate_curvature() const{
		return compensate_curvature_;
	}

	// calculate with inductance data output
	ShHarmonicsDataPr CalcHarmonics::calculate_harmonics(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Harmonics Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%s%sSETTING UP TARGET POINTS%s\n",KBLD,KGRN,KNRM);
		
		// create frame
		const ShFramePr frame = input_path_->create_frame(MeshSettings());
		frame->combine();

		// display settings
		lg->msg(2,"%sSettings%s\n",KBLU,KNRM);
		lg->msg("radius: %s%2.2f [mm]%s\n",KYEL,1e3*reference_radius_,KNRM);
		lg->msg("number of points: %s%llu%s\n",KYEL,num_theta_,KNRM);
		lg->msg(-2,"\n");

		// display input path
		lg->msg(2,"%sInput Path%s\n",KBLU,KNRM);
		lg->msg("number of points: %s%llu%s\n",KYEL,frame->get_num_nodes(0),KNRM);
		lg->msg("total path length: %s%2.2f%s\n",KYEL,arma::accu(frame->calc_ell()),KNRM);
		lg->msg(-2);

		// done
		lg->msg(-2,"\n");

		// create target points for harmonics calculation
		ShHarmonicsDataPr hdata = HarmonicsData::create(frame,reference_radius_,compensate_curvature_,num_theta_);
		hdata->set_num_max(num_max_);
		hdata->set_time(time);
		
		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		// lg->msg(-2,"\n");

		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);

		// multipole method
		fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src, hdata, stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// setup MLFMM and calculate required fields
		mlfmm->setup(lg); 
		mlfmm->calculate(lg);

		// display content
		hdata->display(lg);

		// return the data
		return hdata;
	}

	// generalized calculation
	std::list<ShDataPr> CalcHarmonics::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_harmonics(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcHarmonics::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{
		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// create frame
		const ShFramePr base_frame = input_path_->create_frame(stngs);
		base_frame->combine();

		// create circle
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >
			(0,2*arma::Datum<fltp>::pi,num_theta_+1);
		const arma::Row<fltp> rho = arma::Row<fltp>(num_theta_+1,arma::fill::ones)*reference_radius_;
		const arma::Row<fltp> u = rho%arma::cos(theta);
		const arma::Row<fltp> v = rho%arma::sin(theta);

		// get line properties
		const arma::Mat<fltp> Rb = base_frame->get_coords(0);
		const arma::Mat<fltp> Lb = base_frame->get_direction(0);
		const arma::Mat<fltp> Nb = base_frame->get_normal(0);
		const arma::Mat<fltp> Db = base_frame->get_transverse(0);

		// number of nodes in basepath
		const arma::uword num_ell = Rb.n_cols;

		// allocate circels
		arma::field<arma::Mat<fltp> > R(num_ell);
		arma::field<arma::Mat<fltp> > L(num_ell);
		arma::field<arma::Mat<fltp> > N(num_ell);
		arma::field<arma::Mat<fltp> > D(num_ell);

		// walk over circles
		for(arma::uword i=0;i<num_ell;i++){
			// set coordinates of circle
			R(i).set_size(3,num_theta_+1);
			for(arma::uword k=0;k<3;k++)
				R(i).row(k) = Rb(k,i) + Db(k,i)*v + Nb(k,i)*u;

			// copy orientation
			L(i).set_size(3,num_theta_+1); L(i).each_col() = Lb.col(i);
			N(i).set_size(3,num_theta_+1); N(i).each_col() = Nb.col(i);
			D(i).set_size(3,num_theta_+1); D(i).each_col() = Db.col(i);
		}

		// create frame
		ShFramePr circle_frame = Frame::create(R,L,N,D);

		// allocate cross section
		ShCrossPr crss;

		// point cross section
		if(stngs.visual_radius==RAT_CONST(0.0))crss = CrossPoint::create(
			RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0));

		// circular cross section
		else crss = CrossCircle::create(stngs.visual_radius,stngs.visual_radius/2);

		// create 2d area mesh
		ShAreaPr area = crss->create_area(stngs);

		// drop nodes
		if(stngs.low_poly)circle_frame->simplify(stngs.visual_tolerance, crss->get_bounding());

		// mesh data
		ShMeshDataPr mesh_data = MeshData::create(circle_frame, area);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();

		// mesh data object
		return {mesh_data};
	}

	// is valid
	bool CalcHarmonics::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		if(!InputPath::is_valid(enable_throws))return false;
		if(num_max_<=0){if(enable_throws){rat_throw_line("maximum harmonic must be positive");} return false;}
		if(num_theta_<2*num_max_){if(enable_throws){rat_throw_line("number of points in theta must be less than 2*num_max");} return false;}
		if(reference_radius_<=0){if(enable_throws){rat_throw_line("reference radius must be larger than zero");} return false;}
		return true;
	}

	// serialization
	std::string CalcHarmonics::get_type(){
		return "rat::mdl::calcharmonics";
	}

	void CalcHarmonics::serialize(Json::Value &js, cmn::SList &list) const{
		CalcPath::serialize(js,list);
		// InputPath::serialize(js,list);
		js["type"] = get_type();
		js["visibility"] = visibility_;
		js["reference_radius"] = reference_radius_;
		js["compensate_curvature"] = compensate_curvature_;
		js["num_theta"] = (unsigned int)num_theta_;
		js["num_max"] = (unsigned int)num_max_;
	}
	
	void CalcHarmonics::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcPath::deserialize(js,list,factory_list,pth);
		// InputPath::deserialize(js,list,factory_list,pth);
		visibility_ = js["visibility"].asBool();
		reference_radius_ = js["reference_radius"].asDouble();
		compensate_curvature_= js["compensate_curvature"].asBool();
		num_theta_ = js["num_theta"].asUInt64();
		num_max_ = js["num_max"].asUInt64();
	}

}}