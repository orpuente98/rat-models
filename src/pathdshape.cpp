// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathdshape.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathDShape::PathDShape(){
		set_name("dshape");
		set_ell1(180e-3);	set_ell2(120e-3);
		set_element_size(2e-3); 
	}

	// constructor
	PathDShape::PathDShape(
		const fltp ell1, const fltp ell2, 
		const fltp element_size, const fltp offset){
		// set parameters
		set_name("dshape");
		set_ell1(ell1);	set_ell2(ell2);
		set_element_size(element_size); 
		set_offset(offset);
	}

	// factory
	ShPathDShapePr PathDShape::create(){
		return std::make_shared<PathDShape>();
	}

	// factory with dimensional input
	ShPathDShapePr PathDShape::create(
		const fltp ell1, const fltp ell2, 
		const fltp element_size, const fltp offset){
		return std::make_shared<PathDShape>(ell1,ell2,element_size,offset);
	}

	// set ell1
	void PathDShape::set_ell1(const fltp ell1){
		ell1_ = ell1;
	}

	// set ell2
	void PathDShape::set_ell2(const fltp ell2){
		ell2_ = ell2;
	}

	// set element size 
	void PathDShape::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// path offset
	void PathDShape::set_offset(const fltp offset){
		offset_ = offset;
	}

	// get ell1
	fltp PathDShape::get_ell1() const{
		return ell1_;
	}

	// get ell2
	fltp PathDShape::get_ell2() const{
		return ell2_;
	}

	// get element size 
	fltp PathDShape::get_element_size() const{
		return element_size_;
	}

	// path offset
	fltp PathDShape::get_offset() const{
		return offset_;
	}

	// get frame
	ShFramePr PathDShape::create_frame(const MeshSettings &stngs) const{
		// check input
		if(ell1_<=0)rat_throw_line("first length must be larger than zero");
		if(ell2_<=0)rat_throw_line("second length must be larger than zero");
		if(element_size_<=0)rat_throw_line("element size must be larger than zero");

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();

		// add sections
		pathgroup->add_path(PathBSpline::create(
			{0,0,0}, {0,1,0}, {1,0,0}, {-0.7*ell2_,ell1_/2,0}, {-1,0,0}, {0,1,0}, 0.6*ell1_, ell2_/1.6, element_size_, offset_));
		pathgroup->add_path(PathBSpline::create(
			{0,0,0}, {0,1,0}, {1,0,0}, {-0.3*ell1_/2,0.3*ell2_,0}, {-1,0,0}, {0,1,0}, ell2_/3, ell1_/5, element_size_, offset_));
		pathgroup->add_path(PathStraight::create(0.7*ell1_/2,element_size_));
		pathgroup->add_path(PathStraight::create(0.7*ell1_/2,element_size_));
		pathgroup->add_path(PathBSpline::create(
		 	{0,0,0}, {0,1,0}, {1,0,0}, {-0.3*ell2_,0.3*ell1_/2,0}, {-1,0,0}, {0,1,0}, ell1_/5, ell2_/3, element_size_, offset_));
		pathgroup->add_path(PathBSpline::create(
		 	{0,0,0}, {0,1,0}, {1,0,0}, {-ell1_/2,0.7*ell2_,0}, {-1,0,0}, {0,1,0}, ell2_/1.6, 0.6*ell1_, element_size_, offset_));


		// const arma::Col<fltp>::fixed<3> pstart, const arma::Col<fltp>::fixed<3> lstart, const arma::Col<fltp>::fixed<3> nstart, const arma::Col<fltp>::fixed<3> pend,  const arma::Col<fltp>::fixed<3> lend, const arma::Col<fltp>::fixed<3> nend, const fltp str1, const fltp str2, const fltp element_size, const fltp offset = 0

		// set offset
		pathgroup->add_translation(ell2_,0,0);

		// create frame
		ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations());
		
		// return frame
		return frame;
	}

	// validity check
	bool PathDShape::is_valid(const bool enable_throws) const{
		if(ell1_<=0){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(ell2_<=0){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathDShape::get_type(){
		return "rat::mdl::pathdshape";
	}

	// method for serialization into json
	void PathDShape::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["ell1"] = ell1_;
		js["ell2"] = ell2_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathDShape::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Transformations::deserialize(js,list,factory_list,pth);
		
		// properties
		set_ell1(js["ell1"].asDouble());
		set_ell2(js["ell2"].asDouble());
		set_element_size(js["element_size"].asDouble());
		set_offset(js["offset"].asDouble());
	}


}}