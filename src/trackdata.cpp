// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "trackdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TrackData::TrackData(){
		set_output_type("tracks");
	}

	// factory
	ShTrackDataPr TrackData::create(){
		return std::make_shared<TrackData>();
	}

	// calculate field
	arma::Mat<fltp> TrackData::calc_field(
		const arma::Mat<fltp> &RV,
		const fmm::ShTrackingGridPr &tracking_grid){

		// find indexes on grid
		const arma::Col<arma::uword> idx_on_grid = 
			arma::find(tracking_grid->is_inside(RV.rows(0,2)));

		// no more targets alive
		if(idx_on_grid.empty())return arma::Mat<fltp>(3,RV.n_cols,arma::fill::zeros); 

		// create targets
		const fmm::ShMgnTargetsPr tar = fmm::MgnTargets::create(RV.rows(0,2).eval().cols(idx_on_grid));
		tar->set_field_type('H',3);

		// calculate field
		tar->allocate();
		tracking_grid->calc_field(tar);

		// calculate magnetic field at positions [T]
		arma::Mat<fltp> B(3,RV.n_cols,arma::fill::zeros);
		B.cols(idx_on_grid) = tar->get_field('B');

		// std::cout<<arma::find(arma::all(B==0,0))<<std::endl;

		// return field
		return B;
	}


	// system function
	arma::Mat<fltp> TrackData::system_function(
		const fltp /*t*/, const arma::Mat<fltp> &RV, 
		const arma::Row<fltp> &q, const arma::Row<fltp> &m,
		const fmm::ShTrackingGridPr &tracking_grid){

		// calculate magnetic field at positions [T]
		const arma::Mat<fltp> Bp = calc_field(RV,tracking_grid);

		// calculate force 
		const arma::Mat<fltp> F = cmn::Extra::cross(RV.rows(3,5), Bp).each_row()%q;

		// calculate change
		arma::Mat<fltp> dXp(RV);
		dXp.rows(0,2) = RV.rows(3,5); // change of position is just velocity
		dXp.rows(3,5) = F.each_row()/m; // acceleration: newton second law

		// check for out of scope
		dXp.cols(arma::find(arma::all(Bp==0,0))).fill(0);

		// return change
		return dXp;
	}

	// Runge Kutta algorithm fourth order 
	arma::Mat<fltp> TrackData::runge_kutta(
		const fltp t, const fltp dt, 
		const arma::Mat<fltp> &RV, 
		const arma::Row<fltp> &q, 
		const arma::Row<fltp> &m,
		const fmm::ShTrackingGridPr &tracking_grid){

		// calc coefficients
		const arma::Mat<fltp> k1 = system_function(t, RV, q, m, tracking_grid);
		const arma::Mat<fltp> k2 = system_function(t + dt/2, RV + (dt/2)*k1, q, m, tracking_grid);
		const arma::Mat<fltp> k3 = system_function(t + dt/2, RV + (dt/2)*k2, q, m, tracking_grid);
		const arma::Mat<fltp> k4 = system_function(t + dt, RV + dt*k3, q, m, tracking_grid);

		// change
		arma::Mat<fltp> dRV = dt*(k1 + 2*k2 + 2*k3 + k4)/6.0;

		// particle fallen off grid
		dRV.cols(arma::find(arma::all(k1==0,0) || arma::all(k2==0,0) || 
			arma::all(k3==0,0) || arma::all(k4==0,0))).fill(0);

		// calculate change of x
		return dRV;
	}

	// run tracking code
	void TrackData::create_tracks(
		const fmm::ShTrackingGridPr &tracking_grid,
		const std::list<ShEmitterPr> &emitters,
		const fltp step_size,
		const rat::cmn::ShLogPr &lg){

		// check settings
		if(step_size==0)rat_throw_line("stepsize can not be zero");

		// check if tracking mesh is setup
		if(tracking_grid==NULL)rat_throw_line("tracking grid not set");

		// header
		lg->msg(2,"%s%sPARTICLE TRACING%s\n",KBLD,KGRN,KNRM);

		// get particles from emitter
		lg->msg(2,"%sspawn particles%s\n",KBLU,KNRM);
		arma::field<arma::field<Particle> > particles(emitters.size()); 
		arma::uword cnt = 0; arma::uword num_particles = 0;
		for(auto it=emitters.begin();it!=emitters.end();it++,cnt++){
			if((*it)==NULL)rat_throw_line("emitter is NULL");
			particles(cnt) = (*it)->spawn_particles();
			num_particles += particles(cnt).n_elem;
		}
		particles_.set_size(num_particles); cnt=0;
		for(arma::uword i=0;i<particles.n_elem;i++)
			for(arma::uword j=0;j<particles(i).n_elem;j++)
				particles_(cnt++) = particles(i)(j);

		lg->msg("number of particles: %05llu\n",num_particles);
		lg->msg(-2,"\n");
		
		// tracking direction
		for(arma::sword dir = -1;dir<=1;dir+=2){
			// time and initial time step
			fltp dt = dir*step_size/arma::Datum<fltp>::c_0;
			
			// set time and iteration to zero
			fltp t = 0; arma::uword iter = 0;

			// start time stepping
			for(;;){
				// walk over particles and find alive
				arma::Row<arma::uword> is_alive(num_particles,arma::fill::zeros);
				for(arma::uword i=0;i<num_particles;i++){
					if(dir==1){
						is_alive(i) = particles_(i).is_alive_end();
					}else{
						is_alive(i) = particles_(i).is_alive_start();
					}
				}
				
				// find alive indexes
				const arma::Row<arma::uword> idx_alive = arma::find(is_alive).t();
				const arma::uword num_alive = idx_alive.n_elem;

				// is there any track alive
				if(num_alive==0)break;

				// print header on first iteration 
				// after it is confirmed that there are alive
				// particles
				if(iter==0){
					// header
					if(dir==1){
						lg->msg(2,"%strace particles forward%s\n",KBLU,KNRM);
					}else{
						lg->msg(2,"%strace particles backward%s\n",KBLU,KNRM);
					}
					lg->msg("%s%4s %9s %5s%s\n",KBLD,"iter","time","alive",KNRM);			
				}

				// display time
				if(iter%5==0)lg->msg("%04llu %+9.2e %05llu\n",iter,t,num_alive);

				// allocate coordinate, velocity matrix
				arma::Mat<fltp> RV(6,num_alive);
				arma::Row<fltp> m(num_alive), q(num_alive);
				for(arma::uword i=0;i<num_alive;i++){
					// get particle index from alive list
					const arma::uword idx = idx_alive(i);

					// get particle position and velocity
					if(dir==1){
						RV.submat(0,i,2,i) = particles_(idx).get_last_coord(); // [m]
						RV.submat(3,i,5,i) = particles_(idx).get_last_velocity()*arma::Datum<fltp>::c_0; // [m/s]
					}else{
						RV.submat(0,i,2,i) = particles_(idx).get_first_coord(); // [m]
						RV.submat(3,i,5,i) = particles_(idx).get_first_velocity()*arma::Datum<fltp>::c_0; // [m/s]
					}

					// calculate Lorentz factor
					const fltp vmag = arma::as_scalar(cmn::Extra::vec_norm(RV.submat(3,i,5,i)));
					const fltp gamma = 1.0/std::sqrt(1.0 - (vmag*vmag)/(arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0));
					
					// calculate rest mass [kg]
					const fltp rest_mass = particles_(idx).get_rest_mass()*
						1e9*arma::Datum<fltp>::eV/(arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0);
					
					// calculate relativistic mass [kg]
					m(i) = gamma*rest_mass;

					// calculate particle charge in [C]
					q(i) = particles_(idx).get_charge()*arma::Datum<fltp>::ec;
				}
				
				// calculate next position and time using fourth order runge kutta
				const arma::Mat<fltp> dRV = runge_kutta(t, dt, RV, q, m, tracking_grid);
				RV += dRV; t += dt;

				// adapt stepsize
				dt *= step_size/arma::max(cmn::Extra::vec_norm(dRV.rows(0,2)));

				// store to particles
				for(arma::uword i=0;i<num_alive;i++){
					// get particle index
					const arma::uword idx = idx_alive(i);

					// particle is alive insert next position and velocity
					if(arma::any(dRV.col(i)!=0)){
						const arma::Col<fltp>::fixed<3> Rn = RV.submat(0,i,2,i);
						const arma::Col<fltp>::fixed<3> Vn = RV.submat(3,i,5,i)/arma::Datum<fltp>::c_0;
						if(dir==1){
							particles_(idx).set_next_coord(t,Rn,Vn);
						}else{
							particles_(idx).set_prev_coord(t,Rn,Vn);
						}
					}

					// the particle went off the grid
					else{
						if(dir==1){
							particles_(idx).terminate_end();
						}else{
							particles_(idx).terminate_start();
						}
					}
				}

				// increment iteration
				iter++;
			}

			// done
			if(iter>0){
				lg->msg("%s= tracing complete%s\n",KCYN,KNRM);
				lg->msg(-2,"\n");
			}
		}

		// get track coordinates
		arma::field<arma::Mat<fltp> > Rfld(1,num_particles);
		for(arma::uword i=0;i<particles_.n_elem;i++)
			Rfld(i) = particles_(i).get_track_coords();
		
		// calculate field on tracks
		const fmm::ShMgnTargetsPr tar = fmm::MgnTargets::create(cmn::Extra::field2mat(Rfld));
		tar->set_field_type('H',3); tar->allocate();
		tracking_grid->calc_field(tar);

		// calculate field at targets
		const arma::Mat<fltp> Bt = tar->get_field('B');
		arma::uword cnt2 = 0;
		for(arma::uword i=0;i<particles_.n_elem;i++){
			const arma::uword num_particles = Rfld(i).n_cols;
			particles_(i).set_magnetic_field(Bt.cols(cnt2, cnt2 + num_particles-1));
			cnt2 += num_particles;
		}

		// end
		lg->msg(-2);
	}

	// get a particle from the list
	Particle TrackData::get_particle(const arma::uword idx) const{
		return particles_(idx);
	}

	// write surface to VTK file
	ShVTKObjPr TrackData::export_vtk() const{
		// create VTK data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// get number of particles
		const arma::uword num_particles = particles_.n_elem;

		// allocate field arrays
		arma::field<arma::Mat<fltp> > Rfld(1,num_particles);
		arma::field<arma::Mat<fltp> > Bfld(1,num_particles);
		arma::field<arma::Mat<fltp> > Vfld(1,num_particles);
		arma::field<arma::Mat<arma::uword> > nfld(1,num_particles);
		arma::uword idx_offset = 0;
		for(arma::uword i=0;i<num_particles;i++){
			Rfld(i) = particles_(i).get_track_coords();
			Bfld(i) = particles_(i).get_magnetic_field();
			Vfld(i) = particles_(i).get_track_velocities();

			//std::cout<<Rfld(i).t()<<std::endl;
			if(Rfld(i).n_cols>1){
				nfld(i) = arma::join_vert(
					arma::regspace<arma::Row<arma::uword> >(0,Rfld(i).n_cols-2),
					arma::regspace<arma::Row<arma::uword> >(1,Rfld(i).n_cols-1));
			}else{
				nfld(i) = arma::Mat<arma::uword>(2,0);
			}
			nfld(i) += idx_offset; idx_offset += Rfld(i).n_cols;
		}

		// combine
		const arma::Mat<fltp> R = cmn::Extra::field2mat(Rfld);
		const arma::Mat<fltp> B = cmn::Extra::field2mat(Bfld);
		const arma::Mat<fltp> V = cmn::Extra::field2mat(Vfld);
		const arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);

		// Create a unstructured grid object
		// vtkSmartPointer<vtkUnstructuredGrid> ugrid =	
		// 	vtkSmartPointer<vtkUnstructuredGrid>::New();

		// export points as well
		vtk_data->set_nodes(R);

		// set data
		vtk_data->set_nodedata(B, "Mgn. Flux Density [T]");
		vtk_data->set_nodedata(V, "Velocity [m/s]");

		// create mesh
		const arma::uword line_type = 3;
		vtk_data->set_elements(n,line_type);
		
		// return data object
		return vtk_data;
	}



	// // write output file
	// void TrackData::write(cmn::ShLogPr lg){
	// 	// check if data directory set
	// 	if(output_dir_.empty())return;

	// 	// header
	// 	lg->msg(2,"%s%sWRITING OUTPUT FILES:%s\n",KBLD,KGRN,KNRM);
		
	// 	// check output directory
	// 	if(output_times_.is_empty())rat_throw_line("output times are not set");

	// 	// create output directory
	// 	boost::filesystem::create_directory(output_dir_);

	// 	// report
	// 	lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

	// 	// settings report
	// 	display_settings(lg);

	// 	// report
	// 	lg->msg(2, "%swriting meshdata%s\n",KBLU,KNRM);
	// 	lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

	// 	// output filename
	// 	std::string fname  = output_fname_;

	// 	// walk over timesteps
	// 	for(arma::uword i=0;i<output_times_.n_elem;i++){
	// 		// set time
	// 		set_time(output_times_(i));

	// 		// get data at this time
	// 		ShVTKUnstrPr vtk_mesh = export_vtk();

	// 		// extend filename with index
	// 		if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

	// 		// show in log
	// 		lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

	// 		// write data to file
	// 		vtk_mesh->write(output_dir_/(fname + "_tmsh"));
	// 	}

	// 	// return
	// 	lg->msg(-2,"\n");

	// 	// header
	// 	lg->msg(2, "%swriting trackdata%s\n",KBLU,KNRM);
	// 	lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

	// 	// update name
	// 	fname  = output_fname_;

	// 	// walk over timesteps
	// 	for(arma::uword i=0;i<output_times_.n_elem;i++){
	// 		// set time
	// 		set_time(output_times_(i));

	// 		// get data at this time
	// 		ShVTKUnstrPr vtk_tracks = export_vtk_tracks();

	// 		// extend filename with index
	// 		if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

	// 		// show in log
	// 		lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

	// 		// write data to file
	// 		vtk_tracks->write(output_dir_/(fname + "_trck"));
	// 	}

	// 	// return
	// 	lg->msg(-6,"\n"); 
	// }

	// // get type
	// std::string TrackData::get_type(){
	// 	return "rat::mdl::calctracks";
	// }

	// // method for serialization into json
	// void TrackData::serialize(Json::Value &js, cmn::SList &list) const{
	// 	// parent
	// 	MeshData::serialize(js,list);

	// 	// properties
	// 	js["type"] = get_type();
	// 	js["num_particles"] = (unsigned int)num_particles_;
	// 	js["step_size"] = step_size_;

	// 	// subnodes
	// 	js["emitter"] = cmn::Node::serialize_node(emitter_, list);
	// }

	// // method for deserialisation from json
	// void TrackData::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
	// 	// parent
	// 	MeshData::deserialize(js,list,factory_list,pth);

	// 	// properties
	// 	num_particles_ = js["num_particles"].asUInt64();
	// 	step_size_ = js["step_size"].asDouble();

	// 	// subnodes
	// 	emitter_ = cmn::Node::deserialize_node<Emitter>(js["emitter"], list, factory_list, pth);
	// }


}}
