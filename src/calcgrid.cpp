// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcgrid.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// model headers
#include "crosspoint.hh"
#include "crosscircle.hh"
#include "meshdata.hh"
#include "pathaxis.hh"
#include "pathrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcGrid::CalcGrid(){
		set_name("grid");

		// settinsg
		stngs_->set_num_exp(5);
		stngs_->set_large_ilist(true);
	}

	CalcGrid::CalcGrid(const ShModelPr &model) : CalcGrid(){
		set_model(model);
	}

	CalcGrid::CalcGrid(
		const ShModelPr &model, 
		const fltp size_x, 
		const fltp size_y, 
		const fltp size_z, 
		const arma::uword num_x, 
		const arma::uword num_y, 
		const arma::uword num_z) : CalcGrid(){
		set_model(model);
		set_size_x(size_x); set_size_y(size_y); set_size_z(size_z);
		set_num_x(num_x); set_num_y(num_y); set_num_z(num_z);
	}
	
	// factory methods
	ShCalcGridPr CalcGrid::create(){
		return std::make_shared<CalcGrid>();
	}

	// create and set model
	ShCalcGridPr CalcGrid::create(const ShModelPr &model){
		return std::make_shared<CalcGrid>(model);
	}

	// create and set model and grid dimensions
	ShCalcGridPr CalcGrid::create(
		const ShModelPr &model, 
		const fltp size_x, 
		const fltp size_y, 
		const fltp size_z, 
		const arma::uword num_x, 
		const arma::uword num_y, 
		const arma::uword num_z){
		return std::make_shared<CalcGrid>(model,size_x,size_y,size_z,num_x,num_y,num_z);
	}

	// set position 
	void CalcGrid::set_offset(const arma::Col<fltp>::fixed<3> &offset){
		offset_ = offset;
	}

	// set first side length
	void CalcGrid::set_size_x(const fltp size_x){
		size_x_ = size_x;
	}

	// set first side length
	void CalcGrid::set_size_y(const fltp size_y){
		size_y_ = size_y;
	}

	// set first side length
	void CalcGrid::set_size_z(const fltp size_z){
		size_z_ = size_z;
	}

	// set first side number of steps
	void CalcGrid::set_num_x(const arma::uword num_x){
		num_x_ = num_x;
	}

	// set first side number of steps
	void CalcGrid::set_num_y(const arma::uword num_y){
		num_y_ = num_y;
	}

	// set first side number of steps
	void CalcGrid::set_num_z(const arma::uword num_z){
		num_z_ = num_z;
	}

	// set mesh enabled
	void CalcGrid::set_visibility(const bool visibility){
		visibility_ = visibility;
	}


	// get position 
	arma::Col<fltp>::fixed<3> CalcGrid::get_offset() const{
		return offset_;
	}

	// get first side length
	fltp CalcGrid::get_size_x() const{
		return size_x_;
	}

	// get first side length
	fltp CalcGrid::get_size_y() const{
		return size_y_;
	}
	
	// get first side length
	fltp CalcGrid::get_size_z() const{
		return size_z_;
	}

	// get first side number of steps
	arma::uword CalcGrid::get_num_x() const{
		return num_x_;
	}

	// get first side number of steps
	arma::uword CalcGrid::get_num_y() const{
		return num_y_;
	}
	
	// get first side number of steps
	arma::uword CalcGrid::get_num_z() const{
		return num_z_;
	}

	// set mesh enabled
	bool CalcGrid::get_visibility()const{
		return visibility_;
	}


	// calculate with inductance data output
	ShGridDataPr CalcGrid::calculate_grid(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();
		
		// general info
		lg->msg("%s=== Starting Grid Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// by default we set the third dimension
		const fltp x1 = offset_(0) - size_x_/2, y1 = offset_(1) - size_y_/2, z1 = offset_(2) - size_z_/2; 
		const fltp x2 = offset_(0) + size_x_/2, y2 = offset_(1) + size_y_/2, z2 = offset_(2) + size_z_/2; 

		// create plane
		ShGridDataPr grd = GridData::create(x1,x2,num_x_,y1,y2,num_y_,z1,z2,num_z_);
		grd->set_field_type("AHM",{3,3,3});

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 

		// create multilevel fast multipole method object
		fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));

		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);

		// add sources and targets to the calculation
		mlfmm->set_sources(src);
		mlfmm->set_targets(grd);

		// setup multipole method
		mlfmm->setup(lg);

		// run multipole method
		mlfmm->calculate(lg);

		// return the line data
		return grd;
	}

	// generalized calculation
	std::list<ShDataPr> CalcGrid::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_grid(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcGrid::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// by default we set the third dimension
		const fltp x1 = offset_(0) - size_x_/2, y1 = offset_(1) - size_y_/2, z1 = offset_(2) - size_z_/2; 
		const fltp x2 = offset_(0) + size_x_/2, y2 = offset_(1) + size_y_/2, z2 = offset_(2) + size_z_/2; 

		// allocate coords
		arma::field<arma::Mat<fltp> > R(6);

		// face1
		{
			const arma::Row<fltp> x{x1,x2,x2,x1,x1};
			const arma::Row<fltp> y{y1,y1,y2,y2,y1};
			const arma::Row<fltp> z{z1,z1,z1,z1,z1};
			R(0) = arma::join_vert(x,y,z);
		}

		// face2
		{
			const arma::Row<fltp> x{x1,x2,x2,x1,x1};
			const arma::Row<fltp> y{y1,y1,y2,y2,y1};
			const arma::Row<fltp> z{z2,z2,z2,z2,z2};
			R(1) = arma::join_vert(x,y,z);
		}

		// edges
		{
			const arma::Row<fltp> x{x1,x1};
			const arma::Row<fltp> y{y1,y1};
			const arma::Row<fltp> z{z1,z2};
			R(2) = arma::join_vert(x,y,z);
		}

		{
			const arma::Row<fltp> x{x2,x2};
			const arma::Row<fltp> y{y1,y1};
			const arma::Row<fltp> z{z1,z2};
			R(3) = arma::join_vert(x,y,z);
		}

		{
			const arma::Row<fltp> x{x1,x1};
			const arma::Row<fltp> y{y2,y2};
			const arma::Row<fltp> z{z1,z2};
			R(4) = arma::join_vert(x,y,z);
		}

		{
			const arma::Row<fltp> x{x2,x2};
			const arma::Row<fltp> y{y2,y2};
			const arma::Row<fltp> z{z1,z2};
			R(5) = arma::join_vert(x,y,z);
		}

		// factory with single section input
		ShFramePr frame = Frame::create(R,R,R,R);

		// create point cross section
		ShCrossPr crss = CrossPoint::create(RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1e-6),RAT_CONST(1e-6));

		// create 2d area mesh
		ShAreaPr area = crss->create_area(stngs);

		// mesh data
		ShMeshDataPr mesh_data = MeshData::create(frame, area);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();

		// return mesh
		return {mesh_data};
	}

	// validity check
	bool CalcGrid::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		if(size_x_<=0){if(enable_throws){rat_throw_line("x-dimension must be larger than zero");} return false;}
		if(size_y_<=0){if(enable_throws){rat_throw_line("y-dimension must be larger than zero");} return false;}
		if(size_z_<=0){if(enable_throws){rat_throw_line("z-dimension must be larger than zero");} return false;}
		if(num_x_<=0){if(enable_throws){rat_throw_line("number of nodes in x must be larger than zero");} return false;}
		if(num_y_<=0){if(enable_throws){rat_throw_line("number of nodes in y must be larger than zero");} return false;}
		if(num_z_<=0){if(enable_throws){rat_throw_line("number of nodes in z must be larger than zero");} return false;}
		return true;
	}	

	// serialization
	std::string CalcGrid::get_type(){
		return "rat::mdl::calcgrid";
	}

	void CalcGrid::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		
		js["type"] = get_type();

		// position 
		js["offset_x"] = offset_(0);
		js["offset_y"] = offset_(1);
		js["offset_z"] = offset_(2);

		// dimensions
		js["size_x"] = size_x_;
		js["size_y"] = size_y_; 
		js["size_z"] = size_z_; 
		
		// discretization
		js["num_x"] = (int)num_x_;
		js["num_y"] = (int)num_y_;
		js["num_z"] = (int)num_z_;

		// enable visibility
		js["visibility"] = visibility_;
	}
	
	void CalcGrid::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);

		// position 
		offset_(0) = js["offset_x"].asDouble();
		offset_(1) = js["offset_y"].asDouble();
		offset_(2) = js["offset_z"].asDouble();

		// dimensions
		size_x_ = js["size_x"].asDouble();
		size_y_ = js["size_y"].asDouble(); 
		size_z_ = js["size_z"].asDouble(); 
		
		// discretization
		num_x_ = js["num_x"].asUInt64();
		num_y_ = js["num_y"].asUInt64();
		num_z_ = js["num_z"].asUInt64();

		// enable visibility
		visibility_ = js["visibility"].asBool();
	}

}}