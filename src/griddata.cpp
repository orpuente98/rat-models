// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "griddata.hh"
#include "marchingcubes.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	GridData::GridData(){
		// set for magnetic field calculation
		set_field_type('H',3); set_output_type("grid");
	}

	// constructor
	GridData::GridData(
		const fltp x1, const fltp x2, const arma::uword num_x, 
		const fltp y1, const fltp y2, const arma::uword num_y, 
		const fltp z1, const fltp z2, const arma::uword num_z){
		// set dimensions
		set_dim_x(x1,x2,num_x);	set_dim_y(y1,y2,num_y);	set_dim_z(z1,z2,num_z);
		
		// set for magnetic field calculation
		set_field_type('H',3); set_output_type("grid");
	}

	// constructor automatically enclosing meshes
	GridData::GridData(
		const std::list<ShMeshDataPr> &meshes, 
		const fltp scale_factor, 
		const fltp delem, 
		const arma::uword lim_num_targets){
		
		// output type
		set_output_type("grid");

		// set for magnetic field calculation
		set_field_type('H',3);
		
		// get bounds of meshes
		arma::Mat<fltp> Rl(3,meshes.size());
		arma::Mat<fltp> Ru(3,meshes.size());
		arma::uword idx =0;
		for(auto it=meshes.begin();it!=meshes.end();it++,idx++){
			Rl.col(idx) = (*it)->get_lower_bound();
			Ru.col(idx) = (*it)->get_upper_bound();
		}

		// get lowest lower bound and highest higher upper bound
		const arma::Col<fltp>::fixed<3> Rll = arma::min(Rl,1); 
		const arma::Col<fltp>::fixed<3> Ruu = arma::max(Ru,1);

		// find midpoint
		const arma::Col<fltp>::fixed<3> Rmid = (Rll + Ruu)/2;

		// scale from midpoint
		const arma::Col<fltp>::fixed<3> Rsll = Rmid + scale_factor*(Rll-Rmid);
		const arma::Col<fltp>::fixed<3> Rsuu = Rmid + scale_factor*(Ruu-Rmid);
		
		// calculate the number of nodes in each direction
		arma::Col<fltp> num_nodes = (Rsuu-Rsll)/delem + 1;

		// if the number of target nodes exceeds the limit
		// then adjust the grid
		fltp num_target_points = arma::prod(num_nodes);
		if(num_target_points>lim_num_targets)
 			num_nodes *= std::pow(lim_num_targets/num_target_points, 1.0/3);

		// take the ceil
		num_nodes = arma::ceil(num_nodes);

		// set dimensions
		set_dim_x(Rsll(0), Rsuu(0), num_nodes(0));
		set_dim_y(Rsll(1), Rsuu(1), num_nodes(1));
		set_dim_z(Rsll(2), Rsuu(2), num_nodes(2));
	}

	// factory
	ShGridDataPr GridData::create(){
		return std::make_shared<GridData>();
	}

	// factory
	ShGridDataPr GridData::create(
		const fltp x1, const fltp x2, const arma::uword num_x, 
		const fltp y1, const fltp y2, const arma::uword num_y, 
		const fltp z1, const fltp z2, const arma::uword num_z){
		return std::make_shared<GridData>(x1,x2,num_x,y1,y2,num_y,z1,z2,num_z);
	}

	// factory
	ShGridDataPr GridData::create(
		const std::list<ShMeshDataPr> &meshes, const fltp scale_factor, 
		const fltp delem, const arma::uword lim_num_elements){
		return std::make_shared<GridData>(meshes, scale_factor, delem, lim_num_elements);
	}

	// set dimensions for x
	void GridData::set_dim_x(const fltp x1, const fltp x2, const arma::uword num_x){
		if(x1>=x2 && num_x!=1)rat_throw_line("x1 must be smaller than x2");
		if(num_x<=0)rat_throw_line("number of elements in x must be larger than zero");
		x1_ = x1; x2_ = x2; num_x_ = num_x;
	}

	// set dimensions for y
	void GridData::set_dim_y(const fltp y1, const fltp y2, const arma::uword num_y){
		if(y1>=y2 && num_y!=1)rat_throw_line("y1 must be smaller than y2");
		if(num_y<=0)rat_throw_line("number of elements in y must be larger than zero");
		y1_ = y1; y2_ = y2; num_y_ = num_y;
	}

	// set dimensions for z
	void GridData::set_dim_z(const fltp z1, const fltp z2, const arma::uword num_z){
		if(z1>=z2 && num_z!=1)rat_throw_line("z1 must be smaller than z2");
		if(num_z<=0)rat_throw_line("number of elements in z must be larger than zero");
		z1_ = z1; z2_ = z2; num_z_ = num_z;
	}

	// setup function
	void GridData::setup_targets(){
		// check input
		if(num_x_==0)rat_throw_line("number of elements in x is not set");
		if(num_y_==0)rat_throw_line("number of elements in y is not set");
		if(num_z_==0)rat_throw_line("number of elements in z is not set");
		if(x1_>=x2_ && num_x_!=1)rat_throw_line("dimensions in x not set");
		if(y1_>=y2_ && num_y_!=1)rat_throw_line("dimensions in y not set");
		if(z1_>=z2_ && num_z_!=1)rat_throw_line("dimensions in z not set");

		// arrays defining coordinates
		const arma::Row<fltp> xa = arma::linspace<arma::Row<fltp> >(x1_,x2_,num_x_);
		const arma::Row<fltp> ya = arma::linspace<arma::Row<fltp> >(y1_,y2_,num_y_);
		const arma::Row<fltp> za = arma::linspace<arma::Row<fltp> >(z1_,z2_,num_z_);
		
		// allocate coordinates
		Rt_.set_size(3,num_x_*num_y_*num_z_);

		// walk over x
		for(arma::uword i=0;i<num_z_;i++){
			for(arma::uword j=0;j<num_y_;j++){
				// calculate indexes
				const arma::uword idx1 = i*num_x_*num_y_ + j*num_x_;
				const arma::uword idx2 = i*num_x_*num_y_ + (j+1)*num_x_ - 1;

				// set coordinates
				Rt_.submat(0,idx1,0,idx2) = xa;
				Rt_.submat(1,idx1,1,idx2).fill(ya(j));
				Rt_.submat(2,idx1,2,idx2).fill(za(i));
			}
		}

		// set number of targets
		num_targets_ = Rt_.n_cols;
	}

	// get grid data
	arma::uword GridData::get_num_x() const{
		return num_x_;
	}

	// get grid data
	arma::uword GridData::get_num_y() const{
		return num_y_;
	}
	
	// get grid data
	arma::uword GridData::get_num_z() const{
		return num_z_;
	}

	// get grid data
	fltp GridData::get_x1() const{
		return x1_;
	}

	// get grid data
	fltp GridData::get_x2() const{
		return x2_;
	}

	// get grid data
	fltp GridData::get_y1() const{
		return y1_;
	}
	
	// get grid data
	fltp GridData::get_y2() const{
		return y2_;
	}

	// get grid data
	fltp GridData::get_z1() const{
		return z1_;
	}
	
	// get grid data
	fltp GridData::get_z2() const{
		return z2_;
	}

	// interpolate
	arma::Mat<fltp> GridData::interpolate(
		const char field_type, const arma::Mat<fltp> &R, 
		const bool use_parallel) const{
		// get field
		const arma::Mat<fltp> values = get_field(field_type);

		// check field
		assert(!values.has_nan());
		assert(values.is_finite());

		// get grid spacing
		const fltp dx = (x2_ - x1_)/(num_x_-1);
		const fltp dy = (y2_ - y1_)/(num_y_-1);
		const fltp dz = (z2_ - z1_)/(num_z_-1);

		// interpolated values
		arma::Mat<fltp> interpolated_values(values.n_rows, R.n_cols);

		// walk over coordinates
		// walk over meshes
		cmn::parfor(0,R.n_cols,use_parallel,[&](int i, int /*cpu*/){
			// get coordinate and index of lower corner
			const fltp rx = R(0,i) - x1_, ry = R(1,i) - y1_, rz = R(2,i) - z1_;
			const arma::uword xi = std::min(num_x_-2,arma::uword(rx/dx));
			const arma::uword yi = std::min(num_y_-2,arma::uword(ry/dy)); 
			const arma::uword zi = std::min(num_z_-2,arma::uword(rz/dz));
			
			// calculate relative position in box
			arma::Col<fltp>::fixed<3> Rq{
			 	2*(rx - xi*dx)/dx - RAT_CONST(1.0),
				2*(ry - yi*dy)/dy - RAT_CONST(1.0), 
				2*(rz - zi*dz)/dz - RAT_CONST(1.0)};

			// check indexes
			assert(xi<num_x_-1); assert(yi<num_y_-1); assert(zi<num_z_-1);

			// values at corners
			arma::Mat<fltp>::fixed<3,8> V;
			V.col(0) = values.col(zi*num_x_*num_y_ + yi*num_x_ + xi);
			V.col(1) = values.col(zi*num_x_*num_y_ + yi*num_x_ + xi+1);
			V.col(2) = values.col(zi*num_x_*num_y_ + (yi+1)*num_x_ + xi+1);
			V.col(3) = values.col(zi*num_x_*num_y_ + (yi+1)*num_x_ + xi);
			V.col(4) = values.col((zi+1)*num_x_*num_y_ + yi*num_x_ + xi);
			V.col(5) = values.col((zi+1)*num_x_*num_y_ + yi*num_x_ + xi+1);
			V.col(6) = values.col((zi+1)*num_x_*num_y_ + (yi+1)*num_x_ + xi+1);
			V.col(7) = values.col((zi+1)*num_x_*num_y_ + (yi+1)*num_x_ + xi);

			// interpolate
			interpolated_values.col(i) = cmn::Hexahedron::quad2cart(V,Rq);
		});

		// check field
		assert(!interpolated_values.has_nan());
		assert(interpolated_values.is_finite());
		assert(interpolated_values.n_cols==R.n_cols);
		assert(interpolated_values.n_rows==values.n_rows);

		// return interpolated values
		return interpolated_values;
	}

	// extract iso-surfaces
	ShMeshDataPr GridData::extract_isosurface(
		const char field_type, const char component, 
		const fltp iso_value, const bool use_parallel) const{

		// get values
		arma::Row<fltp> values;
		switch(component){
			case 'x': values = get_field(field_type).row(0); break;
			case 'y': values = get_field(field_type).row(1); break;
			case 'z': values = get_field(field_type).row(2); break;
			case 'm': values = cmn::Extra::vec_norm(get_field(field_type)); break;
			default: rat_throw_line("component not recognized");
		}

		// run marching cubes algorithm
		const ShMarchingCubesPr marching = rat::mdl::MarchingCubes::create();
		marching->setup_hex2isotri();
		arma::Mat<rat::fltp> nodes = marching->polygonise(
			x1_,x2_,num_x_,y1_,y2_,num_y_,z1_,z2_,num_z_,values,iso_value);

		// create elements
		arma::Mat<arma::uword> elements = arma::reshape(
			arma::regspace<arma::Col<arma::uword> >(0,nodes.n_cols-1),3,nodes.n_cols/3);

		// combine nodes 
		const arma::Row<arma::uword> idx = cmn::Extra::combine_nodes(nodes);

		// re-index elements
		elements = arma::reshape(idx(arma::vectorise(elements)),elements.n_rows,elements.n_cols);
		
		// export to VTK
		const ShMeshDataPr md = MeshData::create(nodes, elements, elements);

		// clear the field type list of the surface
		md->clear_field_type();

		// transfer field
		if(has_field()){
			// transfer list of field types
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				md->add_field_type((*it).first, (*it).second);
		}

		// allocate surface to take the data
		md->allocate(); md->setup_targets();

		// transfer the data
		for(auto it = field_type_.begin();it!=field_type_.end();it++){
			arma::Mat<fltp> fld = interpolate((*it).first, md->get_target_coords(), use_parallel);
			md->add_field((*it).first, fld);
		}

		// output meshdata
		return md;
	}



	// display function
	void GridData::display(const cmn::ShLogPr &lg){
	// header
		lg->msg(2,"%s%sVOLUME GRID%s\n",KBLD,KGRN,KNRM);
		
		// display settings
		lg->msg(2,"%sSettings%s\n",KBLU,KNRM);
		lg->msg("grid size in x: %s%llu%s\n",KYEL,num_x_,KNRM);
		lg->msg("grid size in y: %s%llu%s\n",KYEL,num_y_,KNRM);
		lg->msg("grid size in z: %s%llu%s\n",KYEL,num_z_,KNRM);
		lg->msg("range in x: %s%2.2f - %2.2f%s\n",KYEL,x1_,x2_,KNRM);
		lg->msg("range in y: %s%2.2f - %2.2f%s\n",KYEL,y1_,y2_,KNRM);
		lg->msg("range in z: %s%2.2f - %2.2f%s\n",KYEL,z1_,z2_,KNRM);
		lg->msg("number of target coordinates: %s%llu%s\n",KYEL,num_x_*num_y_*num_z_,KNRM);
		lg->msg(-2,"\n");

		// done
		lg->msg(-2);
	}


	// write surface to VTK file
	ShVTKObjPr GridData::export_vtk() const{
		// create VTK file
		ShVTKImgPr vtk_data = VTKImg::create();

		// calculate spacing
		vtk_data->set_dims(x1_,x2_,num_x_, y1_,y2_,num_y_, z1_,z2_,num_z_);

		// set name
		vtk_data->set_name(get_name());

		// magnetic vector potential
		if(has('A') && has_field())vtk_data->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");

		// magnetic flux density
		if(has('H') && has_field())vtk_data->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");

		// magnetic field
		if(has('H') && has_field())vtk_data->set_nodedata(get_field('H'),"Magnetic Field [A/m]");

		// magnetisation
		if(has('M') && has_field())vtk_data->set_nodedata(get_field('M'),"Magnetisation [A/m]");

		// return data object
		return vtk_data;
	}

	// // write surface to VTK file
	// ShVTKObjPr GridData::export_flux_surface_vtk() const{
	// 	// create VTK file
	// 	ShVTKImgPr vtk_data = VTKImg::create();

	// 	// calculate spacing
	// 	vtk_data->set_dims(x1_,x2_,num_x_, y1_,y2_,num_y_, z1_,z2_,num_z_);

	// 	// set name
	// 	vtk_data->set_name(get_name());

	// 	// add data
	// 	vtk_data->set_nodedata(calc_flux_surfaces(),"Flux Surfaces");

	// 	// return data
	// 	return vtk_data;
	// }


	// // calculate flux surfaces
	// arma::Row<fltp> GridData::calc_flux_surfaces() const{
	// 	// create system of equations
	// 	const arma::uword num_eq = num_x_*num_y_*num_z_;

	// 	// spacing
	// 	const fltp dx = (x2_ - x1_)/(num_x_-1);
	// 	const fltp dy = (y2_ - y1_)/(num_y_-1);
	// 	const fltp dz = (z2_ - z1_)/(num_z_-1);

	// 	// allocate sparse matrix entries
	// 	arma::Row<arma::uword> idx1(6*num_eq);
	// 	arma::Row<arma::uword> idx2(6*num_eq);
	// 	arma::Row<fltp> val(6*num_eq);

	// 	// get field
	// 	const arma::Mat<fltp> B = get_field('B');

	// 	// last entry
	// 	const fltp F000 = RAT_CONST(2.0);

	// 	// each gradient
	// 	arma::uword cnt = 0; arma::uword eq = 0;
	// 	for(arma::uword k=0;k<num_z_;k++){
	// 		const arma::uword kp = k==num_z_-1 ? k : k+1;
	// 		const arma::uword km = k==0 ? k : k-1;
	// 		const fltp deltaz = (kp-km)*dz;
	// 		for(arma::uword j=0;j<num_y_;j++){
	// 			const arma::uword jp = j==num_y_-1 ? j : j+1;
	// 			const arma::uword jm = j==0 ? j : j-1;
	// 			const fltp deltay = (jp-jm)*dy;
	// 			for(arma::uword i=0;i<num_x_;i++){
	// 				const arma::uword ip = i==num_x_-1 ? i : i+1;
	// 				const arma::uword im = i==0 ? i : i-1;
	// 				const fltp deltax = (ip-im)*dx;

	// 				// convert to index of node
	// 				const arma::uword idx = k*num_x_*num_y_ + j*num_x_ + i;
	// 				const arma::uword idxp = k*num_x_*num_y_ + j*num_x_ + ip;
	// 				const arma::uword idxm = k*num_x_*num_y_ + j*num_x_ + im;
	// 				const arma::uword idyp = k*num_x_*num_y_ + jp*num_x_ + i;
	// 				const arma::uword idym = k*num_x_*num_y_ + jm*num_x_ + i;
	// 				const arma::uword idzp = kp*num_x_*num_y_ + j*num_x_ + i;
	// 				const arma::uword idzm = km*num_x_*num_y_ + j*num_x_ + i;

	// 				// insert into matrix
	// 				idx1(cnt) = eq; idx2(cnt) = idxp; val(cnt) = B(0,idx)/deltax; cnt++;
	// 				idx1(cnt) = eq; idx2(cnt) = idxm; val(cnt) = -B(0,idx)/deltax; cnt++;
	// 				idx1(cnt) = eq; idx2(cnt) = idyp; val(cnt) = B(1,idx)/deltay; cnt++;
	// 				idx1(cnt) = eq; idx2(cnt) = idym; val(cnt) = -B(1,idx)/deltay; cnt++;
	// 				idx1(cnt) = eq; idx2(cnt) = idzp; val(cnt) = B(2,idx)/deltaz; cnt++;
	// 				idx1(cnt) = eq; idx2(cnt) = idzm; val(cnt) = -B(2,idx)/deltaz; cnt++;

	// 				// next equation
	// 				eq++;
	// 			}
	// 		}
	// 	}

	// 	// sanity check
	// 	if(eq!=num_eq)rat_throw_line("sanity check failed");
	// 	if(cnt!=6*num_eq)rat_throw_line("sanity check failed");
	// 	if(arma::any(idx1>=num_eq))rat_throw_line("first index exceeds number of eq");
	// 	if(arma::any(idx2>=num_eq))rat_throw_line("second index exceeds number of eq");

	// 	// create matrix
	// 	arma::SpMat<fltp> M(arma::join_vert(idx1,idx2), val, num_eq, num_eq, true, true);

	// 	// fix first point
	// 	M.row(0).fill(0);
	// 	M.col(0).fill(0);
	// 	M(0,0) = RAT_CONST(1.0);

	// 	// right hand side
	// 	arma::Col<fltp> b(num_eq, arma::fill::zeros);
	// 	b(0) = F000;

	// 	std::cout<<"solving"<<std::endl;
	// 	const arma::Col<fltp> F = arma::spsolve(M,b,"superlu");
	// 	std::cout<<"solving done"<<std::endl;



	// 	// return solution
	// 	return F.t();
	// }

	// // export a vtk with times
	// void GridData::write(cmn::ShLogPr lg){
	// 	// check if data directory set
	// 	if(output_dir_.empty())return;

	// 	// header
	// 	lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
	// 	// check output directory
	// 	if(output_times_.is_empty())rat_throw_line("output times are not set");

	// 	// create output directory
	// 	boost::filesystem::create_directory(output_dir_);

	// 	// output filename
	// 	std::string fname  = output_fname_;

	// 	// report
	// 	lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

	// 	// settings report
	// 	display_settings(lg);

	// 	// report
	// 	lg->msg(2, "%swriting griddata%s\n",KBLU,KNRM);
	// 	lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

	// 	// walk over timesteps
	// 	for(arma::uword i=0;i<output_times_.n_elem;i++){
	// 		// set time
	// 		set_time(output_times_(i));

	// 		// get data at this time
	// 		ShVTKImgPr vtk_img = export_vtk();

	// 		// extend filename with index
	// 		if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

	// 		// show in log
	// 		lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

	// 		// write data to file
	// 		vtk_img->write(output_dir_/(fname+"_grd"));
	// 	}

	// 	// return
	// 	lg->msg(-6,"\n"); 
	// }


	// // get type
	// std::string GridData::get_type(){
	// 	return "rat::mdl::calcgrid";
	// }

	// // method for serialization into json
	// void GridData::serialize(Json::Value &js, cmn::SList &list) const{
	// 	// serialize fieldmap
	// 	CalcFieldMap::serialize(js,list);

	// 	// properties
	// 	js["type"] = get_type();
	// 	js["x1"] = x1_; js["x2"] = x2_; js["num_x"] = (unsigned int)num_x_;
	// 	js["y1"] = y1_; js["y2"] = y2_; js["num_y"] = (unsigned int)num_y_;
	// 	js["z1"] = z1_; js["z2"] = z2_; js["num_z"] = (unsigned int)num_z_;
	// }

	// // method for deserialisation from json
	// void GridData::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
	// 	// serialize fieldmap
	// 	CalcFieldMap::deserialize(js,list,factory_list,pth);

	// 	// properties
	// 	set_dim_x(js["x1"].asDouble(), js["x2"].asDouble(), js["num_x"].asUInt64());
	// 	set_dim_y(js["y1"].asDouble(), js["y2"].asDouble(), js["num_y"].asUInt64());
	// 	set_dim_z(js["z1"].asDouble(), js["z2"].asDouble(), js["num_z"].asUInt64());
	// }

}}