// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivetrapz.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveTrapz::DriveTrapz(){

	}

	// constructor
	DriveTrapz::DriveTrapz(const 
		fltp amplitude, const fltp tstart, 
		const fltp ramprate, const fltp tpulse, 
		const fltp offset, const bool centered){
		set_amplitude(amplitude); set_tstart(tstart); 
		set_ramprate(ramprate); set_tpulse(tpulse); 
		set_offset(offset);	set_centered(centered);
	}

	// factory
	ShDriveTrapzPr DriveTrapz::create(){
		return std::make_shared<DriveTrapz>();
	}

	// factory
	ShDriveTrapzPr DriveTrapz::create(
		const fltp amplitude, const fltp tstart, 
		const fltp ramprate, const fltp tpulse, 
		const fltp offset, const bool centered){
		return std::make_shared<DriveTrapz>(amplitude,tstart,ramprate,tpulse,offset,centered);
	}

	// set centered
	void DriveTrapz::set_centered(const bool centered){
		centered_ = centered;
	}

	// set amplitude of the pulse
	void DriveTrapz::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	// set amplitude of the pulse
	void DriveTrapz::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set start time
	void DriveTrapz::set_tstart(const fltp tstart){
		tstart_ = tstart;
	}

	// set rate of change
	void DriveTrapz::set_ramprate(const fltp ramprate){
		ramprate_ = ramprate;
	}

	// set pulse duration
	void DriveTrapz::set_tpulse(const fltp tpulse){
		tpulse_ = tpulse;
	}
	

	// set amplitude of the pulse
	fltp DriveTrapz::get_offset() const{
		return offset_;
	}

	// set amplitude of the pulse
	fltp DriveTrapz::get_amplitude() const{
		return amplitude_;
	}

	// set start time
	fltp DriveTrapz::get_tstart() const{
		return tstart_;
	}

	// set rate of change
	fltp DriveTrapz::get_ramprate() const{
		return ramprate_;
	}

	// set pulse duration
	fltp DriveTrapz::get_tpulse() const{
		return tpulse_;
	}

	// get centered
	bool DriveTrapz::get_centered()const{
		return centered_;
	}

	// get scaling
	fltp DriveTrapz::get_scaling(const fltp time) const{
		fltp offset = RAT_CONST(0.0);
		if(centered_)offset = tpulse_/2 + amplitude_/ramprate_;
		return offset_ + rat::cmn::Extra::sign(amplitude_)*
			std::max(RAT_CONST(0.0),std::min(std::abs(amplitude_),
			std::min(ramprate_*(time+offset-tstart_),
			-ramprate_*(time+offset-tstart_-tpulse_)+
			2*std::abs(amplitude_))));
	}

	// get current derivative
	fltp DriveTrapz::get_dscaling(const fltp time) const{
		fltp dsc = 0;
		fltp offset = RAT_CONST(0.0);
		if(centered_)offset = tpulse_/2 + amplitude_/ramprate_;
		const fltp tramp = std::abs(amplitude_)/ramprate_;
		if(time+offset>tstart_ && time+offset<(tstart_+tramp))
			dsc = rat::cmn::Extra::sign(amplitude_)*ramprate_;
		if(time+offset>tstart_+tramp+tpulse_ && time+offset<tstart_+2*tramp+tpulse_)
			dsc = -rat::cmn::Extra::sign(amplitude_)*ramprate_;
		return dsc;
	}

	// get times at which an inflection occurs
	arma::Col<fltp> DriveTrapz::get_inflection_times() const{
		const fltp tramp = std::abs(amplitude_)/ramprate_;
		return arma::Col<fltp>{tstart_, tstart_+tramp, 
			tstart_+tramp+tpulse_, tstart_+2*tramp+tpulse_};
	}

	// get type
	std::string DriveTrapz::get_type(){
		return "rat::mdl::drivetrapz";
	}

	// method for serialization into json
	void DriveTrapz::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["centered"] = centered_;
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["ramprate"] = ramprate_;
		js["tpulse"] = tpulse_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void DriveTrapz::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_centered(js["centered"].asBool());
		set_amplitude(js["amplitude"].asDouble());
		set_tstart(js["tstart"].asDouble());
		set_ramprate(js["ramprate"].asDouble());
		set_tpulse(js["tpulse"].asDouble());
		set_offset(js["offset"].asDouble());
	}

}}