// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transrotate.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransRotate::TransRotate(){
		set_name("rotate");
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const fltp ux, const fltp uy, const fltp uz, const fltp alpha){
		set_rotation(ux,uy,uz,alpha);
		set_name("rotate");
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const arma::Col<fltp>::fixed<3> &vector, const fltp alpha){
		set_rotation(vector,alpha);
		set_name("rotate");
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const arma::Col<fltp>::fixed<3> &origin, 
		const arma::Col<fltp>::fixed<3> &vector, 
		const fltp alpha){
		set_origin(origin); set_rotation(vector,alpha); set_name("rotate");
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const arma::Col<fltp>::fixed<3> &origin, 
		const arma::Col<fltp>::fixed<3> &vector, 
		const fltp alpha,
		const arma::Col<fltp>::fixed<3> &offset){
		set_origin(origin); set_rotation(vector,alpha); set_offset(offset); set_name("rotate");
	}

	// factory
	ShTransRotatePr TransRotate::create(){
		//return ShTransRotatePr(new TransRotate);
		return std::make_shared<TransRotate>();
	}

	// factory with rotation around vector
	ShTransRotatePr TransRotate::create(
		const fltp ux, const fltp uy, const fltp uz, const fltp alpha){
		//return ShTransRotatePr(new TransRotate(ux,uy,uz,alpha));
		return std::make_shared<TransRotate>(ux,uy,uz,alpha);
	}

	// factory with rotation around vector
	ShTransRotatePr TransRotate::create(
		const arma::Col<fltp>::fixed<3> &vector, 
		const fltp alpha){
		//return ShTransRotatePr(new TransRotate(V,alpha));
		return std::make_shared<TransRotate>(vector,alpha);
	}

	// factory with rotation around vector
	ShTransRotatePr TransRotate::create(
		const arma::Col<fltp>::fixed<3> &origin, 
		const arma::Col<fltp>::fixed<3> &vector, 
		const fltp alpha){

		//return ShTransRotatePr(new TransRotate(V,alpha));
		return std::make_shared<TransRotate>(origin,vector,alpha);
	}

	// factory with rotation around vector
	ShTransRotatePr TransRotate::create(
		const arma::Col<fltp>::fixed<3> &origin, 
		const arma::Col<fltp>::fixed<3> &vector, 
		const fltp alpha,
		const arma::Col<fltp>::fixed<3> &offset){

		//return ShTransRotatePr(new TransRotate(V,alpha));
		return std::make_shared<TransRotate>(origin,vector,alpha,offset);
	}

	// get vector
	arma::Col<fltp>::fixed<3> TransRotate::get_vector() const{
		return vector_;
	}

	// get origin
	arma::Col<fltp>::fixed<3> TransRotate::get_origin() const{
		return origin_;
	}

	// get origin
	arma::Col<fltp>::fixed<3> TransRotate::get_offset() const{
		return offset_;
	}

	// get angle
	fltp TransRotate::get_alpha() const{
		return alpha_;
	}

	// set vector
	void TransRotate::set_vector(const arma::Col<fltp>::fixed<3> &vector){
		vector_ = vector;
	}

	// set vector
	void TransRotate::set_offset(const arma::Col<fltp>::fixed<3> &offset){
		offset_ = offset;
	}

	// set vector
	void TransRotate::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}


	// rotation around unit vector
	void TransRotate::set_rotation(
		const fltp ux, const fltp uy, 
		const fltp uz, const fltp alpha){
		// use other set function
		set_rotation({ux,uy,uz},alpha);
	}

	// rotation around unit vector
	void TransRotate::set_rotation(
		const arma::Col<fltp>::fixed<3> &vector, const fltp alpha){
		vector_ = vector; alpha_ = alpha;
	}

	// set origin for rotation
	void TransRotate::set_origin(const arma::Col<fltp>::fixed<3> &origin){
		origin_ = origin;
	}

	// rotation matrix
	arma::Mat<fltp>::fixed<3,3> TransRotate::create_rotation_matrix() const{
		// default matrix
		arma::Mat<fltp>::fixed<3,3> M(arma::fill::zeros);
		M(0,0) = 1; M(1,1) = 1; M(2,2) = 1;

		// rotation matrix
		if(arma::as_scalar(cmn::Extra::vec_norm(vector_))>1e-12){
			// normalize
			const arma::Col<fltp>::fixed<3> vector_normalized = 
				vector_.each_row()/cmn::Extra::vec_norm(vector_);

			// rotation matrix
			M = cmn::Extra::create_rotation_matrix(vector_normalized,alpha_);
		}

		// check matrix
		assert(cmn::Extra::is_pure_rotation_matrix(M));
		
		// return matrix
		return M;
	}

	// apply to coordinates only
	void TransRotate::apply_coords(arma::Mat<fltp> &R) const{
		// apply rotation
		R = (create_rotation_matrix()*(R.each_col()-origin_)).eval().each_col() + (origin_ + offset_);
	}

	// apply transformation to coordinates and vectors
	void TransRotate::apply_vectors(
		const arma::Mat<fltp> &, arma::Mat<fltp> &V, const char) const{
		// apply rotation
		V = create_rotation_matrix()*V;
	}

	// get type
	std::string TransRotate::get_type(){
		return "rat::mdl::transrotate";
	}

	// method for serialization into json
	void TransRotate::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
		js["origin_x"] = origin_(0); js["origin_y"] = origin_(1); js["origin_z"] = origin_(2);
		js["vector_x"] = vector_(0); js["vector_y"] = vector_(1); js["vector_z"] = vector_(2);
		js["offset_x"] = offset_(0); js["offset_y"] = offset_(1); js["offset_z"] = offset_(2);
		js["alpha"] = alpha_;
	}

	// method for deserialisation from json
	void TransRotate::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
		set_origin({js["origin_x"].asDouble(),js["origin_y"].asDouble(),js["origin_z"].asDouble()});
		set_vector({js["vector_x"].asDouble(),js["vector_y"].asDouble(),js["vector_z"].asDouble()});
		set_offset({js["offset_x"].asDouble(),js["offset_y"].asDouble(),js["offset_z"].asDouble()});
		alpha_ = js["alpha"].asDouble();
	}

}}
