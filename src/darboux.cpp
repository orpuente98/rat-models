// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Darboux::Darboux(){

	}

	// constructor
	Darboux::Darboux(const arma::Mat<fltp> &V, const arma::Mat<fltp> &A, const arma::Mat<fltp> &J){
		assert(V.n_cols==A.n_cols); assert(V.n_cols==J.n_cols);
		assert(V.n_rows==3); assert(A.n_rows==3); assert(J.n_rows==3);
		V_ = V; A_ = A; J_ = J;
	}

	// factory
	ShDarbouxPr Darboux::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Darboux>();
	}

	// factory
	ShDarbouxPr Darboux::create(const arma::Mat<fltp> &V, const arma::Mat<fltp> &A, const arma::Mat<fltp> &J){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Darboux>(V,A,J);
	}

	// calculate frame
	void Darboux::setup(const bool analytic_frame){
		// number of points
		const arma::uword num_times = V_.n_cols;

		// normalize velocity
		const arma::Row<fltp> normV = cmn::Extra::vec_norm(V_);
		T_ = V_.each_row()/normV; // previously called T
		
		// setup frame
		B_ = cmn::Extra::cross(V_,A_); 
		const arma::Row<fltp> normB = cmn::Extra::vec_norm(B_);

		// analytic version
		if(analytic_frame){
			// calculate 
			kappa_ = normB/(normV%normV%normV);
			tau_ = cmn::Extra::dot(B_,J_)/(normB%normB);

			// normalize B
			B_.each_row() /= normB;

			// frame	
			D_ = tau_%T_.each_row() + kappa_%B_.each_row();

			// normalize darboux vectors
			D_.each_row() /= cmn::Extra::vec_norm(D_);

			// Scale frame
			D_.each_row() %= arma::sqrt(1.0+arma::square(tau_/kappa_));
		}

		// numeric
		else{
			// normalize 
			const arma::Mat<fltp> Anrm = A_.each_row()/cmn::Extra::vec_norm(A_);

			// create frame
			D_ = cmn::Extra::cross(Anrm.head_cols(num_times-1),Anrm.tail_cols(num_times-1));

			// extend
			D_ = (arma::join_horiz(D_.head_cols(1),D_) + arma::join_horiz(D_,D_.tail_cols(1)))/2;

			// normalize darboux vectors
			D_.each_row() /= cmn::Extra::vec_norm(D_);

			// calculate angle
			const arma::Row<fltp> alpha = arma::acos(cmn::Extra::dot(-T_,D_));

			// Scale frame
			D_.each_row() /= arma::sin(alpha);
		}

	}

	// correct sign flips
	void Darboux::correct_sign(const arma::Col<fltp>::fixed<3> &dstart){
		// check sign flips
		const arma::Row<fltp> sf = 
			cmn::Extra::dot(arma::join_horiz(
			dstart,D_.head_cols(D_.n_cols-1)),D_);

		// flip D when curvature changes from positive to negative
		int flipstate = 1;
		for(arma::uword i=0;i<D_.n_cols;i++){
			// check whether flip is needed
			if(sf(i)<0)flipstate*=-1;

			// flip vectors (if needed)
			D_.col(i)*=flipstate;
			B_.col(i)*=flipstate;

			// stability fix
			if(std::abs(sf(i))<1e-7){
				D_.col(i)=D_.col(i-1);
				B_.col(i)=B_.col(i-1);
			}
		}
	}

	// get orientation vectors
	arma::Mat<fltp> Darboux::get_longitudinal() const{
		assert(!T_.is_empty());
		return T_;
	}

	arma::Mat<fltp> Darboux::get_transverse() const{
		assert(!D_.is_empty());
		return D_;
	}

	arma::Mat<fltp> Darboux::get_normal() const{
		return cmn::Extra::cross(T_,B_);
	}

}}