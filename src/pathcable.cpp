// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcable.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathCable::PathCable(){
		set_name("cable");
	}

	// constructor with input of base path
	PathCable::PathCable(const ShPathPr &input_path){
		set_input_path(input_path); set_name("cable");
	}

	// factory methods
	ShPathCablePr PathCable::create(){
		return std::make_shared<PathCable>();
	}

	// factory with input of base path
	ShPathCablePr PathCable::create(const ShPathPr &input_path){
		return std::make_shared<PathCable>(input_path);
	}

	// reverse winding direction
	void PathCable::set_reverse_base(const bool reverse_base){
		reverse_base_ = reverse_base;
	}

	// set number of turns in coil block
	void PathCable::set_num_turns(const arma::uword num_turns){
		// if(num_turns<=0)rat_throw_line("number of turns must be larger than zero");
		num_turns_ = num_turns;
	}

	// set the thickness of the cable
	void PathCable::set_turn_step(const fltp turn_step){
		// if(turn_step<=0)rat_throw_line("turn step must be larger than zero");
		turn_step_ = turn_step;
	}

	// set the thickness of the insulation layer
	void PathCable::set_idx_start(const arma::uword idx_start){
		idx_start_ = idx_start;
	}

	// set number of sections to add
	void PathCable::set_num_add(const arma::sword num_add){
		num_add_ = num_add;
	}

	// set increment position
	void PathCable::set_idx_incr(const arma::uword idx_incr){
		idx_incr_ = idx_incr;
	}

	// set cable offset
	void PathCable::set_num_offset(const arma::sword num_offset){
		num_offset_ = num_offset;
	}

	// set cable offset
	void PathCable::set_offset(const fltp offset){
		offset_ = offset;
	}

	// allow for disabling increment
	void PathCable::set_disable_increment(
		const bool disable_increment){
		disable_increment_ = disable_increment;
	}


	// reverse winding direction
	bool PathCable::get_reverse_base() const{
		return reverse_base_;
	}

	// set number of turns in coil block
	arma::uword PathCable::get_num_turns() const{
		return num_turns_;
	}

	// set the thickness of the cable
	fltp PathCable::get_turn_step() const{
		return turn_step_;
	}

	// set the thickness of the insulation layer
	arma::uword PathCable::get_idx_start() const{
		return idx_start_;
	}

	// set number of sections to add
	arma::sword PathCable::get_num_add() const{
		return num_add_;
	}

	// set increment position
	arma::uword PathCable::get_idx_incr() const{
		return idx_incr_;
	}

	// set cable offset
	arma::sword PathCable::get_num_offset() const{
		return num_offset_;
	}

	// set cable offset
	fltp PathCable::get_offset() const{
		return offset_;
	}

	// allow for disabling increment
	bool PathCable::get_disable_increment() const{
		return disable_increment_;
	}

	// update function
	ShFramePr PathCable::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// create frame
		ShFramePr frame = input_path_->create_frame(stngs);

		// reverse frame if requested
		if(reverse_base_==true)frame->reverse();	

		// number of sections in base
		const arma::uword num_base_sections = frame->get_num_sections();

		// std::cout<<num_base_sections<<std::endl;
		// std::cout<<num_turns_<<std::endl;
		// std::cout<<num_add_<<std::endl;
		// std::cout<<(arma::sword(num_base_sections*num_turns_) + num_add_)<<std::endl;
		// check number of sections
		if((arma::sword(num_base_sections*num_turns_) + num_add_)<=0){
			rat_throw_line("can not remove all sections");
		}

		// calculate number of sections in cable
		const arma::uword num_cable_sections = num_base_sections*num_turns_ + num_add_;

		// check user input
		// if(idx_incr_>=num_base_sections)rat_throw_line("increment index exceeds number of base sections");
		
		// allocate cable coordinate system
		arma::field<arma::Mat<fltp> > R(1,num_cable_sections); 
		arma::field<arma::Mat<fltp> > L(1,num_cable_sections);
		arma::field<arma::Mat<fltp> > D(1,num_cable_sections); 
		arma::field<arma::Mat<fltp> > N(1,num_cable_sections);
		arma::field<arma::Mat<fltp> > B(1,num_cable_sections);

		// allocate section and turn indexes
		arma::Row<arma::uword> section_idx(num_cable_sections);
		arma::Row<arma::uword> turn_idx(num_cable_sections);

		// shift idx start
		const arma::uword idx_start = idx_start_%num_base_sections;

		// calculate increment position
		// unfortunately we must use sword here to allow negative
		arma::sword incr_real = (arma::sword)idx_incr_ - (arma::sword)idx_start;
		if(incr_real<0)incr_real = num_base_sections + incr_real;
		arma::uword idx_incr_real = (arma::uword)incr_real;

		// walk over number of turns
		for(arma::uword i=0;i<(num_turns_ + std::ceil((fltp)num_add_/num_base_sections));i++){
			// number of sections in this turn
			arma::uword num_sect;
			if((num_cable_sections-i*num_base_sections)<num_base_sections)
				num_sect = num_cable_sections-i*num_base_sections;
			else num_sect = num_base_sections;

			// walk over sections
			for(arma::uword j=0;j<num_sect;j++){
				// sector index
				const arma::uword isect = i*num_base_sections + j;

				// index into base
				arma::uword ibase = j+idx_start;
				if(ibase>=num_base_sections)ibase-=num_base_sections;

				
				// number of nodes in this section
				arma::uword Nsn = frame->get_num_nodes(ibase);
				// num_nodes_(isect) = Nsn;

				// calculate position of non-incremented turn
				fltp pn = ((arma::sword)i+num_offset_)*turn_step_ + offset_;

				// check for turn increment
				arma::Row<fltp> nshift(Nsn);
				if(disable_increment_==false){
					if(j==idx_incr_real)nshift = arma::linspace<arma::Row<fltp> >(pn, pn + turn_step_, Nsn);
					else if(j>idx_incr_real)nshift.fill(pn + turn_step_);
					else nshift.fill(pn);
				}else{
					assert(num_add_==0);
					nshift.fill(pn);
				}

				// calculate shear angle
				//const arma::Row<fltp> ashear = frame->calc_ashear(ibase); 

				// generate coordinates by shifting in the block normal direction 
				// R(isect) = frame->get_coords(ibase) + 
				// 	frame->get_block(ibase).each_row()%(nshift/arma::cos(ashear));

				// generate coordinates by shifting in the normal direction
				//R(isect) = Rb(ibase) + Nb(ibase).each_row()%dN;
				R(isect) = frame->perform_normal_shift(ibase, nshift);

				// calculate direction vector
				arma::Mat<fltp> dL = arma::diff(R(isect),1,1);

				// extend direction vector and store in D
				L(isect).zeros(3,Nsn);
				L(isect).cols(1,Nsn-2) = 
					(dL.cols(1,Nsn-2) + dL.cols(0,Nsn-3))/2;
				L(isect).col(0) = dL.col(0); 
				L(isect).col(Nsn-1) = dL.col(Nsn-2); 
				
				// normalize direction vector
				L(isect).each_row() /= cmn::Extra::vec_norm(L(isect));

				// for the longitudinal vector also take first and 
				// last vectors from base path this ensures proper 
				// current lead connections
				L(isect).head_cols(1) = frame->get_direction(ibase).head_cols(1);
				L(isect).tail_cols(1) = frame->get_direction(ibase).tail_cols(1);

				// copy radial vector
				D(isect) = frame->get_transverse(ibase);

				// copy normal vector (do not use cross here
				// otherwise cables are not touching)
				N(isect) = frame->get_normal(ibase);

				// get block direction vector
				B(isect) = frame->get_block(ibase);

				// set index
				section_idx(isect) = ibase;
				turn_idx(isect) = i;
			}
		}

		// reverse frame
		// if(reverse_base_==true){
		// 	Frame::reverse_field(R); Frame::reverse_field(L);
		// 	Frame::reverse_field(N); Frame::reverse_field(D);
		// 	for(arma::uword i=0;i<L.n_elem;i++)L(i) *= -1;
		// }

		// create frame
		ShFramePr cable_frame = Frame::create(R,L,N,D,B);

		// set cable flag
		cable_frame->set_conductor_type(Frame::DbType::cable);

		// override location
		cable_frame->set_location(section_idx, turn_idx, num_base_sections);

		// reverse frame
		if(reverse_base_==true)cable_frame->reverse();

		// apply transformations
		cable_frame->apply_transformations(get_transformations());

		// create frame and return
		return cable_frame;
	}

	// re-index nodes after deleting
	void PathCable::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathCable::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		if(num_turns_<=0){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;};
		if(turn_step_<0){if(enable_throws){rat_throw_line("turn step must be larger than zero");} return false;};
		return true;
	}


	// get type
	std::string PathCable::get_type(){
		return "rat::mdl::pathcable";
	}

	// method for serialization into json
	void PathCable::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["num_turns"] = (unsigned int)num_turns_;
		js["turn_step"] = turn_step_;
		js["idx_start"] = (unsigned int)idx_start_;
		js["idx_incr"] = (unsigned int)idx_incr_;
		js["num_add"] = (int)num_add_;
		js["num_offset"] = (int)num_offset_;
		js["offset"] = offset_;
		js["disable_increment"] = disable_increment_;
		js["reverse_base"] = reverse_base_;
	}

	// method for deserialisation from json
	void PathCable::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// properties
		set_num_turns(js["num_turns"].asUInt64());
		set_turn_step(js["turn_step"].asDouble());
		set_idx_start(js["idx_start"].asUInt64());
		set_idx_incr(js["idx_incr"].asUInt64());
		set_num_add(js["num_add"].asInt64());
		set_num_offset(js["num_offset"].asInt64());
		set_offset(js["offset"].asDouble());
		set_disable_increment(js["disable_increment"].asBool());
		set_reverse_base(js["reverse_base"].asBool());
	}

}}
