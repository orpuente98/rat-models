// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelmgnsphere.hh"
#include "transrotate.hh"
#include "bardata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelMgnSphere::ModelMgnSphere(){
		set_name("mgnsphere");
	}

	// constructor
	ModelMgnSphere::ModelMgnSphere(
		const fltp radius, 
		const arma::Col<fltp>::fixed<3> &magnetisation, 
		const fltp element_size) : ModelMgnSphere(){
		set_radius(radius); set_element_size(element_size); set_magnetisation(magnetisation);
	}

	// factory
	ShModelMgnSpherePr ModelMgnSphere::create(){
		//return ShModelMgnSpherePr(new ModelMgnSphere);
		return std::make_shared<ModelMgnSphere>();
	}
	
	// factory
	ShModelMgnSpherePr ModelMgnSphere::create(
		const fltp radius, 
		const arma::Col<fltp>::fixed<3> &magnetisation, 
		const fltp element_size){
		//return ShModelSpherePr(new ModelSphere);
		return std::make_shared<ModelMgnSphere>(radius, magnetisation, element_size);
	}

	// set magnetisation
	void ModelMgnSphere::set_magnetisation(const arma::Col<fltp>::fixed<3> &Mf){
		Mf_  = Mf;
	}

	// factory for mesh objects
	std::list<ShMeshDataPr> ModelMgnSphere::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check input
		if(!is_valid(stngs.enable_throws))return{};

		// mesh data
		const ShBarDataPr mesh_data = BarData::create();

		// create spherical mesh
		setup_mesh(mesh_data,stngs);

		// calculate magnetization
		arma::Col<fltp>::fixed<3> Mf = magnetisation_drive_->get_scaling(stngs.time)*Mf_;

		// copy properties
		mesh_data->set_softening(softening_);
		mesh_data->set_magnetisation(Mf);
		mesh_data->set_num_gauss(num_gauss_volume_, num_gauss_surface_);

		// mesh data object
		return {mesh_data};
	}

	// check validity
	bool ModelMgnSphere::is_valid(const bool enable_throws) const{
		// check for absolute zero temperature
		if(!ModelSphere::is_valid(enable_throws))return false;
		
		// no problems found
		return true;
	}

	// get type
	std::string ModelMgnSphere::get_type(){
		return "rat::mdl::modelmgnsphere";
	}

	// method for serialization into json
	void ModelMgnSphere::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		ModelSphere::serialize(js,list);
		js["type"] = get_type();
		js["Mfx"] = Mf_(0); 
		js["Mfy"] = Mf_(1); 
		js["Mfz"] = Mf_(2);
		js["softening"] = softening_;
		js["num_gauss_volume"] = (int)num_gauss_volume_;
		js["num_gauss_surface"] = (int)num_gauss_surface_;
	}

	// method for deserialisation from json
	void ModelMgnSphere::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		ModelSphere::deserialize(js,list,factory_list,pth);
		Mf_(0) = js["Mfx"].asDouble(); 
		Mf_(1) = js["Mfy"].asDouble(); 
		Mf_(2) = js["Mfz"].asDouble(); 
		softening_ = js["softening"].asDouble();
		num_gauss_volume_ = js["num_gauss_volume"].asUInt64();
		num_gauss_surface_ = js["num_gauss_surface"].asUInt64();
	}

}}

