// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKTable::VTKTable(){
		vtk_table_ = vtkSmartPointer<vtkTable>::New();
	}

	// factory
	ShVTKTablePr VTKTable::create(){
		return std::make_shared<VTKTable>();
	}

	// destructor
	VTKTable::~VTKTable(){
		// we destroy data here because VTK smart pointer is flawed
		for(auto it = vtk_numeric_data_.begin();it!=vtk_numeric_data_.end();it++)(*it)->Delete();
		for(auto it = vtk_text_data_.begin();it!=vtk_text_data_.end();it++)(*it)->Delete();
	}

	// write mesh
	void VTKTable::set_num_rows(const arma::uword num_rows){
		vtk_table_->SetNumberOfRows(num_rows);
	}

	// write data at nodes
	void VTKTable::set_data(const arma::Col<fltp> &v, const std::string &name){
		// create VTK array
		vtkSmartPointer<vtkDoubleArray> vvtk = vtkDoubleArray::New();
		
		// set data name	
		vvtk->SetName(name.c_str());
		
		// allocate
		vvtk->SetNumberOfComponents(1); 
		vvtk->SetNumberOfValues(v.n_elem);

		// copy data into array
		for(arma::uword i=0;i<v.n_elem;i++)
			vvtk->SetValue(i,v(i));

		// add to VTK
		vtk_table_->AddColumn(vvtk);
		vtk_numeric_data_.push_back(vvtk);
	}
	
	// write data at nodes
	void VTKTable::set_data(const arma::field<std::string> &v, const std::string &name){
		// create VTK array
		vtkSmartPointer<vtkStringArray> vvtk = vtkStringArray::New();

		// set data name	
		vvtk->SetName(name.c_str());
		
		// allocate
		vvtk->SetNumberOfComponents(1); 
		vvtk->SetNumberOfValues(v.n_elem);

		// copy data into array
		for(arma::uword i=0;i<v.n_elem;i++)
			vvtk->SetValue(i,v(i));

		// add to VTK
		vtk_table_->AddColumn(vvtk);
		vtk_text_data_.push_back(vvtk);
	}

	// get filename extension
	std::string VTKTable::get_filename_ext() const{
		return "vtt";
	}

	// write output file
	void VTKTable::write(const boost::filesystem::path fname, cmn::ShLogPr lg){
		// display file settings and type
		std::string ext = get_filename_ext();
		boost::filesystem::path fname_ext = fname;
		fname_ext.replace_extension(ext);

		lg->msg(2,"%sVTK Table: %s%s\n",KBLU,get_name().c_str(),KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.string().c_str(),KNRM);
		lg->msg("type: table (%s)\n",ext.c_str());

		// create writer
		vtkSmartPointer<vtkXMLTableWriter> writer =  
			vtkSmartPointer<vtkXMLTableWriter>::New();
		writer->SetFileName(fname_ext.string().c_str());
		writer->SetInputData(vtk_table_);
		
		// write data
		writer->Write();

		// done writing VTK table
		lg->msg(-2,"\n");
	}

}}