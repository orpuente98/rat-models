// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathconnect.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathConnect::PathConnect(){

	}

	// constructor with dimension input
	PathConnect::PathConnect(
		const ShPathPr &prev_path, 
		const ShPathPr &next_path, 
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4, 
		const fltp ell_trans, 
		const fltp element_size){

		//check input
		assert(prev_path!=NULL);
		assert(next_path!=NULL);
		assert(ell_trans>=0);
		assert(element_size>0);

		// store to self
		prev_path_ = prev_path;
		next_path_ = next_path;
		
		// set strengths
		str1_ = str1; str2_ = str2; 
		str3_ = str3; str4_ = str4;

		// set transition length
		ell_trans_ = ell_trans;

		// set element size
		element_size_ = element_size;
	}


	// factory
	ShPathConnectPr PathConnect::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathConnect>();
	}

	// factory with dimension input
	ShPathConnectPr PathConnect::create(
		const ShPathPr &prev_path, 
		const ShPathPr &next_path, 
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4, 
		const fltp ell_trans, 
		const fltp element_size){
		//return ShPathCirclePr(new PathCircle(radius,dl));
		return std::make_shared<PathConnect>(prev_path,next_path,str1,str2,str3,str4,ell_trans,element_size);
	}

	// method for setting previous path
	void PathConnect::set_prev_path(ShPathPr prev_path){
		assert(prev_path!=NULL);
		prev_path_ = prev_path;
	}

	// method for setting next path
	void PathConnect::set_next_path(ShPathPr next_path){
		assert(next_path!=NULL);
		next_path_ = next_path;
	}

	// set strengths
	void PathConnect::set_strengths(
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4){
		str1_ = str1; str2_ = str2;
		str3_ = str3; str4_ = str4;
	}

	// set transition length
	void PathConnect::set_ell_trans(
		const fltp ell_trans){
		assert(ell_trans>0);
		ell_trans_ = ell_trans;
	}

	// set element size
	void PathConnect::set_element_size(
		const fltp element_size){
		element_size_ = element_size;
	}

	// update function
	ShFramePr PathConnect::create_frame(const MeshSettings &stngs) const{
		// check connectors
		assert(prev_path_!=NULL);
		assert(next_path_!=NULL);

		// create frame
		ShFramePr prev_gen = prev_path_->create_frame(stngs);
		ShFramePr next_gen = next_path_->create_frame(stngs);

		// get frame of first path
		const arma::field<arma::Mat<fltp> > R0 = prev_gen->get_coords();
		const arma::field<arma::Mat<fltp> > L0 = prev_gen->get_direction();
		const arma::field<arma::Mat<fltp> > N0 = prev_gen->get_normal();
		//const arma::field<arma::Mat<fltp> > D0 = prev_gen->get_transverse();
		
		// get frame of second path
		const arma::field<arma::Mat<fltp> > R1 = next_gen->get_coords();
		const arma::field<arma::Mat<fltp> > L1 = next_gen->get_direction();
		const arma::field<arma::Mat<fltp> > N1 = next_gen->get_normal();
		//const arma::field<arma::Mat<fltp> > D1 = next_gen->get_transverse();

		// start and end vectors
		const arma::uword num_first = R0.n_elem;
		const arma::Col<fltp>::fixed<3> r0 = R0(num_first-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> l0 = L0(num_first-1).tail_cols(1);;
		const arma::Col<fltp>::fixed<3> n0 = N0(num_first-1).tail_cols(1);;
		const arma::Col<fltp>::fixed<3> r1 = R1(0).head_cols(1);;
		const arma::Col<fltp>::fixed<3> l1 = L1(0).head_cols(1);;
		const arma::Col<fltp>::fixed<3> n1 = N1(0).head_cols(1);;

		// create generator pointer
		ShFramePr gen;

		// cubic spline with twist
		if(str3_==0 && str4_==0){
			// check connection
			ShPathBSplinePr bezier = PathBSpline::create(
				r0, l0, n0, r1, l1, n1, str1_, str2_, element_size_);

			// return frame for this path
			gen = bezier->create_frame(stngs);
		}

		// quintic spline with constant perimeter
		else{
			// check connection
			ShPathBezierPr bezier = PathBezier::create(
				r0, l0, n0, r1, l1, n1, str1_, str2_, 
				str3_, str4_, ell_trans_, element_size_);

			// return frame for this path
			gen = bezier->create_frame(stngs);
		}

		// transfer type
		gen->set_conductor_type(prev_gen->get_conductor_type());

		// create generator list
		const std::list<ShFramePr> genlist{prev_gen,gen,next_gen};

		// combine frame and return
		return Frame::create(genlist);
	}

}}