// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "coildata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	CoilData::CoilData(){
		set_output_type("coilmesh");
	}

	// constructor with input
	CoilData::CoilData(const ShFramePr &frame, const ShAreaPr &area){
		set_output_type("coilmesh");
		setup(frame, area); num_gauss_ = std::exp2(3-n_dim_); 
	}

	// factory
	ShCoilDataPr CoilData::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<CoilData>();
	}

	// factory with input
	ShCoilDataPr CoilData::create(const ShFramePr &frame, const ShAreaPr &area){
		return std::make_shared<CoilData>(frame, area);
	}

	// extract surface mesh
	ShSurfaceDataPr CoilData::create_surface() const{
		// create coil surface
		ShCoilSurfaceDataPr surf = CoilSurfaceData::create();
		
		// regular setupp
		setup_surface(surf);

		// set properties
		surf->set_material(get_material());
		surf->set_operating_current(operating_current_);
		surf->set_number_turns(number_turns_);

		// return surface
		return surf;
	}

	// setup sources and targets
	void CoilData::setup_sources(){
		// check input
		assert(!n_.is_empty()); 
		assert(!Rt_.is_empty());
		assert(num_gauss_!=0);

		// get counters
		const arma::uword num_cross_elements = area_->get_num_elements();
		const arma::uword num_cross = n_.n_cols/area_->get_num_elements();
		const arma::uword num_elements = num_cross*num_cross_elements;

		// real number of gauss points
		arma::uword ng = std::abs(num_gauss_);

		// create gauss points
		cmn::Gauss gp(num_gauss_);
		const arma::Row<fltp> xg = gp.get_abscissae();
		const arma::Row<fltp> wg = gp.get_weights();
		
		// setup grid around singularity
		// arma::Mat<fltp> Rqgrd; arma::Row<fltp> wgrd;
		// cmn::Hexahedron::setup_source_grid(Rqgrd, wgrd, arma::Col<fltp>{1,1,1}, xg, wg);
		// assert(std::abs(arma::sum(wgrd)-RAT_CONST(1.0))<1e-6);

		// number of gauss points
		const arma::uword ng_element = std::pow(ng,n_dim_-1);

		// length scaling
		// Rqd = [xi;nu;mu]
		arma::Mat<fltp> Rqgrd(n_dim_,ng_element);
		arma::Row<fltp> ell_scale(ng_element);
		arma::Row<fltp> a_scale(ng_element);

		// for line elements
		if(n_dim_ == 1){
			// single point in middle of line
			Rqgrd(0) = 0; ell_scale(0) = 1.0; a_scale(0) = 1.0;

			// check scales
			assert(std::abs(arma::accu(ell_scale)-RAT_CONST(1.0))<1e-6);
			assert(std::abs(arma::accu(a_scale)-RAT_CONST(1.0))<1e-6);
			assert(std::abs(arma::accu(ell_scale%a_scale)-RAT_CONST(1.0))<1e-6);
		}

		// for quadrilateral elements
		else if(n_dim_ == 2){
			// setup quadrilateral grid coordinates
			arma::uword cnt = 0;
			for(arma::uword k=0;k<ng;k++){
				Rqgrd.col(cnt) = arma::Col<fltp>::fixed<2>{xg(k),0.0};
				ell_scale(cnt) = 1.0; a_scale(cnt) = wg(k)/2;
				cnt++;
			}

			// check scales
			assert(std::abs(arma::accu(ell_scale)-ng)<1e-6);
			assert(std::abs(arma::accu(a_scale)-RAT_CONST(1.0))<1e-6);
			assert(std::abs(arma::accu(ell_scale%a_scale)-RAT_CONST(1.0))<1e-6);
		}

		// for hexahedron elements
		else if(n_dim_ == 3){
			// setup quadrilateral grid coordinates
			// for hex mu is along length
			arma::uword cnt = 0;
			for(arma::uword k=0;k<ng;k++){
				for(arma::uword l=0;l<ng;l++){
					Rqgrd.col(cnt) = arma::Col<fltp>::fixed<3>{xg(k),xg(l),0.0};
					ell_scale(cnt) = 1.0; a_scale(cnt) = wg(k)*wg(l)/4;
					cnt++;
				}
			}
			
			// check scales
			assert(std::abs(arma::accu(ell_scale)-ng*ng)<1e-6);
			assert(std::abs(arma::accu(a_scale)-RAT_CONST(1.0))<1e-6);
			assert(std::abs(arma::accu(ell_scale%a_scale)-RAT_CONST(1.0))<1e-6);
		}

		// quadrilateral coord
		// const arma::Col<fltp>::fixed<2> Rqmid(arma::fill::zeros);

		// calculate cross section area	
		const arma::Row<fltp> cross_area = area_->get_areas();

		// calculate number of sources
		num_sources_ = num_elements*ng_element;

		// allocate one line current for each element
		Rs_.set_size(3,num_sources_);
		dRs_.set_size(3,num_sources_);
		
		// create array fo cross sectional arreas
		Acrss_.set_size(num_sources_);
		
		// walk over elements
		for(arma::uword i=0;i<num_elements;i++){
			// find cable intersections at middle of faces/edges
			// const arma::Col<fltp>::fixed<3> R1 = 
			// 	cmn::Quadrilateral::quad2cart(
			// 	Rt_.cols(n_.submat(0,i,n_.n_rows/2-1,i)),Rqmid);
			// const arma::Col<fltp>::fixed<3> R2 = 
			// 	cmn::Quadrilateral::quad2cart(
			// 	Rt_.cols(n_.submat(n_.n_rows/2,i,n_.n_rows-1,i)),Rqmid);

			// find cable intersections at middle of faces/edges
			const arma::Col<fltp>::fixed<3> R1 = arma::mean(
				Rt_.cols(n_.submat(0,i,n_.n_rows/2-1,i)),1);
			const arma::Col<fltp>::fixed<3> R2 = arma::mean(
				Rt_.cols(n_.submat(n_.n_rows/2,i,n_.n_rows-1,i)),1);

			// create weighted direction vectors
			arma::Mat<fltp> dRtmp(3,ng_element);
			dRtmp.each_col() = R2-R1;
			dRtmp.each_row() %= ell_scale;

			// calculate points in carthesian coordinates
			if(n_dim_==1)Rs_.cols(i*ng_element,(i+1)*ng_element-1) = 
				cmn::Line::quad2cart(Rt_.cols(n_.col(i)),Rqgrd);
			else if(n_dim_==2)Rs_.cols(i*ng_element,(i+1)*ng_element-1) = 
				cmn::Quadrilateral::quad2cart(Rt_.cols(n_.col(i)),Rqgrd);
			else if(n_dim_==3)Rs_.cols(i*ng_element,(i+1)*ng_element-1) = 
				cmn::Hexahedron::quad2cart(Rt_.cols(n_.col(i)),Rqgrd);

			// set direction vector
			dRs_.cols(i*ng_element,(i+1)*ng_element-1) = dRtmp;

			// set element current
			Acrss_.cols(i*ng_element,(i+1)*ng_element-1) = 
				a_scale*cross_area(i%num_cross_elements);
		}

		// set currents to elements
		Is_ = Acrss_*calc_current_density();

		// softening only based on cross sectional area
		// van Lanen kernel assumed
		epss_ = softening_*std::sqrt(RAT_CONST(2.0))*arma::sqrt(Acrss_/arma::Datum<fltp>::pi);
	}

	// set operating current
	void CoilData::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	fltp CoilData::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void CoilData::set_number_turns(const fltp number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	fltp CoilData::get_number_turns() const{
		return number_turns_;
	}

	// calculate current density based on set opertaing current
	fltp CoilData::calc_current_density() const{
		// calculate cross section areas
		const fltp cross_area = area_->get_area();

		// calculate current distribution in cross section
		const fltp Jop = number_turns_*operating_current_/cross_area;

		// return current density
		return Jop;
	}

	// 
	arma::Mat<fltp> CoilData::calc_nodal_current_density()const{
		return L_*calc_current_density();
	}

	// set use volume elements
	void CoilData::set_use_volume_elements(const bool use_volume_elements){
		use_volume_elements_ = use_volume_elements;
	}

	// set softening factor
	void CoilData::set_softening(const fltp softening){
		softening_ = softening;
	}

	// set number of gauss points
	void CoilData::set_num_gauss(const arma::sword num_gauss){
		if(num_gauss==0)rat_throw_line("number of gauss points must be positive");
		num_gauss_ = num_gauss;
	}

	// calculate enclosed flux
	fltp CoilData::calculate_flux() const{
		// check if field calculated
		if(!has_field())rat_throw_line("field not calculated");
		if(!has('A'))rat_throw_line("vector potential not calculated");

		// calculate element volume
		const arma::Row<fltp> Ve = calc_volume();

		// calculate cross sectional area
		const fltp Acrss = area_->get_area();

		// calculate contribution from nodes
		const arma::Row<fltp> phidn = rat::cmn::Extra::dot(get_field('A'), L_);

		// calculate contribution from elements
		arma::Row<fltp> phie(n_.n_cols);
		for(arma::uword l=0;l<n_.n_cols;l++)
			phie(l) = Ve(l)*arma::as_scalar(arma::mean(phidn.cols(n_.col(l)),1));

		// convert to voltage contribution
		const fltp phi = arma::accu(get_number_turns()*phie/Acrss);

		// accumulutae
		return phi;
	}

	// calculate self inductance of elements
	fltp CoilData::calculate_element_self_inductance() const{
		// check if elements setup
		if(dRs_.is_empty() || Acrss_.is_empty())rat_throw_line("elements not setup");
		
		// get area
		const fltp conductor_area = area_->get_area();

		// calculate from elements
		const arma::Row<fltp> length = cmn::Extra::vec_norm(dRs_);
		const arma::Row<fltp> diameter = 2*arma::sqrt(Acrss_/arma::Datum<fltp>::pi);

		// calculate self inductance
		const arma::Row<fltp> self_inductance = 
			rat::fmm::Extra::calc_wire_inductance(diameter, length);

		// scale
		return get_number_turns()*get_number_turns()*
			arma::accu(self_inductance%Acrss_%Acrss_)/
			(conductor_area*conductor_area);
	}


	// VTK export
	ShVTKObjPr CoilData::export_vtk() const{
		// create unstructured grid
		ShVTKUnstrPr vtk = std::dynamic_pointer_cast<VTKUnstr>(MeshData::export_vtk());
		assert(vtk!=NULL);

		// check if field calculated
		if(has('H') && has_field()){
			// field angle
			const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle();

			// set to VTK
			vtk->set_nodedata(360.0*magnetic_field_angle/(arma::Datum<fltp>::pi*2),"Mgn. Field Angle [deg]"); 

			// material related properties
			if(material_!=NULL){
				// current density vectors
				const arma::Mat<fltp> current_density = calc_nodal_current_density();

				// engineering critical current
				const arma::Row<fltp> critical_current_density = calc_critical_current_density();

				// divide current density by critical (engineering) current density
				const arma::Row<fltp> critical_current_fraction =  cmn::Extra::vec_norm(current_density)/critical_current_density;

				// calculate critical temperature
				const arma::Row<fltp> critical_temperature = calc_critical_temperature();

				// calculate force density vectors
				const arma::Mat<fltp> force_density = calc_force_density();

				// calculate electric field
				const arma::Mat<fltp> electric_field = calc_electric_field();

				// percentage on loadline
				const arma::Row<fltp> load_line_fraction = calc_loadline_fraction();

				// calculate pressure
				const arma::Mat<fltp> pressure = calc_pressure();

				// calulate von mises from pressure
				const arma::Row<fltp> von_mises = calc_von_mises();

				// currnet density
				vtk->set_nodedata(1e-6*current_density,"Current Density [A/mm^2]");

				// add engineering current density to VTK
				vtk->set_nodedata(1e-6*critical_current_density,"Eng. Cur. Dens. [A/mm^2]"); 

				// set percentage oif critical current
				vtk->set_nodedata(100*critical_current_fraction,"Pct. Crit. Current [%]");

				// add critical temperature to VTK
				vtk->set_nodedata(critical_temperature,"Crit. Temperature [K]");

				// add temperature margin to VTK
				vtk->set_nodedata(critical_temperature - temperature_,"Temperature Margin [K]");

				// add force density to VTK
				vtk->set_nodedata(1e-6*force_density,"Force Density [N/cm^3]");

				// electric field
				vtk->set_nodedata(electric_field,"Electric Field [V/m]");

				// power density
				vtk->set_nodedata(cmn::Extra::dot(electric_field, current_density),"Power Density [W/m^3]");

				// load line calculation
				vtk->set_nodedata(100*load_line_fraction,"Pct. Loadline [%]");

				// pressure
				vtk->set_nodedata(1e-6*pressure,"Pressure [MPa]");

				// pressure
				vtk->set_nodedata(1e-6*von_mises,"Von Mises [MPa]");
			}
		}

		// return the unstructured mesh
		return vtk;
	}

	// calculate average critical temperature
	arma::Row<fltp> CoilData::calc_critical_temperature() const{
		// hard-coded settings
		const bool use_parallel = true;
		const arma::uword num_cross_per_chunk = 10;

		// get counters
		const arma::uword num_nodes = Rt_.n_cols;

		// check if field calculated
		if(!has('H') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// get cross section mesh
		const arma::Mat<arma::uword> ncrss = area_->get_elements();
		const arma::Mat<fltp> Acrss = area_->get_areas();

		// divide into chunks
		const arma::uword chunk_size = num_cross_per_chunk*area_->get_num_nodes();
		const arma::uword num_chunks = std::ceil((double)num_nodes/chunk_size);

		// allocate
		arma::Row<fltp> critical_temperature(num_nodes);

		// current density at nodes
		const arma::Mat<fltp> current_density = L_*calc_current_density();
		const arma::Row<fltp> current_density_norm = 
			rat::cmn::Extra::vec_norm(current_density);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate angle in radians and return
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle();

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// parallel calculation in chunks of 
		// integer number of cross-sections
		cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
			// get indexes
			const arma::uword idx1 = i*chunk_size;
			const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

			// calculate critical temperature
			// without current sharing
			if(!enable_current_sharing_){
				critical_temperature.cols(idx1,idx2) = 
					material_->calc_cs_temperature_mat(
						current_density_norm.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
				
			}

			// with current sharing
			else{
				critical_temperature.cols(idx1,idx2) = 
					material_->calc_cs_temperature_av_mat(
						current_density_norm.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2), 
						ncrss, Acrss);
			}
		});

		// return critical temperature
		return critical_temperature;
	}

	// calculate average critical temperature
	arma::Row<fltp> CoilData::calc_temperature_margin() const{
		// get counters
		const arma::uword num_nodes = Rt_.n_cols;

		// check if field calculated
		if(!has('H') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// calc critical temperature and derive temperature margin
		return calc_critical_temperature() - temperature_;
	}	

	// calculate magnetic field angle
	arma::Row<fltp> CoilData::calc_magnetic_field_angle()const{
		// get counters
		const arma::uword num_nodes = Rt_.n_cols;

		// check if field available
		if(!has('H') || !has_field())return arma::Row<fltp>(num_nodes, arma::fill::zeros);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate components along L, N and D
		const arma::Row<fltp> Blong = cmn::Extra::dot(magnetic_field,L_);
		const arma::Row<fltp> Bnorm = cmn::Extra::dot(magnetic_field,N_);
		const arma::Row<fltp> Btrns = cmn::Extra::dot(magnetic_field,D_);

		// calculate angle in radians and return
		const arma::Row<fltp> magnetic_field_angle = arma::atan2(arma::sqrt(Blong%Blong + Btrns%Btrns),Bnorm);

		// field angle
		return magnetic_field_angle;
	}


	// critical current calculation
	arma::Row<fltp> CoilData::calc_critical_current_density() const{
		// hard-coded settings
		const bool use_parallel = true;
		const arma::uword num_cross_per_chunk = 10;

		// get counters
		const arma::uword num_nodes = Rt_.n_cols;

		// check if field calculated
		if(!has('H') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// divide in chunks
		const arma::uword chunk_size = num_cross_per_chunk*area_->get_num_nodes();
		const arma::uword num_chunks = std::ceil((double)num_nodes/chunk_size);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// calculate fiel dangle
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle();

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// allocate
		arma::Row<fltp> critical_current_density(num_nodes);

		// parallel calculation in chunks of 
		// integer number of cross-sections
		cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
			// get indexes
			const arma::uword idx1 = i*chunk_size;
			const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

			// Jc calculation with current sharing over cross-section
			// without current sharing
			if(!enable_current_sharing_){
				critical_current_density.cols(idx1,idx2) = 
					material_->calc_critical_current_density_mat(
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			}

			// with current sharing
			else{
				// get cross section mesh
				const arma::Mat<arma::uword> ncrss = area_->get_elements();
				const arma::Mat<fltp> Acrss = area_->get_areas();

				// calculate
				critical_current_density.cols(idx1,idx2) = 
					material_->calc_critical_current_density_av_mat(
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2), 
						scaling_factor.cols(idx1,idx2),
						ncrss, Acrss);
			}
		});

		// return critical current density
		return critical_current_density;
	}


	// critical current calculation
	arma::Row<fltp> CoilData::calc_loadline_fraction() const{
		// hard-coded settings
		const bool use_parallel = true;
		const arma::uword num_cross_per_chunk = 10;

		// get counters
		const arma::uword num_nodes = Rt_.n_cols;

		// check if field calculated
		if(!has('H') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// divide in chunks
		const arma::uword chunk_size = num_cross_per_chunk*area_->get_num_nodes();
		const arma::uword num_chunks = std::ceil((double)num_nodes/chunk_size);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// calculate fiel dangle
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle();

		// current density at nodes
		const arma::Mat<fltp> current_density = L_*calc_current_density();
		const arma::Row<fltp> current_density_norm = rat::cmn::Extra::vec_norm(current_density);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// allocate
		arma::Row<fltp> percent_loadline(num_nodes);

		// parallel calculation in chunks of 
		// integer number of cross-sections
		cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
			// get indexes
			const arma::uword idx1 = i*chunk_size;
			const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

			// electric field calculation
			if(!enable_current_sharing_){
				// load line calculation
				percent_loadline.cols(idx1,idx2) = 
					material_->calc_percent_load_mat(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			}

			else{
				// get cross section mesh
				const arma::Mat<arma::uword> ncrss = area_->get_elements();
				const arma::Mat<fltp> Acrss = area_->get_areas();

				// load line calculation
				percent_loadline.cols(idx1,idx2) = 
					material_->calc_percent_load_mat_av(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2),
						ncrss, Acrss);
			}
		});

		// return critical current density
		return percent_loadline/100;
	}

	// electric field calculation
	arma::Mat<fltp> CoilData::calc_electric_field() const{
		// hard-coded settings
		const bool use_parallel = true;
		const arma::uword num_cross_per_chunk = 10;

		// get counters
		const arma::uword num_nodes = Rt_.n_cols;
		
		// check if field calculated
		if(!has('H') || !has_field() || material_==NULL)
			return arma::Mat<fltp>(3,num_nodes,arma::fill::zeros);

		// divide in chunks
		const arma::uword chunk_size = num_cross_per_chunk*area_->get_num_nodes();
		const arma::uword num_chunks = std::ceil((double)num_nodes/chunk_size);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// calculate fiel dangle
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle();

		// current density at nodes
		const arma::Mat<fltp> current_density = L_*calc_current_density();
		const arma::Row<fltp> current_density_norm = rat::cmn::Extra::vec_norm(current_density);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// allocate
		arma::Row<fltp> electric_field(num_nodes);

		// parallel calculation in chunks of 
		// integer number of cross-sections
		cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
			// get indexes
			const arma::uword idx1 = i*chunk_size;
			const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

			// electric field calculation
			// without current sharing
			if(!enable_current_sharing_){
				electric_field.cols(idx1,idx2) = 
					material_->calc_electric_field_mat(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			}

			// with current sharing
			else{
				// get cross section mesh
				const arma::Mat<arma::uword> ncrss = area_->get_elements();
				const arma::Mat<fltp> Acrss = area_->get_areas();

				// calculate
				electric_field.cols(idx1,idx2) = 
					material_->calc_electric_field_av_mat(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2), 
						ncrss, Acrss);
			}
		});

		// return critical current density
		return L_.each_row()%electric_field;
	}



	// calculate power density at nodes
	arma::Row<fltp> CoilData::calc_power_density() const{
		return calc_current_density()*cmn::Extra::vec_norm(calc_electric_field());
	}

	// calculate power density at nodes
	arma::Row<fltp> CoilData::calc_critical_current_fraction() const{
		// get counters
		const arma::uword num_nodes = Rt_.n_cols;
		
		// check if field calculated
		if(!has('H') || !has_field() || material_==NULL)
			return arma::Row<fltp>(1,num_nodes,arma::fill::zeros);

		// divide current density by critical (engineering) current density
		return cmn::Extra::vec_norm(calc_nodal_current_density())/calc_critical_current_density();
	}


	// export line elements
	ShVTKTablePr CoilData::export_line_elements() const{
		// check if sources were setup
		if(Rs_.is_empty())rat_throw_line("sources are not setup");
		if(dRs_.is_empty())rat_throw_line("sources are not setup");

		// create table
		const ShVTKTablePr table = VTKTable::create();
		table->set_num_rows(Rs_.n_cols);
		for(arma::uword i=0;i<3;i++)
			table->set_data(Rs_.row(i).t(),"R" + std::string({cmn::Extra::idx2xyz(i)}));
		for(arma::uword i=0;i<3;i++)
			table->set_data(dRs_.row(i).t(),"dR" + std::string({cmn::Extra::idx2xyz(i)}));

		// return table
		return table;
	}

	// calculate integrated foce
	arma::Col<fltp>::fixed<3> CoilData::calc_lorentz_force() const{
		// calculate Lorentz force density
		const arma::Mat<fltp> force_density = calc_force_density();

		// interpolate to elements
		arma::Mat<fltp> element_force_density(3,n_.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			element_force_density.col(i) = arma::mean(force_density.cols(n_.col(i)),1);

		// multiply by element volume and sum
		return arma::sum(element_force_density.each_row()%calc_volume(),1);
	}

	// calculate force density
	arma::Mat<fltp> CoilData::calc_force_density() const{
		return cmn::Extra::cross(L_*calc_current_density(),get_field('B'));
	}

	// calculate pressure vectors
	arma::Mat<fltp> CoilData::calc_pressure()const{
		return N_.each_row()%calc_pressure(0) + D_.each_row()%calc_pressure(1);
	}

	// calculate pressure in normal direction
	arma::Row<fltp> CoilData::calc_normal_pressure()const{
		return calc_pressure(0);
	}

	// calculate pressure in normal direction
	arma::Row<fltp> CoilData::calc_transverse_pressure()const{
		return calc_pressure(1);
	}

	// calculate von mises from the pressure
	arma::Row<fltp> CoilData::calc_von_mises()const{
		const arma::Row<fltp> Pt = calc_transverse_pressure();
		const arma::Row<fltp> Pn = calc_normal_pressure();
		return arma::sqrt(arma::square(Pt) - Pt%Pn + arma::square(Pn));
	}

	// calculate accumulated pressure
	// direction given by int_dir
	// 0 Normal direction, 1. Transverse direction
	arma::Row<fltp> CoilData::calc_pressure(const arma::uword int_dir) const{
		// special case for line cross-section
		if(n_dim_<=2 && int_dir==0)return cmn::Extra::dot(calc_force_density(),N_)*area_->get_hidden_thickness();
		if(n_dim_<=2 && int_dir==1)return arma::Row<fltp>(Rt_.n_cols,arma::fill::zeros);

		// hard-coded settings
		const bool use_parallel = true;

		// direction transverse to integration
		const arma::uword trans_dir = 1 - int_dir;

		// calculate Lorentz force density
		const arma::Mat<fltp> force_density = calc_force_density();

		// get in plane components only
		arma::Row<fltp> force_density_int;
		if(int_dir==0)force_density_int = cmn::Extra::dot(force_density, N_);
		else if(int_dir==1)force_density_int = cmn::Extra::dot(force_density, D_);
		else rat_throw_line("direction of integration not recognized");

		// get number of nodes in each cross section
		const arma::uword num_nodes = Rt_.n_cols;
		const arma::uword num_nodes_crss = area_->get_num_nodes();
		const arma::uword num_crss = num_nodes/num_nodes_crss;

		// get elements
		const arma::Mat<arma::uword> ncrss = area_->get_elements();
		const arma::Mat<fltp> Rcrss = area_->get_nodes();

		// create trace line coordinates in the transverse direction
		const arma::Row<fltp> transverse = arma::unique(Rcrss.row(trans_dir));
		// const arma::Row<fltp> normal = arma::unique(Rcrss.row(1));

		// find all unique edge elements
		const arma::Mat<arma::uword>::fixed<4,2> Me = cmn::Quadrilateral::get_edges();
		arma::Mat<arma::uword> ecrss(2,ncrss.n_cols*Me.n_rows);

		for(arma::uword i=0;i<Me.n_rows;i++)
			ecrss.cols(i*ncrss.n_cols,(i+1)*ncrss.n_cols-1) = ncrss.rows(Me.row(i));

		// get edges
		arma::Mat<fltp> R1 = Rcrss.cols(ecrss.row(0));
		arma::Mat<fltp> R2 = Rcrss.cols(ecrss.row(1));

		// throw away edges that run along integration direction
		const arma::Col<arma::uword> idx_shed = arma::find(arma::abs(R1.row(trans_dir) - R2.row(trans_dir))<1e-9);
		ecrss.shed_cols(idx_shed); R1.shed_cols(idx_shed); R2.shed_cols(idx_shed);

		// store nodes
		arma::Row<fltp> pressure(num_nodes,arma::fill::zeros);

		// walk over cross sections
		cmn::parfor(0,num_crss,use_parallel,[&](int i, int){
		// for(arma::uword i=0;i<num_crss;i++){
			// get indexing
			const arma::uword idx1 = i*num_nodes_crss;
			const arma::uword idx2 = (i+1)*num_nodes_crss - 1;

			// get force densities for this cross section
			const arma::Row<fltp> force_density_int_crss = force_density_int.cols(idx1,idx2);
			// const arma::Row<fltp> force_density_transverse_crss = force_density_transverse.cols(idx1,idx2);

			// store nodes
			arma::Row<fltp> pressure_crss(num_nodes_crss, arma::fill::zeros);

			// ray casting
			for(arma::uword j=0;j<transverse.n_elem;j++){
				// get transverse corodinate
				const fltp t = transverse(j);

				// find indexes of edges that cross this line
				const fltp tol = RAT_CONST(1e-14);
				arma::Col<arma::uword> idx = arma::find(
					(t>=R1.row(trans_dir)-tol && t<=R2.row(trans_dir)+tol) || (t<=R1.row(trans_dir)+tol && t>=R2.row(trans_dir)-tol));
				if(idx.empty())rat_throw_line("no intersection points found");

				// force densities along line
				const arma::Row<fltp> Fdn1 = force_density_int_crss.cols(ecrss.cols(idx).eval().row(0));
				const arma::Row<fltp> Fdn2 = force_density_int_crss.cols(ecrss.cols(idx).eval().row(1));
				const arma::Mat<fltp> R1l = R1.cols(idx);
				const arma::Mat<fltp> R2l = R2.cols(idx);

				if(arma::any(arma::abs(R2l.row(trans_dir)-R1l.row(trans_dir))<1e-9))rat_throw_line("edge is in normal direction");

				// calculate weights
				const arma::Row<fltp> w1 = (R2l.row(trans_dir) - t)/(R2l.row(trans_dir) - R1l.row(trans_dir));
				const arma::Row<fltp> w2 = (t - R1l.row(trans_dir))/(R2l.row(trans_dir) - R1l.row(trans_dir));

				// find force and coordinate of intersection
				arma::Row<fltp> Fdni = w1%Fdn1 + w2%Fdn2;
				arma::Mat<fltp> Ri = w1%R1l.each_row() + w2%R2l.each_row();
				if(!Ri.is_finite())rat_throw_line("intersection coords are not finite");
				if(!Fdni.is_finite())rat_throw_line("Line force density is not finite");

				// three dimensional coordinates
				arma::Mat<fltp> R3d = w1%Rt_.cols(idx1 + ecrss.cols(idx).eval().row(0)).eval().each_row() + w2%Rt_.cols(idx1 + ecrss.cols(idx).eval().row(1)).eval().each_row();

				// sort nodes based on their position in the integration direction
				const arma::Col<arma::uword> idx_sort = arma::sort_index(Ri.row(int_dir));

				// perform sorting
				Fdni = Fdni.cols(idx_sort); Ri = Ri.cols(idx_sort); R3d = R3d.cols(idx_sort); 

				// find unique points
				const arma::Col<arma::uword> idx_unique = arma::find_unique(Ri.row(int_dir));

				// perform sorting
				Fdni = Fdni.cols(idx_unique); Ri = Ri.cols(idx_unique);  R3d = R3d.cols(idx_unique);

				// determine sign
				const fltp sgn = cmn::Extra::sign(arma::as_scalar(cmn::Extra::dot(R3d.tail_cols(1) - R3d.col(0), int_dir==0 ?  N_.col(idx1) : D_.col(idx1))));
				Fdni*=sgn;

				// integrate force density
				if(Ri.n_cols!=Fdni.n_elem)rat_throw_line("num elements not ok");
				arma::Row<fltp> pressure_line = cmn::Extra::cumtrapz(Ri.row(int_dir), Fdni, 1);
				if(!pressure_line.is_finite())rat_throw_line("line pressure is not finite");
				if(pressure_line.n_elem!=Fdni.n_elem)rat_throw_line("num elements changed");

				// move minimum up
				pressure_line -= arma::min(pressure_line);

				// find elements
				const arma::Col<arma::uword> idx_nodes_along_ray = arma::find(Rcrss.row(trans_dir)==t);

				// normal direction
				const arma::Mat<fltp> Rn = Rcrss.cols(idx_nodes_along_ray);
				if(!Rn.is_finite())rat_throw_line("node coords is not finite");

				// interpolate
				arma::Col<fltp> pressure_nodes;
				cmn::Extra::interp1(Ri.row(int_dir).t(),pressure_line.t(),Rn.row(int_dir).t(),pressure_nodes,"linear",true);

				// save
				if(idx_nodes_along_ray.empty())rat_throw_line("no nodes associated to this ray");
				pressure_crss.cols(idx_nodes_along_ray) = pressure_nodes.t();
				
				if(!pressure_crss.is_finite())rat_throw_line("crss pressure not finite");
			}

			// store pressure
			pressure.cols(idx1,idx2) = pressure_crss;
		});

		// return pressure
		if(!pressure.is_finite())rat_throw_line("pressure is not finite");
		return pressure;
	}


}}
