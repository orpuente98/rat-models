// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcleaf.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLeaf::CalcLeaf(){
		stngs_ = fmm::Settings::create();
	}

	// set and get input path
	void CalcLeaf::set_settings(const fmm::ShSettingsPr &stngs){
		stngs_ = stngs;
	}
	
	void CalcLeaf::set_background(const ShBackgroundPr &bg){
		bg_ = bg;
	}

	const ShBackgroundPr& CalcLeaf::get_background() const{
		return bg_;
	}

	// get settings
	const fmm::ShSettingsPr& CalcLeaf::get_settings() const{
		return stngs_;
	}

	// validity check
	bool CalcLeaf::is_valid(const bool enable_throws) const{
		if(stngs_==NULL){if(enable_throws){rat_throw_line("settings points to NULL");} return false;};
		return true;
	}

	// add model function
	void CalcLeaf::set_model(const ShModelPr &model){
		if(model==NULL)rat_throw_line("model is NULL pointer");
		model_ = model;
	}

	// add model function
	ShModelPr CalcLeaf::get_model(){
		return model_;
	}

	// get type
	std::string CalcLeaf::get_type(){
		return "rat::mdl::calcleaf";
	}

	// method for serialization into json
	void CalcLeaf::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Calc::serialize(js,list);
		InputCircuits::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize settings
		js["stngs"] = cmn::Node::serialize_node(stngs_, list);
		js["model"] = cmn::Node::serialize_node(model_, list);
		js["bg"] = cmn::Node::serialize_node(bg_, list);
	}

	// method for deserialisation from json
	void CalcLeaf::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Calc::deserialize(js,list,factory_list,pth);
		InputCircuits::deserialize(js,list,factory_list,pth);

		// deserialize settings
		if(js.isMember("stngs"))set_settings(cmn::Node::deserialize_node<fmm::Settings>(js["stngs"], list, factory_list, pth));
		set_model(cmn::Node::deserialize_node<Model>(js["model"], list, factory_list, pth));
		set_background(cmn::Node::deserialize_node<Background>(js["bg"], list, factory_list, pth));
	}


}}
