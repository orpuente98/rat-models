// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "emitterbeam.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	EmitterBeam::EmitterBeam(){
		set_name("beam emitter");
		// protons are default
		set_proton();
	}

	// factory
	ShEmitterBeamPr EmitterBeam::create(){
		return std::make_shared<EmitterBeam>();
	}
				
	// create correlated data
	arma::Mat<fltp> EmitterBeam::correlated_normdist(
		const fltp correlation, const arma::uword num_cols){
		// create correlation atrix for normal direction
		arma::Mat<fltp>::fixed<2,2> M;
		M.row(0) = arma::Row<fltp>{RAT_CONST(1.0), correlation};
		M.row(1) = arma::Row<fltp>{correlation, 1.0};
		const arma::Mat<fltp>::fixed<2,2> Mc = arma::chol(M).t();
		return Mc*arma::Mat<fltp>(2,num_cols,arma::fill::randn);
	}

	// set protons
	void EmitterBeam::set_proton(){
		rest_mass_ = 0.938; // [GeV/C^2] 
		charge_ = 1; // elementary charge
		beam_energy_ = rest_mass_ + 0.1;
	}

	// set electrons
	void EmitterBeam::set_electron(){
		rest_mass_ = 0.51099895000e-3; // [GeV/C^2] 
		charge_ = -1; // elementary charge
		beam_energy_ = rest_mass_ + 0.1;
	}

	// set rest mass
	void EmitterBeam::EmitterBeam::set_rest_mass(const fltp rest_mass){
		if(rest_mass<=0)rat_throw_line("rest mass must be larger than zero");
		rest_mass_ = rest_mass;
	}

	// set charge
	void EmitterBeam::set_charge(const fltp charge){
		charge_ = charge;
	}

	// set beam energy
	void EmitterBeam::set_beam_energy(const fltp beam_energy){
		if(beam_energy<=0)rat_throw_line("beam energy must be larger than zero");
		beam_energy_ = beam_energy;
	}

	// set particle lifetime
	void EmitterBeam::set_lifetime(const arma::uword lifetime){
		if(lifetime<=0)rat_throw_line("lifetime must be larger than zero");
		lifetime_ = lifetime; 
	}

	// set starting index
	void EmitterBeam::set_start_idx(const arma::uword start_idx){
		start_idx_ = start_idx;
	}

	// getting the rest mass of the particles
	fltp EmitterBeam::get_rest_mass() const{
		return rest_mass_;
	}

	// getters
	arma::uword EmitterBeam::get_num_particles() const{
		return num_particles_;
	}

	fltp EmitterBeam::get_charge()const{
		return charge_;
	}

	fltp EmitterBeam::get_beam_energy()const{
		return beam_energy_;
	}

	arma::uword EmitterBeam::get_lifetime()const{
		return lifetime_;
	}

	// set number of particles
	void EmitterBeam::set_num_particles(const arma::uword num_particles){
		num_particles_ = num_particles;
	}

	// set orientation
	void EmitterBeam::set_spawn_coord(
		const arma::Col<fltp>::fixed<3> &R, 
		const arma::Col<fltp>::fixed<3> &L, 
		const arma::Col<fltp>::fixed<3> &N, 
		const arma::Col<fltp>::fixed<3> &D){

		R_ = R;
		L_ = L.each_row()/cmn::Extra::vec_norm(L); 
		N_ = N.each_row()/cmn::Extra::vec_norm(N);
		D_ = D.each_row()/cmn::Extra::vec_norm(D);
	} 

	void EmitterBeam::set_coord(
		const arma::Col<fltp>::fixed<3> &R){
		R_ = R;
	}

	void EmitterBeam::set_longitudinal(
		const arma::Col<fltp>::fixed<3> &L){
		L_ = L;
	}

	void EmitterBeam::set_normal(
		const arma::Col<fltp>::fixed<3> &N){
		N_ = N;
	}

	void EmitterBeam::set_transverse(
		const arma::Col<fltp>::fixed<3> &D){
		D_ = D;
	}

	// set position xx
	void EmitterBeam::set_xx(const fltp corr_xx, const fltp sigma_x, const fltp sigma_xa){
		corr_xx_ = corr_xx; sigma_x_ = sigma_x; sigma_xa_ = sigma_xa;
	}

	// set position yy
	void EmitterBeam::set_yy(const fltp corr_yy, const fltp sigma_y, const fltp sigma_ya){
		corr_yy_ = corr_yy; sigma_y_ = sigma_y; sigma_ya_ = sigma_ya;
	}


	// particle creation
	arma::field<Particle> EmitterBeam::spawn_particles() const{
		// error check
		is_valid(true);

		// calculate relativistic velocity
		// const fltp V = std::sqrt(1.0 - ));
		// if(V>RAT_CONST(1.0))rat_throw_line("can not exceed the speed of light sherlock");

		// set seed for random number generator
		arma::arma_rng::set_seed(seed_);

		const fltp gamma = beam_energy_/rest_mass_;
		const fltp V = std::sqrt(1.0 - 1.0/(gamma*gamma));

		// generate data in x
		const arma::Mat<fltp> XX = correlated_normdist(corr_xx_, num_particles_);
		const arma::Mat<fltp> YY = correlated_normdist(corr_yy_, num_particles_);

		// calculate angles
		const arma::Row<fltp> alpha = sigma_xa_*XX.row(1);
		const arma::Row<fltp> beta = sigma_ya_*YY.row(1);

		// get position in local coordinates
		const arma::Row<fltp> u = sigma_x_*XX.row(0);
		const arma::Row<fltp> v = sigma_y_*YY.row(0);
		const arma::Row<fltp> w(num_particles_,arma::fill::zeros);

		// get velocity in local coordinates
		const arma::Row<fltp> du = V*arma::sin(alpha);
		const arma::Row<fltp> dv = V*arma::sin(beta);
		const arma::Row<fltp> dw = V*arma::cos(alpha)%arma::cos(beta);
		
		// create start positions
		arma::Mat<fltp> R0(3,num_particles_);
		arma::Mat<fltp> V0(3,num_particles_);

		// calculate cartesian coordinates
		for(arma::uword i=0;i<3;i++){
			R0.row(i) = R_(i) + L_(i)*w + N_(i)*u + D_(i)*v;
			V0.row(i) = L_(i)*dw + N_(i)*du + D_(i)*dv;
		}	

		// allocate list of particles
		arma::field<Particle> particles(num_particles_);

		// create all particles
		for(arma::uword i=0;i<num_particles_;i++){
			// set maximum number of steps
			particles(i).set_num_steps(lifetime_);

			// set start coordinate and velocity
			particles(i).set_startcoord(R0.col(i),V0.col(i));

			// set start index
			particles(i).set_start_index(start_idx_);

			// set mass
			particles(i).set_rest_mass(rest_mass_);
			particles(i).set_charge(charge_);

			// setup internal storage
			particles(i).setup();
		}

		// return list of particles
		return particles;
	}

	// validity check
	bool EmitterBeam::is_valid(const bool enable_throws) const{
		if(rest_mass_<=0){if(enable_throws){rat_throw_line("rest mass must be larger than zero");} return false;};
		if(beam_energy_<=rest_mass_){if(enable_throws){rat_throw_line("beam energy must exceed rest mass");} return false;};
		if(lifetime_<=0){if(enable_throws){rat_throw_line("lifetime must be larger than zero");} return false;};
		if(corr_xx_<0){if(enable_throws){rat_throw_line("correlation must be larger than zero");} return false;};
		if(corr_xx_>1){if(enable_throws){rat_throw_line("correlation must be smaller than one");} return false;};
		if(sigma_x_<0){if(enable_throws){rat_throw_line("beam size must be positive");} return false;};
		if(corr_yy_<0){if(enable_throws){rat_throw_line("correlation must be larger than zero");} return false;};
		if(corr_yy_>1){if(enable_throws){rat_throw_line("correlation must be smaller than one");} return false;};
		if(sigma_y_<0){if(enable_throws){rat_throw_line("beam size must be positive");} return false;};
		return true;
	}

	// get type
	std::string EmitterBeam::get_type(){
		return "rat::mdl::emitterbeam";
	}

	// method for serialization into json
	void EmitterBeam::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);

		// properties
		js["type"] = get_type();

		// rng
		js["seed"] = (unsigned int)seed_;

		// beam parameters
		js["num_particles"] = (unsigned int)num_particles_;
		js["beam_energy"] = beam_energy_;
		js["rest_mass"] = rest_mass_;
		js["charge"] = charge_;

		// position orientation
		js["position"] = Node::serialize_matrix(R_);
		js["longitudinal"] = Node::serialize_matrix(L_);
		js["normal"] = Node::serialize_matrix(N_);
		js["transverse"] = Node::serialize_matrix(D_);

		// beam
		js["sigma"] = sigma_;
		js["corr_xx"] = corr_xx_;
		js["sigma_x"] = sigma_x_;
		js["sigma_xa"] = sigma_xa_;
		js["corr_yy"] = corr_yy_;
		js["sigma_y"] = sigma_y_;
		js["sigma_ya"] = sigma_ya_;

		// tracking
		js["lifetime"] = (unsigned int)lifetime_;
		js["start_idx"] = (unsigned int)start_idx_;
	}

	// method for deserialisation from json
	void EmitterBeam::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		Node::deserialize(js,list,factory_list,pth);
		
		// rng
		seed_ = js["seed"].asUInt64();

		// beam parameters
		num_particles_ = js["num_particles"].asUInt64();
		beam_energy_ = js["beam_energy"].asDouble();
		rest_mass_ = js["rest_mass"].asDouble();
		charge_ = js["charge"].asDouble();

		// position orientation
		R_ = Node::deserialize_matrix(js["position"]);
		L_ = Node::deserialize_matrix(js["longitudinal"]);
		N_ = Node::deserialize_matrix(js["normal"]);
		D_ = Node::deserialize_matrix(js["transverse"]);

		// beam
		sigma_ = js["sigma"].asDouble();
		corr_xx_ = js["corr_xx"].asDouble();
		sigma_x_ = js["sigma_x"].asDouble();
		sigma_xa_ = js["sigma_xa"].asDouble();
		corr_yy_ = js["corr_yy"].asDouble();
		sigma_y_ = js["sigma_y"].asDouble();
		sigma_ya_ = js["sigma_ya"].asDouble();

		// tracking
		lifetime_ = js["lifetime"].asUInt64();
		start_idx_ = js["start_idx"].asUInt64();
	}


}}
