// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcpoints.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcPoints::CalcPoints(){
		set_name("points"); 

		// settins
		stngs_->set_num_exp(5);
		stngs_->set_large_ilist(true);
	}

	CalcPoints::CalcPoints(const ShModelPr &model) : CalcPoints(){
		set_model(model);
	}
	
	// factory methods
	ShCalcPointsPr CalcPoints::create(){
		return std::make_shared<CalcPoints>();
	}

	ShCalcPointsPr CalcPoints::create(const ShModelPr &model){
		return std::make_shared<CalcPoints>(model);
	}

	// set coordinates
	void CalcPoints::set_coords(const arma::Mat<fltp> &Rt){
		Rt_ = Rt;
	}

	// get coordinates
	arma::Mat<fltp> CalcPoints::get_coords() const{
		return Rt_;
	}

	// get mesh enabled
	bool CalcPoints::get_visibility() const{
		return visibility_;
	}

	// set mesh enabled
	void CalcPoints::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	// calculate with inductance data output
	ShMeshDataPr CalcPoints::calculate_points(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Point Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create point elements
		const arma::Row<arma::uword> n = arma::regspace<arma::Row<arma::uword> >(0,Rt_.n_cols-1);

		// create targets
		const ShMeshDataPr tar = MeshData::create(Rt_,n,n);
		tar->set_field_type("AHM",{3,3,3});
		tar->set_time(time);
		
		// set temperature
		tar->set_operating_temperature(0);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 

		// create multilevel fast multipole method object
		fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)
			src->add_sources(*it);

		// add sources and targets to the calculation
		mlfmm->set_sources(src);
		mlfmm->set_targets(tar);

		// setup multipole method
		mlfmm->setup(lg);

		// run multipole method
		mlfmm->calculate(lg);

		// return the line data
		return tar;
	}

	// generalized calculation
	std::list<ShDataPr> CalcPoints::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_points(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcPoints::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// create point elements
		const arma::Row<arma::uword> n = arma::regspace<arma::Row<arma::uword> >(0,Rt_.n_cols-1);
			
		// mesh data
		const ShMeshDataPr mesh_data = MeshData::create(Rt_,n,n);

		// set time
		mesh_data->set_time(stngs.time);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();

		// mesh data object
		return {mesh_data};
	}

	// is valid
	bool CalcPoints::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		if(Rt_.n_cols<=0){if(enable_throws){rat_throw_line("no points to calculate");} return false;};
		return true;
	}

	// serialization
	std::string CalcPoints::get_type(){
		return "rat::mdl::calcpoints";
	}

	void CalcPoints::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
		js["visibility"] = visibility_;
		for(arma::uword i=0;i<Rt_.n_cols;i++){
			Json::Value jsp;
			jsp["x"] = Rt_(0,i);
			jsp["y"] = Rt_(1,i);
			jsp["z"] = Rt_(2,i);
			js["points"].append(jsp);
		}
	}
	
	void CalcPoints::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
		visibility_ = js["visibility"].asBool();
		Rt_.set_size(3,js["points"].size());
		arma::uword cnt = 0;
		for(auto it=js["points"].begin();it!=js["points"].end();it++,cnt++){
			Rt_(0,cnt) = (*it)["x"].asDouble();
			Rt_(1,cnt) = (*it)["y"].asDouble();
			Rt_(2,cnt) = (*it)["z"].asDouble();
		}
	}

}}