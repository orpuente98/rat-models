// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcircle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCircle::PathCircle(){
		set_name("circle");
		radius_ = RAT_CONST(0.05);
		num_sections_ = 4;
		element_size_ = RAT_CONST(2e-3);
		offset_ = RAT_CONST(0.0);
	}

	// constructor with dimension input
	PathCircle::PathCircle(
		const fltp radius, const arma::uword num_sections, 
		const fltp element_size, const fltp offset){
		
		// set values with respective functions
		set_name("circle");
		set_radius(radius);
		set_num_sections(num_sections);
		set_element_size(element_size);
		set_offset(offset);
	}

	// factory
	ShPathCirclePr PathCircle::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathCircle>();
	}

	// factory with dimension input
	ShPathCirclePr PathCircle::create(
		const fltp radius, const arma::uword num_sections, 
		const fltp element_size, const fltp offset){
		return std::make_shared<PathCircle>(radius,num_sections,element_size,offset);
	}

	// set transverse
	void PathCircle::set_transverse(const bool transverse){
		transverse_ = transverse;
	}

	// set element size 
	void PathCircle::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set divisor
	// void PathCircle::set_element_divisor(const arma::uword element_divisor){
	// 	element_divisor_ = element_divisor;
	// }

	// path offset
	void PathCircle::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set radius
	void PathCircle::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set radius
	void PathCircle::set_num_sections(const arma::uword num_sections){
		num_sections_ = num_sections;
	}

	// set transverse
	bool PathCircle::get_transverse()const{
		return transverse_;
	}
	

	// set element size 
	fltp PathCircle::get_element_size() const{
		return element_size_;
	}

	// set radius
	fltp PathCircle::get_radius() const{
		return radius_;
	}

	// set radius
	fltp PathCircle::get_offset() const{
		return offset_;
	}

	// set radius
	arma::uword PathCircle::get_num_sections() const{
		return num_sections_;
	}


	// get frame
	ShFramePr PathCircle::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// check input
		if(radius_<=0)rat_throw_line("radius must be larger than zero");
		if(element_size_<=0)rat_throw_line("element size must be larger than zero");
		if(num_sections_<=0)rat_throw_line("number of sections must be larger than zero");

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();

		// add sections
		for(arma::uword i=0;i<num_sections_;i++){
			ShPathArcPr arc = PathArc::create(radius_,
				2*arma::Datum<fltp>::pi/num_sections_,element_size_);
			arc->set_offset(offset_);
			// arc->set_element_divisor(element_divisor_);
			arc->set_transverse(transverse_);
			pathgroup->add_path(arc);
		}

		// move
		if(!transverse_)pathgroup->add_translation(radius_,0,0);
		else pathgroup->add_translation(0,0,radius_);

		// create frame
		ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations());
		
		// return frame
		return frame;
	}

	// validity check
	bool PathCircle::is_valid(const bool enable_throws) const{
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(num_sections_<=0){if(enable_throws){rat_throw_line("number of sections must be larger than zero");} return false;};
		return true;
	}


	// get type
	std::string PathCircle::get_type(){
		return "rat::mdl::pathcircle";
	}

	// method for serialization into json
	void PathCircle::serialize(Json::Value &js, cmn::SList &list) const{
		// parent nodes
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["num_sections"] = (unsigned int)num_sections_;
		js["transverse"] = transverse_;
		js["radius"] = radius_;
		js["element_size"] = element_size_; 
		js["offset"] = offset_; 
	}

	// method for deserialisation from json
	void PathCircle::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent nodes
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_transverse(js["transverse"].asBool());
		set_num_sections(js["num_sections"].asUInt64());
		set_radius(js["radius"].asDouble());
		set_element_size(js["element_size"].asDouble()); 
		set_offset(js["offset"].asDouble()); 
	}

}}