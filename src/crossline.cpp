// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crossline.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossLine::CrossLine(){
		set_name("line");
	}

	// constructor with input
	CrossLine::CrossLine(const arma::Row<fltp> &u, const arma::Row<fltp> &v, const fltp thickness){
		// set to self
		set_coord(u,v); set_thickness(thickness); set_name("line");
	}

	// construct line with 2 points
	CrossLine::CrossLine(const fltp u1, const fltp v1, const fltp u2, const fltp v2, const fltp thickness, const fltp dl){
		set_coord(u1,v1,u2,v2,dl); set_thickness(thickness); set_name("line");
	}

	// factory
	ShCrossLinePr CrossLine::create(){
		return std::make_shared<CrossLine>();
	}

	// factory with dimension input
	ShCrossLinePr CrossLine::create(const arma::Row<fltp> &u, const arma::Row<fltp> &v, const fltp thickness){
		return std::make_shared<CrossLine>(u,v,thickness);
	}

	// factory with dimension input
	ShCrossLinePr CrossLine::create(const fltp u1, const fltp v1, const fltp u2, const fltp v2, const fltp thickness, const fltp dl){
		return std::make_shared<CrossLine>(u1,v1,u2,v2,thickness,dl);
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossLine::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;
		bnd.col(0) = arma::Col<fltp>::fixed<2>{arma::min(u_), arma::max(u_)};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{arma::min(v_), arma::max(v_)};
		return bnd;
	}

	// set dimensions in the normal direction
	void CrossLine::set_coord(const arma::Row<fltp> &u, const arma::Row<fltp> &v){
		if(u.n_elem!=v.n_elem)rat_throw_line("u and v must have same number of elements");
		u_ = u; v_ = v;
	}

	// set thickness
	void CrossLine::set_thickness(const fltp thickness){
		if(thickness<=0)rat_throw_line("thickness must be larger than zero");
		thickness_ = thickness;
	}

	// set coordinates
	void CrossLine::set_coord(const fltp u1, const fltp v1, const fltp u2, const fltp v2, const fltp dl){
		const fltp du = u2-u1; const fltp dv = v2-v1; const fltp ell = std::sqrt(du*du + dv*dv);
		arma::uword num_elem = std::max(2,(int)std::ceil(ell/dl)+1);
		set_coord(arma::linspace<arma::Row<fltp> >(u1,u2,num_elem),
			arma::linspace<arma::Row<fltp> >(v1,v2,num_elem));
	}

	// volume mesh
	ShAreaPr CrossLine::create_area(const MeshSettings &/*stngs*/) const{
		// check settings
		is_valid(true);
		
		// number of coordinates in line
		const arma::uword num_coord = u_.n_elem;

		// coordinate matrix
		arma::Mat<fltp> Rl = arma::join_vert(u_,v_);

		// elements
		const arma::Mat<arma::uword> n = arma::join_vert(
			arma::regspace<arma::Row<arma::uword> >(0,num_coord-2),
			arma::regspace<arma::Row<arma::uword> >(1,num_coord-1));

		// find
		const arma::Mat<fltp> dRl = arma::diff(Rl,1,1);
		arma::Mat<fltp> D = arma::join_horiz(dRl,arma::Col<fltp>{0,0}) + arma::join_horiz(arma::Col<fltp>{0,0},dRl);
		D.each_row()/=cmn::Extra::vec_norm(D);
		const arma::Mat<fltp> N = arma::flipud(D);

		// create area
		ShAreaPr area = Area::create(Rl,N,D, n, thickness_);

		// return area
		return area;
	}

	// surface mesh
	ShPerimeterPr CrossLine::create_perimeter() const{
		// coordinate
		const arma::Col<fltp> Rn(2,0);
		const arma::Row<arma::uword> s(2,0);

		// extract periphery and return
		return Perimeter::create(Rn,s);
	}

	// vallidity check
	bool CrossLine::is_valid(const bool enable_throws) const{
		if(u_.is_empty() || v_.is_empty()){if(enable_throws){rat_throw_line("line coordinates are not set");} return false;};
		if(u_.n_elem!=v_.n_elem){if(enable_throws){rat_throw_line("line coordinates u and v are not equal in length");} return false;};
		return true;
	}

	// get type
	std::string CrossLine::get_type(){
		return "rat::mdl::crossline";
	}

	// method for serialization into json
	void CrossLine::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		js["type"] = get_type();
		js["u"] = rat::cmn::Node::serialize_matrix(u_);
		js["v"] = rat::cmn::Node::serialize_matrix(v_);
		js["thickness"] = thickness_;
	}

	// method for deserialisation from json
	void CrossLine::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		set_coord(rat::cmn::Node::deserialize_matrix(js["u"]),
			rat::cmn::Node::deserialize_matrix(js["v"]));
		set_thickness(js["thickness"].asDouble());
	}

}}
