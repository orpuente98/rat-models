// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathBezier::PathBezier(){

	}

	// constructor
	PathBezier::PathBezier(
		const arma::Col<fltp>::fixed<3> pstart, 
		const arma::Col<fltp>::fixed<3> lstart, 
		const arma::Col<fltp>::fixed<3> nstart,
		const arma::Col<fltp>::fixed<3> pend, 
		const arma::Col<fltp>::fixed<3> lend, 
		const arma::Col<fltp>::fixed<3> nend,
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4,
		const fltp ell_trans, const fltp element_size, 
		const fltp path_offset){
		
		// start point
		pstart_ = pstart; lstart_ = lstart; nstart_ = nstart;

		// end point
		pend_ = pend; lend_ = lend; nend_ = nend;

		// set strengths
		str1_ = str1; str2_ = str2; 
		str3_ = str3; str4_ = str4;

		// transition length
		ell_trans_ = ell_trans;

		// element size
		element_size_ = element_size;
		path_offset_ = path_offset;
	}

	// factory
	ShPathBezierPr PathBezier::create(){
		return std::make_shared<PathBezier>();
	}

	// factory
	ShPathBezierPr PathBezier::create(
		const arma::Col<fltp>::fixed<3> pstart, 
		const arma::Col<fltp>::fixed<3> lstart, 
		const arma::Col<fltp>::fixed<3> nstart,
		const arma::Col<fltp>::fixed<3> pend, 
		const arma::Col<fltp>::fixed<3> lend, 
		const arma::Col<fltp>::fixed<3> nend,
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4,
		const fltp ell_trans, const fltp element_size,
		const fltp path_offset){
		return std::make_shared<PathBezier>(pstart,lstart,nstart,
			pend,lend,nend,str1,str2,str3,str4,ell_trans,element_size,path_offset);
	}

	// set planar winding 
	void PathBezier::set_planar_winding(const bool planar_winding){
		planar_winding_ = planar_winding;
	}

	// set planar winding 
	void PathBezier::set_analytic_frame(const bool analytic_frame){
		analytic_frame_ = analytic_frame;
	}

	// set number of sections
	void PathBezier::set_num_sections(const arma::uword num_sections){
		if(num_sections<1)rat_throw_line("number of sections must be larger than one");
		num_sections_ = num_sections;
	}

	// get frame
	ShFramePr PathBezier::create_frame(const MeshSettings &/*stngs*/) const{
		// create control points for quintic spline
		// arma::Mat<fltp> P(3,6);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - nstart_*str3_;
		// P.col(3) = pend_ - lend_*ell_trans_ - lend_*(str1_+str2_) - nend_*str4_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(5) = pend_ - lend_*ell_trans_;

		// control points for septic spline
		// arma::Mat<fltp> P(3,8);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - nstart_*str3_;
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - 2*nstart_*str3_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - 2*nend_*str4_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+1*str2_) - nend_*str4_;
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(7) = pend_ - lend_*ell_trans_;

		// control points for septic spline
		// arma::Mat<fltp> P(3,8);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_);
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - nstart_*str3_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - nend_*str4_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+1*str2_);
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(7) = pend_ - lend_*ell_trans_;

		// control points for nonantic spline
		// arma::Mat<fltp> P(3,10);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - (1.0/5)*nstart_*str3_;
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - (3.0/5)*nstart_*str3_;
		// P.col(4) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+3*str2_) - 1.0*nstart_*str3_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+3*str2_) - 1.0*nend_*str4_;
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - (3.0/5)*nend_*str4_;
		// P.col(7) = pend_ - lend_*ell_trans_ - lend_*(str1_+str2_) - (1.0/5)*nend_*str4_;
		// P.col(8) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(9) = pend_ - lend_*ell_trans_ ;

		// Thomas Nes Point definition 35e-3, 14e-3
		arma::Mat<fltp> P(3,16);
		P.col(0) = pstart_;
		P.col(1) = pstart_ + (lstart_*1*str1_);
		P.col(2) = pstart_ + (lstart_*2*str1_);
		P.col(3) = pstart_ + (lstart_*3*str1_);
		P.col(4) = pstart_ + (lstart_*(4*str1_) + 1*nstart_*str3_);
		P.col(5) = pstart_ + (lstart_*(5*str1_) + 2*nstart_*str3_);
		P.col(6) = pstart_ + (lstart_*(6*str1_) + 4*nstart_*str3_);
		P.col(7) = pstart_ + (lstart_*(6*str1_) + 10*nstart_*str3_);
		P.col(8) = pend_ - (lend_*(6*str2_) - 10*nend_*str4_);
		P.col(9) = pend_ - (lend_*(6*str2_) - 4*nend_*str4_);
		P.col(10) = pend_ - (lend_*(5*str2_) - 2*nend_*str4_);
		P.col(11) = pend_ - (lend_*(4*str2_) - 1*nend_*str4_);
		P.col(12) = pend_ - (lend_*3*str2_);
		P.col(13) = pend_ - (lend_*2*str2_);
		P.col(14) = pend_ - (lend_*1*str2_);
		P.col(15) = pend_;

		// calculate start and end frame
		const arma::Col<fltp>::fixed<3> dstart = cmn::Extra::cross(nstart_,lstart_);
		const arma::Col<fltp>::fixed<3> dend = cmn::Extra::cross(nend_,lend_);

		// create times
		const arma::Row<fltp> t = regular_times(P);

		// number of times
		const arma::uword num_times = t.n_elem;

		// create spline and its derivatives
		//arma::Mat<fltp> R1,V1,A1; create_bezier(R1,V1,A1,t,P);
		const arma::field<arma::Mat<fltp> > C = create_bezier_tn(t,P,4);
		
		// get coordinates
		arma::Mat<fltp> R = C(0); 

		// get derivatives
		const arma::Mat<fltp> V = C(1); // velocity
		const arma::Mat<fltp> A = C(2); // acceleration
		const arma::Mat<fltp> J = C(3); // jerk

		// create darboux frame
		Darboux db(V,A,J);
		db.setup(analytic_frame_); db.correct_sign(dstart);
		arma::Mat<fltp> D = db.get_transverse();
		arma::Mat<fltp> L = db.get_longitudinal();
		// arma::Mat<fltp> N = db.get_normal();

		// check if transition region needed
		if(ell_trans_>0){
			// number of transition elements
			const arma::uword num_trans = std::max(2,(int)std::ceil(1.5*ell_trans_/element_size_));

			// allocate transition
			arma::Mat<fltp> R1trans(3,num_trans);
			arma::Mat<fltp> R2trans(3,num_trans);
			arma::Mat<fltp> D1trans(3,num_trans);
			arma::Mat<fltp> D2trans(3,num_trans);
			arma::Mat<fltp> L1trans(3,num_trans);
			arma::Mat<fltp> L2trans(3,num_trans);

			// times for transition
			arma::Row<fltp> ttrans = arma::linspace<arma::Row<fltp> >(0,RAT_CONST(1.0),num_trans);

			// calculate transition
			for(arma::uword i=0;i<num_trans;i++){
				R1trans.col(i) = (1.0-ttrans(i))*pstart_ + ttrans(i)*R.col(0);
				R2trans.col(i) = (1.0-ttrans(i))*R.col(num_times-1) + ttrans(i)*pend_;
				D1trans.col(i) = (1.0-ttrans(i))*dstart + ttrans(i)*D.col(0);
				D2trans.col(i) = (1.0-ttrans(i))*D.col(num_times-1) + ttrans(i)*dend;
			}
			L1trans.each_col() = lstart_;
			L2trans.each_col() = lend_;

			// atach transition
			R = arma::join_horiz(arma::join_horiz(
				R1trans.head_cols(num_trans-1),R),
				R2trans.tail_cols(num_trans-1));
			D = arma::join_horiz(arma::join_horiz(
				D1trans.head_cols(num_trans-1),D),
				D2trans.tail_cols(num_trans-1));
			L = arma::join_horiz(arma::join_horiz(
				L1trans.head_cols(num_trans-1),L),
				L2trans.tail_cols(num_trans-1));
		}

		// Calculate normal vector
		arma::Mat<fltp> N = cmn::Extra::cross(L,D);
		N.each_row()/=cmn::Extra::vec_norm(N);

		// allocate block direction vectors
		arma::Mat<fltp> B;

		// in case of planar windings
		if(planar_winding_){
			// get out of plane vector
			arma::Col<fltp>::fixed<3> plane_vec = cmn::Extra::cross(lstart_,nstart_);
			assert(std::abs(arma::as_scalar(cmn::Extra::dot(plane_vec,lend_))-RAT_CONST(1.0))>1e-6);

			// calculate block vector
			B = cmn::Extra::cross(arma::Mat<fltp>(3,L.n_cols,arma::fill::ones).each_col()%plane_vec,L);

			B = (B+N)/2;
		}

		// non-planar winding
		else{
			B = N;
		}

		// check sizes
		assert(R.n_cols==L.n_cols); assert(R.n_cols==N.n_cols);
		assert(R.n_cols==D.n_cols); assert(R.n_cols==B.n_cols);

		// create field arrays
		// arma::field<arma::Mat<fltp> > 
		// 	Rfld(1,num_sections_), Lfld(1,num_sections_), 
		// 	Nfld(1,num_sections_), Dfld(1,num_sections_), 
		// 	Bfld(1,num_sections_);

		// // create indexes at which we want to split the sections
		// arma::Row<arma::uword> idx_divide = 
		// 	arma::regspace<arma::Row<arma::uword> >(0,num_sections_)*
		// 	std::floor(num_times/num_sections_);
		// idx_divide(num_sections_) = num_times-1;

		// // split sections
		// for(arma::uword i=0;i<num_sections_;i++){
		// 	const arma::uword idx1 = idx_divide(i);
		// 	const arma::uword idx2 = idx_divide(i+1);
		// 	Rfld(i) = R.cols(idx1,idx2); Lfld(i) = L.cols(idx1,idx2);
		// 	Nfld(i) = N.cols(idx1,idx2); Dfld(i) = D.cols(idx1,idx2);
		// 	Bfld(i) = B.cols(idx1,idx2);
		// }

		// subdivide
		//ShFramePr frame = Frame::create(Rfld,Lfld,Nfld,Dfld,Bfld);

		// create frame and return 
		ShFramePr frame = Frame::create(R,L,N,D,B,num_sections_);

		// return the frame
		return frame;
	}

	// regular time function
	arma::Row<fltp> PathBezier::regular_times(
		const arma::Mat<fltp> &P) const{

		// calculate line with regular spaced times
		const arma::Row<fltp> treg = arma::linspace<arma::Row<fltp> >(1e-9,1.0-1e-9,num_reg_);

		// create spline
		//arma::Mat<fltp> R,V,A;
		//create_bezier(R,V,A,treg,P);
		const arma::field<arma::Mat<fltp> > C = create_bezier_tn(treg,P,3);
		// const arma::Mat<fltp> R = C(0);
		const arma::Mat<fltp> V = C(1);
		const arma::Mat<fltp> A = C(2);

		// calculate radius of curvature from velocity and acceleration
		const arma::Row<fltp> radius = arma::pow(cmn::Extra::vec_norm(V),3)/cmn::Extra::vec_norm(cmn::Extra::cross(V,A));

		// calculate scaling factor based on radius
		// the clamp here can be debated
		const arma::Row<fltp> factor = (radius + path_offset_)/radius;

		// calculate spacing
		const arma::Row<fltp> Vnorm = factor%cmn::Extra::vec_norm(V);
		const arma::Row<fltp> s = arma::join_horiz(arma::Row<fltp>{0}, 
			arma::cumsum(arma::diff(treg,1,1)%(Vnorm.head_cols(num_reg_-1)+Vnorm.tail_cols(num_reg_-1)/2)));

		// divide into elements
		const arma::uword num_times = std::max(2,(int)std::ceil(s.back()/element_size_));

		// find position along length
		const arma::Row<fltp> sreg = arma::linspace<arma::Row<fltp> >(0,s.back(),num_times);

		// get 
		arma::Col<fltp> tset;
 		cmn::Extra::interp1(s.t(),treg.t(),sreg.t(),tset,"linear",true);

		// // acceleration fix/cheat (zero acceleration gives weird results)
		// const arma::Row<arma::uword> idx = arma::find(cmn::Extra::vec_norm(A)<1e-9).t();
		// if(!idx.is_empty())A.cols(idx) = A.cols(idx-1);

		
		// // calculate length
		// const arma::Row<fltp> ell = factor%arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0));

		// // calculate number of times required
		// const arma::uword num_times = std::max(2,(int)(std::ceil(arma::sum(ell)/element_size_)));

		// // calculate line with regular spaced times
		// const arma::Row<fltp> treq = arma::linspace<arma::Row<fltp> >(0,RAT_CONST(1.0),num_times);

		// // calculate accumalation of length
		// arma::Row<fltp> rp(num_reg_,arma::fill::zeros);
		// rp.tail_cols(num_reg_-1) = arma::cumsum(ell)/arma::sum(ell);

		// // allocate output
		// arma::Col<fltp> tset;

		// // use interpolation
		// arma::interp1(rp.t(),treg.t(),treq.t(),tset,"linear");

		// fix first and last point
		tset(0) = 1e-9; tset.tail(1) = 1.0-1e-9;

		// check if sorted
		assert(tset.is_sorted("ascend"));

		// return time
		return tset.t();
	}


	// bezier function
	// general version
	void PathBezier::create_bezier(
		arma::Mat<fltp> &R,
		arma::Mat<fltp> &V,
		arma::Mat<fltp> &A,
		const arma::Row<fltp> &t, 
		const arma::Mat<fltp> &P){

		// get dimensions
		const arma::uword num_dim = P.n_rows;
		const arma::uword num_times = t.n_elem;
		const arma::uword nmax = P.n_cols-1; // the order of the spline
		const fltp spline_order = fltp(nmax);

		// allocate to zeros
		R.zeros(num_dim, num_times);
		V.zeros(num_dim, num_times);
		A.zeros(num_dim, num_times);

		// create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(int(nmax));

		// walk over source points
		for(arma::sword i=0;i<=arma::sword(nmax);i++){
			// calculate binomial coefficient
			fltp nk = facs(nmax)/(facs(i)*facs(nmax-i));

			// walk over dimensions
			for(arma::uword j=0;j<num_dim;j++){
				// calculate coordinates
				R.row(j) += nk*P(j,i)*arma::pow(RAT_CONST(1.0)-t,spline_order-i)%arma::pow(t,fltp(i));

				// calculate velocity (first derivative)
				V.row(j) += nk*P(j,i)*(spline_order*t-fltp(i))%arma::pow(RAT_CONST(1.0)-t,spline_order-fltp(i))%arma::pow(t,fltp(i)-1)/(t-RAT_CONST(1.0));

				// calculate acceleration (second derivative)
				A.row(j) += nk*P(j,i)*((-1+fltp(i))*fltp(i)*arma::pow(RAT_CONST(1.0)-t,-fltp(i)+spline_order)%arma::pow(t,-2+fltp(i)) - 
					2*i*(-fltp(i)+spline_order)*arma::pow(RAT_CONST(1.0)-t,-1-i+spline_order)%arma::pow(t,-1+fltp(i)) + 
					(-1-fltp(i)+spline_order)*(-fltp(i)+spline_order)*arma::pow(RAT_CONST(1.0)-t,-2-i+spline_order)%arma::pow(t,fltp(i)));
			}
		}

		// acceleration fix/cheat (zero acceleration gives weird results)
		const arma::Row<arma::uword> idx = arma::find(cmn::Extra::vec_norm(A)<RAT_CONST(1e-9)).t();
		if(!idx.is_empty())A.cols(idx) = A.cols(idx-1);
	}

	// bezier function with casteljau's algorithm
	// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/bezier-der.html
	arma::field<arma::Mat<fltp> > PathBezier::create_bezier_tn(
		const arma::Row<fltp> &t, const arma::Mat<fltp> &P, const arma::uword depth){

		 // get dimensions
		const arma::uword num_dim = P.n_rows;
		const arma::uword num_times = t.n_elem;
		const arma::uword n = P.n_cols-1; // the order of the spline

	    // create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(int(n));

		// allocate
		arma::field<arma::Mat<fltp> > C(depth);

		// store p for recursive calculation
		arma::Mat<fltp> D = P;

		// keep track of prefac
		fltp prefac = 1.0;

		// calculate coordinates
		for(arma::uword k=0;k<depth;k++){
			// allocate spline
			C(k).zeros(3,num_times);

			// walk over n
			for(arma::uword i=0;i<=n-k;i++){
				// calculate binomial coefficient
				const fltp nk = facs(n-k)/(facs(i)*facs(n-k-i));

				// calculate bezier function
				const arma::Row<fltp> B = nk*arma::pow(RAT_CONST(1.0)-t,n-k-fltp(i))%arma::pow(t,fltp(i));

				// walk over dimensions
				for(arma::uword j=0;j<num_dim;j++){
					// calculate coordinate or derivatives thereof
					C(k).row(j) += prefac*D(j,i)*B;
				}
			}
			
			// casteljau's algorithm	
			D = arma::diff(D,1,1);

			// update prefactor
			prefac *= (n-k);
		}

		// return the spline and its derivatives
		return C;
	}

}}