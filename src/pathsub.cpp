// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathsub.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathSub::PathSub(){

	}

	// default constructor
	PathSub::PathSub(ShPathPr base, const arma::uword idx1, const arma::uword idx2){
		set_base(base); set_idx(idx1,idx2);
	}

	// factory
	ShPathSubPr PathSub::create(){
		return std::make_shared<PathSub>();
	}

	// factory
	ShPathSubPr PathSub::create(ShPathPr base, const arma::uword idx1, const arma::uword idx2){
		return std::make_shared<PathSub>(base, idx1, idx2);
	}


	// set base 
	void PathSub::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base path points to zero");
		base_ = base;
	}

	// set indexes
	void PathSub::set_idx(const arma::uword idx1, const arma::uword idx2){
		idx1_ = idx1; idx2_ = idx2;
	}


	// get frame
	ShFramePr PathSub::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		if(base_==NULL)rat_throw_line("base path is not set");

		// create frame
		ShFramePr frame = base_->create_frame(stngs);

		// copy coordinates
		const arma::field<arma::Mat<fltp> > R = frame->get_coords();
		const arma::field<arma::Mat<fltp> > L = frame->get_direction();
		const arma::field<arma::Mat<fltp> > D = frame->get_transverse();
		const arma::field<arma::Mat<fltp> > N = frame->get_normal();
		const arma::field<arma::Mat<fltp> > B = frame->get_block();
		const arma::Row<arma::uword> section = frame->get_section();
		const arma::Row<arma::uword> turn = frame->get_turn();
		const arma::uword num_section_base = frame->get_num_section_base();

		// check indexes
		if(idx1_>idx2_)rat_throw_line("index1 must be smaller than index2");
		if(idx1_>=R.n_cols)rat_throw_line("index out of bounds");
		if(idx2_>=R.n_cols)rat_throw_line("index out of bounds");

		// create offset frame
		ShFramePr sub_frame = Frame::create(
			R.cols(idx1_,idx2_),L.cols(idx1_,idx2_),
			N.cols(idx1_,idx2_),D.cols(idx1_,idx2_),
			B.cols(idx1_,idx2_));

		// conserve location
		sub_frame->set_location(
			section.cols(idx1_,idx2_), 
			turn.cols(idx1_,idx2_), 
			num_section_base);

		// apply transformations
		sub_frame->apply_transformations(get_transformations());

		// create new frame
		return sub_frame;
	}

	// get type
	std::string PathSub::get_type(){
		return "rat::mdl::pathsub";
	}

	// method for serialization into json
	void PathSub::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		js["type"] = get_type();

		// properties
		js["base"] = cmn::Node::serialize_node(base_,list);
		js["idx1"] = (int)idx1_;
		js["idx2"] = (int)idx2_;
	}

	// method for deserialisation from json
	void PathSub::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		set_base(cmn::Node::deserialize_node<Path>(js["base"],list,factory_list,pth));
		set_idx(js["idx1"].asUInt64(), js["idx2"].asUInt64());
	}


}}