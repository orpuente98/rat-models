// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modeltoroid.hh"

#include "rat/common/parfor.hh"
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelToroid::ModelToroid(){
		set_name("toroid");
	}

	// constructor with setting of the base path
	ModelToroid::ModelToroid(const ShModelPr &base_model){
		add_model(base_model);
		set_name("toroid");
	}

	// factory
	ShModelToroidPr ModelToroid::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelToroid>();
	}

	// factory with setting of the base path
	ShModelToroidPr ModelToroid::create(const ShModelPr &base_model){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelToroid>(base_model);
	}
	
	// setting the number of coils
	void ModelToroid::set_num_coils(const arma::uword num_coils){
		num_coils_ = num_coils;
	}

	// setting the radius
	void ModelToroid::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set azymuthal angle
	void ModelToroid::set_phi(const fltp phi){
		phi_ = phi;
	}

	// getting the radius
	fltp ModelToroid::get_radius()const{
		return radius_;
	}

	// getting the number of coils
	arma::uword ModelToroid::get_num_coils()const{
		return num_coils_;
	}

	// set coil axis
	void ModelToroid::set_coil_azymuth(const char coil_azymuth){
		if(!cmn::Extra::is_xyz(coil_azymuth))
			rat_throw_line("coil azymuth not recognized");
		coil_azymuth_ = coil_azymuth;
	}

	// set coil axis
	void ModelToroid::set_toroid_axis(const char toroid_axis){
		if(!cmn::Extra::is_xyz(toroid_axis))
			rat_throw_line("toroid axis not recognized");
		toroid_axis_ = toroid_axis;
	}

	void ModelToroid::set_coil_radial(const char coil_radial){
		if(!cmn::Extra::is_xyz(coil_radial))
			rat_throw_line("coil radial not recognized");
		coil_radial_ = coil_radial;
	}
	
	void ModelToroid::set_toroid_radial(const char toroid_radial){
		if(!cmn::Extra::is_xyz(toroid_radial))
			rat_throw_line("toroid radial not recognized");
		toroid_radial_ = toroid_radial;
	}

	// set alternation
	void ModelToroid::set_alternate(const bool alternate){
		alternate_ = alternate;
	}
	
	// set alternation
	bool ModelToroid::get_alternate() const{
		return alternate_;
	}

	// get axus of coil
	char ModelToroid::get_coil_azymuth() const{
		return coil_azymuth_;
	}

	// get axis of toroid
	char ModelToroid::get_toroid_axis() const{
		return toroid_axis_;
	}

		// get axus of coil
	char ModelToroid::get_coil_radial() const{
		return coil_radial_;
	}

	// get axis of toroid
	char ModelToroid::get_toroid_radial() const{
		return toroid_radial_;
	}

	// get azymuthal angle
	fltp ModelToroid::get_phi()const{
		return phi_;
	}

	// splitting the modelgroup
	std::list<ShModelPr> ModelToroid::split(const ShSerializerPr &slzr) const{
		// create new model list
		std::list<ShModelPr> model_list;

		// orientation vectors for toroid and coil
		const arma::Col<fltp>::fixed<3> V1 = rat::cmn::Extra::unit_vec(coil_azymuth_);
		const arma::Col<fltp>::fixed<3> V2 = rat::cmn::Extra::unit_vec(coil_radial_);
		const arma::Col<fltp>::fixed<3> V3 = rat::cmn::Extra::unit_vec(toroid_axis_);
		const arma::Col<fltp>::fixed<3> V4 = rat::cmn::Extra::unit_vec(toroid_radial_);

		// toroid azymuthal vector
		const arma::Col<fltp>::fixed<3> V5 = rat::cmn::Extra::cross(V1,V2);

		// coil normal vector
		const arma::Col<fltp>::fixed<3> V6 = -rat::cmn::Extra::cross(V3,V4);

		// derive rotation vectors
		arma::Mat<fltp>::fixed<3,3> M = arma::join_horiz(V4,V3,V6)*arma::join_horiz(V2,V5,V1).t();

		// get rotation vector
		const arma::Col<fltp>::fixed<4> V = cmn::Extra::rotmatrix2angle(M);

		// walk over models
		const arma::Col<fltp>::fixed<3> origin = {0,0,0};
		for(arma::uword j=0;j<num_coils_;j++){
			for(auto it=models_.begin();it!=models_.end();it++){
				// create a copy
				ShModelPr model_copy = slzr->copy((*it).second);

				// alternation
				if(alternate_ && j%2==1)
					model_copy->add_transformation(TransReverse::create()); 

				// set tree closed
				model_copy->set_tree_open(false);

				// rotate and translate
				model_copy->add_transformation(
					TransRotate::create(origin,V.rows(0,2),V(3),radius_*V4));

				// rotate coil into position
				model_copy->add_transformation(TransRotate::create(
					V3,phi_+j*2*arma::Datum<fltp>::pi/num_coils_));

				// add copy of my transformations
				for(auto it2=trans_.begin();it2!=trans_.end();it2++)
					model_copy->add_transformation(slzr->copy((*it2).second));

				// expand name
				std::stringstream tname;
				tname << "Lm" << std::setfill('0') << 
					std::setw(2) << j << "_" << model_copy->get_name();
				model_copy->set_name(tname.str());

				// get model
				model_list.push_back(model_copy);
			}
		}

		// return list of models
		return model_list;
	}

	// mesh generation
	std::list<ShMeshDataPr> ModelToroid::create_meshes_core(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{
		// toroid without coils
		if(!is_valid(stngs.enable_throws))return{};

		// orientation vectors for toroid and coil
		const arma::Col<fltp>::fixed<3> V1 = rat::cmn::Extra::unit_vec(coil_azymuth_);
		const arma::Col<fltp>::fixed<3> V2 = rat::cmn::Extra::unit_vec(coil_radial_);
		const arma::Col<fltp>::fixed<3> V3 = rat::cmn::Extra::unit_vec(toroid_axis_);
		const arma::Col<fltp>::fixed<3> V4 = rat::cmn::Extra::unit_vec(toroid_radial_);

		// toroid azymuthal vector
		const arma::Col<fltp>::fixed<3> V5 = rat::cmn::Extra::cross(V1,V2);

		// coil normal vector
		const arma::Col<fltp>::fixed<3> V6 = -rat::cmn::Extra::cross(V3,V4);

		// derive rotation vectors
		arma::Mat<fltp>::fixed<3,3> M = arma::join_horiz(V4,V3,V6)*arma::join_horiz(V2,V5,V1).t();

		// get rotation vector
		const arma::Col<fltp>::fixed<4> V = cmn::Extra::rotmatrix2angle(M);

		// origin vector for rotation
		const arma::Col<fltp>::fixed<3> origin{0,0,0};

		// allocate temporary array of magnet signals
		arma::field<std::list<ShMeshDataPr> > limb_meshes(num_coils_);

		// walk over coils again and collect magnet signals
		cmn::parfor(0,num_coils_,parallel_setup_,[&](int i, int /*cpu*/){
		// for(arma::uword i=0;i<num_coils_;i++){
			// get meshes for limb
			limb_meshes(i) = ModelGroup::create_meshes_core(trace,stngs);

			// walk over each mesh
			for(auto it=limb_meshes(i).begin();it!=limb_meshes(i).end();it++){
				// alternation
				if(alternate_ && i%2==1)
					(*it)->apply_transformation(TransReverse::create()); 

				// rotate coil to toroid coordinate
				// system and translate radially
				(*it)->apply_transformation(
					TransRotate::create(origin,V.rows(0,2),V(3),radius_*V4));

				// rotate coil into position
				(*it)->apply_transformation(
					TransRotate::create(V3,phi_+i*2*arma::Datum<fltp>::pi/num_coils_));

				std::stringstream tname;
				tname << "Lm" << std::setfill('0') << std::setw(2) << i;

				// add name
				(*it)->append_name(tname.str());
			}
		});

		// collect all
		std::list<ShMeshDataPr> toroid_meshes;
		for(arma::uword i=0;i<num_coils_;i++)
			toroid_meshes.splice(toroid_meshes.end(), limb_meshes(i));

		// combine into single coilmesh and return
		return toroid_meshes;
	}

	// check validity
	bool ModelToroid::is_valid(const bool enable_throws) const{
		if(!ModelGroup::is_valid(enable_throws))return false;
		if(num_coils_<=0){if(enable_throws){rat_throw_line("number of limbs must be larger than zero");} return false;};
		if(!rat::cmn::Extra::is_xyz(coil_azymuth_)){if(enable_throws){rat_throw_line("coil azymuth must be x,y or z");} return false;};
		if(!rat::cmn::Extra::is_xyz(coil_radial_)){if(enable_throws){rat_throw_line("coil radial must be x,y or z");} return false;};
		if(!rat::cmn::Extra::is_xyz(toroid_axis_)){if(enable_throws){rat_throw_line("toroid axis must be x,y or z");} return false;};
		if(!rat::cmn::Extra::is_xyz(toroid_radial_)){if(enable_throws){rat_throw_line("toroid radial must be x,y or z");} return false;};
		if(coil_azymuth_==coil_radial_){if(enable_throws){rat_throw_line("coil azymuthal and coil radial axes can not be identical");} return false;};
		if(toroid_axis_==toroid_radial_){if(enable_throws){rat_throw_line("toroid azymuthal and toroid radial axes can not be identical");} return false;};
		return true;
	}

	// get type
	std::string ModelToroid::get_type(){
		return "rat::mdl::modeltoroid";
	}

	// method for serialization into json
	void ModelToroid::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		// Nameable::serialize(js,list);
		// Transformations::serialize(js,list);
		ModelGroup::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["alternate"] = alternate_;
		js["num_coils"] = (unsigned int)num_coils_;
		js["radius"] = radius_;
		js["coil_azymuth"] = coil_azymuth_;
		js["coil_radial"] = coil_radial_;
		js["toroid_axis"] = toroid_axis_;
		js["toroid_radial"] = toroid_radial_;
		js["phi"] = phi_;

		// subnodes
		// js["base"] = cmn::Node::serialize_node(base_model_, list);
	}

	// method for deserialisation from json
	void ModelToroid::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		// Nameable::deserialize(js,list,factory_list,pth);
		// Transformations::deserialize(js,list,factory_list,pth);
		ModelGroup::deserialize(js,list,factory_list,pth);

		// settings
		set_alternate(js["alternate"].asBool());
		set_num_coils(js["num_coils"].asUInt64());
		set_radius(js["radius"].asDouble());
		set_coil_azymuth(js["coil_azymuth"].asInt());
		set_coil_radial(js["coil_radial"].asInt());
		set_toroid_radial(js["toroid_radial"].asInt());
		set_toroid_axis(js["toroid_axis"].asInt());
		set_phi(js["phi"].asDouble());

		// subnodes
		// base_model_ = cmn::Node::deserialize_node<Model>(js["base"], list, factory_list, pth);
	}

}}