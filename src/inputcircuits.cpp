// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "inputcircuits.hh"

#include "coildata.hh"

// code specific to Rat
namespace rat{namespace mdl{


	// apply currents from circuits 
	void InputCircuits::apply_circuit_currents(
		const fltp time, 
		const std::list<ShMeshDataPr> &meshes) const{

		// sort circuits by their index
		std::map<arma::uword, ShCircuitPr> circuits;
		for(auto it=circuits_.begin();it!=circuits_.end();it++){
			const arma::uword circuit_id = (*it).second->get_circuit_id();
			circuits[circuit_id] = (*it).second;
		}

		// walk over meshes
		for(auto it=meshes.begin();it!=meshes.end();it++){
			const ShCoilDataPr coil = std::dynamic_pointer_cast<CoilData>(*it);

			// it is a coil
			if(coil!=NULL){
				// get circuit id from coil
				const arma::uword coil_circuit_id = coil->get_circuit_index();
				
				// exclude zero id
				if(coil_circuit_id!=0){
					const auto it2 = circuits.find(coil_circuit_id);
					if(it2!=circuits.end())
						coil->set_operating_current((*it2).second->get_current(time));
				}
			}
		}

	}

	// add model
	arma::uword InputCircuits::add_circuit(const ShCircuitPr &circuit){
		// check if already an index set otherwise 
		// assign the first id that is not used
		if(circuit->get_circuit_id()==0){
			for(arma::uword idx = 1;;idx++){
				bool id_exists = false;
				for(auto it=circuits_.begin();it!=circuits_.end();it++)
					if((*it).second->get_circuit_id()==idx)id_exists=true;
				if(id_exists)continue;
				circuit->set_circuit_id(idx);
				break;
			}
		}

		// add circuit
		const arma::uword index = circuits_.size()+1;
		circuits_.insert({index,circuit}); return index;
	}

	// retreive model at index
	ShCircuitPr InputCircuits::get_circuit(const arma::uword index) const{
		auto it = circuits_.find(index);
		if(it==circuits_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// get list of all models
	std::list<ShCircuitPr> InputCircuits::get_circuits() const{
		std::list<ShCircuitPr> model_list;
		for(auto it=circuits_.begin();it!=circuits_.end();it++)
			model_list.push_back((*it).second);
		return model_list;
	}

	// delete model at index
	bool InputCircuits::delete_circuit(const arma::uword index){	
		auto it = circuits_.find(index);
		if(it==circuits_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void InputCircuits::reindex(){
		std::map<arma::uword, ShCircuitPr> new_circuits; arma::uword idx = 1;
		for(auto it=circuits_.begin();it!=circuits_.end();it++)
			if((*it).second!=NULL)new_circuits.insert({idx++, (*it).second});
		circuits_ = new_circuits;
	}

	// get number of circuits
	arma::uword InputCircuits::num_circuits() const{
		return circuits_.size();
	}	

	// is valid
	bool InputCircuits::is_valid(const bool enable_throws) const{
		for(auto it=circuits_.begin();it!=circuits_.end();it++){
			const ShCircuitPr& circuit = (*it).second;
			if(circuit==NULL)rat_throw_line("the circuit list contains NULL");
			if(!circuit->is_valid(enable_throws))return false;
		}
		return true;
	}

	// get type
	std::string InputCircuits::get_type(){
		return "rat::mdl::inputcircuit";
	}

	// method for serialization into json
	void InputCircuits::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize the circuits
		for(auto it = circuits_.begin();it!=circuits_.end();it++)
			js["circuits"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void InputCircuits::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// deserialize the circuits
		for(auto it = js["circuits"].begin();it!=js["circuits"].end();it++)
			add_circuit(cmn::Node::deserialize_node<Circuit>((*it), list, factory_list, pth));
	}


}}
