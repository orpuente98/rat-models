// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcspharm.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcSpHarm::CalcSpHarm(){
		// name
		set_name("spharm"); 

		// settins
		stngs_->set_num_exp(7);
		stngs_->set_large_ilist(true);
	}

	CalcSpHarm::CalcSpHarm(
		const ShModelPr &model) : CalcSpHarm(){
		set_model(model);
	}

	CalcSpHarm::CalcSpHarm(
		const ShModelPr &model, 
		const fltp reference_radius, 
		const arma::uword nmax, 
		const arma::uword mmax,
		const arma::Col<fltp>::fixed<3> &R) : CalcSpHarm(model){
		set_reference_radius(reference_radius); 
		set_nmax(nmax); set_mmax(mmax); set_coordinate(R);
	}
	
	// factory methods
	ShCalcSpHarmPr CalcSpHarm::create(){
		return std::make_shared<CalcSpHarm>();
	}

	ShCalcSpHarmPr CalcSpHarm::create(
		const ShModelPr &model, 
		const fltp reference_radius, 
		const arma::uword nmax, 
		const arma::uword mmax,
		const arma::Col<fltp>::fixed<3> &R){
		return std::make_shared<CalcSpHarm>(model,reference_radius,nmax,mmax,R);
	}


	// set the reference radius
	void CalcSpHarm::set_reference_radius(const fltp reference_radius){
		reference_radius_ = reference_radius;
	}

	// set maximum order of harmonics calculated
	void CalcSpHarm::set_nmax(const arma::uword nmax){
		nmax_ = nmax;
	}

	// set number of points in azymuthal direction
	void CalcSpHarm::set_mmax(const arma::uword mmax){
		mmax_ = mmax;
	}

	void CalcSpHarm::set_coordinate(const arma::Col<fltp>::fixed<3> &R){
		R_ = R;
	}

	bool CalcSpHarm::get_visibility() const{
		return visibility_;
	}

	


	// get the reference radius
	fltp CalcSpHarm::get_reference_radius() const{
		return reference_radius_;
	}

	// get number of points in azymuthal direction 
	arma::uword CalcSpHarm::get_nmax() const{
		return nmax_;
	}

	// get maximum order of harmonics calculated
	arma::uword CalcSpHarm::get_mmax() const{
		return mmax_;
	}

	arma::Col<fltp>::fixed<3> CalcSpHarm::get_coordinate()const{
		return R_;
	}

	void CalcSpHarm::set_visibility(const bool visibility){
		visibility_ = visibility;
	}


	// calculate with inductance data output
	ShSpHarmDataPr CalcSpHarm::calculate_spherical_harmonics(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Spoherical Harmonics Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%s%sGENERAL INFO%s\n",KBLD,KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%s%sSETTING UP TARGET POINTS%s\n",KBLD,KGRN,KNRM);
		
		// display settings
		lg->msg(2,"%sSettings%s\n",KBLU,KNRM);
		lg->msg("reference radius: %s%2.2f [mm]%s\n",KYEL,1e3*reference_radius_,KNRM);
		lg->msg("nmax: %s%llu%s, mmax: %s%llu%s\n",KYEL,nmax_,KNRM,KYEL,mmax_,KNRM);
		lg->msg(-2,"\n");

		// create target points for harmonics calculation
		const ShSpHarmDataPr sphdata = SpHarmData::create(reference_radius_,nmax_,mmax_,R_);
		sphdata->set_time(time);
		
		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");

		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);

		// multipole method
		fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src, sphdata, stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// setup MLFMM and calculate required fields
		mlfmm->setup(lg); 
		mlfmm->calculate(lg);

		// display content
		lg->msg(2,"%s%sRESULT%s\n",KBLD,KGRN,KNRM);
		sphdata->display(lg); sphdata->display_ppm(lg);
		lg->msg(-2);

		// return the data
		return sphdata;
	}

	// generalized calculation
	std::list<ShDataPr> CalcSpHarm::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_spherical_harmonics(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcSpHarm::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{
		
		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// get points
		ShSpHarmDataPr spharm = SpHarmData::create(reference_radius_, nmax_, mmax_);
		spharm->setup();

		// get target coordinates
		arma::Mat<fltp> Rt = spharm->get_target_coords();

		// create point elements
		const arma::Row<arma::uword> n = arma::regspace<arma::Row<arma::uword> >(0,Rt.n_cols-1);
			
		// mesh data
		const ShMeshDataPr mesh_data = MeshData::create(Rt,n,n);

		// set time
		mesh_data->set_time(stngs.time);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();

		// mesh data object
		return {mesh_data};
	}

	// is valid
	bool CalcSpHarm::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		
		if(nmax_<=0){if(enable_throws){rat_throw_line("nmax must be larger than zero");} return false;};
		if(mmax_<=0){if(enable_throws){rat_throw_line("mmax must be larger than zero");} return false;};
		if(mmax_>nmax_){if(enable_throws){rat_throw_line("mmax must be smaller than or equal to nmax");} return false;};
		if(reference_radius_<=0){if(enable_throws){rat_throw_line("reference radius must be larger than zero");} return false;};
		return true;
	}

	// serialization
	std::string CalcSpHarm::get_type(){
		return "rat::mdl::calcspharm";
	}

	void CalcSpHarm::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		// InputPath::serialize(js,list);
		js["type"] = get_type();
		js["visibility"] = visibility_;
		js["reference_radius"] = reference_radius_;
		js["nmax"] = (unsigned int)nmax_;
		js["mmax"] = (unsigned int)mmax_;
		js["Rx"] = R_(0);
		js["Ry"] = R_(1);
		js["Rz"] = R_(2);
	}
	
	void CalcSpHarm::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
		// InputPath::deserialize(js,list,factory_list,pth);
		visibility_ = js["visibility"].asBool();
		reference_radius_ = js["reference_radius"].asDouble();
		nmax_ = js["nmax"].asUInt64();
		mmax_ = js["mmax"].asUInt64();
		R_(0) = js["Rx"].asDouble();
		R_(1) = js["Ry"].asDouble();
		R_(2) = js["Rz"].asDouble();
	}

}}