// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calclength.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLength::CalcLength(){
		set_name("length"); 
	}

	CalcLength::CalcLength(const ShModelPr &model) : CalcLength(){
		set_model(model);
	}

	// factory methods
	ShCalcLengthPr CalcLength::create(){
		return std::make_shared<CalcLength>();
	}

	ShCalcLengthPr CalcLength::create(const ShModelPr &model){
		return std::make_shared<CalcLength>(model);
	}

	// calculate with inductance data output
	ShLengthDataPr CalcLength::calculate_length(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// check if model set
		if(model_==NULL)rat_throw_line("model not set");

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;
		ShLengthDataPr ld = LengthData::create(model_->create_meshes({},mesh_settings));
		ld->display(lg);
		return ld;
	}

	// generalized calculation
	std::list<ShDataPr> CalcLength::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_length(time,lg)};
	}

	// is valid
	bool CalcLength::is_valid(const bool /*enable_throws*/) const{
		return true;
	}


	// serialization type
	std::string CalcLength::get_type(){
		return "rat::mdl::calclength";
	}

	// serialization
	void CalcLength::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
	}
		
	// deserialization
	void CalcLength::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
	}

}}