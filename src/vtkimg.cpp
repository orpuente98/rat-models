// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "vtkimg.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKImg::VTKImg(){
		vtk_igrid_ = vtkSmartPointer<vtkImageData>::New();
	}

	// factory
	ShVTKImgPr VTKImg::create(){
		return std::make_shared<VTKImg>();
	}

	// write mesh
	void VTKImg::set_dims(
		const fltp x1, const fltp x2, const arma::uword num_x,
		const fltp y1, const fltp y2, const arma::uword num_y,
		const fltp z1, const fltp z2, const arma::uword num_z){
		
		// calculate spacing
		fltp dx=0,dy=0,dz=0;
		if(num_x>1)dx = (x2-x1)/(num_x-1);
		if(num_y>1)dy = (y2-y1)/(num_y-1);
		if(num_z>1)dz = (z2-z1)/(num_z-1);

		// setup grid
		// vtkSmartPointer<vtkImageData> voxels = vtkImageData::New();
		vtk_igrid_->SetDimensions(num_x,num_y,num_z);
		vtk_igrid_->SetSpacing(dx,dy,dz);
		vtk_igrid_->SetOrigin(x1,y1,z1);
	}

	// write data at nodes
	void VTKImg::set_nodedata(const arma::Mat<fltp> &v, const std::string &data_name){
		// create vtk array
		vtkSmartPointer<vtkDoubleArray> vvtk = 
			vtkSmartPointer<vtkDoubleArray>::New();

		// add name to data
		vvtk->SetName(data_name.c_str());

		// allocate
		vvtk->SetNumberOfComponents(v.n_rows);
		vvtk->SetNumberOfValues(v.n_elem);
		
		// insert data elements
		for(arma::uword i=0;i<v.n_elem;i++)
			vvtk->SetValue(i,v(i));

		// set data to vtk grid
		vtk_igrid_->GetPointData()->AddArray(vvtk);
	}

	// get filename extension
	std::string VTKImg::get_filename_ext() const{
		return "vti";
	}

	// write output file
	void VTKImg::write(const boost::filesystem::path fname, cmn::ShLogPr lg){
		// create extended filename
		const std::string ext = get_filename_ext();
		boost::filesystem::path fname_ext = fname;
		fname_ext.replace_extension(ext);

		// display file settings and type
		// lg->msg(2,"%s%sWriting VTK File%s\n",KBLD,KGRN,KNRM);
		// lg->msg(2,"%sFile Settings%s\n",KBLU,KNRM);
		// lg->msg("filename: %s%s%s\n",KYEL,fname_ext.c_str(),KNRM);
		// lg->msg("type: image data (%s)\n",ext.c_str());

		// display file settings and type
		lg->msg(2,"%sVTK Image: %s%s\n",KBLU,get_name().c_str(),KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.string().c_str(),KNRM);
		lg->msg("type: unstructed grid (%s)\n",ext.c_str());

		// Write the file
		vtkSmartPointer<vtkXMLImageDataWriter> writer =  
			vtkSmartPointer<vtkXMLImageDataWriter>::New();
		writer->SetFileName(fname_ext.string().c_str());
		writer->SetInputData(vtk_igrid_);
		
		// write data
		writer->Write();

		// done writing VTK image data
		lg->msg(-2,"\n");
	}

}}