// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcGroup::CalcGroup(){
		set_name("calcgroup");
	}

	// create calc group with multiple calculations
	CalcGroup::CalcGroup(const std::list<ShCalcPr> &calculations) : CalcGroup(){
		for(auto it=calculations.begin();it!=calculations.end();it++)add_calculation(*it);
	}

	// factory
	ShCalcGroupPr CalcGroup::create(){
		return std::make_shared<CalcGroup>();
	}

	// factory
	ShCalcGroupPr CalcGroup::create(const std::list<ShCalcPr> &calculations){
		return std::make_shared<CalcGroup>(calculations);
	}

	// calculation function
	std::list<ShDataPr> CalcGroup::calculate(const fltp time, const cmn::ShLogPr &lg){
		// allocate list of data
		std::list<ShDataPr> data_list;

		// walk over different calculations
		for(auto it = calc_list_.begin();it!=calc_list_.end();it++)
			data_list.splice(data_list.end(), (*it).second->calculate(time,lg));

		// forward data
		return data_list;
	}

	// calculation meshes
	std::list<ShMeshDataPr> CalcGroup::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// get trace index
		std::list<arma::uword> next_trace = trace;
		if(!trace.empty()){next_trace.pop_front();}

		// allocate output list of meshes
		std::list<ShMeshDataPr> mesh_list;

		// walk over calculations
		for(auto it=calc_list_.begin();it!=calc_list_.end();it++){
			// check index
			if(!trace.empty())if(trace.front()!=(*it).first)continue;

			// create meshes
			std::list<ShMeshDataPr> meshes = (*it).second->create_meshes(next_trace,stngs);
			
			// set id
			for(auto it2=meshes.begin();it2!=meshes.end();it2++)
				(*it2)->append_trace_id((*it).first);

			// add to list
			mesh_list.splice(mesh_list.end(), meshes);
		}

		// return list of meshes
		return mesh_list;
	}

	// add calculation
	arma::uword CalcGroup::add_calculation(const ShCalcPr &calc){
		const arma::uword index = calc_list_.size()+1;
		calc_list_.insert({index,calc}); return index;
	}

	// add multiple calculations
	arma::Row<arma::uword> CalcGroup::add_calculations(const std::list<ShCalcPr> &calcs){
		arma::Row<arma::uword> indices(calcs.size()); arma::uword idx = 0;
		for(auto it = calcs.begin();it!=calcs.end();it++,idx++)indices(idx) = add_calculation(*it);
		return indices;
	}

	// retreive calculation at index
	ShCalcPr CalcGroup::get_calculation(const arma::uword index) const{
		auto it = calc_list_.find(index);
		if(it==calc_list_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}
	
	// get list of all calculations
	std::list<ShCalcPr> CalcGroup::get_calculations() const{
		std::list<ShCalcPr> calc_list;
		for(auto it=calc_list_.begin();it!=calc_list_.end();it++)
			calc_list.push_back((*it).second);
		return calc_list;
	}

	// delete calculation at index
	bool CalcGroup::delete_calculation(const arma::uword index){	
		auto it = calc_list_.find(index);
		if(it==calc_list_.end())return false;
		calc_list_.erase(it); return true;
	}

	// re-index nodes after deleting
	void CalcGroup::reindex(){
		std::map<arma::uword, ShCalcPr> new_calcs; arma::uword idx=1;
		for(auto it=calc_list_.begin();it!=calc_list_.end();it++,idx++)
			new_calcs.insert({idx, (*it).second});
		calc_list_ = new_calcs;
	}

	// get number of calculatoins
	arma::uword CalcGroup::num_calculations()const{
		return calc_list_.size();
	}

	// check validity
	bool CalcGroup::is_valid(const bool enable_throws) const{
		// check input calculations
		for(auto it = calc_list_.begin();it!=calc_list_.end();it++){
			const ShCalcPr &calc = (*it).second;
			if(calc==NULL)rat_throw_line("calcgroup list contains NULL");
			if(!calc->is_valid(enable_throws))return false;
		}

		// no problem found
		return true;
	}

	// serialization
	std::string CalcGroup::get_type(){
		return "rat::mdl::calcgroup";
	}

	// method for serialization into json
	void CalcGroup::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Calc::serialize(js,list);

		// properties
		js["type"] = get_type();
		for(auto it = calc_list_.begin();it!=calc_list_.end();it++)
			js["calc_list"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void CalcGroup::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Calc::deserialize(js,list,factory_list,pth);
		
		for(auto it = js["calc_list"].begin();it!=js["calc_list"].end();it++)
			add_calculation(cmn::Node::deserialize_node<Calc>(*it, list, factory_list, pth));
	}

}}
