// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelGroup::ModelGroup(){
		set_name("group");
		set_tree_open(true);
	}

	// constructor with model input
	ModelGroup::ModelGroup(const ShModelPr &model) : ModelGroup(){
		add_model(model);
	}

	// constructor with model input
	ModelGroup::ModelGroup(const std::list<ShModelPr> &models) : ModelGroup(){
		for(auto it=models.begin();it!=models.end();it++)add_model(*it);
	}

	// constructor with multiple input models
	// ModelGroup::ModelGroup(ShModelPrList models){
	// 	for(arma::uword i=0;i<models.n_elem;i+
	// 	models_ = models;
	// }

	// factory
	ShModelGroupPr ModelGroup::create(){
		return std::make_shared<ModelGroup>();
	}

	// factory
	ShModelGroupPr ModelGroup::create(const ShModelPr &model){
		return std::make_shared<ModelGroup>(model);
	}

	// factory
	ShModelGroupPr ModelGroup::create(const std::list<ShModelPr> &models){
		return std::make_shared<ModelGroup>(models);
	}

	// set parallel setup
	void ModelGroup::set_parallel_setup(const bool parallel_setup){
		parallel_setup_ = parallel_setup;
	}


	// splitting the modelgroup
	std::list<ShModelPr> ModelGroup::split(const ShSerializerPr &slzr) const{
		// create new model list
		std::list<ShModelPr> model_list;

		// walk over models
		for(auto it=models_.begin();it!=models_.end();it++){
			// create copy
			ShModelPr model_copy = slzr->copy((*it).second);

			// add my transformations
			for(auto it2=trans_.begin();it2!=trans_.end();it2++)
				model_copy->add_transformation(slzr->copy((*it2).second));

			// set tree closed
			model_copy->set_tree_open(false);

			// add model to list
			model_list.push_back(model_copy);
		}

		// return list of models
		return model_list;
	}

	// create meshes of stored coils
	std::list<ShMeshDataPr> ModelGroup::create_meshes_core(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// get trace index
		std::list<arma::uword> next_trace = trace;
		if(!trace.empty()){next_trace.pop_front();}

		// copy model list
		std::list<std::pair<arma::uword, ShModelPr> > models;

		// copy models that are in trace
		for(auto it = models_.begin();it!=models_.end();it++){
			if(!trace.empty())if(trace.front()!=(*it).first)continue;
			models.push_back((*it));
		}

		// create an armadillo field array is easier to index parallel
		arma::field<std::pair<arma::uword, ShModelPr> > mdl_list(models.size()); arma::uword cnt = 0;
		for(auto it=models.begin();it!=models.end();it++)mdl_list(cnt++) = (*it);

		// allocate output list
		arma::field<std::list<ShMeshDataPr> > mesh_list_fld(mdl_list.n_elem);

		// walk over models
		cmn::parfor(0,mdl_list.n_elem,parallel_setup_,[&](const arma::uword i, int /*cpu*/){
			// create meshes for this model
			mesh_list_fld(i) = mdl_list(i).second->create_meshes(next_trace, stngs);

			// walk over sub meshes
			for(auto it2=mesh_list_fld(i).begin();it2!=mesh_list_fld(i).end();it2++){
				// add identifier
				(*it2)->append_trace_id(mdl_list(i).first);
			}
		});

		// combine
		std::list<ShMeshDataPr> mesh_list;
		for(arma::uword i=0;i<mesh_list_fld.n_elem;i++)
			mesh_list.splice(mesh_list.end(), mesh_list_fld(i));

		// combine into single coilmesh and return
		return mesh_list;
	}

	// create a mesh of the stored coils
	std::list<ShMeshDataPr> ModelGroup::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// create meshes
		std::list<ShMeshDataPr> mesh_list = create_meshes_core(trace,stngs);

		// get transformation list
		const std::list<ShTransPr> trans_list = get_transformations();

		// walk over meshes
		for(auto it=mesh_list.begin();it!=mesh_list.end();it++){
			// apply transformations
			(*it)->apply_transformations(trans_list);
		}

		// combine into single coilmesh and return
		return mesh_list;
	}


	// check validity
	bool ModelGroup::is_valid(const bool enable_throws) const{
		// check parent
		if(!Transformations::is_valid(enable_throws))return false;

		// check input models
		for(auto it = models_.begin();it!=models_.end();it++){
			const ShModelPr &model = (*it).second;
			if(model==NULL)rat_throw_line("model list contains NULL");
			if(!model->is_valid(enable_throws))return false;
		}

		// no problem found
		return true;
	}


	// add model
	arma::uword ModelGroup::add_model(const ShModelPr &model){
		const arma::uword index = models_.size()+1;
		models_.insert({index,model}); return index;
	}


	// add model
	arma::Row<arma::uword> ModelGroup::add_models(const std::list<ShModelPr> &models){
		arma::Row<arma::uword> indices(models.size()); arma::uword idx = 0;
		for(auto it = models.begin();it!=models.end();it++,idx++)indices(idx) = add_model(*it);
		return indices;
	}


	// retreive model at index
	ShModelPr ModelGroup::get_model(const arma::uword index) const{
		auto it = models_.find(index);
		if(it==models_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}
	
	// get list of all models
	std::list<ShModelPr> ModelGroup::get_models() const{
		std::list<ShModelPr> model_list;
		for(auto it=models_.begin();it!=models_.end();it++)
			model_list.push_back((*it).second);
		return model_list;
	}

	// delete model at index
	bool ModelGroup::delete_model(const arma::uword index){	
		auto it = models_.find(index);
		if(it==models_.end())return false;
		(*it).second = NULL; return true;
	}

	// number of models
	arma::uword ModelGroup::num_models() const{
		return models_.size();
	}

	// re-index nodes after deleting
	void ModelGroup::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShModelPr> new_models; arma::uword idx = 1;
		for(auto it=models_.begin();it!=models_.end();it++)
			if((*it).second!=NULL)new_models.insert({idx++, (*it).second});
		models_ = new_models;
	}

	// get type
	std::string ModelGroup::get_type(){
		return "rat::mdl::modelgroup";
	}

	// method for serialization into json
	void ModelGroup::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Model::serialize(js,list);

		// type
		js["type"] = get_type();

		// parallel setup
		js["psetup"] = parallel_setup_;

		// serialize the models
		for(auto it = models_.begin();it!=models_.end();it++)
			js["models"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void ModelGroup::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Model::deserialize(js,list,factory_list,pth);

		// deserialize the models
		for(auto it = js["models"].begin();it!=js["models"].end();it++)
			add_model(cmn::Node::deserialize_node<Model>(
				(*it), list, factory_list, pth));

		// parallel setup
		parallel_setup_ = js["psetup"].asBool();
	}


}}
