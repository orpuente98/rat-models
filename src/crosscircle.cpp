// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crosscircle.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	CrossCircle::CrossCircle(){
		set_name("circle");
		set_radius(4e-3);
		set_element_size(1e-3);
	}

	// constructors
	CrossCircle::CrossCircle(const fltp radius, const fltp dl){
		set_radius(radius); set_element_size(dl);
		set_center(0,0);
		set_name("circle");
	}

	// constructors
	CrossCircle::CrossCircle(
		const fltp uc, const fltp vc,
		const fltp radius, const fltp dl){
		set_radius(radius); set_element_size(dl);
		set_center(uc,vc);
		set_name("circle");
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(){
		return std::make_shared<CrossCircle>();
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(const fltp radius, const fltp dl){
		return std::make_shared<CrossCircle>(radius,dl);
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(
		const fltp uc, const fltp vc,
		const fltp radius, const fltp dl){
		return std::make_shared<CrossCircle>(uc,vc,radius,dl);
	}

	// set radius
	void CrossCircle::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set element size
	void CrossCircle::set_element_size(const fltp dl){
		dl_ = dl;
	}

	// set center position u and v
	void CrossCircle::set_center(const fltp nc, const fltp tc){
		set_nc(nc); set_tc(tc);
	}

	// normal center coordinate
	void CrossCircle::set_nc(const fltp nc){
		nc_ = nc;
	}

	// transverse center coordinate
	void CrossCircle::set_tc(const fltp tc){
		tc_ = tc;
	}

	// set radius
	void CrossCircle::set_radial_vectors(const bool radial_vectors){
		radial_vectors_ = radial_vectors;
	}

	// get normal coordinate of center
	fltp CrossCircle::get_nc() const{
		return nc_;
	}

	// get transverse coordinate of center
	fltp CrossCircle::get_tc() const{
		return tc_;
	}

	// get circle radius
	fltp CrossCircle::get_radius() const{
		return radius_;
	}

	// get element size
	fltp CrossCircle::get_element_size() const{
		return dl_;
	}

	// get radial vectors
	bool CrossCircle::get_radial_vectors() const{
		return radial_vectors_;
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossCircle::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;
		bnd.col(0) = arma::Col<fltp>::fixed<2>{nc_-radius_, nc_+radius_};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{tc_-radius_, tc_+radius_};
		return bnd;
	}

	// create area data object
	ShAreaPr CrossCircle::create_area(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// calculate size of central rectangle
		const fltp drect = 2*radius_/3;

		// calculate number of elements
		arma::uword nrect = std::max(2,(int)std::ceil(2*arma::Datum<fltp>::pi*radius_/(4*dl_)));
		arma::uword nrad = std::max(2,(int)std::ceil((radius_-std::sqrt(2.0)*drect/2)/dl_));
		
		// limit number of elements 
		// when low poly requested
		if(stngs.low_poly){
			const fltp dl = 2*arma::Datum<fltp>::pi*radius_/32;
			nrect = std::min(nrect, (arma::uword)std::max(2,(int)std::ceil(2*arma::Datum<fltp>::pi*radius_/(4*dl))));
			nrad = std::min(nrad, (arma::uword)std::max(2,(int)std::ceil((radius_-std::sqrt(2.0)*drect/2)/dl)));
		}

		// create rectangle
		// allocate coordinates
		arma::Mat<fltp> ur(nrect+1,nrect+1);
		arma::Mat<fltp> vr(nrect+1,nrect+1);

		// set values
		ur.each_row() = arma::linspace<arma::Row<fltp> >(-drect/2,drect/2,nrect+1);
		vr.each_col() = arma::linspace<arma::Col<fltp> >(-drect/2,drect/2,nrect+1);

		// draw quarter circle
		const arma::Col<fltp> theta = arma::linspace<arma::Col<fltp> >(-arma::Datum<fltp>::pi/4,arma::Datum<fltp>::pi/4,nrect+1);
		const arma::Col<fltp> rho = arma::linspace<arma::Col<fltp> >(radius_,radius_,nrect+1);
		const arma::Col<fltp> uc = rho%arma::cos(theta); 
		const arma::Col<fltp> vc = rho%arma::sin(theta);

		// build right quadrant
		arma::Mat<fltp> u1(nrad+1,nrect+1);
		arma::Mat<fltp> v1(nrad+1,nrect+1);

		// fill in
		arma::Row<fltp> t = arma::linspace<arma::Row<fltp> >(0,1,nrad+1);
		for(arma::uword i=0;i<nrad+1;i++){
			u1.row(i) = ((1-t(i))*ur.col(nrect) + t(i)*uc).t();
			v1.row(i) = ((1-t(i))*vr.col(nrect) + t(i)*vc).t();
		}
		
		// build other quadrants by rotation
		arma::Mat<fltp> ucc(nrad+1,nrect*4);
		arma::Mat<fltp> vcc(nrad+1,nrect*4);
		for(arma::uword i=0;i<4;i++){
			// get matrix
			arma::Mat<fltp> u = u1.cols(0,nrect-1);
			arma::Mat<fltp> v = v1.cols(0,nrect-1);

			// convert to polar and rotate
			arma::Mat<fltp> thetac = arma::atan2(v,u) + i*arma::Datum<fltp>::pi/2;
			arma::Mat<fltp> rhoc = arma::sqrt(v%v + u%u);

			// convert to carthesian and store in matrix
			ucc.cols(i*nrect,(i+1)*nrect-1) = rhoc%arma::cos(thetac);
			vcc.cols(i*nrect,(i+1)*nrect-1) = rhoc%arma::sin(thetac);
		}

		// count number of nodes
		const arma::uword num_nodes_circle = (nrad+1)*nrect*4;

		// create matrix of node indices for the circular area
		arma::Mat<arma::uword> node_idx_circle = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes_circle-1);
		node_idx_circle.reshape(nrad+1,4*nrect);

		// connect start to end
		node_idx_circle = arma::join_horiz(node_idx_circle,node_idx_circle.col(0));

		// number of elements
		const arma::uword num_elements_circle = nrad*nrect*4;

		// create coordinates
		arma::Mat<fltp> Rc(2,num_nodes_circle);
		Rc.row(0) = arma::reshape(ucc,1,num_nodes_circle);
		Rc.row(1) = arma::reshape(vcc,1,num_nodes_circle);

		// generate internal element node indexes
		arma::Mat<arma::uword> nc(4,num_elements_circle);
		nc.row(0) = arma::reshape(node_idx_circle.submat(0, 0, nrad-1, 4*nrect-1), 1, num_elements_circle);
		nc.row(1) = arma::reshape(node_idx_circle.submat(1, 0, nrad, 4*nrect-1), 1, num_elements_circle);
		nc.row(2) = arma::reshape(node_idx_circle.submat(1, 1, nrad, 4*nrect), 1, num_elements_circle);
		nc.row(3) = arma::reshape(node_idx_circle.submat(0, 1, nrad-1, 4*nrect), 1, num_elements_circle);

		// count number of nodes
		const arma::uword num_nodes_rectangle = (nrect-1)*(nrect-1);

		// now create rectangle (only select central nodes as edges are part of circle)
		arma::Mat<fltp> Rr(2,num_nodes_rectangle);
		Rr.row(0) = arma::reshape(ur.submat(1,1,nrect-1,nrect-1),1,num_nodes_rectangle);
		Rr.row(1) = arma::reshape(vr.submat(1,1,nrect-1,nrect-1),1,num_nodes_rectangle);

		// create matrix of node indices for the rectangular area
		arma::Mat<arma::uword> node_idx_rect = num_nodes_circle + 
			arma::regspace<arma::Mat<arma::uword> >(0,(nrect-1)*(nrect-1)-1);
		node_idx_rect.reshape(nrect-1,nrect-1);

		// expand rectangle
		arma::Mat<arma::uword> node_idx_rect_ext(nrect+1,nrect+1,arma::fill::zeros);
		node_idx_rect_ext.submat(1,1,nrect-1,nrect-1) = node_idx_rect;
		
		// connect edges
		node_idx_rect_ext.col(nrect) = node_idx_circle.submat(0,0,0,nrect).t();
		node_idx_rect_ext.row(nrect) = arma::fliplr(node_idx_circle.submat(0,1*nrect,0,2*nrect));
		node_idx_rect_ext.col(0) = arma::flipud(node_idx_circle.submat(0,2*nrect,0,3*nrect).t());
		node_idx_rect_ext.row(0) = node_idx_circle.submat(0,3*nrect,0,4*nrect);
		
		// calculate number of elements in rectangle
		arma::uword num_elements_rectangle = nrect*nrect;

		// generate internal element node indexes
		arma::Mat<arma::uword> nr(4,num_elements_rectangle);
		nr.row(0) = arma::reshape(node_idx_rect_ext.submat(0, 0, nrect-1, nrect-1), 1, num_elements_rectangle);
		nr.row(1) = arma::reshape(node_idx_rect_ext.submat(1, 0, nrect, nrect-1), 1, num_elements_rectangle);
		nr.row(2) = arma::reshape(node_idx_rect_ext.submat(1, 1, nrect, nrect), 1, num_elements_rectangle);
		nr.row(3) = arma::reshape(node_idx_rect_ext.submat(0, 1, nrect-1, nrect), 1, num_elements_rectangle);

		// connect meshes
		arma::Mat<arma::uword> n = arma::join_horiz(nc,arma::flipud(nr));
		arma::Mat<fltp> Rn = arma::join_horiz(Rc,Rr);
		
		// get orientation
		arma::Mat<fltp> N, D; 
		if(radial_vectors_){
			N = Rn.each_row()/cmn::Extra::vec_norm(Rn);
			D = arma::flipud(Rn).eval().each_row()/cmn::Extra::vec_norm(Rn);
			D.row(0) *= -1;
		}else{
			N.zeros(2,Rn.n_cols); N.row(0).fill(RAT_CONST(1.0));
			D.zeros(2,Rn.n_cols); D.row(1).fill(RAT_CONST(1.0));
		}

		// apply center position offset
		Rn.row(0) += nc_; Rn.row(1) += tc_;
		
		// count total number of nodes and elements
		// const arma::uword num_nodes = num_nodes_rectangle + num_nodes_circle;
		// const arma::uword num_elements = num_elements_rectangle + num_elements_circle;

		// flip mesh
		n = arma::flipud(n);

		// circle area
		ShAreaPr circle_area = Area::create(Rn,N,D,n);

		// mesh smoothing
		circle_area->smoothen(num_smooth_iter_,smooth_dampen_);

		// check areas and periphery within percent of perfect circle
		// assert(std::abs(carea->calculate_area()-arma::Datum<fltp>::pi*radius_*radius_)/(arma::Datum<fltp>::pi*radius_*radius_)<5e-2);
		// assert(std::abs(carea->calculate_periphery()-2*arma::Datum<fltp>::pi*radius_)/(2*arma::Datum<fltp>::pi*radius_)<5e-2);

		// return positive
		return circle_area;
	}

	// surface mesh
	ShPerimeterPr CrossCircle::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area(MeshSettings());

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	// validity check
	bool CrossCircle::is_valid(const bool enable_throws) const{
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(dl_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(dl_<=RAT_CONST(1e-2)*radius_){if(enable_throws){rat_throw_line("element size too small");} return false;};
		return true;
	}

	// get type
	std::string CrossCircle::get_type(){
		return "rat::mdl::crosscircle";
	}

	// method for serialization into json
	void CrossCircle::serialize(Json::Value &js, cmn::SList &list) const{
		Cross::serialize(js,list);
		js["type"] = get_type();
		js["radial_vectors"] = radial_vectors_;
		js["radius"] = radius_;
		js["nc"] = nc_; js["tc"] = tc_; js["dl"] = dl_;
	}

	// method for deserialisation from json
	void CrossCircle::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Cross::deserialize(js,list,factory_list,pth);
		set_radius(js["radius"].asDouble());
		set_center(js["nc"].asDouble(),js["tc"].asDouble());
		set_element_size(js["dl"].asDouble());
		set_radial_vectors(js["radial_vectors"].asBool());
	}

}}