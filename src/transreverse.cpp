// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transreverse.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	TransReverse::TransReverse(){
		set_name("reverse");
	}

	// factory methods
	ShTransReversePr TransReverse::create(){
		return std::make_shared<TransReverse>();
	}

	// apply to coordinates only
	void TransReverse::apply_coords(arma::Mat<fltp> &R) const{
		R = arma::fliplr(R);
	}

	// apply reversion
	void TransReverse::apply_vectors(const arma::Mat<fltp> &, arma::Mat<fltp> &V, const char str) const{
		// invert 
		V = arma::fliplr(V);

		// inverse direction
		if(str=='L' || str=='D')V *= -1;
	}

	// apply reversion to mesh elements
	void TransReverse::apply_mesh(arma::Mat<arma::uword> &n, const arma::uword num_nodes) const{
		// swap faces
		for(arma::uword i=0;i<n.n_rows/2;i++)
			n.swap_rows(i,n.n_rows-i-1);

		// when swapping the sides of a surface the face 
		// is inverted. Here we invert it back by swapping
		// the other two edges
		if(n.n_rows==4){
			n.swap_rows(0,1);
			n.swap_rows(2,3);
		}

		// change node indices to reflect the
		// reordering of the indices in coords
		n = num_nodes - n - 1;
	}

	// apply to vectors for field arrays
	void TransReverse::apply_coords(
		arma::field<arma::Mat<fltp> > &R) const{
		// reverse everything
		cmn::Extra::reverse_field(R);
	}


	// apply to vectors for field arrays
	void TransReverse::apply_vectors(
		const arma::field<arma::Mat<fltp> > &, 
		arma::field<arma::Mat<fltp> > &V, const char str) const{
		// reverse everything
		cmn::Extra::reverse_field(V);

		// inverse direction
		if(str=='L' || str=='D')
			for(arma::uword i=0;i<V.n_elem;i++)
				V(i) *= -1;
	}

	// get type
	std::string TransReverse::get_type(){
		return "rat::mdl::transreverse";
	}

	// method for serialization into json
	void TransReverse::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void TransReverse::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
	}

}}