/* Raccoon - An Electro-Magnetic and Thermal network 
solver accelerated with the Multi-Level Fast Multipole Method
Copyright (C) 2020  Jeroen van Nugteren*/

// include header
#include "serializer.hh"

// serializable objects
#include "rat/mlfmm/settings.hh"
#include "crosscircle.hh"
#include "crossdmsh.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "crossline.hh"
#include "crosspath.hh"
#include "modelcoil.hh"
#include "modelmesh.hh"
#include "modeltoroid.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "modelbar.hh"
#include "modelimport.hh"
#include "modelmgnsphere.hh"
#include "pathaxis.hh"
#include "pathcable.hh"
#include "pathcircle.hh"
#include "pathclover.hh"
#include "pathrectangle.hh"
#include "pathflared.hh"
#include "pathdshape.hh"
#include "patharc.hh"
#include "pathcct.hh"
#include "pathcctcustom.hh"
#include "pathoffset.hh"
#include "pathsub.hh"
#include "pathplasma.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "pathpolygon.hh"
#include "pathspiral.hh"
#include "transbend.hh"
#include "transreflect.hh"
#include "transflip.hh"
#include "transreverse.hh"
#include "transrotate.hh"
#include "transtranslate.hh"
#include "driveac.hh"
#include "drivedc.hh"
#include "drivetrapz.hh"
#include "drivestep.hh"
#include "driveinterp.hh"
#include "drivelinear.hh"
#include "calcinductance.hh"
#include "calcline.hh"
#include "calclength.hh"
#include "calcpath.hh"
#include "calcgroup.hh"
#include "calcplane.hh"
#include "calcgrid.hh"
#include "calcpoints.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "calcspharm.hh"
#include "calctracks.hh"
#include "emitterbeam.hh"
#include "emittercoordscan.hh"
#include "emitteromni.hh"
#include "modelsolenoid.hh"
#include "modelracetrack.hh"
#include "modelrectangle.hh"
#include "modeltrapezoid.hh"
#include "modelcct.hh"
#include "modeldshape.hh"
#include "modelstick.hh"
#include "modelmirror.hh"
#include "modelroot.hh"
#include "modelsphere.hh"
#include "modelflared.hh"
#include "modelclover.hh"
#include "area.hh"
#include "frame.hh"
#include "driveptrain.hh"
#include "pathfrenet.hh"
#include "circuit.hh"
#include "background.hh"
#include "modelbarcube.hh"
#include "modelring.hh"
#include "modelline.hh"


// code specific to Raccoon
namespace rat{namespace mdl{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(const ShModelPr &model){
		flatten_tree(model); register_constructors();
	}

	// constructor
	Serializer::Serializer(const boost::filesystem::path &fname){
		import_json(fname); register_constructors();
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(const ShModelPr &model){
		return std::make_shared<Serializer>(model);
	}

	// factory
	ShSerializerPr Serializer::create(const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// factories from MLFMM
		register_factory<fmm::Settings>();

		// cross sections
		register_factory<CrossCircle>();
		register_factory<CrossDMsh>();
		register_factory<CrossRectangle>();
		register_factory<CrossPoint>();
		register_factory<CrossLine>();
		register_factory<CrossPath>();

		// model nodes
		register_factory<ModelArray>();
		register_factory<ModelCoil>();
		register_factory<ModelMesh>();
		register_factory<ModelToroid>();
		register_factory<ModelGroup>();
		register_factory<ModelSolenoid>();
		register_factory<ModelRacetrack>();
		register_factory<ModelRectangle>();
		register_factory<ModelTrapezoid>();
		register_factory<ModelRoot>();
		register_factory<ModelBar>();
		register_factory<ModelMirror>();
		register_factory<ModelStick>();
		register_factory<ModelImport>();
		register_factory<ModelSphere>();
		register_factory<ModelMgnSphere>();
		register_factory<ModelDShape>();
		register_factory<ModelFlared>();
		register_factory<ModelClover>();
		register_factory<ModelCCT>();
		register_factory<ModelBarCube>();
		register_factory<ModelRing>();
		register_factory<ModelLine>();


		// path nodes
		register_factory<PathAxis>();
		register_factory<PathCable>();
		register_factory<PathCircle>();
		register_factory<PathClover>();
		register_factory<PathRectangle>();
		register_factory<PathFlared>();
		register_factory<PathDShape>();
		register_factory<PathArc>();
		register_factory<PathCCT>();
		register_factory<PathCCTCustom>();
		register_factory<CCTHarmonicDrive>(); 
		register_factory<PathOffset>(); 
		register_factory<PathSub>(); 
		register_factory<PathPlasma>(); 
		register_factory<PathStraight>(); 
		register_factory<PathGroup>(); 
		register_factory<PathFrenet>(); 
		register_factory<PathPolygon>(); 
		register_factory<PathSpiral>(); 
		
		register_factory<CCTHarmonicInterp>();

		// transformations
		register_factory<Transformations>();
		register_factory<TransBend>();
		register_factory<TransFlip>();
		register_factory<TransReflect>();
		register_factory<TransReverse>();
		register_factory<TransRotate>();
		register_factory<TransTranslate>();

		// drives
		register_factory<DriveAC>();
		register_factory<DriveDC>();
		register_factory<DriveTrapz>();
		register_factory<DriveStep>();
		register_factory<DriveInterp>();
		register_factory<DrivePTrain>();
		register_factory<DriveLinear>();

		// calculation
		register_factory<CalcInductance>();
		register_factory<CalcGroup>();
		register_factory<CalcLine>();
		register_factory<CalcPath>();
		register_factory<CalcPlane>();
		register_factory<CalcGrid>();
		register_factory<CalcMesh>();
		register_factory<CalcLength>();
		register_factory<CalcHarmonics>();
		register_factory<CalcSpHarm>();
		register_factory<CalcPoints>();
		register_factory<CalcTracks>();
		register_factory<Circuit>();
		register_factory<Background>();

		// emitters
		register_factory<EmitterBeam>();
		register_factory<EmitterOmni>();
		register_factory<EmitterCoordScan>();
	}

}}