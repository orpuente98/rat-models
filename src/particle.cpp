// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "particle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// create particle
	Particle::Particle(){

	}

	// set mass
	void Particle::set_rest_mass(const fltp rest_mass){
		rest_mass_ = rest_mass;
	}

	// set charge
	void Particle::set_charge(const fltp charge){
		charge_ = charge;
	}

	// get mass
	fltp Particle::get_rest_mass() const{
		return rest_mass_;
	}

	// get charge
	fltp Particle::get_charge() const{
		return charge_;
	}

	// set number of steps
	void Particle::set_num_steps(const arma::uword num_steps){
		if(num_steps<=0)rat_throw_line("number of steps must be larger than zero");
		num_steps_ = num_steps;
	}

	// set start index
	void Particle::set_start_index(const arma::uword start_idx){
		// start index
		idx_max_ = start_idx; idx_min_ = start_idx;
	}

	// set start coordinate
	void Particle::set_startcoord(
		const arma::Col<fltp>::fixed<3> &R0, 
		const arma::Col<fltp>::fixed<3> &V0){
		R0_ = R0; V0_ = V0;
	}

	// allocate
	void Particle::setup(){
		// check input
		if(idx_min_!=idx_max_)rat_throw_line("track must start and end at same index");
		if(idx_max_>num_steps_-1)rat_throw_line("start index exceeds number of steps");
		if(num_steps_==0)rat_throw_line("must have at least one step");

		// allocate storage
		Rt_.set_size(3,num_steps_); Vt_.set_size(3,num_steps_); t_.set_size(num_steps_);
		//Lt_.set_size(3,num_steps_); Nt_.set_size(3,num_steps_); Dt_.set_size(3,num_steps_);
		
		// set start point and velocity
		Rt_.col(idx_max_) = R0_; Vt_.col(idx_max_) = V0_; t_(idx_max_) = 0;
		
		// set allive
		alive_start_ = true; alive_end_ = true;
		if(idx_min_==0)alive_start_ = false;
		if(idx_max_==num_steps_-1)alive_end_ = false;
	}

	// check if the track start is alive
	bool Particle::is_alive_start() const{
		return alive_start_;
	}

	// check if the track end is alive
	bool Particle::is_alive_end() const{
		return alive_end_;
	}

	// get coordinate of track start
	arma::Col<fltp>::fixed<3> Particle::get_first_coord() const{
		return Rt_.col(idx_min_);
	}

	// get velocity of track start
	arma::Col<fltp>::fixed<3> Particle::get_first_velocity() const{
		return Vt_.col(idx_min_);
	}

	// get time
	fltp Particle::get_first_time() const{
		return t_(idx_min_);
	}

	// get coordinate of track end
	arma::Col<fltp>::fixed<3> Particle::get_last_coord() const{
		return Rt_.col(idx_max_);
	}

	// get velocity of track end 
	arma::Col<fltp>::fixed<3> Particle::get_last_velocity() const{
		return Vt_.col(idx_max_);
	}

	// get time
	fltp Particle::get_last_time() const{
		return t_(idx_max_);
	}

	// set coordinates
	void Particle::set_prev_coord(
		const fltp tp, 
		const arma::Col<fltp>::fixed<3> &Rp, 
		const arma::Col<fltp>::fixed<3> &Vp){
		// check if there is still space
		if(idx_min_==0)rat_throw_line("zero index reached (dead)");
		
		// update index
		idx_min_--;

		// store data at new index
		t_(idx_min_) = tp; 
		Rt_.col(idx_min_) = Rp; 
		Vt_.col(idx_min_) = Vp; 

		// check alive
		if(idx_min_==0)alive_start_ = false;
	}

	// set coordinates
	void Particle::set_next_coord(
		const fltp tp, 
		const arma::Col<fltp>::fixed<3> &Rp, 
		const arma::Col<fltp>::fixed<3> &Vp){
		// check if there is space left
		if(idx_max_==num_steps_-1)rat_throw_line("max index reached (dead)");

		// update index
		idx_max_++;

		// store data at new index
		t_(idx_max_) = tp; 
		Rt_.col(idx_max_) = Rp; 
		Vt_.col(idx_max_) = Vp;

		// check alive
		if(idx_max_==num_steps_-1)alive_end_ = false;
	}

	// terminate end
	void Particle::terminate_end(){
		alive_end_ = false;
	}

	// terminate start
	void Particle::terminate_start(){
		alive_start_ = false;
	}

	// get all track coordinates
	arma::Mat<fltp> Particle::get_track_coords() const{
		return Rt_.cols(idx_min_,idx_max_);
	}

	// get all track velocities
	arma::Mat<fltp> Particle::get_track_velocities() const{
		return Vt_.cols(idx_min_,idx_max_);
	}

	arma::uword Particle::get_num_coords(){
		return idx_max_ - idx_min_ + 1;
	}

	// set magnetic field on track
	void Particle::set_magnetic_field(const arma::Mat<fltp> &Bt){
		Bt_ = Bt;
	}

	// get magnetic field
	arma::Mat<fltp> Particle::get_magnetic_field()const{
		return Bt_;
	}

}}
