// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "driveinterp.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveInterp::DriveInterp(){

	}

	// constructor
	DriveInterp::DriveInterp(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi){
		set_interpolation_table(ti,vi);
	}

	// factory
	ShDriveInterpPr DriveInterp::create(){
		return std::make_shared<DriveInterp>();
	}

	// factory
	ShDriveInterpPr DriveInterp::create(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi){
		return std::make_shared<DriveInterp>(ti,vi);
	}


	// set and get current
	void DriveInterp::set_interpolation_table(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi){
		if(ti.n_elem!=vi.n_elem)rat_throw_line("number of elements must be the same");
		ti_ = ti; vi_ = vi;
	}

	// get times
	void DriveInterp::set_interpolation_times(const arma::Col<fltp> &ti){
		ti_ = ti;
	}

	// get values
	void DriveInterp::set_interpolation_values(const arma::Col<fltp> &vi){
		vi_ = vi;
	}

	// get times
	arma::Col<fltp> DriveInterp::get_interpolation_times()const{
		return ti_;
	}

	// get values
	arma::Col<fltp> DriveInterp::get_interpolation_values()const{
		return vi_;
	}

	// get number of times
	arma::uword DriveInterp::get_num_times()const{
		return ti_.n_elem;
	}

	// get drive scaling
	fltp DriveInterp::get_scaling(const fltp time) const{
		assert(!ti_.is_empty()); assert(!vi_.is_empty());
		if(time<=ti_.front())return vi_.front();
		if(time>=ti_.back())return vi_.back();
		const arma::uword idx = arma::as_scalar(arma::find(ti_.head_rows(ti_.n_elem-1)<time && ti_.tail_rows(ti_.n_elem-1)>=time));
		return vi_(idx) + (time - ti_(idx))*((vi_(idx+1)-vi_(idx))/(ti_(idx+1)-ti_(idx)));
		// return cmn::Extra::interp1(ti_,vi_,time,"linear",true);
	}

	// get drive scaling
	// arma::Col<fltp> DriveInterp::get_scaling(const arma::Col<fltp> &times) const{
	// 	// use scaler drive scaling
	// 	arma::Col<fltp> v(times.n_elem);
	// 	for(arma::uword i=0;i<times.n_elem;i++)
	// 		v(i) = get_scaling(times(i));
	// 	return v;
	// }

	// get current derivative
	fltp DriveInterp::get_dscaling(const fltp time) const{
		// find index
		assert(!ti_.is_empty()); assert(!vi_.is_empty());
		if(time<=ti_.front() || time>=ti_.back())return 0;
		const arma::uword idx = arma::as_scalar(arma::find(ti_.head_rows(ti_.n_elem-1)<time && ti_.tail_rows(ti_.n_elem-1)>=time));
		return (vi_(idx+1)-vi_(idx))/(ti_(idx+1)-ti_(idx));
	}

	// get times at which an inflection occurs
	arma::Col<fltp> DriveInterp::get_inflection_times() const{
		// check if there are inflection points
		if(ti_.n_elem>2){
			arma::Col<fltp> times =  ti_.rows(1,ti_.n_elem-2);
			times = times(arma::find(arma::join_vert(arma::Col<arma::uword>{1},
				(times.tail_rows(times.n_elem-1)-
				times.head_rows(times.n_elem-1))>1e-8)));
			return times;
		}
		else return arma::Col<fltp>{};
	}

	// get type
	std::string DriveInterp::get_type(){
		return "rat::mdl::driveinterp";
	}

	// method for serialization into json
	void DriveInterp::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		
		// walk over timesteps and export interpolation table
		for(arma::uword i=0;i<ti_.n_elem;i++){
			js["ti"].append(ti_(i)); js["vi"].append(vi_(i));
		}
	}

	// method for deserialisation from json
	void DriveInterp::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		// const arma::uword num_times = js["ti"].size();
		// ti_.set_size(num_times); vi_.set_size(num_times);
		// for(arma::uword i=0;i<num_times;i++){
		// 	ti_(i) = js["ti"].get(i,0).asDouble(); 
		// 	vi_(i) = js["vi"].get(i,0).asDouble();
		// }

		ti_.set_size(js["ti"].size()); vi_.set_size(js["vi"].size()); arma::uword i;
		if(ti_.n_elem!=vi_.n_elem)rat_throw_line("interpolation arrays do not have same size");
		i = 0; for(auto it = js["ti"].begin(); it!=js["ti"].end();it++,i++)ti_(i) = (*it).asDouble();
		i = 0; for(auto it = js["vi"].begin(); it!=js["vi"].end();it++,i++)vi_(i) = (*it).asDouble();

	}

}}