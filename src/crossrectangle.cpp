// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crossrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossRectangle::CrossRectangle(){
		set_dim_normal(RAT_CONST(0.0), RAT_CONST(1e-2), RAT_CONST(2e-3));
		set_dim_transverse(RAT_CONST(-0.5e-2), RAT_CONST(0.5e-2), RAT_CONST(2e-3));
		set_name("rectangle");
	}

	// constructor with input
	CrossRectangle::CrossRectangle(
		const fltp n1, const fltp n2, 
		const fltp t1, const fltp t2, 
		const fltp dl){

		// set to self
		set_name("rectangle");
		set_dim_normal(n1,n2,dl);
		set_dim_transverse(t1,t2,dl);
	}

	// constructor with input
	CrossRectangle::CrossRectangle(
		const fltp n1, const fltp n2, 
		const fltp t1, const fltp t2, 
		const fltp dn, const fltp dt){

		// set to self
		set_name("rectangle");
		set_dim_normal(n1,n2,dn);
		set_dim_transverse(t1,t2,dt);
	}

	// constructor with input
	CrossRectangle::CrossRectangle(
		const fltp n1, const fltp n2, 
		const fltp t1, const fltp t2, 
		const arma::uword num_n, 
		const arma::uword num_t){

		// set to self
		set_name("rectangle");
		set_dim_normal(n1,n2,num_n);
		set_dim_transverse(t1,t2,num_t);
	}

	// factory
	ShCrossRectanglePr CrossRectangle::create(){
		return std::make_shared<CrossRectangle>();
	}

	// factory with dimension input
	ShCrossRectanglePr CrossRectangle::create(
		const fltp n1, const fltp n2, 
		const fltp t1, const fltp t2, 
		const fltp dl){
		return std::make_shared<CrossRectangle>(n1,n2,t1,t2,dl);
	}

	// factory with dimension input
	ShCrossRectanglePr CrossRectangle::create(
		const fltp n1, const fltp n2, 
		const fltp t1, const fltp t2, 
		const fltp dn, const fltp dt){
		return std::make_shared<CrossRectangle>(n1,n2,t1,t2,dn,dt);
	}

	// factory with dimension input
	ShCrossRectanglePr CrossRectangle::create(
		const fltp n1, const fltp n2, 
		const fltp t1, const fltp t2, 
		const arma::uword num_n, 
		const arma::uword num_t){
		return std::make_shared<CrossRectangle>(n1,n2,t1,t2,num_n,num_t);
	}

	// set dimensions in the normal direction
	void CrossRectangle::set_dim_normal(
		const fltp n1, const fltp n2, const arma::uword num_n){

		// check user input
		if(n1>=n2)rat_throw_line("inner must be smaller than outer dimension");
		if(num_n<=0)rat_throw_line("number of elements must be larger than zero");

		// set to self
		set_n1(n1); set_n2(n2); set_dn((n2_-n1_)/num_n);
	}

	// set dimensions in the normal direction
	void CrossRectangle::set_dim_normal(
		const fltp n1, const fltp n2, const fltp dn){

		// check element size
		if(n1>=n2)rat_throw_line("inner must be smaller than outer dimension");
		if(dn<=0)rat_throw_line("element size must be larger than zero");

		
		// set to self
		set_n1(n1); set_n2(n2); set_dn(dn);
	}

	// set dimensions in the transverse direction
	void CrossRectangle::set_dim_transverse(
		const fltp t1, const fltp t2, const arma::uword num_t){

		// check user input
		if(t1>=t2)rat_throw_line("inner must be smaller than outer dimension");
		if(num_t<=0)rat_throw_line("number of elements must be larger than zero");

		// set to self
		set_t1(t1); set_t2(t2); set_dt((t2_-t1_)/num_t);
	}

	// set dimensions in the transverse direction
	void CrossRectangle::set_dim_transverse(
		const fltp t1, const fltp t2, const fltp dt){

		// check element size
		if(t1>=t2)rat_throw_line("inner must be smaller than outer dimension");
		if(dt<=0)rat_throw_line("element size must be larger than zero");

		// set to self
		set_t1(t1); set_t2(t2); set_dt(dt);
	}

	// set normal first position
	void CrossRectangle::set_n1(const fltp n1){
		n1_ = n1;
	}

	// set normal second position
	void CrossRectangle::set_n2(const fltp n2){
		n2_ = n2;
	}

	// element size in normal direction
	void CrossRectangle::set_dn(const fltp dn){
		dn_ = dn;
	}

	// set transverse first position
	void CrossRectangle::set_t1(const fltp t1){
		t1_ = t1;
	}

	// set transverse second position
	void CrossRectangle::set_t2(const fltp t2){
		t2_ = t2;
	}

	// element size in transverse direction
	void CrossRectangle::set_dt(const fltp dt){
		dt_ = dt;
	}

	// set normal first position
	fltp CrossRectangle::get_n1() const{
		return n1_;
	}

	// set normal second position
	fltp CrossRectangle::get_n2() const{
		return n2_;
	}

	// element size in normal direction
	fltp CrossRectangle::get_dn() const{
		return dn_;
	}

	// set transverse first position
	fltp CrossRectangle::get_t1() const{
		return t1_;
	}

	// set transverse second position
	fltp CrossRectangle::get_t2() const{
		return t2_;
	}

	// element size in transverse direction
	fltp CrossRectangle::get_dt() const{
		return dt_;
	}

	// set gauss enable
	void CrossRectangle::set_enable_gauss(const bool enable_gauss){
		enable_gauss_ = enable_gauss;
	}

	// volume mesh
	ShAreaPr CrossRectangle::create_area(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);
		
		// calculate number of elements for each direction
		arma::uword num_t = std::max((int)std::ceil((t2_-t1_)/dt_),1);
		arma::uword num_n = std::max((int)std::ceil((n2_-n1_)/dn_),1);

		// override for low poly
		if(stngs.low_poly){num_n = 1; num_t = 1;}

		// allocate coordinates
		arma::Mat<fltp> u(num_t+1, num_n+1);
		arma::Mat<fltp> v(num_t+1, num_n+1);

		// set values
		if(!enable_gauss_){
			u.each_row() = arma::linspace<arma::Row<fltp> >(n1_,n2_,num_n+1);
			v.each_col() = arma::linspace<arma::Col<fltp> >(t1_,t2_,num_t+1);
		}else{
			// create gauss points
			cmn::Gauss gpn(num_n); cmn::Gauss gpt(num_t);
			u.each_row() = n1_ + arma::join_horiz(arma::Row<fltp>{0},arma::cumsum(gpn.get_weights()/2))*(n2_-n1_);
			v.each_col() = t1_ + arma::join_horiz(arma::Row<fltp>{0},arma::cumsum(gpt.get_weights()/2)).t()*(t2_-t1_);
		}

		// calculate number of nodes
		const arma::uword num_nodes = (num_t+1)*(num_n+1);

		// create node coordinates
		arma::Mat<fltp> Rn(2,num_nodes);
		Rn.row(0) = arma::reshape(u,1,num_nodes);
		Rn.row(1) = arma::reshape(v,1,num_nodes);

		// create matrix of node indices
		arma::Mat<arma::uword> node_idx = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes-1);
		node_idx.reshape(num_t+1, num_n+1);

		// number of elements
		// const arma::uword num_elements = num_n*num_t;

		// get corner matrices
		const arma::Mat<arma::uword> M0 = node_idx.submat(0, 0, num_t-1, num_n-1);
		const arma::Mat<arma::uword> M1 = node_idx.submat(1, 0, num_t, num_n-1);
		const arma::Mat<arma::uword> M2 = node_idx.submat(1, 1, num_t, num_n);
		const arma::Mat<arma::uword> M3 = node_idx.submat(0, 1, num_t-1, num_n);

		// reshape into element matrix
		const arma::Mat<arma::uword> n = arma::join_vert(
			arma::vectorise(M0).t(), arma::vectorise(M1).t(),
			arma::vectorise(M2).t(), arma::vectorise(M3).t());

		// check 
		assert(n.n_rows==4);

		// create orientaton
		arma::Mat<fltp> N(2,Rn.n_cols,arma::fill::zeros); N.row(0).fill(RAT_CONST(1.0));
		arma::Mat<fltp> D(2,Rn.n_cols,arma::fill::zeros); D.row(1).fill(RAT_CONST(1.0));

		// return positive
		return Area::create(Rn,N,D,n);
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossRectangle::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;
		bnd.col(0) = arma::Col<fltp>::fixed<2>{n1_,n2_};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{t1_,t2_};
		return bnd;
	}

	// surface mesh
	ShPerimeterPr CrossRectangle::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area(MeshSettings());

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	// validity check
	bool CrossRectangle::is_valid(const bool enable_throws) const{
		if(n1_>=n2_){if(enable_throws){rat_throw_line("n1 must be smaller than n2");} return false;};
		if(t1_>=t2_){if(enable_throws){rat_throw_line("t1 must be smaller than t2");} return false;};
		if(dn_<=0){if(enable_throws){rat_throw_line("normal element size must be larger than zero");} return false;};
		if(dt_<=0){if(enable_throws){rat_throw_line("transverse element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string CrossRectangle::get_type(){
		return "rat::mdl::crossrectangle";
	}

	// method for serialization into json
	void CrossRectangle::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["enable_gauss"] = enable_gauss_;
		js["n1"] = n1_; js["t1"] = t1_;
		js["n2"] = n2_; js["t2"] = t2_;
		js["dn"] = dn_; js["dt"] = dt_;
	}

	// method for deserialisation from json
	void CrossRectangle::deserialize (
		const Json::Value &js, cmn::DSList &, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/) {

		// use set functions
		set_enable_gauss(js["enable_gauss"].asBool());

		// decode the json object
		set_n1(js["n1"].asDouble()); set_t1(js["t1"].asDouble());
		set_n2(js["n2"].asDouble()); set_t2(js["t2"].asDouble());
		set_dn(js["dn"].asDouble()); set_dt(js["dt"].asDouble());
	}

}}
