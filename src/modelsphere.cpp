// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelsphere.hh"
#include "drivedc.hh"
#include "transrotate.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelSphere::ModelSphere(){
		set_name("modelsphere");
	}

	// constructor
	ModelSphere::ModelSphere(const fltp radius, const fltp element_size) : ModelSphere(){
		set_radius(radius); set_element_size(element_size);
	}

	// factory
	ShModelSpherePr ModelSphere::create(){
		//return ShModelSpherePr(new ModelSphere);
		return std::make_shared<ModelSphere>();
	}

	// factory
	ShModelSpherePr ModelSphere::create(const fltp radius, const fltp element_size){
		//return ShModelSpherePr(new ModelSphere);
		return std::make_shared<ModelSphere>(radius, element_size);
	}

	// set operating temperature 
	fltp ModelSphere::get_operating_temperature() const{
		return operating_temperature_;
	}

	// set operating temperature 
	void ModelSphere::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// set drive
	void ModelSphere::set_temperature_drive(const ShDrivePr &temperature_drive){
		if(temperature_drive==NULL)rat_throw_line("supplied drive points to NULL");
		temperature_drive_ = temperature_drive;
	}

	// setting
	void ModelSphere::set_radius(const fltp radius){
		radius_ = radius;
	}

	// getting
	fltp ModelSphere::get_radius()const{
		return radius_;
	}

	// setting
	void ModelSphere::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// getting
	fltp ModelSphere::get_element_size()const{
		return element_size_;
	}

	// set polar
	void ModelSphere::set_polar_orientation(const bool polar_orientation){
		polar_orientation_ = polar_orientation;
	}

	// create a sphere mesh
	void ModelSphere::setup_mesh(const ShMeshDataPr &mesh_data, const MeshSettings &stngs) const{
		// core radius
		const fltp box_side = core_size_*2*radius_/std::sqrt(RAT_CONST(3.0));

		// number of elemnets
		const arma::uword num_elem_core = std::max(1,int(box_side/element_size_));
		const arma::uword num_node_core = num_elem_core + 1;

		// create nodes
		arma::Mat<fltp> Rncore(3,num_node_core*num_node_core*num_node_core);

		// indexes
		// arma::Cube<arma::uword> n(num_node_core,num_node_core,num_node_core);

		// create a mesh of a cube
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_node_core;i++){
			for(arma::uword j=0;j<num_node_core;j++){
				for(arma::uword k=0;k<num_node_core;k++){
					Rncore(0,cnt) = i*box_side/num_elem_core - box_side/2;
					Rncore(1,cnt) = j*box_side/num_elem_core - box_side/2;
					Rncore(2,cnt) = k*box_side/num_elem_core - box_side/2;
					cnt++;
				}
			}
		}

		// create elements
		arma::Mat<arma::uword> ncore(8,num_elem_core*num_elem_core*num_elem_core);
		cnt = 0;
		for(arma::uword i=0;i<num_elem_core;i++){
			for(arma::uword j=0;j<num_elem_core;j++){
				for(arma::uword k=0;k<num_elem_core;k++){
					ncore(0,cnt) = i*num_node_core*num_node_core + j*num_node_core + k;
					ncore(1,cnt) = (i+1)*num_node_core*num_node_core + j*num_node_core + k;
					ncore(2,cnt) = (i+1)*num_node_core*num_node_core + (j+1)*num_node_core + k;
					ncore(3,cnt) = i*num_node_core*num_node_core + (j+1)*num_node_core + k;
					ncore(4,cnt) = i*num_node_core*num_node_core + j*num_node_core + k + 1;
					ncore(5,cnt) = (i+1)*num_node_core*num_node_core + j*num_node_core + k + 1;
					ncore(6,cnt) = (i+1)*num_node_core*num_node_core + (j+1)*num_node_core + k + 1;
					ncore(7,cnt) = i*num_node_core*num_node_core + (j+1)*num_node_core + k + 1;
					cnt++;
				}
			}
		}

		// number of elements in shell
		const arma::uword num_elem_shell = std::max(1,int((radius_ - box_side/2)/element_size_));
		const arma::uword num_node_shell = num_elem_shell+1;
 
		// shell coordinates
		arma::Mat<fltp> Rnshell(3,num_node_core*num_node_core*num_node_shell);

		// create a mesh of a cube
		cnt = 0;
		for(arma::uword i=0;i<num_node_core;i++){
			for(arma::uword j=0;j<num_node_core;j++){
				// core surface
				arma::Col<fltp>::fixed<3> R0{
					i*box_side/num_elem_core - box_side/2,
					j*box_side/num_elem_core - box_side/2,
					box_side/2};

				// coordinate projected on sphere surface
				arma::Col<fltp>::fixed<3> R1 = 
					radius_*(R0.each_row()/cmn::Extra::vec_norm(R0));

				// walk over layers
				for(arma::uword k=0;k<num_node_shell;k++){
					Rnshell.col(cnt) = R0 + (float(k)/(num_node_shell-1))*(R1-R0);
					cnt++;
				}
			}
		}

		// shell volume mesh
		arma::Mat<arma::uword> nshell(8,num_elem_core*num_elem_core*num_elem_shell);

		// create elements
		cnt = 0;
		for(arma::uword i=0;i<num_elem_core;i++){
			for(arma::uword j=0;j<num_elem_core;j++){
				for(arma::uword k=0;k<num_elem_shell;k++){
					nshell(0,cnt) = i*num_node_core*num_node_shell + j*num_node_shell + k;
					nshell(1,cnt) = (i+1)*num_node_core*num_node_shell + j*num_node_shell + k;
					nshell(2,cnt) = (i+1)*num_node_core*num_node_shell + (j+1)*num_node_shell + k;
					nshell(3,cnt) = i*num_node_core*num_node_shell + (j+1)*num_node_shell + k;
					nshell(4,cnt) = i*num_node_core*num_node_shell + j*num_node_shell + k + 1;
					nshell(5,cnt) = (i+1)*num_node_core*num_node_shell + j*num_node_shell + k + 1;
					nshell(6,cnt) = (i+1)*num_node_core*num_node_shell + (j+1)*num_node_shell + k + 1;
					nshell(7,cnt) = i*num_node_core*num_node_shell + (j+1)*num_node_shell + k + 1;
					cnt++;
				}
			}
		}

		// surface mesh
		arma::Mat<arma::uword> sshell(4,num_elem_core*num_elem_core);

		// create surface mesh
		cnt = 0;
		for(arma::uword i=0;i<num_elem_core;i++){
			for(arma::uword j=0;j<num_elem_core;j++){
				sshell(3,cnt) = i*num_node_core*num_node_shell + j*num_node_shell + num_node_shell-1;
				sshell(2,cnt) = (i+1)*num_node_core*num_node_shell + j*num_node_shell + num_node_shell-1;
				sshell(1,cnt) = (i+1)*num_node_core*num_node_shell + (j+1)*num_node_shell + num_node_shell-1;
				sshell(0,cnt) = i*num_node_core*num_node_shell + (j+1)*num_node_shell + num_node_shell-1;
				cnt++;
			}
		}

		// // check mesh
		// for(arma::uword i=0;i<sshell.n_cols;i++){
			
		// 	// rat_throw_line("mesh is not planar");
		// 	// get typical size
		// 	fltp diag = std::sqrt(arma::as_scalar(arma::sum(
		// 		(Rnshell.col(nshell(0,i)) - Rnshell.col(nshell(2,i)))%
		// 		(Rnshell.col(nshell(0,i)) - Rnshell.col(nshell(2,i))),0)));

		// 	// calculate the plane in which the face is located
		// 	arma::Col<fltp>::fixed<3> V1 = Rnshell.col(nshell(1,i))-Rnshell.col(nshell(0,i));
		// 	arma::Col<fltp>::fixed<3> V2 = Rnshell.col(nshell(3,i))-Rnshell.col(nshell(1,i));

		// 	// get face normal
		// 	arma::Col<fltp>::fixed<3> N = cmn::Extra::cross(V1,V2);
		// 	N = N/arma::as_scalar(cmn::Extra::vec_norm(N));

		// 	// check if diagonals are in plane
		// 	fltp eps1 = arma::as_scalar(cmn::Extra::dot(N,Rnshell.col(nshell(2,i))-Rnshell.col(nshell(0,i))))/diag;
		// 	fltp eps2 = arma::as_scalar(cmn::Extra::dot(N,Rnshell.col(nshell(1,i))-Rnshell.col(nshell(3,i))))/diag;


		// 	std::cout<<"sh "<<i<<" "<<eps1<<" "<<eps2<<std::endl;
		// 	std::cout<<cmn::Quadrilateral::check_nodes(Rnshell.cols(nshell.col(i)))<<std::endl;
		// }

		// create all parts
		arma::field<arma::Mat<fltp> > Rnfld(1,7);
		arma::field<arma::Mat<arma::uword> > nfld(1,7);
		arma::field<arma::Mat<arma::uword> > sfld(1,6);
		cnt = 0;
		for(arma::uword i=0;i<4;i++){
			Rnfld(i) = Rnshell; nfld(i) = nshell + cnt; sfld(i) = sshell + cnt;
			TransRotate::create({1,0,0},i*arma::Datum<fltp>::pi/2)->apply_coords(Rnfld(i));
			cnt += Rnfld(i).n_cols;
		}
		for(arma::uword i=0;i<2;i++){
			Rnfld(4+i) = Rnshell; nfld(4+i) = nshell + cnt; sfld(4+i) = sshell + cnt;
			TransRotate::create({0,1,0},(float(i)-RAT_CONST(0.5))*arma::Datum<fltp>::pi)->apply_coords(Rnfld(4+i));
			cnt += Rnfld(4+i).n_cols;
		}
		Rnfld(6) = Rncore; nfld(6) = ncore + cnt; 
		cnt += Rnfld(6).n_cols;

		// combine
		arma::Mat<fltp> Rn = cmn::Extra::field2mat(Rnfld);
		arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);
		arma::Mat<arma::uword> s = cmn::Extra::field2mat(sfld);

		// combine nodes in a mesh
		const arma::Row<arma::uword> idx = cmn::Extra::combine_nodes(Rn);

		// remove duplicate points
		// Rn = Rn.cols(sect.row(0));
		n = arma::reshape(idx(arma::vectorise(n)),n.n_rows,n.n_cols);
		s = arma::reshape(idx(arma::vectorise(s)),s.n_rows,s.n_cols);

		// offset
		Rn.each_col()+=R0_;

		// check output
		assert(arma::max(arma::max(n))<Rn.n_cols); 
		assert(arma::max(arma::max(s))<Rn.n_cols);

		// invert hexahedrons
		{
			arma::Mat<arma::uword> n0 = n.rows(0,3);
			arma::Mat<arma::uword> n1 = n.rows(4,7);
			n.rows(0,3) = n1; n.rows(4,7) = n0;
		}

		// set to self
		mesh_data->set_target_coords(Rn); 
		mesh_data->set_elements(n); 
		mesh_data->set_surface_elements(s);

		// cartesian
		if(!polar_orientation_){
			arma::Mat<fltp> L(Rn.n_rows,Rn.n_cols,arma::fill::zeros); L.row(0).fill(RAT_CONST(1.0));
			arma::Mat<fltp> N(Rn.n_rows,Rn.n_cols,arma::fill::zeros); N.row(1).fill(RAT_CONST(1.0));
			arma::Mat<fltp> D(Rn.n_rows,Rn.n_cols,arma::fill::zeros); D.row(2).fill(RAT_CONST(1.0));

			// set orientation
			mesh_data->set_longitudinal(L); mesh_data->set_normal(N); mesh_data->set_transverse(D); 
		}

		// polar
		else{
			// set orientation
			mesh_data->set_normal(Rn.each_row()/cmn::Extra::vec_norm(Rn));

			// do not use L, N and D
			mesh_data->set_transverse(arma::Mat<fltp>(Rn.n_rows,Rn.n_cols,arma::fill::zeros)); 
			mesh_data->set_longitudinal(arma::Mat<fltp>(Rn.n_rows,Rn.n_cols,arma::fill::zeros)); 
		}

		// determine dimensions for surface and volume mesh
		mesh_data->determine_dimensions();

		// set time
		mesh_data->set_time(stngs.time);

		// set temperature
		mesh_data->set_operating_temperature(operating_temperature_);

		// set name (is appended by models later)
		mesh_data->set_name(myname_);

		// apply transformations to mesh
		mesh_data->apply_transformations(get_transformations());

		// set material
		mesh_data->set_material(conductor_);
	}

	// factory for mesh objects
	std::list<ShMeshDataPr> ModelSphere::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check input
		if(!is_valid(stngs.enable_throws))return{};

		// mesh data
		const ShMeshDataPr mesh_data = MeshData::create();

		// create spherical mesh
		setup_mesh(mesh_data, stngs);

		// mesh data object
		return {mesh_data};
	}

	// check validity
	bool ModelSphere::is_valid(const bool enable_throws) const{
		if(operating_temperature_<=0){if(enable_throws){rat_throw_line("operating temperature must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelSphere::get_type(){
		return "rat::mdl::modelsphere";
	}

	// method for serialization into json
	void ModelSphere::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Model::serialize(js,list);
		mat::InputConductor::serialize(js,list);

		// type
		js["type"] = get_type();

		// temperature and drive
		js["temperature_drive"] = cmn::Node::serialize_node(temperature_drive_, list);
		js["operating_temperature"] = operating_temperature_;

		// coordinate
		js["Rx"] = R0_(0); js["Ry"] = R0_(1); js["Rz"] = R0_(2);

		// geometry
		js["radius"] = radius_;
		js["element_size"] = element_size_;	
		js["core_size"] = core_size_;

		js["polar_orientation"] = polar_orientation_;
	}

	// method for deserialisation from json
	void ModelSphere::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Model::deserialize(js,list,factory_list,pth);
		mat::InputConductor::deserialize(js,list,factory_list,pth);
		
		// geometry
		radius_ = js["radius"].asDouble();
		element_size_ = js["element_size"].asDouble();
		core_size_ = js["core_size"].asDouble();

		// temperature and drive
		set_temperature_drive(cmn::Node::deserialize_node<Drive>(
			js["temperature_drive"], list, factory_list, pth));
		set_operating_temperature(js["operating_temperature"].asDouble());
		
		// coordinate
		R0_(0) = js["Rx"].asDouble(); 
		R0_(1) = js["Ry"].asDouble(); 
		R0_(2) = js["Rz"].asDouble();

		polar_orientation_ = js["polar_orientation"].asDouble(); 
	}

}}

