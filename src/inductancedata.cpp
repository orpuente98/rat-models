// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "inductancedata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	InductanceData::InductanceData(){
		set_output_type("inductance");
	}

	// constructor
	InductanceData::InductanceData(
		const fltp time, 
		const arma::Mat<fltp> &M, const arma::Mat<fltp> &E, 
		const arma::field<std::string> &coil_names, 
		const fltp calculation_time){
		set_output_type("inductance");
		time_ = time;
		M_ = M; E_ = E; 
		coil_names_ = coil_names; 
		calculation_time_ = calculation_time;
	}

	// factory
	ShInductanceDataPr InductanceData::create(){
		return std::make_shared<InductanceData>();
	}

	// factory
	ShInductanceDataPr InductanceData::create(
		const fltp time,
		const arma::Mat<fltp> &M, const arma::Mat<fltp> &E, 
		const arma::field<std::string> &coil_names, 
		const fltp calculation_time){
		return std::make_shared<InductanceData>(time,M,E,coil_names,calculation_time);
	}


	// get names
	arma::field<std::string> InductanceData::get_coil_names() const{
		return coil_names_;
	}

	// get inductance
	arma::Mat<fltp> InductanceData::get_matrix() const{
		return M_;
	}

	// get energy matrix
	arma::Mat<fltp> InductanceData::get_energy_matrix() const{
		return E_;
	}

	// get total inductance
	fltp InductanceData::get_inductance() const{
		return arma::accu(M_);
	}

	// get stored energy
	fltp InductanceData::get_stored_energy() const{
		return arma::accu(E_);
	}

	// write surface to VTK file
	ShVTKObjPr InductanceData::export_vtk() const{
		// create table
		ShVTKTablePr vtk_table = VTKTable::create();

		// dump data
		for(arma::uword i=0;i<M_.n_cols;i++)
			vtk_table->set_data(M_.col(i),std::to_string(i));

		// return table
		return vtk_table;
	}

	// write output
	void InductanceData::display(cmn::ShLogPr lg){
		// report results
		lg->msg(2,"%s%sINDUCTANCE CALCULATION%s\n",KBLD,KGRN,KNRM);

		// report matrix
		lg->msg(2,"%sInductance Matrix [mH]%s\n",KBLU,KNRM);

		// print matrix header
		lg->msg("%2s "," ");
		for(arma::uword i=0;i<M_.n_cols;i++){
			lg->msg(0,"%s%05llu%s ",KBLD,i,KNRM);
		}
		lg->msg(0,"\n");
		
		// print matrix data
		for(arma::uword i=0;i<M_.n_rows;i++){
			lg->msg("%s%02llu%s ",KBLD,i,KNRM);
			for(arma::uword j=0;j<M_.n_cols;j++){
				lg->msg(0,"%05.2f ",1e3*M_(i,j));
			}
			lg->msg(0,"\n");
		}

		lg->msg("stored energy: %s%6.2f [kJ]%s\n",
			KYEL,1e-3*get_stored_energy(),KNRM);
		lg->msg("inductance: %s%6.2f [mH]%s\n",
			KYEL,1e3*get_inductance(),KNRM);

		// return
		lg->msg(-2,"\n");

		// return
		lg->msg(-2,"\n");
	}

}}


