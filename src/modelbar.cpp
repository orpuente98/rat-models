// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelbar.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelBar::ModelBar(){
		set_name("bar");
	}

	// constructor
	ModelBar::ModelBar(const ShPathPr &input_path, const ShCrossPr &input_cross) : ModelBar(){
		set_name("bar"); set_input_path(input_path); set_input_cross(input_cross);
	}

	// factory
	ShModelBarPr ModelBar::create(){
		return std::make_shared<ModelBar>();
	}

	// factory immediately setting base and cross section
	ShModelBarPr ModelBar::create(const ShPathPr &input_path, const ShCrossPr &input_cross){
		return std::make_shared<ModelBar>(input_path,input_cross);
	}

	// set number of turns
	void ModelBar::set_softening(const fltp softening){
		if(softening_<0)rat_throw_line("softening must be zero or larger");
		softening_ = softening;
	}

	// set number of gauss points
	void ModelBar::set_num_gauss_volume(const arma::sword num_gauss_volume){
		if(num_gauss_volume==0)rat_throw_line("number of gauss points must be positive");
		num_gauss_volume_ = num_gauss_volume;
	}

	// set number of gauss points
	void ModelBar::set_num_gauss_surface(const arma::sword num_gauss_surface){
		if(num_gauss_surface==0)rat_throw_line("number of gauss points must be positive");
		num_gauss_surface_ = num_gauss_surface;
	}
	
	// set magnetisation
	void ModelBar::set_magnetisation(const arma::Col<fltp>::fixed<3> &Mf){
		Mf_  = Mf;
	}

	// magnetisation of the bar
	arma::Col<fltp>::fixed<3> ModelBar::get_magnetisation() const{
		return Mf_;
	}

	// set drive
	void ModelBar::set_magnetisation_drive(const ShDrivePr &magnetisation_drive){
		if(magnetisation_drive==NULL)rat_throw_line("supplied drive points to NULL");
		magnetisation_drive_ = magnetisation_drive;
	}

	// create data object
	ShMeshDataPr ModelBar::create_data()const{
		return BarData::create();
	}

	// factory for mesh objects
	std::list<ShMeshDataPr> ModelBar::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check model
		if(!is_valid(stngs.enable_throws))return{};

		// calculate magnetization
		const arma::Col<fltp>::fixed<3> Mf = magnetisation_drive_->get_scaling(stngs.time)*Mf_;

		// create meshes
		std::list<ShMeshDataPr> meshes = ModelMesh::create_meshes(trace, stngs);

		// allocate list of bar meshes
		std::list<ShMeshDataPr> bar_meshes;
		for(auto it = meshes.begin();it!=meshes.end();it++){
			// reinterpret cast
			const ShBarDataPr bar_mesh = std::dynamic_pointer_cast<BarData>(*it);
			if(bar_mesh==NULL)rat_throw_line("pointer cast failed");

			// copy properties
			bar_mesh->set_softening(softening_);
			bar_mesh->set_magnetisation(Mf);
			bar_mesh->set_num_gauss(num_gauss_volume_, num_gauss_surface_);

			// add to list
			bar_meshes.push_back(bar_mesh);
		}

		// return mesh data
		return bar_meshes;
	}

	// check validity
	bool ModelBar::is_valid(const bool enable_throws) const{
		// check mesh
		if(!ModelMesh::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelBar::get_type(){
		return "rat::mdl::modelbar";
	}

	// method for serialization into json
	void ModelBar::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelMesh::serialize(js,list);

		// drive
		js["magnetisation_drive"] = cmn::Node::serialize_node(magnetisation_drive_, list);

		// magnetization
		js["ML"] = Mf_(0); js["MN"] = Mf_(1); js["MD"] = Mf_(2);

		// properties
		js["type"] = get_type();
		js["softening"] = softening_;
		js["num_gauss_volume"] = (int)num_gauss_volume_;
		js["num_gauss_surface"] = (int)num_gauss_surface_;
	}

	// method for deserialisation from json
	void ModelBar::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		ModelMesh::deserialize(js,list,factory_list,pth);

		// current drive
		set_magnetisation_drive(cmn::Node::deserialize_node<Drive>(
			js["magnetisation_drive"], list, factory_list, pth));

		// magnetization
		Mf_(0) = js["ML"].asDouble(); Mf_(1) = js["MN"].asDouble(); Mf_(2) = js["MD"].asDouble();

		// properties
		set_softening(js["softening"].asDouble());
		set_num_gauss_volume(js["num_gauss_volume"].asInt64());
		set_num_gauss_surface(js["num_gauss_surface"].asInt64());
	}

}}