// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "bardata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	BarData::BarData(){
		add_field_type('M',3); set_output_type("bar");
	}

	// constructor with input
	BarData::BarData(const ShFramePr &frame, const ShAreaPr &area){
		setup(frame, area); add_field_type('M',3); set_output_type("bar");
		if(n_dim_!=3)rat_throw_line("bar mesh must be volumetric (3 dim)");
	}

	// factory
	ShBarDataPr BarData::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<BarData>();
	}

	// factory with input
	ShBarDataPr BarData::create(const ShFramePr &frame, const ShAreaPr &area){
		return std::make_shared<BarData>(frame,area);
	}

	// set softening factor
	void BarData::set_softening(const fltp softening){
		softening_ = softening;
	}

	// set magnetisation
	void BarData::set_magnetisation(const arma::Col<fltp>::fixed<3> &Mf){
		Mf_  = Mf;
	}

	// default create sources function to be overridden
	void BarData::setup_sources(){
		// allocate
		fmm::ShSourcesPrList srcs(1);

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = L_*Mf_(0) + N_*Mf_(1) + D_*Mf_(2);

		// use barmesh
		set_mesh(Rt_,n_,Mn);

		// call setup on multisources
		MultiSources::setup_sources();
	}


	// calculate integrated Lorentz forces
	arma::Col<fltp>::fixed<3> BarData::calc_lorentz_force() const{
		rat_throw_line("needs implementation");
		return {0,0,0}; // not implemented yet
	}

}}