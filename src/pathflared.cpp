// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathflared.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathFlared::PathFlared(){
		set_name("flared");
	}

	// constructor with dimension input
	PathFlared::PathFlared(
		const fltp ell1, const fltp ell2, 
		const fltp arcl, const fltp radius1, 
		const fltp radius2, const fltp element_size, 
		const fltp offset, const fltp offset_arc){

		// set to self
		set_name("flared");
		set_ell1(ell1);	set_ell2(ell2);
		set_arcl(arcl);	set_radius1(radius1); set_radius2(radius2);
		set_element_size(element_size);	set_offset(offset);
		set_offset_arc(offset_arc);
	}

	// factory
	ShPathFlaredPr PathFlared::create(){
		//return ShPathFlaredPr(new PathFlared);
		return std::make_shared<PathFlared>();
	}

	// factory with dimension input
	ShPathFlaredPr PathFlared::create(
		const fltp ell1, const fltp ell2, 
		const fltp arcl, const fltp radius1, 
		const fltp radius2, const fltp element_size, 
		const fltp offset, const fltp offset_arc){
		return std::make_shared<PathFlared>(ell1,ell2,arcl,
			radius1,radius2,element_size,offset,offset_arc);
	}

	// set element size 
	void PathFlared::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// path offset
	void PathFlared::set_offset(const fltp offset){
		offset_ = offset;
	}

	// path offset for hardway bend
	void PathFlared::set_offset_arc(const fltp offset_arc){
		offset_arc_ = offset_arc;
	}

	// set first length
	void PathFlared::set_ell1(const fltp ell1){
		ell1_ = ell1;
	}

	// set second length
	void PathFlared::set_ell2(const fltp ell2){
		ell2_ = ell2;
	}

	// set arc length
	void PathFlared::set_arcl(const fltp arcl){
		arcl_ = arcl;
	}

	// set first radius
	void PathFlared::set_radius1(const fltp radius1){
		radius1_ = radius1;
	}

	// set second radius
	void PathFlared::set_radius2(const fltp radius2){
		radius2_ = radius2;
	}


	// set element size 
	fltp PathFlared::get_element_size() const{
		return element_size_;
	}

	// path offset
	fltp PathFlared::get_offset() const{
		return offset_;
	}

	// path offset for hardway bend
	fltp PathFlared::get_offset_arc() const{
		return offset_arc_;
	}

	// set first length
	fltp PathFlared::get_ell1() const{
		return ell1_;
	}

	// set second length
	fltp PathFlared::get_ell2() const{
		return ell2_;
	}

	// set arc length
	fltp PathFlared::get_arcl() const{
		return arcl_;
	}

	// set first radius
	fltp PathFlared::get_radius1() const{
		return radius1_;
	}

	// set second radius
	fltp PathFlared::get_radius2() const{
		return radius2_;
	}


	// get frame
	ShFramePr PathFlared::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// create arcs
		ShPathArcPr arc1 = PathArc::create(radius1_,arcl_,element_size_,offset_arc_); arc1->set_transverse(true);
		ShPathArcPr arc2 = PathArc::create(radius2_,arma::Datum<fltp>::pi/2,element_size_,offset_);
		ShPathStraightPr str1 = PathStraight::create(ell1_/2,element_size_);
		ShPathStraightPr str2 = PathStraight::create(ell2_/2,element_size_);

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();
		pathgroup->add_path(str1); pathgroup->add_path(arc1); pathgroup->add_path(str2); pathgroup->add_path(arc2);
		pathgroup->add_path(arc2); pathgroup->add_path(str2); pathgroup->add_path(arc1); pathgroup->add_path(str1);
		pathgroup->add_path(str1); pathgroup->add_path(arc1); pathgroup->add_path(str2); pathgroup->add_path(arc2);
		pathgroup->add_path(arc2); pathgroup->add_path(str2); pathgroup->add_path(arc1); pathgroup->add_path(str1);
		pathgroup->add_translation(radius2_,0,0);
		
		// create frame
		ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations());

		// return frame
		return frame;
	}

	// vallidity check
	bool PathFlared::is_valid(const bool enable_throws) const{
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(ell1_<=0){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(ell2_<=0){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(arcl_==0){if(enable_throws){rat_throw_line("arc length can not be zero");} return false;};
		if(radius1_<=0){if(enable_throws){rat_throw_line("first radius must be larger than zero");} return false;};
		if(radius2_<=0){if(enable_throws){rat_throw_line("second radius must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathFlared::get_type(){
		return "rat::mdl::pathflared";
	}

	// method for serialization into json
	void PathFlared::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["ell1"] = ell1_;
		js["ell2"] = ell2_;
		js["arcl"] = arcl_;
		js["radius1"] = radius1_;
		js["radius2"] = radius2_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
		js["offset_arc"] = offset_arc_;
	}

	// method for deserialisation from json
	void PathFlared::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Transformations::deserialize(js,list,factory_list,pth);
		
		// properties
		set_ell1(js["ell1"].asDouble());
		set_ell2(js["ell2"].asDouble());
		set_arcl(js["arcl"].asDouble());
		set_radius1(js["radius1"].asDouble());
		set_radius2(js["radius2"].asDouble());
		set_element_size(js["element_size"].asDouble());
		set_offset(js["offset"].asDouble());
		set_offset_arc(js["offset_arc"].asDouble());
	}

}}
