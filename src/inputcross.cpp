// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "inputcross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	InputCross::InputCross(const ShCrossPr &input_cross){
		set_input_cross(input_cross);
	}

	// set cross
	void InputCross::set_input_cross(const ShCrossPr &input_cross){
		input_cross_ = input_cross;
	}

	// set cross
	ShCrossPr InputCross::get_input_cross() const{
		return input_cross_;
	}

	// is custom coil
	bool InputCross::is_custom_cross() const{
		return true;
	}

	// is valid
	bool InputCross::is_valid(const bool enable_throws) const{
		if(is_custom_cross()){
			if(input_cross_==NULL){if(enable_throws){rat_throw_line("input cross section points to NULL");} return false;};
			if(!input_cross_->is_valid(enable_throws))return false;
		}
		return true;
	}

	// get type
	std::string InputCross::get_type(){
		return "rat::mdl::inputcross";
	}

	// method for serialization into json
	void InputCross::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// serialize cross
		js["input_cross"] = cmn::Node::serialize_node(input_cross_, list);
	}

	// method for deserialisation from json
	void InputCross::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// deserialize cross
		set_input_cross(cmn::Node::deserialize_node<Cross>(js["input_cross"], list, factory_list, pth));
	}


}}
