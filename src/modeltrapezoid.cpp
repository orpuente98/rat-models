// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modeltrapezoid.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathgroup.hh"
#include "patharc.hh"
#include "pathstraight.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelTrapezoid::ModelTrapezoid(){
		set_name("trapezoid");
	}

	// factory
	ShModelTrapezoidPr ModelTrapezoid::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelTrapezoid>();
	}

	// set arc radius
	void ModelTrapezoid::set_arc_radius(const fltp arc_radius){
		arc_radius_ = arc_radius;
	}

	// set length1
	void ModelTrapezoid::set_length_1(const fltp length_1){
		length_1_ = length_1-arc_radius_;
	}

	// set length2
	void ModelTrapezoid::set_length_2(const fltp length_2){
		length_2_ = length_2-arc_radius_;
	}

		// set width
	void ModelTrapezoid::set_width(const fltp width){
		width_ = width-arc_radius_;
	}

	// set coil thickness
	void ModelTrapezoid::set_coil_thickness(const fltp coil_thickness){
		coil_thickness_ = coil_thickness;
	}

	// set coil width
	void ModelTrapezoid::set_coil_width(const fltp coil_width){
		coil_width_ = coil_width;
	}

	// set axial element size
	void ModelTrapezoid::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}


	// get arc radius
	fltp ModelTrapezoid::get_arc_radius()const{
		return arc_radius_;
	}

	// get length 1
	fltp ModelTrapezoid::get_length_1()const{
		return length_1_+arc_radius_;
	}

	// get length 2
	fltp ModelTrapezoid::get_length_2()const{
		return length_2_+arc_radius_;
	}

	// get width
	fltp ModelTrapezoid::get_width()const{
		return width_+arc_radius_;
	}

	// get coil thickness
	fltp ModelTrapezoid::get_coil_thickness()const{
		return coil_thickness_;
	}

	// get coil width
	fltp ModelTrapezoid::get_coil_width()const{
		return coil_width_;
	}

	// get axial element size
	fltp ModelTrapezoid::get_element_size()const {
		return element_size_;
	}

	// get base
	ShPathPr ModelTrapezoid::get_input_path() const{
		// check input
		is_valid(true);

		// Length 1 is always bigger than length 2
		// So alpha is always the smallest angle
		const fltp alpha = std::atan(width_/(std::abs(length_1_-length_2_)/2.));
		const fltp beta = arma::Datum<fltp>::pi-alpha;

		// Calculate the lengths that need to be subtracted
		// from the total to compensate for the rounded corners
		const fltp length_alpha = arc_radius_ / std::tan(alpha/2.);
		const fltp length_beta = arc_radius_ * std::tan(0.5*arma::Datum<fltp>::pi - 0.5*beta);

		// Now calculate the lengths
		const fltp length_3 = std::sqrt(((length_1_ - length_2_)/2)*((length_1_ - length_2_)/2) + width_*width_);

		// create geometry
		ShPathGroupPr path_group = PathGroup::create();
		path_group->add_path(PathStraight::create(length_2_/2 - (length_1_<length_2_ ? length_alpha : length_beta), element_size_));
		path_group->add_path(PathArc::create(arc_radius_, 
			arma::Datum<fltp>::pi - (length_1_<length_2_ ? alpha : beta), 
			element_size_, coil_thickness_));
		path_group->add_path(PathStraight::create(length_3 - length_alpha - length_beta, element_size_));
		path_group->add_path(PathArc::create(arc_radius_, 
			arma::Datum<fltp>::pi - (length_1_>length_2_ ? alpha : beta), 
			element_size_, coil_thickness_));
		path_group->add_path(PathStraight::create(length_1_/2 - (length_1_>length_2_ ? length_alpha : length_beta), element_size_));
		
		// set one-fold symmetry
		path_group->set_sym(2);

		// move start point
		path_group->set_start_coord({width_/2., 0, 0});

		// return the rectangle
		return path_group;
	}

	// get cross
	ShCrossPr ModelTrapezoid::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, coil_thickness_, -coil_width_/2, coil_width_/2, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// check input
	bool ModelTrapezoid::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(arc_radius_<=0){if(enable_throws){rat_throw_line("arc radius must be larger than zero");} return false;};
		if(length_1_<2*arc_radius_){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(length_2_<2*arc_radius_){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(width_<2*arc_radius_){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;};
		if(coil_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(coil_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelTrapezoid::get_type(){
		return "rat::mdl::modeltrapezoid";
	}

	// method for serialization into json
	void ModelTrapezoid::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// set properties
		js["arc_radius"] = arc_radius_;
		js["length_1"] = length_1_;
		js["length_2"] = length_2_;
		js["width"] = width_;
		js["coil_thickness"] = coil_thickness_;
		js["coil_width"] = coil_width_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelTrapezoid::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// set properties
		arc_radius_ = js["arc_radius"].asDouble();
		length_1_ = js["length_1"].asDouble();
		length_2_ = js["length_2"].asDouble();
		width_ = js["width"].asDouble();		
		coil_thickness_ = js["coil_thickness"].asDouble();
		coil_width_ = js["coil_width"].asDouble();
		element_size_ = js["element_size"].asDouble();
	}

}}