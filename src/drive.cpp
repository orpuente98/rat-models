// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{


	// get scaling for arrays
	arma::Row<fltp> Drive::get_scaling(const arma::Row<fltp> &time) const{
		arma::Row<fltp> v(time);
		for(arma::uword i=0;i<time.n_elem;i++)v(i)=get_scaling(time(i));
		return v;
	}

	arma::Row<fltp> Drive::get_dscaling(const arma::Row<fltp> &time) const{
		arma::Row<fltp> v(time);
		for(arma::uword i=0;i<time.n_elem;i++)v(i)=get_dscaling(time(i));
		return v;
	}

	// get time array
	arma::Col<fltp> Drive::get_inflection_times() const{
		return arma::Col<fltp>{};
	}

	// set range start
	void Drive::set_t1(const fltp t1){
		t1_ = t1;
	}

	// set range end
	void Drive::set_t2(const fltp t2){
		t2_ = t2;
	}

	// get range start
	fltp Drive::get_t1() const{
		return t1_;
	}

	// get range end
	fltp Drive::get_t2() const{
		return t2_;
	}

	// get type
	std::string Drive::get_type(){
		return "rat::mdl::drive";
	}

	// method for serialization into json
	void Drive::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["t1"] = t1_;
		js["t2"] = t2_;
	}

	// method for deserialisation from json
	void Drive::deserialize(
		const Json::Value &js, 
		cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		if(js.isMember("t1"))set_t1(js["t1"].asDouble());
		if(js.isMember("t2"))set_t2(js["t2"].asDouble());
	}


}}