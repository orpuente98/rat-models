// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcplane.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// model headers
#include "crosspoint.hh"
#include "crosscircle.hh"
#include "meshdata.hh"
#include "pathaxis.hh"
#include "pathrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcPlane::CalcPlane(){
		set_name("plane");

		// settinsg
		stngs_->set_num_exp(5);
		stngs_->set_large_ilist(true);
	}

	CalcPlane::CalcPlane(const ShModelPr &model) : CalcPlane(){
		set_model(model);
	}
	
	CalcPlane::CalcPlane(const ShModelPr &model, const char plane_normal, const fltp ell1, const fltp ell2, const arma::uword num_steps1, const arma::uword num_steps2) : CalcPlane(model){
		set_plane_normal(plane_normal); set_ell1(ell1); set_ell2(ell2); 
		set_num_steps1(num_steps1); set_num_steps2(num_steps2);
	}

	// factory methods
	ShCalcPlanePr CalcPlane::create(){
		return std::make_shared<CalcPlane>();
	}

	ShCalcPlanePr CalcPlane::create(const ShModelPr &model){
		return std::make_shared<CalcPlane>(model);
	}

	ShCalcPlanePr CalcPlane::create(const ShModelPr &model, const char plane_normal, const fltp ell1, const fltp ell2, const arma::uword num_steps1, const arma::uword num_steps2){
		return std::make_shared<CalcPlane>(model,plane_normal,ell1,ell2,num_steps1,num_steps2);
	}


	// get mesh enabled
	bool CalcPlane::get_visibility() const{
		return visibility_;
	}

	// set mesh enabled
	void CalcPlane::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	// set plane normal vector
	void CalcPlane::set_plane_normal(const char plane_normal){
		plane_normal_ = plane_normal;
	}

	// set position 
	void CalcPlane::set_offset(const arma::Col<fltp>::fixed<3> &offset){
		offset_ = offset;
	}

	// set first side length
	void CalcPlane::set_ell1(const fltp ell1){
		ell1_ = ell1;
	}

	// set second side length
	void CalcPlane::set_ell2(const fltp ell2){
		ell2_ = ell2;
	}

	// set first side number of steps
	void CalcPlane::set_num_steps1(const arma::uword num_steps1){
		num_steps1_ = num_steps1;
	}

	// set second side number of steps
	void CalcPlane::set_num_steps2(const arma::uword num_steps2){
		num_steps2_ = num_steps2;
	}


	// get plane normal vector
	char CalcPlane::get_plane_normal() const{
		return plane_normal_;
	}

	// get position 
	arma::Col<fltp>::fixed<3> CalcPlane::get_offset() const{
		return offset_;
	}

	// get first side length
	fltp CalcPlane::get_ell1() const{
		return ell1_;
	}

	// get second side length
	fltp CalcPlane::get_ell2() const{
		return ell2_;
	}

	// get first side number of steps
	arma::uword CalcPlane::get_num_steps1() const{
		return num_steps1_;
	}

	// get second side number of steps
	arma::uword CalcPlane::get_num_steps2() const{
		return num_steps2_;
	}


	// calculate with inductance data output
	ShGridDataPr CalcPlane::calculate_plane(const fltp time, const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();
		
		// general info
		lg->msg("%s=== Starting Plane Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// by default we set the third dimension
		fltp x1 = RAT_CONST(0.0),y1 = RAT_CONST(0.0),z1 = RAT_CONST(0.0); 
		fltp x2 = RAT_CONST(0.0),y2 = RAT_CONST(0.0),z2 = RAT_CONST(0.0);
		arma::uword nx = 1,ny = 1,nz = 1;

		// first dimension
		if(plane_normal_=='x'){
			y1 = -ell1_/2; y2 = +ell1_/2; ny = num_steps1_;
			z1 = -ell2_/2; z2 = +ell2_/2; nz = num_steps2_;
		}else if(plane_normal_=='y'){
			x1 = -ell1_/2; x2 = +ell1_/2; nx = num_steps1_;
			z1 = -ell2_/2; z2 = +ell2_/2; nz = num_steps2_;
		}else if(plane_normal_=='z'){
			x1 = -ell1_/2; x2 = +ell1_/2; nx = num_steps1_;
			y1 = -ell2_/2; y2 = +ell2_/2; ny = num_steps2_;
		}else{
			rat_throw_line("first plane vector not recognized");
		}

		// apply offset
		x1+=offset_(0); x2+=offset_(0); 
		y1+=offset_(1); y2+=offset_(1); 
		z1+=offset_(2); z2+=offset_(2);

		// create plane
		ShGridDataPr plane = GridData::create(x1,x2,nx,y1,y2,ny,z1,z2,nz);
		plane->set_field_type("AHM",{3,3,3});
		plane->set_output_type("plane");
		plane->set_time(time);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 

		// create multilevel fast multipole method object
		fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);

		// add sources and targets to the calculation
		mlfmm->set_sources(src);
		mlfmm->set_targets(plane);

		// setup multipole method
		mlfmm->setup(lg);

		// run multipole method
		mlfmm->calculate(lg);

		// return the line data
		return plane;
	}

	// generalized calculation
	std::list<ShDataPr> CalcPlane::calculate(const fltp time, const cmn::ShLogPr &lg){
		return {calculate_plane(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcPlane::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// square assuming Z-plane
		ShPathRectanglePr pth = PathRectangle::create(
			ell1_, ell2_, std::min(ell1_,ell2_)/100, std::min(ell1_,ell2_)/400);

		// rotate in plane
		if(plane_normal_=='x'){
			pth->add_transformation(TransRotate::create({0,1,0},arma::Datum<fltp>::pi/2));
			pth->add_transformation(TransRotate::create({1,0,0},arma::Datum<fltp>::pi/2));
		}
		if(plane_normal_=='y')pth->add_transformation(TransRotate::create({1,0,0},arma::Datum<fltp>::pi/2));

		// apply offset
		pth->add_transformation(TransTranslate::create(offset_));

		// allocate cross section
		ShCrossPr crss;

		// point cross section
		if(stngs.visual_radius==RAT_CONST(0.0))crss = CrossPoint::create(
			RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1e-6),RAT_CONST(1e-6));

		// circular cross section
		else crss = CrossCircle::create(stngs.visual_radius,stngs.visual_radius/2);

		// create frame
		ShFramePr frame = pth->create_frame(stngs);

		// create 2d area mesh
		ShAreaPr area = crss->create_area(stngs);

		// drop nodes
		if(stngs.low_poly)frame->simplify(stngs.visual_tolerance, crss->get_bounding());

		// combine
		if(stngs.combine_sections)frame->combine();

		// mesh data
		ShMeshDataPr mesh_data = MeshData::create(frame, area);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();

		// mesh data object
		return {mesh_data};
	}

	// validity check
	bool CalcPlane::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		if(ell1_<=0){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(ell2_<=0){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(num_steps1_<=0){if(enable_throws){rat_throw_line("first number of steps must be larger than zero");} return false;};
		if(num_steps2_<=0){if(enable_throws){rat_throw_line("second number of steps must be larger than zero");} return false;};
		return true;
	}	

	// serialization
	std::string CalcPlane::get_type(){
		return "rat::mdl::calcplane";
	}

	void CalcPlane::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		
		js["type"] = get_type();

		// orientation
		js["plane_normal"] = plane_normal_;

		// position 
		js["offset_x"] = offset_(0);
		js["offset_y"] = offset_(1);
		js["offset_z"] = offset_(2);

		// dimensions
		js["ell1"] = ell1_;
		js["ell2"] = ell2_; 
		
		// discretization
		js["num_steps1"] = (int)num_steps1_;
		js["num_steps2"] = (int)num_steps2_;

		// enable visibility
		js["visibility"] = visibility_;
	}
	
	void CalcPlane::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		CalcLeaf::deserialize(js,list,factory_list,pth);

		// orientation
		plane_normal_ = js["plane_normal"].asInt();

		// position 
		offset_(0) = js["offset_x"].asDouble();
		offset_(1) = js["offset_y"].asDouble();
		offset_(2) = js["offset_z"].asDouble();

		// dimensions
		ell1_ = js["ell1"].asDouble();
		ell2_ = js["ell2"].asDouble(); 
		
		// discretization
		num_steps1_ = js["num_steps1"].asUInt64();
		num_steps2_ = js["num_steps2"].asUInt64();

		// enable visibility
		visibility_ = js["visibility"].asBool();
	}

}}