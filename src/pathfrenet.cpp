// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathfrenet.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathFrenet::PathFrenet(){
		set_name("frenet-serret");
	}

	// default constructor
	PathFrenet::PathFrenet(const ShPathPr &input_path){
		set_name("frenet-serret"); set_input_path(input_path);
	}

	// factory
	ShPathFrenetPr PathFrenet::create(){
		//return ShPathFrenetPr(new PathFrenet);
		return std::make_shared<PathFrenet>();
	}

	// factory
	ShPathFrenetPr PathFrenet::create(const ShPathPr &input_path){
		//return ShPathFrenetPr(new PathFrenet);
		return std::make_shared<PathFrenet>(input_path);
	}


	// keep winding direction
	void PathFrenet::set_keep_block_vector(const bool keep_block_vector){
		keep_block_vector_ = keep_block_vector;
	}

	// get winding direction setting
	bool PathFrenet::get_keep_block_vector() const{
		return keep_block_vector_;
	}

	// get frame
	ShFramePr PathFrenet::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// get frame from input path
		ShFramePr frame = input_path_->create_frame(stngs);

		// calculate derivatives
		const arma::Col<fltp>::fixed<3> null_vec{0,0,0};

		// get coordinates
		arma::field<arma::Mat<fltp> > R = frame->get_coords();
		const arma::uword num_sections = R.n_elem;

		// check if the path is closed
		const bool is_closed = frame->is_loop();

		// calculate velocity
		arma::field<arma::Mat<fltp> > V(1,num_sections);
		for(arma::uword i=0;i<num_sections;i++){
			V(i) = arma::diff(R(i),1,1);
			V(i) = arma::join_horiz(V(i),null_vec) + arma::join_horiz(null_vec,V(i));
			V(i).cols(1,V(i).n_cols-2)/=2;
		}

		// calculate acceleration
		arma::field<arma::Mat<fltp> > A(1,num_sections);
		for(arma::uword i=0;i<num_sections;i++){
			A(i) = arma::diff(V(i),1,1);
			A(i) = arma::join_horiz(A(i),null_vec) + arma::join_horiz(null_vec,A(i));
			A(i).cols(1,A(i).n_cols-2)/=2;
		}

		// average last to first
		for(arma::uword i=0;i<num_sections-1;i++){
			V(i).col(V(i).n_cols-1) += V(i+1).col(0);
			V(i).col(V(i).n_cols-1)/=2;
			V(i+1).col(0) = V(i).col(V(i).n_cols-1);
		}

		// average last to first
		for(arma::uword i=0;i<num_sections-1;i++){
			A(i).col(A(i).n_cols-1) += A(i+1).col(0);
			A(i).col(A(i).n_cols-1)/=2;
			A(i+1).col(0) = A(i).col(A(i).n_cols-1);
		}

		// average last to first for entire loop
		if(is_closed){
			V(num_sections-1).tail_cols(1) += V(0).head_cols(1);
			V(num_sections-1).tail_cols(1)/=2;
			V(0).head_cols(1) = V(num_sections-1).tail_cols(1);
			A(num_sections-1).tail_cols(1) += A(0).head_cols(1);
			A(num_sections-1).tail_cols(1)/=2;
			A(0).head_cols(1) = A(num_sections-1).tail_cols(1);
		}

		// calculate darboux vectors
		arma::field<arma::Mat<fltp> > D(1,num_sections);
		arma::field<arma::Mat<fltp> > T(1,num_sections);
		arma::field<arma::Mat<fltp> > N(1,num_sections);
		for(arma::uword i=0;i<num_sections;i++){
			// number of points
			const arma::uword num_times = V(i).n_cols;

			// normalize velocity
			const arma::Row<fltp> normV = cmn::Extra::vec_norm(V(i));
			T(i) = V(i).each_row()/normV; // previously called T
			
			// setup frame
			const arma::Mat<fltp> B = cmn::Extra::cross(V(i),A(i)); 
			const arma::Row<fltp> normB = cmn::Extra::vec_norm(B);

			// normalize 
			const arma::Mat<fltp> Anrm = A(i).each_row()/cmn::Extra::vec_norm(A(i));

			// create frame
			D(i) = cmn::Extra::cross(Anrm.head_cols(num_times-1),Anrm.tail_cols(num_times-1));

			// extend
			D(i) = (arma::join_horiz(D(i).head_cols(1),D(i)) + arma::join_horiz(D(i),D(i).tail_cols(1)))/2;

			// normalize darboux vectors
			D(i).each_row() /= cmn::Extra::vec_norm(D(i));

			// calculate angle
			const arma::Row<fltp> alpha = arma::acos(cmn::Extra::dot(-T(i),D(i)));

			// Scale frame
			D(i).each_row() /= arma::sin(alpha);

			// normal direction
			N(i) = cmn::Extra::cross(T(i),B.each_row()/normB);
		}

		// average last to first
		for(arma::uword i=0;i<num_sections-1;i++){
			D(i).col(D(i).n_cols-1) += D(i+1).col(0);
			D(i).col(D(i).n_cols-1)/=2;
			D(i+1).col(0) = D(i).col(D(i).n_cols-1);
		}

		// average last to first for entire loop
		if(is_closed){
			D(num_sections-1).tail_cols(1) += D(0).head_cols(1);
			D(num_sections-1).tail_cols(1)/=2;
			D(0).head_cols(1) = D(num_sections-1).tail_cols(1);
		}

		// block vector
		arma::field<arma::Mat<fltp> > B;
		if(keep_block_vector_)B = frame->get_block(); else B = N;

		// create new frame
		ShFramePr frenet_frame = Frame::create(R, T, N, D, B);

		// transfer sections
		frenet_frame->set_location(frame->get_section(), frame->get_turn(), frame->get_num_section_base());

		// apply transformations
		frenet_frame->apply_transformations(get_transformations());

		// return the new frame
		return frenet_frame;
	}


	// vallidity check
	bool PathFrenet::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathFrenet::get_type(){
		return "rat::mdl::pathfrenet";
	}

	// method for serialization into json
	void PathFrenet::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);
		InputPath::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["keep_block_vector"] = keep_block_vector_;
	}

	// method for deserialisation from json
	void PathFrenet::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Node::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		set_keep_block_vector(js["keep_block_vector"].asBool());

	}

}}
