// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcmesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcMesh::CalcMesh(){
		set_name("mesh");
	}

	// constructor immediately setting base and cross section
	CalcMesh::CalcMesh(const ShModelPr &model){
		set_name("mesh"); set_model(model);
	}

	// factory with input
	ShCalcMeshPr CalcMesh::create(){
		return std::make_shared<CalcMesh>();
	}

	// factory with input
	ShCalcMeshPr CalcMesh::create(const ShModelPr &model){
		return std::make_shared<CalcMesh>(model);
	}

	// no time calculation t=0
	std::list<ShDataPr> CalcMesh::calculate(const fltp time, const cmn::ShLogPr &lg){
		// calculate
		std::list<ShMeshDataPr> mesh_data = calculate_mesh(time,lg);
		
		// convert to data 
		std::list<ShDataPr> data;
		for(auto it = mesh_data.begin();it!=mesh_data.end();it++)
			data.push_back(*it);

		// return data
		return data;
	}

	// run calculation
	std::list<ShMeshDataPr> CalcMesh::calculate_mesh(const fltp time, const cmn::ShLogPr &lg){
		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();
		
		// general info
		lg->msg("%s=== Starting Mesh Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%sSETTING UP MESHES%s\n",KGRN,KNRM);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// remove calculation meshes
		for(auto it = meshes.begin();it!=meshes.end();)
			if((*it)->get_calc_mesh())it = meshes.erase(it); else it++;

		// set field type and time
		for(auto it=meshes.begin();it!=meshes.end();it++){
			(*it)->set_field_type("AHM",{3,3,3});
			// (*it)->set_time(time);
		}

		// set circuit currents
		apply_circuit_currents(time, meshes);


		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");

		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();
		fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);

		// add meshes to MLFMM targets
		for(auto it=meshes.begin();it!=meshes.end();it++)tar->add_targets(*it);

		// multipole method
		const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src, tar, stngs_);

		// set background field
		if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// add background field
		// if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));

		// setup MLFMM and calculate required fields
		mlfmm->setup(lg); 
		mlfmm->calculate(lg);

		// return the output data
		return meshes;
	}

	// get type
	std::string CalcMesh::get_type(){
		return "rat::mdl::calcmesh";
	}

	// method for serialization into json
	void CalcMesh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void CalcMesh::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);
	}


}}