// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelclover.hh"

#include "pathclover.hh"
#include "crossrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelClover::ModelClover(){
		set_name("clover");
	}

	// constructor with dimension input
	ModelClover::ModelClover(
		const fltp ellstr1, const fltp ellstr2, 
		const fltp height, const fltp dpack, const fltp ell_trans, 
		const fltp str12, const fltp str34, const fltp element_size){
		set_name("clover");
		set_ellstr1(ellstr1); set_ellstr2(ellstr2);
		set_height(height); set_dpack(dpack); set_ell_trans(ell_trans);
		set_str12(str12); set_str34(str34); set_element_size(element_size);
	}

	// factory
	ShModelCloverPr ModelClover::create(){
		//return ShModelCloverPr(new ModelClover);
		return std::make_shared<ModelClover>();
	}

	// factory with dimension input
	ShModelCloverPr ModelClover::create(
		const fltp ellstr1, const fltp ellstr2, 
		const fltp height, const fltp dpack, const fltp ell_trans, 
		const fltp str12, const fltp str34, const fltp element_size){
		return std::make_shared<ModelClover>(
			ellstr1,ellstr2,height,dpack,
			ell_trans,str12,str34,element_size);
	}

	// get base
	ShPathPr ModelClover::get_input_path() const{
		// check input
		is_valid(true);

		// create clover path
		const ShPathCloverPr pth = PathClover::create(
			ellstr1_,ellstr2_,height_,dpack_,
			ell_trans_,str12_,str34_,element_size_);

		// path 
		pth->set_bending_radius(bending_radius_);
		pth->set_bridge_angle(bridge_angle_);
		pth->set_planar_winding(planar_winding_);

		// path
		return pth;
	}

	// get cross
	ShCrossPr ModelClover::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, coil_thickness_, 0, coil_width_, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// set planar winding feature
	void ModelClover::set_planar_winding(const bool planar_winding){
		planar_winding_ = planar_winding;
	}

	// set first length
	void ModelClover::set_ellstr1(const fltp ellstr1){
		ellstr1_ = ellstr1;
	}

	// set second length
	void ModelClover::set_ellstr2(const fltp ellstr2){
		ellstr2_ = ellstr2;
	}

	// set bridge height
	void ModelClover::set_height(const fltp height){
		height_ = height;
	}

	// set spacing needed for winding pack
	void ModelClover::set_dpack(const fltp dpack){
		dpack_ = dpack;
	}

	// set spacing needed for winding pack
	void ModelClover::set_ell_trans(const fltp ell_trans){
		ell_trans_ = ell_trans;
	}

	// set first and second strength
	void ModelClover::set_str12(const fltp str12){
		str12_ = str12;
	}

	void ModelClover::set_str34(const fltp str34){
		str34_ = str34;
	}

	// set element size 
	void ModelClover::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set bending radius
	void ModelClover::set_bending_radius(const fltp bending_radius){
		bending_radius_ = bending_radius;
	}

	// set bridge angle
	void ModelClover::set_bridge_angle(const fltp bridge_angle){
		bridge_angle_ = bridge_angle;
	}

	// set coil thickness
	void ModelClover::set_coil_thickness(const fltp coil_thickness){
		coil_thickness_ = coil_thickness;
	}

	// set coil width
	void ModelClover::set_coil_width(const fltp coil_width){
		coil_width_ = coil_width;
	}


	// set planar winding feature
	bool ModelClover::get_planar_winding() const{
		return planar_winding_;
	}

	// get first straight section length
	fltp ModelClover::get_ellstr1() const{
		return ellstr1_;
	}

	// get second straight section length
	fltp ModelClover::get_ellstr2() const{
		return ellstr2_;
	}

	// set bridge height
	fltp ModelClover::get_height() const{
		return height_;
	}

	// set spacing needed for winding pack
	fltp ModelClover::get_dpack() const{
		return dpack_;
	}

	// set spacing needed for winding pack
	fltp ModelClover::get_ell_trans() const{
		return ell_trans_;
	}

	// set first and second strength
	fltp ModelClover::get_str12() const{
		return str12_;
	}

	fltp ModelClover::get_str34() const{
		return str34_;
	}

	// set element size 
	fltp ModelClover::get_element_size() const{
		return element_size_;
	}

	// set bending radius
	fltp ModelClover::get_bending_radius() const{
		return bending_radius_;
	}

	// set bridge angle
	fltp ModelClover::get_bridge_angle() const{
		return bridge_angle_;
	}
	
	// get coil thickness
	fltp ModelClover::get_coil_thickness()const{
		return coil_thickness_;
	}

	// get coil width
	fltp ModelClover::get_coil_width()const{
		return coil_width_;
	}


	// validity check
	bool ModelClover::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(ellstr1_<=0){if(enable_throws){rat_throw_line("first straight section length must be larger than zero");} return false;};
		if(ellstr2_<=0){if(enable_throws){rat_throw_line("second straight section length must be larger than zero");} return false;};
		if(dpack_<0){if(enable_throws){rat_throw_line("pack thickness must be equal to or larger than zero");} return false;};
		if(ell_trans_<0){if(enable_throws){rat_throw_line("transition length must be equal to or larger than zero");} return false;};
		if(str12_<=0){if(enable_throws){rat_throw_line("first strength must be larger than zero");} return false;};
		if(str34_<=0){if(enable_throws){rat_throw_line("second strength must be larger than zero");} return false;};
		if(coil_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(coil_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		if(bending_radius_-ellstr2_/2<=0 && bending_radius_!=0){if(enable_throws){rat_throw_line("bending radius too small");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("second strength must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelClover::get_type(){
		return "rat::mdl::modelclover";
	}

	// method for serialization into json
	void ModelClover::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		ModelCoilWrapper::serialize(js,list);

		js["type"] = get_type();
		js["ell_trans"] = ell_trans_;
		js["dpack"] = dpack_;
		js["height"] = height_; 
		js["str12"] = str12_; 
		js["str34"] = str34_; 
		js["element_size"] = element_size_; 
		js["ellstr1"] = ellstr1_; 
		js["ellstr2"] = ellstr2_; 
		js["bending_radius"] = bending_radius_; 
		js["bridge_angle"] = bridge_angle_;
		js["coil_thickness"] = coil_thickness_;
		js["coil_width"] = coil_width_;

	}

	// method for deserialisation from json
	void ModelClover::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		set_ell_trans(js["ell_trans"].asDouble());
		set_dpack(js["dpack"].asDouble());
		set_height(js["height"].asDouble()); 
		set_str12(js["str12"].asDouble()); 
		set_str34(js["str34"].asDouble()); 
		set_element_size(js["element_size"].asDouble()); 
		set_ellstr1(js["ellstr1"].asDouble());
		set_ellstr2(js["ellstr2"].asDouble()); 
		set_bending_radius(js["bending_radius"].asDouble()); 
		set_bridge_angle(js["bridge_angle"].asDouble());
		set_coil_thickness(js["coil_thickness"].asDouble());
		set_coil_width(js["coil_width"].asDouble());
	}

}}
