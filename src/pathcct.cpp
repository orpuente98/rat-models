// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcct.hh"

// common headers
#include "rat/common/extra.hh"

// models headers
#include "darboux.hh"
#include "transbend.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCCT::PathCCT(){
		set_name("cct path");
	}

	// constructor with input
	PathCCT::PathCCT(const arma::uword num_poles, const fltp radius, const fltp a, 
		const fltp omega, const fltp num_turns, const arma::uword num_nodes_per_turn){
		set_num_poles(num_poles); set_radius(radius); set_amplitude(a); 
		set_pitch(omega); set_num_turns(num_turns); set_num_nodes_per_turn(num_nodes_per_turn);
		set_name("cct path");
	}

	// factory
	ShPathCCTPr PathCCT::create(){
		return std::make_shared<PathCCT>();
	}

	// factory
	ShPathCCTPr PathCCT::create(const arma::uword num_poles, const fltp radius, const fltp a, 
		const fltp omega, const fltp num_turns, const arma::uword num_nodes_per_turn){
		return std::make_shared<PathCCT>(num_poles,radius,a,omega,num_turns,num_nodes_per_turn);
	}

	// set the direction of the layer
	void PathCCT::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}

	// set the direction of the layer
	void PathCCT::set_bending_arc_length(const fltp bending_arc_length){
		bending_arc_length_ = bending_arc_length;
	}

	// set twisting
	void PathCCT::set_twist(const fltp twist){
		twist_ = twist;
	}

	// set amplitude
	void PathCCT::set_amplitude(const fltp a){
		a_ = a;
	}

	// set winding pitch
	void PathCCT::set_pitch(const fltp omega){
		omega_ = omega;
	}

	// set number of turns
	void PathCCT::set_num_turns(const fltp num_turns){
		num_turns_ = num_turns;
	}

	// set radius
	void PathCCT::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set scaling in x
	void PathCCT::set_scale_x(const fltp scale_x){
		scale_x_ = scale_x;
	}
	
	// set scaling in y
	void PathCCT::set_scale_y(const fltp scale_y){
		scale_y_ = scale_y;
	}

	// set number of poles
	void PathCCT::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	// set number of poles
	void PathCCT::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// set the direction of the layer
	bool PathCCT::get_is_reverse() const{
		return is_reverse_;
	}

	// set the direction of the layer
	fltp PathCCT::get_bending_arc_length() const{
		return bending_arc_length_;
	}

	// set twisting
	fltp PathCCT::get_twist()const{
		return twist_;
	}

	// set amplitude
	fltp PathCCT::get_amplitude()const{
		return a_;
	}

	// set winding pitch
	fltp PathCCT::get_pitch()const{
		return omega_;
	}

	// set number of turns
	fltp PathCCT::get_num_turns()const{
		return num_turns_;
	}

	// set radius
	fltp PathCCT::get_radius()const{
		return radius_;
	}

	// set scaling in x
	fltp PathCCT::get_scale_x()const{
		return scale_x_;
	}
	
	// set scaling in y
	fltp PathCCT::get_scale_y()const{
		return scale_y_;
	}

	// set number of poles
	arma::uword PathCCT::get_num_poles()const{
		return num_poles_;
	}

	// set number of poles
	arma::uword PathCCT::get_num_nodes_per_turn() const{
		return num_nodes_per_turn_;
	}


	// calculate skew angle
	fltp PathCCT::calc_alpha() const{
		return num_poles_*std::atan(radius_/a_);
	}

	// set number of layers
	void PathCCT::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	// get number of layers
	arma::uword PathCCT::get_num_layers() const{
		return num_layers_;
	}

	// set radius increment between layers
	void PathCCT::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	// get radius increment between layers
	fltp PathCCT::get_rho_increment()const{
		return rho_increment_;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathCCT::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);
		
		// check input
		if(radius_<=0)rat_throw_line("radius must be positive");
		if(num_poles_==0)rat_throw_line("number of poles must be positive");
		if(num_nodes_per_turn_==0)rat_throw_line("number of nodes per turn must be positive");

		// calculate number of sections per half
		const arma::uword num_sect_per_turn = num_poles_*4;
		const fltp sectsize = 2*arma::Datum<fltp>::pi/num_sect_per_turn;

		// calculate maximum theta
		const fltp thetamin = -num_turns_*arma::Datum<fltp>::pi;
		const fltp thetamax = +num_turns_*arma::Datum<fltp>::pi;
		
		arma::uword num_int = arma::uword(std::floor(thetamax/sectsize));
		if(std::abs(thetamax-num_int*sectsize)<RAT_CONST(1e-8))num_int--;

		// calculate number of sections
		const arma::uword num_sections = 2*(num_int+1);

		// allocate theta
		arma::Row<fltp> theta_start(num_sections);
		arma::Row<fltp> theta_end(num_sections);

		// set theta start positions
		theta_start(0) = thetamin;
		theta_start.cols(1,num_sections-1) = arma::linspace<arma::Row<fltp> >(-(num_int*sectsize),num_int*sectsize,num_sections-1);
		
		// set theta end positions
		theta_end.cols(0,num_sections-2) = arma::linspace<arma::Row<fltp> >(-(num_int*sectsize),num_int*sectsize,num_sections-1);
		theta_end(num_sections-1) = thetamax;

		//std::cout<<arma::join_horiz(theta_start.t(),theta_end.t())<<std::endl;

		// allocate list of frames
		std::list<ShFramePr> frame_list;

		// walk over layers
		for(arma::uword k=0;k<num_layers_;k++){
			// reverse
			const bool rev = ((int)is_reverse_ + k)%2==1;

			//calculate radius
			const fltp rho = radius_ + k*rho_increment_;

			// allocate
			arma::field<arma::Mat<fltp> > R(1,num_sections);
			arma::field<arma::Mat<fltp> > L(1,num_sections);
			arma::field<arma::Mat<fltp> > N(1,num_sections); 
			arma::field<arma::Mat<fltp> > D(1,num_sections);

			// allocate section and turn indexes
			arma::Row<arma::uword> section(num_sections);
			arma::Row<arma::uword> turn(num_sections); 


			// walk over sections
			for(arma::uword i=0;i<num_sections;i++){
				// calculate section
				const fltp theta1 = theta_start(i);
				const fltp theta2 = theta_end(i);
				assert(theta2>theta1);

				// calculate number of nodes
				arma::uword num_nodes = std::max(2,(int)std::ceil(num_nodes_per_turn_*(theta2-theta1)/(2*arma::Datum<fltp>::pi)));
				while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

				// create theta (taking into account last section
				const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1, theta2, num_nodes);

				// calculate expensive functions
				const arma::Row<fltp> cos_theta = arma::cos(theta);
				const arma::Row<fltp> sin_theta = arma::sin(theta);

				// create coordinates
				const arma::Row<fltp> x = scale_x_*rho*cos_theta;
				const arma::Row<fltp> y = scale_y_*rho*sin_theta;
				//arma::Row<fltp> z = (radius_/(num_poles_*std::tan(calc_alpha())))*arma::sin(num_poles_*theta);
				arma::Row<fltp> z = (a_/num_poles_)*arma::sin(fltp(num_poles_)*theta);
				
				// add turn increments
				if(rev==false)z += (omega_/(2*arma::Datum<fltp>::pi))*theta;
				else z -= (omega_/(2*arma::Datum<fltp>::pi))*theta;
				
				// create direction vectors
				const arma::Row<fltp> vx = -scale_x_*rho*sin_theta;
				const arma::Row<fltp> vy = scale_y_*rho*cos_theta;
				// arma::Row<fltp> vz = (num_poles_*(radius_/(num_poles_*std::tan(calc_alpha())))*arma::cos(num_poles_*theta) + 
				// 	num_poles_*omega_/(2*arma::Datum<fltp>::pi))*(2*arma::Datum<fltp>::pi/num_nodes_per_turn_);
				arma::Row<fltp> vz = a_*arma::cos(fltp(num_poles_)*theta);
				if(rev==false)vz += omega_/(2*arma::Datum<fltp>::pi);
				else vz -= omega_/(2*arma::Datum<fltp>::pi);

				// acceleration
				const arma::Row<fltp> ax = -scale_x_*rho*cos_theta;
				const arma::Row<fltp> ay = -scale_y_*rho*sin_theta;
				arma::Row<fltp> az = -a_*num_poles_*arma::sin(fltp(num_poles_)*theta);

				// jerk
				const arma::Row<fltp> jx = scale_x_*rho*sin_theta;
				const arma::Row<fltp> jy = -scale_y_*rho*cos_theta;
				arma::Row<fltp> jz = -a_*num_poles_*num_poles_*arma::cos(fltp(num_poles_)*theta);

				// reverse axial direction if needed
				if(num_poles_%2==0){
					z *= -1; vz *= -1; az *= -1; jz *= -1;
				}

				// reverse axial direction if needed
				if(rev){
					z *= -1; vz *= -1; az *= -1; jz *= -1;
				}

				// create darboux frame
				// Darboux db(
				// 	arma::join_vert(vx,vy,vz),
				// 	arma::join_vert(ax,ay,az),
				// 	arma::join_vert(jx,jy,jz));
				// db.setup(true);

				// store position vectors
				// R(i).set_size(3,num_nodes);
				// R(i).row(0) = x; R(i).row(1) = y; R(i).row(2) = z;
				R(i) = arma::join_vert(x,y,z);

				// store longitudinal vectors
				L(i) = arma::join_vert(vx,vy,vz);

				// calculate radial vector
				arma::Mat<fltp> Vax(3,num_nodes,arma::fill::zeros);
				Vax.row(2).fill(RAT_CONST(1.0));
				D(i) = cmn::Extra::cross(L(i),Vax);

				// create normal vectors
				N(i) = cmn::Extra::cross(L(i),D(i));
				
				// normalize
				L(i).each_row()/=cmn::Extra::vec_norm(L(i));
				D(i).each_row()/=cmn::Extra::vec_norm(D(i));
				N(i).each_row()/=cmn::Extra::vec_norm(N(i));

				// get darboux
				// L(i) = db.get_longitudinal();
				// D(i) = db.get_transverse();
				// N(i) = db.get_normal();

				
				// check size
				assert(R(i).n_rows==3);	assert(L(i).n_rows==3);
				assert(N(i).n_rows==3);	assert(D(i).n_rows==3);

				// set section and turn
				section(i) = i%num_sect_per_turn;
				turn(i) = i/num_sect_per_turn;

				// add aditional twist
				if(twist_!=0){
					arma::Row<fltp> twist = arma::sin(fltp(num_poles_)*theta)*twist_;
					if(rev)twist*=-1;
					const arma::Mat<fltp> Dtemp = D(i); const arma::Mat<fltp> Ntemp = N(i);
					const arma::Row<fltp> cos_twist = arma::cos(twist);
					const arma::Row<fltp> sin_twist = arma::sin(twist);
					D(i) = Dtemp.each_row()%cos_twist + Ntemp.each_row()%sin_twist;
					N(i) = Ntemp.each_row()%cos_twist - Dtemp.each_row()%sin_twist;
				}
			}

			// reverse frame
			if(rev){
				cmn::Extra::reverse_field(R); cmn::Extra::reverse_field(L);
				cmn::Extra::reverse_field(D); cmn::Extra::reverse_field(N);
				for(arma::uword i=0;i<L.n_elem;i++){
					L(i) *= -1; N(i) *= -1;
				}
			}

			// create frame
			ShFramePr frame = Frame::create(R,L,N,D);

			// circle CCT
			if(bending_arc_length_!=RAT_CONST(0.0)){
				const fltp bend_radius = omega_*num_turns_/bending_arc_length_;
				frame->apply_transformation(TransBend::create({0,1,0},{1,0,0},bend_radius));
				frame->apply_transformation(TransTranslate::create({bend_radius,0,0}));
			}

			// set section and turn indices
			frame->set_location(section,turn,num_sections);

			// add frame to list
			frame_list.push_back(frame);
		}

		// combine frames
		ShFramePr gen = Frame::create(frame_list);

		// transformations
		gen->apply_transformations(get_transformations());

		// create frame
		return gen;
	}

	// check input
	bool PathCCT::is_valid(const bool enable_throws) const{
		// check input
		if(!Transformations::is_valid(enable_throws))return false;
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles must be larger than zero");} return false;};
		if(num_turns_<=0){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;};
		if(num_nodes_per_turn_==0){if(enable_throws){rat_throw_line("number of nodes per turn can not be zero");} return false;};
		if(scale_x_<=0){if(enable_throws){rat_throw_line("x scaling must be larger than zero");} return false;};
		if(scale_y_<=0){if(enable_throws){rat_throw_line("y scaling must be larger than zero");} return false;};
		if(num_layers_<=0){if(enable_throws){rat_throw_line("number of layers must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathCCT::get_type(){
		return "rat::mdl::pathcct";
	}

	// method for serialization into json
	void PathCCT::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["num_poles"] = (unsigned int)num_poles_; 
		js["is_reverse_"] = is_reverse_;
		js["radius"] = radius_;
		js["a"] = a_; 
		js["omega"] = omega_; 
		js["num_turns"] = num_turns_; 
		js["num_nodes_per_turn"] = (unsigned int)num_nodes_per_turn_; 
		js["twist"] = twist_; 
		js["bending_arc_length"] = bending_arc_length_;
		js["scale_y"] = scale_y_;
		js["scale_x"] = scale_x_;
		js["num_layers"] = (int)num_layers_;
		js["rho_increment"] = rho_increment_;
	}

	// method for deserialisation from json
	void PathCCT::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_num_poles(js["num_poles"].asUInt64()); 
		set_is_reverse(js["is_reverse_"].asBool());
		set_radius(js["radius"].asDouble());
		set_amplitude(js["a"].asDouble()); 
		set_pitch(js["omega"].asDouble()); 
		set_num_turns(js["num_turns"].asDouble()); 
		set_num_nodes_per_turn(js["num_nodes_per_turn"].asUInt64()); 
		set_twist(js["twist"].asDouble());
		set_bending_arc_length(js["bending_arc_length"].asDouble());
		set_scale_y(js["scale_y"].asDouble());
		set_scale_x(js["scale_x"].asDouble());
		set_num_layers(js["num_layers"].asUInt64());
		set_rho_increment(js["rho_increment"].asDouble());
	}

}}


