// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "model.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// export edges to freecad file
	void Model::export_freecad(const cmn::ShFreeCADPr &freecad) const{
		// check opera
		if(freecad==NULL)rat_throw_line("freecad points to NULL");

		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = false;
		stngs.combine_sections = false;

		// create edges
		// ShEdgesPrList edges = create_edge();
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// export to freecad
		for(auto it = meshes.begin();it!=meshes.end();it++)
			(*it)->export_freecad(freecad);
	}

	// export edges to opera file
	void Model::export_opera(const cmn::ShOperaPr &opera) const{
		// check opera
		if(opera==NULL)rat_throw_line("opera points to NULL");

		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = false;
		stngs.combine_sections = false;

		// create edges
		// ShEdgesPrList edges = create_edge();
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// export to opera
		for(auto it = meshes.begin();it!=meshes.end();it++)
			(*it)->export_opera(opera);
	}

	// function for calculating mesh volume
	fltp Model::calc_total_volume(const fltp time) const{
		// settings
		MeshSettings stngs;
		stngs.time = time;

		// create mesh list
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// calculate volume
		fltp V = 0;
		for(auto it = meshes.begin();it!=meshes.end();it++)
			V += (*it)->calc_total_volume();

		// return volume
		return V;
	}

	// function for calculating mesh volume
	fltp Model::calc_total_surface_area(const fltp time) const{
		// settings
		MeshSettings stngs;
		stngs.time = time;

		// create mesh list
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// calculate volume
		fltp A = 0;
		for(auto it = meshes.begin();it!=meshes.end();it++)
			A += (*it)->calc_total_surface_area();

		// return volume
		return A;
	}

	// create specific mesh
	std::list<ShMeshDataPr> Model::create_meshes(
		const std::list<arma::uword> &/*trace*/, 
		const MeshSettings &/*stngs*/) const{
		return {};
	}

	std::string Model::get_type(){
		return "rat::mdl::model";
	}

	// method for serialization into json
	void Model::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);
		Transformations::serialize(js,list);
	}

	// method for deserialisation from json
	void Model::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		Node::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
	}

}}