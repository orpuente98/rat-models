// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transflip.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	TransFlip::TransFlip(){
		set_name("flip");
	}

	// factory methods
	ShTransFlipPr TransFlip::create(){
		return std::make_shared<TransFlip>();
	}

	// apply reversion
	void TransFlip::apply_vectors(const arma::Mat<fltp> &, arma::Mat<fltp> &V, const char str) const{
		// inverse direction
		if(str=='D' || str=='N' || str=='B')V *= -1;
	}

	// get type
	std::string TransFlip::get_type(){
		return "rat::mdl::transflip";
	}

	// method for serialization into json
	void TransFlip::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
		
	}

	// method for deserialisation from json
	void TransFlip::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
	}
}}