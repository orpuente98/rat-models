// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calctracks.hh"

// mlfmm headers
#include "rat/mlfmm/ilist.hh"
#include "rat/mlfmm/trackinggrid.hh"
#include "rat/mlfmm/nodelevel.hh"
#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// track calculations
	CalcTracks::CalcTracks(){
		set_name("calc tracks");
		tracking_model_ = ModelGroup::create();
		tracking_model_->set_name("tracking geometry");
	}

	CalcTracks::CalcTracks(
		const ShModelPr &model) : CalcTracks(){
		set_model(model);
	}	

	CalcTracks::CalcTracks(
		const ShModelPr &model, 
		const ShModelPr &tracking_model, 
		const std::list<ShEmitterPr> &emitters) : CalcTracks(model){
		set_tracking_model(tracking_model);
		add_emitters(emitters);
	}

	ShCalcTracksPr CalcTracks::create(){
		return std::make_shared<CalcTracks>();
	}

	ShCalcTracksPr CalcTracks::create(const ShModelPr &model){
		return std::make_shared<CalcTracks>(model);
	}

	ShCalcTracksPr CalcTracks::create(
		const ShModelPr &model, 
		const ShModelPr &tracking_model, 
		const std::list<ShEmitterPr> &emitters){
		return std::make_shared<CalcTracks>(model, tracking_model, emitters);
	}

	// get mesh enabled
	bool CalcTracks::get_visibility() const{
		return visibility_;
	}

	// set mesh enabled
	void CalcTracks::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	// set the stepsize
	void CalcTracks::set_stepsize(const fltp stepsize){
		stepsize_ = stepsize;
	}

	// get the stepsize
	fltp CalcTracks::get_stepsize()const{
		return stepsize_;
	}

	// add emitter
	arma::uword CalcTracks::add_emitter(const ShEmitterPr &emitter){
		const arma::uword index = emitters_.size()+1;
		emitters_.insert({index,emitter}); return index;
	}

	// add multiple emitters
	arma::Row<arma::uword> CalcTracks::add_emitters(const std::list<ShEmitterPr> &emitters){
		arma::Row<arma::uword> idx(emitters.size()); arma::uword i=0;
		for(auto it = emitters.begin();it!=emitters.end();it++,i++){
			idx(i) = add_emitter(*it);
		}
		return idx;
	}

	// delete emitter at index
	bool CalcTracks::delete_emitter(const arma::uword index){	
		auto it = emitters_.find(index);
		if(it==emitters_.end())return false;
		(*it).second = NULL; return true;
	}

	// number of emitters
	arma::uword CalcTracks::num_emitters() const{
		return emitters_.size();
	}

	// get emitter
	ShEmitterPr CalcTracks::get_emitter(const arma::uword index)const{
		auto it = emitters_.find(index);
		if(it==emitters_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// get list of all emitters
	std::list<ShEmitterPr> CalcTracks::get_emitters() const{
		std::list<ShEmitterPr> emitter_list;
		for(auto it=emitters_.begin();it!=emitters_.end();it++)
			emitter_list.push_back((*it).second);
		return emitter_list;
	}

	// re-index nodes after deleting
	void CalcTracks::reindex(){
		std::map<arma::uword, ShEmitterPr> new_emitters; arma::uword idx = 1;
		for(auto it=emitters_.begin();it!=emitters_.end();it++)
			if((*it).second!=NULL)new_emitters.insert({idx++, (*it).second});
		emitters_ = new_emitters;
	}

	// set model to define the shape of the tracking region
	void CalcTracks::set_tracking_model(const ShModelPr &tracking_model){
		tracking_model_ = tracking_model;
	}

	// get model to define the shape of the tracking region
	ShModelPr CalcTracks::get_tracking_model()const{
		return tracking_model_;
	}

	// calculate with inductance data output
	ShTrackDataPr CalcTracks::calculate_tracks(
		const fltp time, 
		const cmn::ShLogPr &lg){
		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Track Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%sSETTING UP MESHES%s\n",KGRN,KNRM);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// remove calculation meshes
		for(auto it = meshes.begin();it!=meshes.end();)
			if((*it)->get_calc_mesh())it = meshes.erase(it); else it++;

		// set circuit currents
		apply_circuit_currents(time, meshes);

		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");

		// combine sources and targets
		fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// set meshes as sources for MLFMM
		for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources(*it);


		// create a list of meshes
		std::list<ShMeshDataPr> tracking_meshes = tracking_model_->create_meshes({},mesh_settings);
		if(tracking_meshes.empty())rat_throw_line("tracking model is empty");

		// create targets
		fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();
	
		// add meshes to MLFMM targets
		for(auto it=tracking_meshes.begin();it!=tracking_meshes.end();it++)tar->add_targets(*it);


		// SETUP TRACKING GRID
		// display results
		lg->msg(2,"%s%sSETUP TRACKING%s\n",KBLD,KGRN,KNRM);

		// call setup function on sources and targets
		src->setup_sources(); tar->setup_targets();

		// setup oct-tree grid
		fmm::ShTrackingGridPr grid = fmm::TrackingGrid::create();
		grid->set_settings(stngs_);
		grid->set_sources(src);
		grid->set_targets(tar);

		// check whether to use MLFMM interpolation
		if(stngs_->get_direct()!=fmm::DirectMode::ALWAYS){
			// create interaction list object
			fmm::ShIListPr ilist = fmm::IList::create(stngs_);
		
			// set ilist to grid
			grid->set_ilist(ilist);
		
			// create root nodelevel object
			fmm::ShNodeLevelPr root = fmm::NodeLevel::create(stngs_,ilist,grid);

			// setup interaction lists
			ilist->setup(); ilist->display(lg);
			
			// call setup functions
			grid->setup(lg);
			grid->setup_matrices(); 
			grid->display(lg);
				
			// setup tree
			root->setup(lg);

			// run multipole method recursively from root
			root->run_mlfmm(lg);
		}

		// setup grid only
		else{
			grid->setup_direct();
		}

		// go back
		lg->msg(-2,"\n");

		// track data
		ShTrackDataPr track_data = TrackData::create();

		// perform tracking
		track_data->create_tracks(grid, get_emitters(), stepsize_, lg);

		// return tracks
		return track_data;
	}

	// generalized calculation
	std::list<ShDataPr> CalcTracks::calculate(
		const fltp time, const cmn::ShLogPr &lg){
		return {calculate_tracks(time,lg)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcTracks::create_meshes(
		const std::list<arma::uword> &trace, const MeshSettings &stngs)const{

		// check visibility
		if(!visibility_ || tracking_model_==NULL)return {};

		// get trace index
		std::list<arma::uword> next_trace = trace;
		if(!trace.empty()){next_trace.pop_front();}

		// create meshes from tracking model
		std::list<ShMeshDataPr> meshes = tracking_model_->create_meshes(next_trace,stngs);
		for(auto it = meshes.begin();it!=meshes.end();it++){
			(*it)->append_trace_id(1);
		}

		// return meshes
		return meshes;
	}

	// validity check
	bool CalcTracks::is_valid(const bool enable_throws) const{
		// settings
		if(stepsize_<=0){if(enable_throws){rat_throw_line("stepsize must be larger than zero");} return false;};

		// emitters
		for(auto it=emitters_.begin();it!=emitters_.end();it++){
			const ShEmitterPr& emitter = (*it).second;
			if(emitter==NULL)rat_throw_line("emitter list contains NULL");
			if(!emitter->is_valid(enable_throws))return false;
		}

		// tracking model
		if(tracking_model_==NULL){if(enable_throws){rat_throw_line("tracking model is NULL");} return false;};
		if(!tracking_model_->is_valid(enable_throws))return false;
		return true;
	}

	// serialization
	std::string CalcTracks::get_type(){
		return "rat::mdl::calctracks";
	}

	void CalcTracks::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		
		// parent
		CalcLeaf::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["visibility"] = visibility_;
		js["stepsize"] = stepsize_;

		// serialize the emitters
		for(auto it = emitters_.begin();it!=emitters_.end();it++)
			js["emitters"].append(cmn::Node::serialize_node((*it).second, list));

		// 
		js["tracking_model"] = cmn::Node::serialize_node(tracking_model_, list);
	}
	
	void CalcTracks::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);

		// settings
		set_visibility(js["visibility"].asBool());
		set_stepsize(js["stepsize"].asDouble());

		// deserialize the emitters
		for(auto it = js["emitters"].begin();it!=js["emitters"].end();it++)
			add_emitter(cmn::Node::deserialize_node<Emitter>(
				(*it), list, factory_list, pth));

		// tracking model
		set_tracking_model(cmn::Node::deserialize_node<Model>(
			js["tracking_model"], list, factory_list, pth));
	}


}}