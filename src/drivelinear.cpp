// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivelinear.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveLinear::DriveLinear(){

	}

	// factory
	ShDriveLinearPr DriveLinear::create(){
		return std::make_shared<DriveLinear>();
	}

	// set frequency
	void DriveLinear::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set frequency
	void DriveLinear::set_slope(const fltp slope){
		slope_ = slope;
	}

	// set frequency
	fltp DriveLinear::get_offset() const{
		return offset_;
	}

	// set frequency
	fltp DriveLinear::get_slope() const{
		return slope_;
	}

	// get current
	fltp DriveLinear::get_scaling(const fltp time) const{
		return slope_*time + offset_;
	}

	// get current derivative
	fltp DriveLinear::get_dscaling(const fltp /*time*/) const{
		return slope_;
	}

	// get type
	std::string DriveLinear::get_type(){
		return "rat::mdl::drivelinear";
	}

	// method for serialization into json
	void DriveLinear::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["slope"] = slope_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void DriveLinear::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_offset(js["offset"].asDouble());
		set_slope(js["slope"].asDouble());
	}

}}