// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelmesh.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelMesh::ModelMesh(){
		temperature_drive_ = DriveDC::create(); set_name("modelmesh");
	}

	// constructor immediately setting base and cross section
	ModelMesh::ModelMesh(const ShPathPr &input_path, const ShCrossPr &input_cross) : ModelMesh(){
		// set base path and cross section
		set_input_path(input_path); set_input_cross(input_cross);
	}

	// factory
	ShModelMeshPr ModelMesh::create(){
		//return ShModelMeshPr(new ModelMesh);
		return std::make_shared<ModelMesh>();
	}

	// factory immediately setting base and cross section
	ShModelMeshPr ModelMesh::create(const ShPathPr &input_path, const ShCrossPr &input_cross){
		//return ShModelMeshPr(new ModelMesh(base,crss));
		return std::make_shared<ModelMesh>(input_path,input_cross);
	}

	// set current sharing 
	void ModelMesh::set_enable_current_sharing(const bool enable_current_sharing){
		enable_current_sharing_ = enable_current_sharing;
	}
	
	// get current sharing 
	bool ModelMesh::get_enable_current_sharing()const{
		return enable_current_sharing_;
	}

	// set operating temperature 
	fltp ModelMesh::get_operating_temperature() const{
		return operating_temperature_;
	}

	// set circuit index
	void ModelMesh::set_circuit_index(const arma::uword circuit_index){
		circuit_index_ = circuit_index;
	}

	// set start connection
	void ModelMesh::set_connect_start(const bool connect_start){
		connect_start_ = connect_start;
	}

	// set end connection
	void ModelMesh::set_connect_end(const bool connect_end){
		connect_end_ = connect_end;
	}


	// get circuit index
	arma::uword ModelMesh::get_circuit_index()const{
		return circuit_index_;
	}

	// set start connection
	bool ModelMesh::get_connect_start()const{
		return connect_start_;
	}

	// set end connection
	bool ModelMesh::get_connect_end()const{
		return connect_end_;
	}

	// set operating temperature 
	void ModelMesh::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// set drive
	void ModelMesh::set_temperature_drive(const ShDrivePr &temperature_drive){
		if(temperature_drive==NULL)rat_throw_line("supplied drive points to NULL");
		temperature_drive_ = temperature_drive;
	}

	// create data object
	ShMeshDataPr ModelMesh::create_data()const{
		return MeshData::create();
	}


	// get current mesh that represents this coil
	std::list<ShMeshDataPr> ModelMesh::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs) const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check input
		if(!is_valid(stngs.enable_throws))return{};

		// calculate scaled temperature
		const fltp coil_temperature = temperature_drive_->get_scaling(stngs.time)*operating_temperature_;

		// get or create base and cross
		const ShPathPr base = get_input_path();
		const ShCrossPr crss = get_input_cross();

		// check that they are not NULL pointers
		if(base==NULL)rat_throw_line("base points to null");
		if(crss==NULL)rat_throw_line("cross points to null");

		// create frame
		const ShFramePr frame = base->create_frame(stngs);

		// create frame list
		const std::list<ShFramePr> frames = frame->separate();

		// allocate meshes
		std::list<ShMeshDataPr> meshes;

		// walk over parts
		arma::uword cnt = 0;
		for(auto it = frames.begin();it!=frames.end();it++,cnt++){
			// create mesh
			ShMeshDataPr mesh = create_data();
			const ShFramePr& myframe = (*it);

			// split
			if(stngs.num_sub!=0)myframe->split(stngs.num_sub);

			// create 2d area mesh
			const ShAreaPr area = crss->create_area(stngs);

			// drop nodes
			if(stngs.low_poly)myframe->simplify(stngs.visual_tolerance, crss->get_bounding());

			// combine frame into one single section 
			// this allows to form a single mesh
			if(stngs.combine_sections)myframe->combine();

			// set conductor
			mesh->set_material(get_input_conductor());

			// circuit and connectivity
			mesh->set_circuit_index(circuit_index_);
			if(it==frames.begin())mesh->set_connect_start(connect_start_);
			if(it==std::prev(frames.end()))mesh->set_connect_end(connect_end_);
			mesh->set_enable_current_sharing(enable_current_sharing_);
			
			// create mesh
			mesh->setup(myframe,area);

			// set temperature
			mesh->set_operating_temperature(coil_temperature); // design temperature
			mesh->set_temperature(coil_temperature); // at nodes

			// set time
			mesh->set_time(stngs.time);

			// set name (is appended by models later)
			if(frames.size()==0)mesh->set_name(myname_);
			else mesh->set_name(myname_ + "_pt" + std::to_string(cnt));

			// apply transformations to mesh
			mesh->apply_transformations(get_transformations());

			// meshes
			meshes.push_back(mesh);
		}

		// return list of meshes
		return meshes;
	}


	// check validity
	bool ModelMesh::is_valid(const bool enable_throws) const{
		// check if cross and base are valid
		if(!InputPath::is_valid(enable_throws))return false;
		if(!InputCross::is_valid(enable_throws))return false;

		// check for absolute zero temperature
		if(operating_temperature_<=0){if(enable_throws){rat_throw_line("operating temperature must be larger than zero");} return false;};

		// no problems found
		return true;
	}

	// get type
	std::string ModelMesh::get_type(){
		return "rat::mdl::modelmesh";
	}

	// method for serialization into json
	void ModelMesh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Model::serialize(js,list);
		InputPath::serialize(js,list);
		InputCross::serialize(js,list);
		mat::InputConductor::serialize(js,list);

		// type
		js["type"] = get_type();

		// temperature and drive
		js["enable_current_sharing"] = enable_current_sharing_;
		js["temperature_drive"] = cmn::Node::serialize_node(temperature_drive_, list);
		js["operating_temperature"] = operating_temperature_;

		js["circuit_index"] = (int)circuit_index_;
		js["connect_start"] = connect_start_;
		js["connect_end"] = connect_end_;

	}

	// method for deserialisation from json
	void ModelMesh::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Model::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		InputCross::deserialize(js,list,factory_list,pth);
		mat::InputConductor::deserialize(js,list,factory_list,pth);

		// temperature and drive
		set_enable_current_sharing(js["enable_current_sharing"].asBool());
		if(js.isMember("temperature_drive"))set_temperature_drive(cmn::Node::deserialize_node<Drive>(
			js["temperature_drive"], list, factory_list, pth));
		set_operating_temperature(js["operating_temperature"].asDouble());

		set_circuit_index(js["circuit_index"].asInt64());
		set_connect_start(js["connect_start"].asBool());
		set_connect_end(js["connect_end"].asBool());
	}

}}