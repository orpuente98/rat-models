// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcct.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathcct.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelCCT::ModelCCT(){
		set_name("cct");
		set_number_turns(10);
		set_operating_current(400);
	}

	// factory
	ShModelCCTPr ModelCCT::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelCCT>();
	}

	// set the direction of the layer
	void ModelCCT::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}

	// set the direction of the layer
	void ModelCCT::set_bending_arc_length(const fltp bending_arc_length){
		bending_arc_length_ = bending_arc_length;
	}

	// set twisting
	void ModelCCT::set_twist(const fltp twist){
		twist_ = twist;
	}

	// set amplitude
	void ModelCCT::set_skew_angle(const fltp skew_angle){
		skew_angle_ = skew_angle;
	}

	// set winding pitch
	void ModelCCT::set_drib(const fltp drib){
		drib_ = drib;
	}

	// set number of turns
	void ModelCCT::set_num_turns(const fltp num_turns){
		num_turns_ = num_turns;
	}

	// set radius
	void ModelCCT::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set number of poles
	void ModelCCT::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	// set number of poles
	void ModelCCT::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// set the direction of the layer
	bool ModelCCT::get_is_reverse() const{
		return is_reverse_;
	}

	// set coil thickness
	void ModelCCT::set_cable_thickness(const fltp cable_thickness){
		cable_thickness_ = cable_thickness;
	}

	// set coil width
	void ModelCCT::set_cable_width(const fltp cable_width){
		cable_width_ = cable_width;
	}

	// set axial element size
	void ModelCCT::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}



	// set the direction of the layer
	fltp ModelCCT::get_bending_arc_length() const{
		return bending_arc_length_;
	}

	// set twisting
	fltp ModelCCT::get_twist()const{
		return twist_;
	}

	// set skew angle
	fltp ModelCCT::get_skew_angle()const{
		return skew_angle_;
	}

	// set winding pitch
	fltp ModelCCT::get_drib()const{
		return drib_;
	}

	// set number of turns
	fltp ModelCCT::get_num_turns()const{
		return num_turns_;
	}

	// set radius
	fltp ModelCCT::get_radius()const{
		return radius_;
	}

	// set number of poles
	arma::uword ModelCCT::get_num_poles()const{
		return num_poles_;
	}

	// set number of poles
	arma::uword ModelCCT::get_num_nodes_per_turn() const{
		return num_nodes_per_turn_;
	}

	// set number of layers
	void ModelCCT::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	// get number of layers
	arma::uword ModelCCT::get_num_layers() const{
		return num_layers_;
	}

	// set radius increment between layers
	void ModelCCT::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	// get radius increment between layers
	fltp ModelCCT::get_rho_increment()const{
		return rho_increment_;
	}

	// get coil thickness
	fltp ModelCCT::get_cable_thickness()const{
		return cable_thickness_;
	}

	// get coil width
	fltp ModelCCT::get_cable_width()const{
		return cable_width_;
	}

	// get axial element size
	fltp ModelCCT::get_element_size()const {
		return element_size_;
	}


	// get base
	ShPathPr ModelCCT::get_input_path() const{
		// check input
		is_valid(true);

		// calculate pitch
		const fltp omega = (cable_thickness_ + drib_)/std::sin(skew_angle_);
		const fltp amplitude = radius_/std::tan(skew_angle_);

		// create path
		ShPathCCTPr cct_pth = PathCCT::create(num_poles_,radius_,amplitude,omega,num_turns_,num_nodes_per_turn_);
		cct_pth->set_is_reverse(is_reverse_);
		cct_pth->set_num_layers(num_layers_);
		cct_pth->set_rho_increment(rho_increment_);
		cct_pth->set_bending_arc_length(bending_arc_length_);
		cct_pth->set_twist(twist_);

		// return the circle
		return cct_pth;
	}

	// get cross
	ShCrossPr ModelCCT::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			-cable_thickness_/2, cable_thickness_/2, 0, cable_width_, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// check input
	bool ModelCCT::is_valid(const bool enable_throws) const{
		if(!Transformations::is_valid(enable_throws))return false;
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles must be larger than zero");} return false;};
		if(num_turns_<=0){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;};
		if(num_nodes_per_turn_==0){if(enable_throws){rat_throw_line("number of nodes per turn can not be zero");} return false;};
		if(num_layers_<=0){if(enable_throws){rat_throw_line("number of layers must be larger than zero");} return false;};
		if(cable_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(cable_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelCCT::get_type(){
		return "rat::mdl::modelcct";
	}

	// method for serialization into json
	void ModelCCT::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// settings
		js["type"] = get_type();
		js["num_poles"] = (unsigned int)num_poles_; 
		js["is_reverse_"] = is_reverse_;
		js["radius"] = radius_;
		js["skew_angle"] = skew_angle_; 
		js["drib"] = drib_; 
		js["num_turns"] = num_turns_; 
		js["num_nodes_per_turn"] = (unsigned int)num_nodes_per_turn_; 
		js["twist"] = twist_; 
		js["bending_arc_length"] = bending_arc_length_;
		js["num_layers"] = (int)num_layers_;
		js["rho_increment"] = rho_increment_;
		js["cable_thickness"] = cable_thickness_;
		js["cable_width"] = cable_width_;
		js["element_size"] = element_size_;

	}

	// method for deserialisation from json
	void ModelCCT::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// settings
		set_num_poles(js["num_poles"].asUInt64()); 
		set_is_reverse(js["is_reverse_"].asBool());
		set_radius(js["radius"].asDouble());
		set_skew_angle(js["skew_angle"].asDouble()); 
		set_drib(js["drib"].asDouble()); 
		set_num_turns(js["num_turns"].asDouble()); 
		set_num_nodes_per_turn(js["num_nodes_per_turn"].asUInt64()); 
		set_twist(js["twist"].asDouble());
		set_bending_arc_length(js["bending_arc_length"].asDouble());
		set_num_layers(js["num_layers"].asUInt64());
		set_rho_increment(js["rho_increment"].asDouble());
		set_cable_thickness(js["cable_thickness"].asDouble());
		set_cable_width(js["cable_width"].asDouble());
		set_element_size(js["element_size"].asDouble());
	}

}}