// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "meshdata.hh"

#include "coildata.hh"
#include "bardata.hh"
#include "bardata.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MeshData::MeshData(){
		set_output_type("mesh");
	}

	// constructor with input
	MeshData::MeshData(const ShFramePr &frame, const ShAreaPr &area){
		setup(frame, area); set_output_type("mesh");
	}

	// manual mesh construction
	MeshData::MeshData(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s){

		// mesh
		set_output_type("mesh");

		// set to self
		Rt_ = R; n_ = n; s_ = s;

		// do not use L, N and D
		L_.zeros(Rt_.n_rows,Rt_.n_cols); 
		N_.zeros(Rt_.n_rows,Rt_.n_cols); 
		D_.zeros(Rt_.n_rows,Rt_.n_cols);

		// number of target nodes
		num_targets_ = Rt_.n_cols;

		// determine dimensions for surface and volume mesh
		determine_dimensions();

		// check output
		assert(arma::max(arma::max(n_))<Rt_.n_cols); 
		assert(arma::max(arma::max(s_))<Rt_.n_cols);
	}

	// factory
	ShMeshDataPr MeshData::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<MeshData>();
	}

	// factory with input
	ShMeshDataPr MeshData::create(const ShFramePr &frame, const ShAreaPr &area){
		return std::make_shared<MeshData>(frame,area);
	}

	// factory with input
	ShMeshDataPr MeshData::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s){
		return std::make_shared<MeshData>(R,n,s);
	}

	// set enable current sharing
	void MeshData::set_enable_current_sharing(const bool enable_current_sharing){
		enable_current_sharing_ = enable_current_sharing;
	}

	// get enable current sharing
	bool MeshData::get_enable_current_sharing()const{
		return enable_current_sharing_;
	}


	// set circuit index
	void MeshData::set_circuit_index(const arma::uword circuit_index){
		circuit_index_ = circuit_index;
	}

	// set start connection
	void MeshData::set_connect_start(const bool connect_start){
		connect_start_ = connect_start;
	}

	// set end connection
	void MeshData::set_connect_end(const bool connect_end){
		connect_end_ = connect_end;
	}

	// get circuit index
	arma::uword MeshData::get_circuit_index()const{
		return circuit_index_;
	}

	// set start connection
	bool MeshData::get_connect_start()const{
		return connect_start_;
	}

	// set end connection
	bool MeshData::get_connect_end()const{
		return connect_end_;
	}

	// append id
	void MeshData::append_trace_id(const arma::uword trace_id){
		trace_.push_front(trace_id);
	}

	// clear id
	void MeshData::clear_trace_id(){
		trace_.clear();
	}

	// get id
	std::list<arma::uword> MeshData::get_trace(){
		return trace_;
	}

	// set calculation mesh flag
	void MeshData::set_calc_mesh(const bool calc_mesh){
		calc_mesh_ = calc_mesh;
	}
	
	// get calculation mesh flag
	bool MeshData::get_calc_mesh() const{
		return calc_mesh_;
	}

	// setup function
	void MeshData::setup(const ShFramePr &frame, const ShAreaPr &area){
		// check input
		assert(frame!=NULL); assert(area!=NULL);
		
		// store in self
		area_ = area; frame_ = frame;

		// get nodes
		const arma::Mat<fltp>& R2d = area->get_nodes();
		const arma::Mat<arma::uword>& n2d = area->get_elements();
		const arma::Mat<fltp>& N2d = area->get_normal();
		const arma::Mat<fltp>& D2d = area->get_transverse();

		// edge matrix construction
		arma::Mat<arma::uword> s2d,i2d;	
		area->create_edge_matrix(s2d,i2d);

		// call internal loft function
		frame->create_loft(Rt_,L_,N_,D_,n_,s_,R2d,N2d,D2d,n2d,s2d);

		// number of target nodes
		num_targets_ = Rt_.n_cols;

		// determine dimensions for surface and volume mesh
		determine_dimensions();

		// check output
		assert(n_.n_rows==n2d.n_rows*2); 
		assert(s_.n_rows==s2d.n_rows*2);
		assert(arma::max(arma::max(n_))<Rt_.n_cols); 
		assert(arma::max(arma::max(s_))<Rt_.n_cols);
		assert(L_.n_cols==Rt_.n_cols); 
		assert(N_.n_cols==Rt_.n_cols);
		assert(D_.n_cols==Rt_.n_cols);
	}

	// determine dimensions
	void MeshData::determine_dimensions(){
		// volume dimensionality
		if(n_.n_rows==1)n_dim_ = 0; // points
		if(n_.n_rows==2)n_dim_ = 1; // lines
		if(n_.n_rows==3)n_dim_ = 2; // triangles
		if(n_.n_rows==4)n_dim_ = 2; // quadrilaterals
		if(n_.n_rows==8)n_dim_ = 3; // hexahedrons

		// surface dimensionality
		if(s_.n_rows==1)s_dim_ = 0; // points
		if(s_.n_rows==2)s_dim_ = 1; // lines
		if(n_.n_rows==3)n_dim_ = 2; // triangles
		if(s_.n_rows==4)s_dim_ = 2; // quadrilaterals
	}


	// setup surface mesh
	void MeshData::setup_surface(const ShSurfaceDataPr &surf) const{
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// find indexes of unique nodes
		const arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(s_,1,s_.n_elem));

		// figure out which nodes to drop as 
		// they are no longer contained in the mesh
		arma::Row<arma::uword> node_indexing(1,num_nodes,arma::fill::zeros);
		node_indexing.cols(idx_node_surface) = 
			arma::regspace<arma::Row<arma::uword> >(0,idx_node_surface.n_elem-1);

		// re-index connectivity
		const arma::Mat<arma::uword> s = arma::reshape(
			node_indexing(arma::reshape(s_,1,s_.n_elem)),s_.n_rows,s_.n_cols);

		// get node coordinates
		//const arma::Mat<fltp> R = Rt_.cols(idx_node_surface);

		// surface field calculation
		surf->set_mesh(Rt_.cols(idx_node_surface),s);
		surf->set_longitudinal(L_.cols(idx_node_surface));
		surf->set_normal(N_.cols(idx_node_surface));
		surf->set_transverse(D_.cols(idx_node_surface));

		// transfer name
		surf->set_name(get_name());

		// clear the field type list of the surface
		surf->clear_field_type();

		// transfer field
		if(has_field()){
			// transfer list of field types
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				surf->add_field_type((*it).first, (*it).second);
			
			// allocate surface to take the data
			surf->allocate();

			// transfer the data
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				surf->add_field((*it).first, get_field((*it).first).cols(idx_node_surface));
		}

		// set temperature
		surf->set_operating_temperature(operating_temperature_);
		if(!temperature_.empty())
			surf->set_temperature(temperature_.cols(idx_node_surface));

		// set frame and frame
		surf->set_frame(frame_); surf->set_area(area_);
	}

	// extract surface mesh
	ShSurfaceDataPr MeshData::create_surface() const{
		// create coil surface
		ShSurfaceDataPr surf = SurfaceData::create();
		
		// regular setupp
		setup_surface(surf);

		// return surface
		return surf;
	}

	// create an interpolation mesh
	fmm::ShInterpPr MeshData::create_interp() const{
		fmm::ShInterpPr interp = fmm::Interp::create();
		interp->set_mesh(Rt_,n_);
		interp->set_interpolation_values('H',get_field('H'));
		return interp;
	}

	// VTK export
	ShVTKObjPr MeshData::export_vtk() const{

		// create unstructured mesh
		ShVTKUnstrPr vtk = VTKUnstr::create();

		// get element type
		const arma::uword element_type = VTKUnstr::get_element_type(n_.n_rows);

		// set the mesh
		vtk->set_mesh(Rt_, n_, element_type);
		
		// tranfer name
		vtk->set_name(get_name());

		// add temperature to VTK
		if(!temperature_.empty())vtk->set_nodedata(temperature_,"Temperature [K]");
		else vtk->set_nodedata(operating_temperature_*
			arma::Row<fltp>(get_num_nodes(),arma::fill::ones),"Temperature [K]");

		// orientation vectors
		if(!L_.empty())vtk->set_nodedata(L_,"Longitudinal"); 
		if(!N_.empty())vtk->set_nodedata(N_,"Normal"); 
		if(!D_.empty())vtk->set_nodedata(D_,"Transverse"); 
		
		// add calculated vector potential
		if(has('A') && has_field())vtk->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");

		// add calculated magnetic flux density
		if(has('H') && has_field())vtk->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");

		// add calculated magnetisation
		if(has('M') && has_field())vtk->set_nodedata(get_field('M'),"Magnetisation [A/m]");

		// add calculated magnetic field
		if(has('H') && has_field())vtk->set_nodedata(get_field('H'),"Magnetic Field [A/m]");

		// return the unstructured mesh
		return vtk;
	}

	// get the area
	ShAreaPr MeshData::get_area(){
		return area_;
	}

	// get the frame
	ShFramePr MeshData::get_frame(){
		return frame_;
	}

	// get number of dimensions
	arma::uword MeshData::get_n_dim() const{
		return n_dim_;
	}

	// get number of dimensions
	arma::uword MeshData::get_s_dim() const{
		return s_dim_;
	}

	// calculate volume for each element
	arma::Row<fltp> MeshData::calc_volume() const{
		// volume dimensionality
		arma::Row<fltp> V;
		if(n_dim_==0)V = arma::Row<fltp>(n_.n_cols,arma::fill::ones);
		if(n_dim_==1)V = cmn::Line::calc_length(Rt_,n_); // lines
		if(n_dim_==2)V = cmn::Quadrilateral::calc_area(Rt_,n_); // quadrilaterals
		if(n_dim_==3)V = cmn::Hexahedron::calc_volume(Rt_,n_); // hexahedrons

		// return volume
		return area_==NULL ? V : area_->get_hidden_dim()*V;
	}

	// calculate total volume
	fltp MeshData::calc_total_volume() const{
		return arma::accu(calc_volume());
	}

	// calculate length
	fltp MeshData::calc_ell() const{
		return frame_==NULL ? RAT_CONST(0.0) : frame_->calc_total_ell();
	}

	// calculate volume
	fltp MeshData::calc_total_surface_area() const{
		return arma::sum(cmn::Quadrilateral::calc_area(Rt_,s_));
	}

	// get number of nodes
	arma::uword MeshData::get_num_nodes() const{
		return Rt_.n_cols;
	}

	// create element coordinates
	arma::Mat<fltp> MeshData::get_element_coords()const{
		arma::Mat<fltp> Re(3,n_.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			Re.col(i) = arma::mean(Rt_.cols(n_.col(i)),1);
		return Re;
	}

	// get nodes
	const arma::Mat<fltp>& MeshData::get_nodes()const{
		return Rt_;
	}

	// get coordinates for section
	const arma::Mat<arma::uword>& MeshData::get_elements() const{
		return n_;
	}

	// get number of elements
	arma::uword MeshData::get_num_elements() const{
		return n_.n_cols;
	}

	// get coordinates for section
	const arma::Mat<arma::uword>& MeshData::get_surface_elements() const{
		return s_;
	}

	// get number of surface elements
	arma::uword MeshData::get_num_surface() const{
		return s_.n_cols;
	}

	// set longitudinal vector
	void MeshData::set_longitudinal(const arma::Mat<fltp> &L){
		assert(L.n_rows==3);
		L_ = L;
	}

	// set transverse vector
	void MeshData::set_transverse(const arma::Mat<fltp> &D){
		assert(D.n_rows==3);
		D_ = D;
	}

	// set normal vectors
	void MeshData::set_normal(const arma::Mat<fltp> &N){
		assert(N.n_rows==3);
		N_ = N;
	}

	// set elements
	void MeshData::set_elements(const arma::Mat<arma::uword> &n){
		n_ = n;
	}

	// set elements
	void MeshData::set_surface_elements(const arma::Mat<arma::uword> &s){
		s_ = s;
	}

	// apply multiple transformations
	void MeshData::apply_transformations(const std::list<ShTransPr> &trans_list){
		// walk over transformations and apply
		for(auto it=trans_list.begin();it!=trans_list.end();it++)
			apply_transformation(*it);
	}

	// apply transformation
	void MeshData::apply_transformation(const ShTransPr &trans){
		// transform node orientation vectors
		trans->apply_vectors(Rt_,L_,'L'); 
		trans->apply_vectors(Rt_,N_,'N');
		trans->apply_vectors(Rt_,D_,'D'); 
		
		// transform node coordinates
		trans->apply_coords(Rt_);

		// transform mesh
		trans->apply_mesh(n_,get_num_nodes()); 
		trans->apply_mesh(s_,get_num_nodes());

		// apply the transformation to the frame
		if(frame_!=NULL)frame_->apply_transformation(trans);

		// apply the transformation to the area
		if(area_!=NULL)area_->apply_transformation(trans);
	}

	// get coordinates for section
	const arma::Mat<fltp>& MeshData::get_longitudinal() const{
		return L_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& MeshData::get_normal() const{
		return N_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& MeshData::get_transverse() const{
		return D_;
	}

	// set operating temperature
	void MeshData::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// set node by node temperature
	void MeshData::set_temperature(const fltp temperature){
		temperature_.set_size(Rt_.n_cols); 
		temperature_.fill(temperature);
	}

	void MeshData::set_temperature(const arma::Row<fltp> &temperature){
		assert(temperature.n_cols==Rt_.n_cols);
		temperature_ = temperature;
	}

	// get operating temperature
	arma::Row<fltp> MeshData::get_temperature() const{
		return temperature_;
	}

	// get operating temperature
	fltp MeshData::get_operating_temperature() const{
		return operating_temperature_;
	}


	// get size of the mesh
	arma::Col<fltp>::fixed<3> MeshData::get_lower_bound() const{
		return arma::min(Rt_,1);
	}

	// get size of the mesh
	arma::Col<fltp>::fixed<3> MeshData::get_upper_bound() const{
		return arma::max(Rt_,1);
	}

	// get number of turns
	fltp MeshData::get_number_turns() const{
		return RAT_CONST(0.0);
	}

	// get operating current
	fltp MeshData::get_operating_current() const{
		return RAT_CONST(0.0);
	}

	// calculate enclosed flux
	fltp MeshData::calculate_flux() const{
		return RAT_CONST(0.0);
	}

	// calculate self inductance of elements
	fltp MeshData::calculate_element_self_inductance() const{
		return RAT_CONST(0.0);
	}

	// critical current calculation
	arma::Mat<fltp> MeshData::calc_curvature() const{
		// check
		if(area_==NULL)rat_throw_line("area is null");

		// get nodes in area
		const arma::uword num_nodes = get_num_nodes();
		const arma::uword num_nodes_crss = area_->get_num_nodes();
		const arma::uword num_nodes_frame = num_nodes/num_nodes_crss;

		// check if frame is closed
		const bool is_loop = frame_->is_loop();

		// allocate curvature
		arma::Mat<fltp> K(3,num_nodes);

		// walk over cross nodes
		for(arma::uword i=0;i<num_nodes_crss;i++){
			// indices of nodes
			const arma::Col<arma::uword> idx = 
				arma::regspace<arma::Col<arma::uword> >(
				i,num_nodes_crss,Rt_.n_cols-1);

			// coordinates
			arma::Mat<fltp> R = Rt_.cols(idx);
			if(is_loop)R = arma::join_horiz(R.tail_cols(1), R, R.head_cols(1));

			// calculate velocity
			arma::Mat<fltp> V = arma::diff(R,1,1);
			if(!is_loop){
				V = (arma::join_horiz(V.head_cols(1),V) + 
					arma::join_horiz(V,V.tail_cols(1)))/2;
			}
			
			// calculate accelerations
			arma::Mat<fltp> A = arma::diff(V,1,1);
			if(!is_loop){
				A = (arma::join_horiz(A.head_cols(1),A) + 
					arma::join_horiz(A,A.tail_cols(1)))/2;
			}
			
			// calculate curvature normal vector
			const arma::Mat<fltp> T = V.each_row()/cmn::Extra::vec_norm(V);
			arma::Mat<fltp> N = arma::diff(V,1,1);
			if(!is_loop){
				N = (arma::join_horiz(N.head_cols(1),N) + 
					arma::join_horiz(N,N.tail_cols(1)))/2;
			}

			// normalize
			N = N.each_row()/cmn::Extra::vec_norm(N);

			// average velocity
			if(is_loop){
				V = (V.head_cols(V.n_cols-1) + V.tail_cols(V.n_cols-1))/2;
			}

			// std::cout<<N.n_cols<<std::endl;
			// std::cout<<V.n_cols<<std::endl;
			// std::cout<<A.n_cols<<std::endl;
			// std::cout<<idx.n_elem<<std::endl;

			// radius of curvature equation
			// https://math.libretexts.org/Bookshelves/Calculus/Supplemental_Modules_(Calculus)/Vector_Calculus/2%3A_Vector-Valued_Functions_and_Motion_in_Space/2.3%3A_Curvature_and_Normal_Vectors_of_a_Curve
			K.cols(idx) = N.each_row()%(cmn::Extra::vec_norm(cmn::Extra::cross(A,V))/arma::pow(cmn::Extra::vec_norm(V),3));
		}


		// std::cout<<K<<std::endl;

		// return radius of curvature
		return K;
	}

	// calculate MFOM in 2D based on cross section at ell/2
	fltp MeshData::calculate_mfom_2d(fltp bore_length) const{
		// check area and frame
		if(area_==NULL)rat_throw_line("area not set");
		if(frame_==NULL)rat_throw_line("frame not set");

		// Rt_ are the nodes of my mesh
		// n_ are the elements of my mesh
		// But since I want the 2D version, I should ask for the elements and nodes from the area only
		arma::Mat<fltp> d2_rt = area_->get_nodes();
		arma::Mat<arma::uword> d2_n = area_->get_elements();
		arma::Mat<fltp> d2_N;
		arma::Mat<fltp> d2_D;

		// Now if have my 2D nodes and elements, now I need the field on the nodes
		// First get the field of all the nodes
		arma::Mat<fltp> b_field_total = get_field('B');
		if(!b_field_total.is_finite())rat_throw_line("field not finite");

		// Now get the field of just my 2D nodes:
		// First figure out the number of elements in my 2D area:
		arma::uword number_of_rt_2d = d2_rt.n_cols;
		arma::uword number_of_n_2d = d2_n.n_cols;
		// Now check how many "areas" fit in the 3D mesh (number of plane in the 3D object)
		arma::uword number_of_areas = Rt_.n_cols / number_of_rt_2d;
		// If the number of planes in the 3D object is even, we need to 
		// interpolate, otherwise we can just get the field of the one in the center

		arma::Mat<fltp> b_field_2d ;
		const arma::uword area_index = std::floor(number_of_areas/2);
		if (number_of_areas%2 != 0)	{		// odd
			const arma::uword idx1 = number_of_rt_2d * area_index;
			const arma::uword idx2 = number_of_rt_2d * (area_index+1) - 1;
			b_field_2d = b_field_total.cols(idx1, idx2);
			d2_N = N_.cols(idx1, idx2);
			d2_D = D_.cols(idx1, idx2);
		} else {							// even
			arma::uword idx1 = (area_index+0) * number_of_rt_2d;
			arma::uword idx2 = (area_index+1) * number_of_rt_2d - 1;
			arma::uword idx3 = (area_index+1) * number_of_rt_2d;
			arma::uword idx4 = (area_index+2) * number_of_rt_2d - 1;
			b_field_2d = (b_field_total.cols(idx1, idx2) + b_field_total.cols(idx3, idx4))/2;
			d2_N = (N_.cols(idx1, idx2) + N_.cols(idx3, idx4))/2;
			d2_D = (D_.cols(idx1, idx2) + D_.cols(idx1, idx2))/2;
		}

		// Now we need to calculate the in plane component of the field for each node (B_inplane = sqrt((N dot B)^2 + (D dot B)^2))
		arma::Row<fltp> b_in_plane = arma::sqrt( 
			arma::square(rat::cmn::Extra::dot(d2_N, b_field_2d)) + 
			arma::square(rat::cmn::Extra::dot(d2_D, b_field_2d)) );
		if(!b_in_plane.is_finite())rat_throw_line("in plane field not finite");

		// Now I have the Bfield in 2D that is a matrix, I can calculate the average field for each node
		// So I loop over the elements, each column is an elements, and it contains the indexes of the nodes
		// By defenition there are 4 nodes per element
		fltp mfom2d = 0;
		// Get the areas that we need for the integration later:
		arma::Row<fltp> d2_element_areas = area_->get_areas();
		for (arma::uword i = 0; i<number_of_n_2d; i++) {			// index i is the index of the element
			fltp element_b_field = arma::as_scalar(arma::mean( b_in_plane.cols(d2_n.col(i)), 1));
			mfom2d += element_b_field * element_b_field * d2_element_areas(i);
		}

		// multiply with L squared
		if (bore_length==0) 
			return mfom2d * frame_->calc_total_ell() * frame_->calc_total_ell();
		else 
			return mfom2d * bore_length * bore_length;
	}

	// calculate MFOM in 3D
	fltp MeshData::calculate_mfom_3d() const{
		// check area and frame
		if(area_==NULL)rat_throw_line("area not set");
		if(frame_==NULL)rat_throw_line("frame not set");

		// Rt_ are the nodes of my mesh
		// n_ are the elements of my mesh
		// But since I want the 3D version, Rt_ and n_ are already the elements and the node that I need
		// So I can go right away to asking the field for every element and node
		arma::Mat<fltp> b_field_total = get_field('B');
		if(!b_field_total.is_finite())rat_throw_line("field not finite");

		// Regardless of which element we take, we always need the in plane component, so we calculate that first
		// Now we need to calculate the in plane component of the field for each node (B_inplane = sqrt((N dot B)^2 + (D dot B)^2))
		arma::Row<fltp> b_in_plane = arma::sqrt( 
			arma::square(rat::cmn::Extra::dot(N_, b_field_total)) + 
			arma::square(rat::cmn::Extra::dot(D_, b_field_total)) );
		if(!b_in_plane.is_finite())rat_throw_line("in plane field not finite");
		// b_in_plane is on each of the corners of the element still, so later we still need to average !!!

		// We need to get the number of elements in a plane, because that is the amount of line we need to integrate over
		// And then we need the number of planes, because that is the number of points in a line :)
		arma::uword number_of_lines = area_->get_num_nodes(); // Number of lines that we need to do the area intergration over
		arma::uword number_of_points = Rt_.n_cols / number_of_lines; // Number of points in a path 

		// Now we need to get the B-field on each point in the line
		// The b_in_plane is a row vector, which we can reshape
		// such that each row is one line
		arma::Mat<fltp> b_lines_matrix = arma::reshape(b_in_plane.t(), number_of_lines, number_of_points);

		// Now we need to get the lenght of the line elements
		// We calculate this, because then the user can have an arched path
		// First we reorganise all the coordinates such that each row has the coordinates of a line
		arma::Mat<fltp> x_lines_matrix = arma::reshape(Rt_.row(0).t(), number_of_lines, number_of_points);
		arma::Mat<fltp> y_lines_matrix = arma::reshape(Rt_.row(1).t(), number_of_lines, number_of_points);
		arma::Mat<fltp> z_lines_matrix = arma::reshape(Rt_.row(2).t(), number_of_lines, number_of_points);

		// Then we calculate the differces between each point in a line, and take the root of the square sum for the length matrix
		arma::Mat<fltp> line_length_matrix = arma::sqrt( arma::square(arma::diff(x_lines_matrix, 1, 1)) + 
												arma::square(arma::diff(y_lines_matrix, 1, 1)) +
												arma::square(arma::diff(z_lines_matrix, 1, 1)) );

		// Now we need to interpolate the b-field on each node of each line to the b-field on the center of each line element
		arma::Mat<fltp> b_lines_interpolated_matrix = (b_lines_matrix.tail_cols(number_of_points-1) + b_lines_matrix.head_cols(number_of_points-1)) / 2 ;

		// Finally we can multiply the field of each element by the length of each line element, and integrate
		// This integral need to be squared
		arma::Row<fltp> line_integrals = arma::square(arma::sum(b_lines_interpolated_matrix % line_length_matrix, 1)).t();   // [T^2m^2]

		// Now that we have the line integrals, we can do the area integration
		// For this we need a for loop, and we loop over elements in the 2D area
		// For that we need to get the 2D elements so that we can select the correct columns that belong to an element
		// The we average again
		arma::Mat<arma::uword> d2_n = area_->get_elements();
		arma::uword number_of_elements = d2_n.n_cols;
		arma::Row<fltp> b_element(number_of_elements);
		for(arma::uword i = 0; i<number_of_elements; i++)
			b_element(i) = arma::as_scalar(arma::mean(line_integrals.cols(d2_n.col(i)), 1));

		// Then we only have to multiply by the area and do the last integration
		arma::Row<fltp> d2_element_areas = area_->get_areas();
		fltp mfom3d = arma::accu(b_element%d2_element_areas); // [T^2m^4]

		return mfom3d;
	}

	// // set drive ID
	// void MeshData::set_drive_id(const arma::uword drive_id){
	// 	drive_id_ = drive_id;
	// }

	// // set drive ID
	// arma::uword MeshData::get_drive_id() const{
	// 	return drive_id_;
	// }

	// // extract surface mesh
	// ShSurfacePr MeshData::create_surface() const{
	// 	ShSurfacePr surf = Surface::create();
	// 	setup_surface(surf);
	// 	return surf;
	// }

	// // create target points at nodes
	// fmm::ShMgnTargetsPr MeshData::create_node_targets() const{
	// 	return fmm::MgnTargets::create(Rt_);
	// }

	// // cretae target points at element barycenters
	// fmm::ShMgnTargetsPr MeshData::create_element_targets() const{
	// 	// get counters
	// 	arma::uword num_elements = n_.n_cols;

	// 	// allocate
	// 	arma::Mat<fltp> Re(3,num_elements);

	// 	// walk over elements and calculate center point
	// 	for(arma::uword i=0;i<num_elements;i++){
	// 		Re.col(i) = cmn::Hexahedron::quad2cart(
	// 			Rt_.cols(n_.col(i)),arma::Col<fltp>::fixed<3>{0,0,0});
	// 	}

	// 	// create target points and return
	// 	return fmm::MgnTargets::create(Re);
	// }

	// // default create sources function to be overridden
	// fmm::ShSourcesPrList MeshData::create_sources() const{
	// 	return fmm::ShSourcesPrList(0);
	// }

	// // gmsh export
	// void MeshData::export_gmsh(
	// 	cmn::ShGmshFilePr gmsh, 
	// 	const bool incl_surface, 
	// 	const bool incl_vectors){

	// 	// write nodes and elements
	// 	gmsh->write_nodes(Rt_);
	// 	if(incl_surface)gmsh->write_elements(
	// 		n_,arma::Row<arma::uword>(n_.n_cols,arma::fill::ones),
	// 		s_,arma::Row<arma::uword>(s_.n_cols,arma::fill::ones));
	// 	else gmsh->write_elements(n_,arma::Row<arma::uword>(n_.n_cols,arma::fill::ones));
		
	// 	// write vectors
	// 	if(incl_vectors){
	// 		gmsh->write_nodedata(L_,"dir. vector [m]");
	// 		gmsh->write_nodedata(N_,"norm. vector [m]");
	// 		gmsh->write_nodedata(D_,"trans. vector [m]");
	// 	}
	// }

	// export freecad
	arma::field<arma::Mat<fltp> > MeshData::get_edges() const{
		if(area_==NULL)return{};

		// get indexes to section or turn
		// const arma::Row<arma::uword> section_idx = frame_->get_section();
		// const arma::Row<arma::uword> turn_idx = frame_->get_turn();

		// allocate output
		const arma::Row<arma::uword> num_nodes_long = frame_->get_num_nodes();

		// get corner nodes 
		const arma::Col<arma::uword> corners = area_->get_corner_nodes();

		// get number of sections
		const arma::uword num_sections = frame_->get_num_sections();
		const arma::uword num_edges = corners.n_elem;
		const arma::uword num_nodes_cross = area_->get_num_nodes();

		// section indices
		const arma::Row<arma::uword> node_indices = arma::join_horiz(
			arma::Row<arma::uword>{0}, arma::cumsum<arma::Row<arma::uword> >(num_nodes_long*num_nodes_cross));

		// allocate sections in edge matrices
		arma::field<arma::Mat<fltp> > Re(num_edges,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// number of nodes in the section
			const arma::uword num_nodes_section = num_nodes_long(i)*num_nodes_cross;

			// create indices for nodes
			arma::Row<arma::uword> idx = 
				arma::regspace<arma::Row<arma::uword> >(
				0,num_nodes_cross,num_nodes_section-1);

			// walk over edges
			for(arma::uword j=0;j<num_edges;j++)
				Re(j,i) = Rt_.cols(idx + corners(j) + node_indices(i));
		}

		// return edge coordinates
		return Re;
	}

	// get xyz matrices
	void MeshData::create_xyz(
		arma::field<arma::Mat<fltp> > &x, 
		arma::field<arma::Mat<fltp> > &y, 
		arma::field<arma::Mat<fltp> > &z,
		arma::uword &num_edges) const{ 

		// get edges
		const arma::field<arma::Mat<fltp> > Re = get_edges();

		// count number sectoins and edges
		const arma::uword num_sections = Re.n_cols;
		num_edges = Re.n_rows;

		// allocate
		x.set_size(1,num_sections);
		y.set_size(1,num_sections);
		z.set_size(1,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// allocate edge matrix for this section
			x(i).set_size(num_edges,Re(0,i).n_cols);
			y(i).set_size(num_edges,Re(0,i).n_cols);
			z(i).set_size(num_edges,Re(0,i).n_cols);

			// walk over edges
			for(arma::uword j=0;j<num_edges;j++){
				// copy edge
				x(i).row(j) = Re(j,i).row(0);
				y(i).row(j) = Re(j,i).row(1);
				z(i).row(j) = Re(j,i).row(2);
			}
		}
	}

	// calculate Lorentz forces
	arma::Col<fltp>::fixed<3> MeshData::calc_lorentz_force() const{
		return {0,0,0};
	}

	// set material
	void MeshData::set_material(const mat::ShConductorPr &material){
		material_ = material;
	}

	// get material
	mat::ShConductorPr MeshData::get_material() const{
		//assert(material_!=NULL);
		return material_;
	}

	// calculate force density
	arma::Mat<fltp> MeshData::calc_force_density() const{
		return arma::Mat<fltp>(3,Rt_.n_cols,arma::fill::zeros);
	}

	// export edges to freecad
	void MeshData::export_freecad(const cmn::ShFreeCADPr &freecad) const{
		// check if area set
		if(area_==NULL)return;

		// allocate
		arma::field<arma::Mat<fltp> > x,y,z; arma::uword num_edges; 
		create_xyz(x,y,z,num_edges);

		// section and turn index
		const arma::Row<arma::uword> section = frame_->get_section();
		const arma::Row<arma::uword> turn = frame_->get_turn();

		// write to freecad
		if(num_edges==1 || num_edges==2 || num_edges==4)
			freecad->write_edges(x,y,z,section,turn,get_name());
	}

	// export edges to freecad
	void MeshData::export_opera(const cmn::ShOperaPr &opera) const{
		// check if area set
		if(area_==NULL)return;
		
		// allocate and get edge matrices
		arma::field<arma::Mat<fltp> > x,y,z; arma::uword num_edges; 
		create_xyz(x,y,z,num_edges);

		// calculate current density
		const fltp J = 0; //number_turns_*circuit_->get_operating_current()/area_crss_;

		// write edges with current density
		if(num_edges==4)opera->write_edges(x,y,z,J);
	}

	// export to a csv file
	void MeshData::export_gmsh(const cmn::ShGmshFilePr &gmsh) const{
		gmsh->write_nodes(Rt_);
		gmsh->write_elements(n_);
	}

	// display function
	void MeshData::display(const cmn::ShLogPr &lg, const std::list<ShMeshDataPr> &meshes){
		// header
		lg->msg(2,"%slist of coils%s\n",KBLU,KNRM);

		// count number of meshes
		const arma::uword num_coils = meshes.size();

		// allocate counters
		arma::Row<arma::uword> num_nodes(num_coils);
		arma::Row<arma::uword> num_elements(num_coils);
		arma::Row<arma::uword> num_surface(num_coils);

		// create table with stats for each coil
		lg->msg("%s%3s %12s %8s %8s %8s%s\n",KBLD,"idx","name","#node","#elem","#surf",KNRM);
		arma::uword idx = 0;
		for(auto it=meshes.begin();it!=meshes.end();it++,idx++){
			// get mesh
			ShMeshDataPr mesh = (*it);

			// get coil name
			std::string name = mesh->get_name();
			if(name.length()>12)name = name.substr(0,9) + "...";

			// get counters
			num_nodes(idx) = mesh->get_num_nodes();
			num_elements(idx) = mesh->get_num_elements();
			num_surface(idx) = mesh->get_num_surface();

			// print entr
			lg->msg("%03llu %12s %08llu %08llu %08llu\n",idx,name.c_str(),num_nodes(idx),num_elements(idx),num_surface(idx));
		}
		lg->msg("%03s %12s %08llu %08llu %08llu\n","+++","all",
			arma::sum(num_nodes),arma::sum(num_elements),arma::sum(num_surface));

		// footer
		lg->msg(-2);
	}

	// calculate the mass of this mesh
	fltp MeshData::calc_mass()const{
		// check if material set
		if(material_!=NULL){
			const fltp volume = calc_total_volume();
			const fltp density = material_->calc_density(operating_temperature_);
			return volume*density;
		}
		return RAT_CONST(0.0);
	}

	// calculate the energy density based on the temperature
	// energy density is given in [J m^-3]
	fltp MeshData::calc_thermal_energy(
		const fltp temperature, 
		const fltp delta_temperature) const{

		// check if material set
		if(material_!=NULL){
			const fltp volume = calc_total_volume();
			const fltp energy_density = 
				material_->calc_thermal_energy_density(
				temperature, delta_temperature);
			return volume*energy_density;
		}
		return RAT_CONST(0.0);
	}

	// calculate temperature increase
	fltp MeshData::calc_temperature_increase(
		const fltp initial_temperature, 
		const fltp added_heat,
		const fltp temperature_stepsize) const{

		// check if material set
		if(material_!=NULL){
			const fltp volume = calc_total_volume();
			const fltp temperature_increase = 
				material_->calc_temperature_increase(
				initial_temperature, volume, 
				added_heat, temperature_stepsize);
			return temperature_increase;
		}
		return RAT_CONST(0.0);
	}

}}