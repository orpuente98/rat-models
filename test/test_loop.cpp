// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/error.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "modelcoil.hh"
#include "linedata.hh"
#include "pathaxis.hh"
#include "crosspoint.hh"
#include "calcmlfmm.hh"

// analytical field on axis of a current loop with radius R and current I as function of z
// this function is used for cross-checking the direct calculations
arma::Mat<rat::fltp> analytic_current_loop_axis(
	const arma::Mat<rat::fltp> &z, 
	const rat::fltp R, 
	const rat::fltp I){
	
	// calculate field on axis
	const arma::Mat<rat::fltp> Bz = (arma::Datum<rat::fltp>::mu_0 / (4*arma::Datum<rat::fltp>::pi))*
		((2*arma::Datum<rat::fltp>::pi*R*R*I)/arma::pow(z%z + R*R,1.5));

	// return field on axis
	return Bz;
}

// main
int main(){
	// model geometric input parameters
	const rat::fltp radius = 40e-3; // coil inner radius [m]
	const rat::fltp delem = 2e-3; // element size [m]
	const rat::fltp thickness = 1e-3; // loop cross area [m]
	const rat::fltp width = 1e-3; // loop cross area [m]
	const rat::fltp Iop = 100; // operating current [A]
	const rat::fltp ell = 0.2; // length of axis [m]
	const arma::uword num_sections = 4; // number of coil sections
	const rat::fltp tol = 1e-2; // relative tolerance
	const arma::uword num_exp = 7;
	
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);

	// create a rectangular cross section object
	rat::mdl::ShCrossPointPr point = rat::mdl::CrossPoint::create(0,0,thickness,width);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, point);
	coil->set_number_turns(1); coil->set_operating_current(Iop);

	// create axis
	rat::mdl::ShPathAxisPr path = rat::mdl::PathAxis::create('z','x',ell,0,0,0,delem);

	// create line calculation
	rat::mdl::ShLineDataPr line = rat::mdl::LineData::create(path->create_frame(rat::mdl::MeshSettings()));
	line->set_field_type('H',3);
	
	// create multipole method calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(coil);
	mlfmm->add_target(line);
	mlfmm->get_settings()->set_num_exp(num_exp);
	mlfmm->calculate(0,lg);

	// get result
	arma::Mat<rat::fltp> Rl = line->get_target_coords();
	arma::Mat<rat::fltp> Bfmm = line->get_field('B');
	arma::Row<rat::fltp> Bz = analytic_current_loop_axis(Rl.row(2),radius,Iop);
	arma::Row<rat::fltp> err = arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz));

	// compare results
	if(arma::any(arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz))>tol))
		rat_throw_line("axial field is outside tolerance");
}