// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/error.hh"
#include "crossrectangle.hh"
#include "area.hh"

// main
int main(){
	// settings
	rat::fltp normal_inner = 0.01;
	rat::fltp normal_outer = 0.04;
	rat::fltp trans_inner = -0.01;
	rat::fltp trans_outer = 0.02;
	arma::uword normal_elements = 5;
	arma::uword trans_elements = 10;

	// create rectangle object
	rat::mdl::ShCrossRectanglePr rect = rat::mdl::CrossRectangle::create(
		normal_inner,normal_outer,trans_inner,
		trans_outer,normal_elements,trans_elements);

	// create area
	rat::mdl::ShAreaPr area = rect->create_area(rat::mdl::MeshSettings());
	rat::mdl::ShPerimeterPr peri = rect->create_perimeter();

	// check area
	rat::fltp A = area->get_area();	
	rat::fltp Ach = (normal_outer-normal_inner)*(trans_outer-trans_inner);
	if(std::abs(A - Ach)>1e-6)rat_throw_line("unexpected surface area");
	
	// check perimeter
	rat::fltp p = peri->calculate_length();
	rat::fltp pch = 2*(normal_outer - normal_inner + trans_outer - trans_inner);
	if(std::abs(p - pch)>1e-6)rat_throw_line("unexpected perimeter length");
	
	// check number of surface elements
	arma::uword Ns = area->get_num_elements();
	arma::uword Nsch = normal_elements*trans_elements;
	if(Ns!=Nsch)rat_throw_line("unexpected number of elements");
	
	// check number of perimeter elements
	arma::uword Np = peri->get_num_elements();
	arma::uword Npch = 2*(normal_elements + trans_elements);
	if(Np!=Npch)rat_throw_line("unexpected number of elements in perimeter");
	
}