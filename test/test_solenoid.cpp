// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/error.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "linedata.hh"
#include "pathdshape.hh"
#include "calcinductance.hh"
#include "pathaxis.hh"
#include "calcmlfmm.hh"

// main
int main(){
	// parameters
	const rat::fltp inner_radius = 0.1;
	const rat::fltp outer_radius = 0.11;
	const rat::fltp element_size = 2.5e-3;
	const rat::fltp height = 0.1;
	const arma::uword num_sections = 4;
	const arma::sword num_gauss = 2;
	const arma::uword num_exp = 7;
	const bool volume_elements = false;

	// operating settings
	const rat::fltp num_turns = 1000;
	const rat::fltp operating_current = 400;

	// tolerance for comparison
	const rat::fltp tol = 1e-2;

	// create circle
	rat::mdl::ShPathCirclePr path_circle = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		0,outer_radius-inner_radius,-height/2,height/2,element_size);

	// create coil
	rat::mdl::ShModelCoilPr model_coil = rat::mdl::ModelCoil::create(path_circle, cross_rect);
	model_coil->set_number_turns(num_turns);
	model_coil->set_operating_current(operating_current);
	model_coil->set_use_volume_elements(volume_elements);
	model_coil->set_num_gauss(num_gauss);

	// calculate field on axis
	rat::mdl::ShLineDataPr line = rat::mdl::LineData::create(
		rat::mdl::PathAxis::create('z','x',0.4,0.01,0,0,4e-3)->create_frame(rat::mdl::MeshSettings()));
	
		
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);
		
	// create multipole method
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model_coil);
	mlfmm->get_settings()->set_num_exp(num_exp);
	mlfmm->add_target(line);
	mlfmm->calculate(0,lg);

	// soleno calculation for comparison
	// setup soleno calculation
	rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	sol->set_solenoid(inner_radius,outer_radius,-height/2,height/2,operating_current,num_turns,5);

	// get target points
	const arma::Mat<rat::fltp> Rt = line->get_target_coords();

	// calculate at target points
	const arma::Mat<rat::fltp> Bsol = sol->calc_B(Rt);

	// get field
	const arma::Mat<rat::fltp> Bfmm = line->get_field('B');

	// compare soleno to direct
	rat::fltp sol2fmm = arma::max(arma::max(arma::abs(Bsol-Bfmm)/arma::max(rat::cmn::Extra::vec_norm(Bsol)),1));

	// now we calculate the inductance
	rat::mdl::ShInductanceDataPr ind = rat::mdl::CalcInductance::create(model_coil)->calculate_inductance(0,lg);
	rat::fltp Efmm = ind->get_stored_energy();

	// stored energy with soleno
	rat::fltp Lsol = arma::accu(sol->calc_M());
	rat::fltp Esol = 0.5*Lsol*operating_current*operating_current;

	std::cout<<Efmm<<std::endl;
	std::cout<<Esol<<std::endl;

	// checking if vector potential descending
	if(sol2fmm>tol)rat_throw_line("difference in magnetic field exceeds tolerance");
	
	// checking if vector potential descending
	if(std::abs(Esol-Efmm)/Esol>tol)rat_throw_line("difference in inductance field exceeds tolerance");
}