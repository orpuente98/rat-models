// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "harmonicsdata.hh"
#include "calcharmonics.hh"
#include "modelmesh.hh"
#include "pathaxis.hh"
#include "calcmlfmm.hh"

// main
int main(){
	// geometry settings
	const rat::fltp dcable = 2e-3;
	const rat::fltp wcable = 5e-3;
	const bool is_reverse = false;
	const arma::uword max_num_poles = 5; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const rat::fltp radius = 0.05; // radius
	const rat::fltp alpha = 30.0*2*arma::Datum<rat::fltp>::pi/360; // skew angle for inner radius
	const rat::fltp askew = radius/std::tan(alpha); // skew amplitude
	const rat::fltp delta = dcable + 0.5e-3; // spacing between turns
	const rat::fltp num_turns = 10; // number of turns (can be non-integer)
	const arma::uword num_nodes_per_pole = 90;
	const rat::fltp element_size = 2e-3;
	const rat::fltp dradial = -5e-3; // spacing between cylinders
	const rat::fltp operating_current = 4000;
	const arma::uword num_theta = 64;
	const rat::fltp tolerance = 1; // tolerance on field error [units]
	const rat::fltp omega = delta/std::sin(alpha);

	// walk over number of poles
	for(arma::uword i=0;i<max_num_poles;i++){
		// walk over rotation
		for(arma::uword j=0;j<2;j++){
			// number of poles
			const arma::uword num_poles = i+1;
			const arma::uword num_nodes_per_turn = num_poles*num_nodes_per_pole;

			// calculate rotation angle around z-axis for skew component
			const rat::fltp phi = j*arma::Datum<rat::fltp>::pi/(2*num_poles);

			// create unified path
			rat::mdl::ShPathCCTPr path_cct1 = rat::mdl::PathCCT::create(
				num_poles,radius,askew,omega,
				num_turns,num_nodes_per_turn);
			path_cct1->set_is_reverse(is_reverse);

			// create unified path
			rat::mdl::ShPathCCTPr path_cct2 = rat::mdl::PathCCT::create(
				num_poles,radius+wcable+dradial,askew,
				omega,num_turns,num_nodes_per_turn);
			path_cct2->set_is_reverse(!is_reverse);

			// create cross section
			rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,0,wcable,element_size);

			// create coil
			rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect);
			coil1->set_name("inner");
			coil1->set_operating_current(operating_current);
			rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect);
			coil2->set_name("outer");
			coil2->set_operating_current(operating_current);

			// create model 
			rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
			model->add_model(coil1);
			model->add_model(coil2);
			model->add_rotation(0,0,1,phi);

			// create axis
			rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',0.5,0,0,0,1e-3);

			// create harmonics calculator
			rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(model,pth,(2.0/3)*radius);
			harmonics_calculator->set_num_theta(num_theta);

			// run calculation and get data
			const rat::fltp time = 0;
			rat::mdl::ShHarmonicsDataPr harmonics_data = harmonics_calculator->calculate_harmonics(time,rat::cmn::Log::create());

			// get integrated harmonics
			arma::Row<rat::fltp> An, Bn;
			harmonics_data->get_harmonics(An,Bn);

			// allocate reduced harmonics
			arma::Row<rat::fltp> an,bn;

			// calculate in units depending on whether main or skew harmonics
			if(j==0){an = 1e4*An/Bn(num_poles); bn = 1e4*Bn/Bn(num_poles);}
			else{an = 1e4*An/An(num_poles); bn = 1e4*Bn/An(num_poles);}

			// create indices of non-main harmonics
			const arma::Row<arma::uword> a = arma::regspace<arma::Row<arma::uword> >(1,1,num_poles-1);
			const arma::Row<arma::uword> b = arma::regspace<arma::Row<arma::uword> >(num_poles+1,1,max_num_poles);
			const arma::Row<arma::uword> idx = arma::join_horiz(a,b);

			// get field error
			arma::Row<rat::fltp> field_error;
			if(j==0){field_error = arma::join_horiz(bn.cols(idx),an);}
			else{field_error = arma::join_horiz(bn,an.cols(idx));}

			// check
			if(arma::any(arma::abs(field_error)>tolerance))
				rat_throw_line("field error larger than tolerance");
		}
	}

}