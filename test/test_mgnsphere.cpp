// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/error.hh"
#include "modelmgnsphere.hh"
#include "calcpoints.hh"

// main
int main(){
	// settings
	const rat::fltp radius = RAT_CONST(0.05);
	const rat::fltp element_size = RAT_CONST(0.003);
	const arma::Col<rat::fltp>::fixed<3> magnetisation{
		RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)/arma::Datum<rat::fltp>::mu_0};
	const rat::fltp tol = RAT_CONST(1e-2);

	// center point
	const arma::Col<rat::fltp>::fixed<3> Rc{
		RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

	// create magnetizaed sphere
	const rat::mdl::ShModelMgnSpherePr sphere = 
		rat::mdl::ModelMgnSphere::create(
		radius, magnetisation, element_size);
	sphere->set_polar_orientation(false);

	// calculate points
	const rat::mdl::ShCalcPointsPr calc_points = 
		rat::mdl::CalcPoints::create(sphere); calc_points->set_coords(Rc);

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// calculate
	const rat::mdl::ShMeshDataPr mesh_data = calc_points->calculate_points({0}, lg);

	// get field
	const arma::Col<rat::fltp>::fixed<3> M = mesh_data->get_field('M');
	const arma::Col<rat::fltp>::fixed<3> H = mesh_data->get_field('H');
	const arma::Col<rat::fltp>::fixed<3> B = mesh_data->get_field('B');

	// theoretical values
	const arma::Col<rat::fltp>::fixed<3> H2 = -magnetisation/3;
	const arma::Col<rat::fltp>::fixed<3> B2 = (2.0/3)*arma::Datum<rat::fltp>::mu_0*magnetisation;
	const arma::Col<rat::fltp>::fixed<3> M2 = magnetisation;

	std::cout<<H<<std::endl;
	std::cout<<-magnetisation/3<<std::endl;
	std::cout<<B<<std::endl;
	std::cout<<(2.0/3)*arma::Datum<rat::fltp>::mu_0*magnetisation<<std::endl;
	std::cout<<M<<std::endl;
	std::cout<<magnetisation<<std::endl;

	// compare
	if(arma::as_scalar(rat::cmn::Extra::vec_norm(H2 - H)/rat::cmn::Extra::vec_norm(H2))>tol)rat_throw_line("magnetic field exceeds tolerance");
	if(arma::as_scalar(rat::cmn::Extra::vec_norm(B2 - B)/rat::cmn::Extra::vec_norm(B2))>tol)rat_throw_line("magnetic flux density exceeds tolerance");
	if(arma::as_scalar(rat::cmn::Extra::vec_norm(M2 - M)/rat::cmn::Extra::vec_norm(M2))>tol)rat_throw_line("magnetization exceeds tolerance");
}