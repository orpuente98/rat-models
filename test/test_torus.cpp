// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/error.hh"
#include "crosscircle.hh"
#include "area.hh"
#include "pathcircle.hh"
#include "modelcoil.hh"

// main
int main(){
	// settings
	rat::fltp inner_radius = 0.05;
	rat::fltp outer_radius = 0.08;
	rat::fltp element_size = 0.003;
	arma::uword num_sections = 4;

	// create rectangle object
	rat::mdl::ShCrossCirclePr crss = rat::mdl::CrossCircle::create(
		0,0,(outer_radius-inner_radius)/2,element_size);
		
	// circular path
	rat::mdl::ShPathCirclePr base = rat::mdl::PathCircle::create(
		(inner_radius+outer_radius)/2,num_sections,element_size);

	// create area
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(base, crss);

	// calculate volume
	rat::fltp V = coil->calc_total_volume();
	rat::fltp Vch = (1.0/4)*arma::Datum<rat::fltp>::pi*
		arma::Datum<rat::fltp>::pi*(inner_radius+outer_radius)*
		(outer_radius-inner_radius)*(outer_radius-inner_radius);
	if(std::abs(V - Vch)/Vch>1e-2){
		rat_throw_line("unexpected volume");
	}	

	// calculate surface area
	rat::fltp S = coil->calc_total_surface_area();
	rat::fltp Sch = arma::Datum<rat::fltp>::pi*arma::Datum<rat::fltp>::pi*
		(outer_radius*outer_radius - inner_radius*inner_radius);
	if(std::abs(S - Sch)/Sch>1e-2){
		rat_throw_line("unexpected surface area");
	}	

}